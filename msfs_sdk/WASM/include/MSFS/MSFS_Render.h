#ifndef _MSFS_RENDER_H
#define _MSFS_RENDER_H

#ifdef __cplusplus
extern "C" {
#endif

	typedef unsigned long long FsContext;

	struct FsColor {
		union {
			float rgba[4];
			struct {
				float r, g, b, a;
			};
		};
	};
	typedef struct FsColor FsColor;

	struct FsPaint {
		float xform[6];
		float extent[2];
		float radius;
		float feather;
		FsColor innerColor;
		FsColor outerColor;
		int image;
	};
	typedef struct FsPaint FsPaint;

	enum FsBlendFactor {
		FS_ZERO = 1 << 0,
		FS_ONE = 1 << 1,
		FS_SRC_COLOR = 1 << 2,
		FS_ONE_MINUS_SRC_COLOR = 1 << 3,
		FS_DST_COLOR = 1 << 4,
		FS_ONE_MINUS_DST_COLOR = 1 << 5,
		FS_SRC_ALPHA = 1 << 6,
		FS_ONE_MINUS_SRC_ALPHA = 1 << 7,
		FS_DST_ALPHA = 1 << 8,
		FS_ONE_MINUS_DST_ALPHA = 1 << 9,
		FS_SRC_ALPHA_SATURATE = 1 << 10,
	};

	struct FsCompositeOperationState {
		int srcRGB;
		int dstRGB;
		int srcAlpha;
		int dstAlpha;
	};
	typedef struct FsCompositeOperationState FsCompositeOperationState;

	typedef enum FsClipMode {
		ClipModeReplace = 0,		// Specifies that the existing region is replaced by the new region.
		ClipModeIntersect = 1,	// Specifies that the existing region is replaced by the intersection of itselfand the new region.
		ClipModeUnion = 2,		// Specifies that the existing region is replaced by the union of itselfand the new region.
		ClipModeXor = 3,			// Specifies that the existing region is replaced by the result of performing an XOR on the two regions. A point is in the XOR of two regions if it is in one region or the other but not in both regions.
		ClipModeExclude = 4,		// Specifies that the existing region is replaced by the portion of itself that is outside of the new region.
		ClipModeComplement = 5,	// Specifies that the existing region is replaced by the portion of the new region that is outside of the existing region.
		ClipModeIgnore = 8,
		ClipModeUse = 16
	} FsClipMode;

	struct FsScissor {
		float xform[6];
		float extent[2];
		bool use;
		bool set;
		FsClipMode mode;
	};
	typedef struct FsScissor FsScissor;

	struct FsVertex {
		float x, y, u, v;
	};
	typedef struct FsVertex FsVertex;

	struct FsPath {
		int first;
		int count;
		unsigned char closed;
		int nbevel;
		FsVertex* fill;
		int nfill;
		FsVertex* stroke;
		int nstroke;
		int winding;
		int convex;
	};
	typedef struct FsPath FsPath;

	extern int fsRenderCreate(FsContext ctx);
	extern int fsRenderCreateTexture(FsContext ctx, int type, int w, int h, int imageFlags, const unsigned char* data, const char* debugName);
	extern int fsRenderDeleteTexture(FsContext ctx, int image);
	extern int fsRenderUpdateTexture(FsContext ctx, int image, int x, int y, int w, int h, const unsigned char* data);
	extern int fsRenderGetTextureSize(FsContext ctx, int image, int* w, int* h);
	extern void fsRenderViewport(FsContext ctx, float width, float height, float devicePixelRatio);
	extern void fsRenderCancel(FsContext ctx);
	extern void fsRenderFlush(FsContext ctx);
	extern void fsRenderFill(FsContext ctx, FsPaint* paint, FsCompositeOperationState compositeOperation, FsScissor* scissor, float fringe, const float* bounds, const FsPath* paths, int npaths);
	extern void fsRenderStroke(FsContext ctx, FsPaint* paint, FsCompositeOperationState compositeOperation, FsScissor* scissor, float fringe, float strokeWidth, const FsPath* paths, int npaths);
	extern void fsRenderTriangles(FsContext ctx, FsPaint* paint, FsCompositeOperationState compositeOperation, FsScissor* scissor, const FsVertex* verts, int nverts);
	extern void fsRenderClearStencil(FsContext ctx);
	extern void fsRenderDelete(FsContext ctx);

#ifdef __cplusplus
}
#endif

#endif