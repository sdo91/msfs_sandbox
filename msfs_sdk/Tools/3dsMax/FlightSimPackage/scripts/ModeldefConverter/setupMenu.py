import os

import sys

from pymxs import runtime as rt

from maxsdk import menu as sdkmenu


toolFolder = os.path.dirname(__file__)

if toolFolder not in sys.path:
    sys.path.append(toolFolder)


import ModeldefConverter.xml2jsonAnimationGroup as xml2json

def installMenu():
    sdkmenu.createMacroScript(_func=xml2json.run, category="FlightSim", name="ConvertModelDefToJson", button_text="Legacy modeldef Converter")

    actionItem0 = rt.menuMan.createActionItem("ConvertModelDefToJson", "FlightSim")

    FlightSimMenu = rt.menuMan.findMenu("FlightSim")
    maxMenuBar = rt.menuMan.getMainMenuBar()
    if FlightSimMenu:
        sdkmenu.safeAddItem(FlightSimMenu, actionItem0)
        sdkmenu.safeAddItem(maxMenuBar, rt.menuMan.createSubMenuItem("FlightSim", FlightSimMenu))
    else:
        print("Cannot find FlightSim menu")


    rt.menuMan.updateMenuBar()