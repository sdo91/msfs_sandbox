﻿using System.Windows;

namespace Simvars
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        // can't write to console here
        //Console.WriteLine("App start?");
    }
}