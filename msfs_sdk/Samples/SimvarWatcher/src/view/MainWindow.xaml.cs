﻿using FsAutopilot.AircraftLogic;
using FsAutopilot.Autopilot;
using FsAutopilot.Config;
using FsAutopilot.Events;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Navigation;
using System.Windows.Threading;

namespace FsAutopilot.View
{
    internal interface IBaseSimConnectWrapper
    {
        int GetUserSimConnectWinEvent();

        void ReceiveSimConnectMessage();

        void SetWindowHandle(IntPtr _hWnd);

        void Disconnect();
    }

    /// <summary>
    ///
    /// NOTE:
    /// to modify UI elements from a different thread (eg: main loop):
    /// Dispatcher.Invoke(() =>
    /// {
    ///     // your code here.
    /// });
    /// 
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly AircraftLogicService aircraftLogicService;
        private readonly ConfigService configService;
        private readonly CustomAutopilot customAutopilot;
        private readonly EventService eventService;
        private readonly FlightPlanManager flightPlanManager;
        private readonly SimvarsViewModel simvarsViewModel;

        /// <summary>
        /// 
        /// this is where the App starts
        /// 
        /// </summary>
        public MainWindow()
        {
            Console.WriteLine("constructing MainWindow");

            InitializeComponent();

            eventService = EventService.Create(false);
            eventService.AddHandler(EventTopic.CHANGE_PITCH_MODE, ChangePitchModeHandler);
            eventService.AddHandler(EventTopic.CHANGE_ROLL_MODE, ChangeRollModeHandler);

            aircraftLogicService = new AircraftLogicService();

            configService = new ConfigService(eventService, aircraftLogicService);

            flightPlanManager = new FlightPlanManager(configService, eventService);

            customAutopilot = new CustomAutopilot(eventService, configService, flightPlanManager, aircraftLogicService);

            simvarsViewModel = new SimvarsViewModel(
                eventService, configService, flightPlanManager, customAutopilot, aircraftLogicService);
            simvarsViewModel.SetUiDispatcher(this.Dispatcher);
            this.DataContext = simvarsViewModel;

            // hacky
            aircraftLogicService.SetConfigService(configService);
        }

        void App_Closing(object sender, CancelEventArgs e)
        {
            Console.WriteLine("App_Closing");

            // todo: cleanup/close

            simvarsViewModel.Cleanup();

            //MessageBox.Show("Closing called");
        }

        private void ChangePitchModeHandler(Event myEvent)
        {
            PitchMode mode = (PitchMode)myEvent.data;
            Dispatcher.Invoke(() =>
            {
                Console.WriteLine($"ChangePitchModeHandler: {cbb_PitchMode.SelectedItem} -> {mode}");
                cbb_PitchMode.SelectedItem = AutopilotModes.ToString(mode);
            });
        }

        /// <summary>
        /// this will trigger cbb_RollMode_SelectionChanged
        /// </summary>
        /// <param name="myEvent"></param>
        private void ChangeRollModeHandler(Event myEvent)
        {
            RollMode mode = (RollMode)myEvent.data;
            Dispatcher.Invoke(() =>
            {
                Console.WriteLine($"ChangeRollModeHandler: {cbb_RollMode.SelectedItem} -> {mode}");
                cbb_RollMode.SelectedItem = AutopilotModes.ToString(mode);
            });
        }

        protected HwndSource GetHWinSource()
        {
            return PresentationSource.FromVisual(this) as HwndSource;
        }

        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);
            GetHWinSource().AddHook(WndProc);
            if (this.DataContext is IBaseSimConnectWrapper oBaseSimConnectWrapper)
            {
                oBaseSimConnectWrapper.SetWindowHandle(GetHWinSource().Handle);
            }
        }

        private IntPtr WndProc(IntPtr hWnd, int iMsg, IntPtr hWParam, IntPtr hLParam, ref bool bHandled)
        {
            if (this.DataContext is IBaseSimConnectWrapper oBaseSimConnectWrapper)
            {
                try
                {
                    if (iMsg == oBaseSimConnectWrapper.GetUserSimConnectWinEvent())
                    {
                        oBaseSimConnectWrapper.ReceiveSimConnectMessage();
                    }
                }
                catch
                {
                    oBaseSimConnectWrapper.Disconnect();
                }
            }

            return IntPtr.Zero;
        }

        private void LinkOnRequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            System.Diagnostics.Process.Start(e.Uri.ToString());
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            string sText = e.Text;
            foreach (char c in sText)
            {
                if (!(('0' <= c && c <= '9') || c == '+' || c == '-' || c == ',' || c == '.'))
                {
                    e.Handled = true;
                    break;
                }
            }
        }

        private void Slider_DragCompleted(object sender, DragCompletedEventArgs e)
        {
            if (sender is Slider oSlider && this.DataContext is SimvarsViewModel oContext)
            {
                int sliderValue = (int)oSlider.Value;
                Console.WriteLine($"slider value: {sliderValue}");
                oContext.SetTickSliderValue(sliderValue);
            }
        }

        private void btn_SyncAltitude_Click(object sender, RoutedEventArgs e)
        {
            customAutopilot.SyncAltitudeBug();
        }

        private void btn_SyncHeading_Click(object sender, RoutedEventArgs e)
        {
            customAutopilot.SyncHeadingBug();
        }

        /// <summary>
        /// for dropdown select menus
        /// parse the item that was just selected
        /// </summary>
        private string ParseDropdownSelection(SelectionChangedEventArgs e)
        {
            var previousMode = "x";
            if (e.RemovedItems.Count == 1)
            {
                previousMode = (string)e.RemovedItems[0];
            }
            var selectedMode = (string)e.AddedItems[0];
            Console.WriteLine($"parseSelectionEvent: {previousMode} -> {selectedMode}");
            return selectedMode;
        }

        private string ParseTabSelection(SelectionChangedEventArgs e)
        {
            if (e.Source is TabControl)
            {
                // Check which tab is selected
                TabControl tabControl = (TabControl)e.Source;
                TabItem selectedTab = (TabItem)tabControl.SelectedItem;
                return selectedTab.Header.ToString();
            }
            return "";
        }

        private void cbb_RollMode_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedMode = ParseDropdownSelection(e);
            customAutopilot.SetRollMode(selectedMode);
        }

        private void cbb_PitchMode_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedMode = ParseDropdownSelection(e);
            customAutopilot.SetPitchMode(selectedMode);
        }

        private void cbb_ThrottleMode_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedMode = ParseDropdownSelection(e);
            customAutopilot.SetThrottleMode(selectedMode);
        }

        private void cbb_AircraftVariations_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedValue = ParseDropdownSelection(e);
            Console.WriteLine($"selected variation: {selectedValue}");
            // todo: fire an event to reload PIDs
        }

        /// <summary>
        /// 
        /// process dev cmds
        /// 
        /// </summary>
        private void tb_dev_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                // parse the text
                TextBox devCmdBox = (TextBox)sender;
                string devCmdText = devCmdBox.Text.ToUpper();
                Console.WriteLine($"DEV_CMD: {devCmdText}");
                DevCommand devCommand = new DevCommand(devCmdText);

                // do something amazing
                eventService.FireEvent(EventTopic.DEV_CMD, devCommand);
            }
            catch
            {
                Console.WriteLine("tb_dev_LostFocus exception");
            }
        }

        private void btn_AddWaypoint_Click(object sender, RoutedEventArgs e)
        {
            simvarsViewModel.HandleAddWaypointButton();
        }

        private void btn_FlipSigns_Click(object sender, RoutedEventArgs e)
        {
            simvarsViewModel.HandleFlipSignsButton();
        }

        private void btn_ResetPidCharts_Click(object sender, RoutedEventArgs e)
        {
            simvarsViewModel.ChartManager.HandleResetButton();
        }

        private void btn_InjectPidData_Click(object sender, RoutedEventArgs e)
        {
            simvarsViewModel.ChartManager.InjectRandomData();
        }

        private void HandleUiTabSelected(object sender, SelectionChangedEventArgs e)
        {
            var selectedTab = ParseTabSelection(e);
            Console.WriteLine($"selectedTab: {selectedTab}");
            if (selectedTab.ToUpper().Contains("CHART"))
            {
                simvarsViewModel.ChartManager.HandleResetButton();
            }
        }
    }
}
