﻿using System.Windows;

namespace FsAutopilot.View
{
    partial class NewAircraftDialog : Window
    {

        public NewAircraftDialog()
        {
            InitializeComponent();
        }

        public string ResponseText
        {
            get { return ResponseTextBox.Text; }
            set { ResponseTextBox.Text = value; }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
