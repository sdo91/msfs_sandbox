﻿using FsAutopilot.AircraftLogic;
using FsAutopilot.Autopilot;
using FsAutopilot.Config;
using FsAutopilot.Events;
using FsAutopilot.Hardware;
using FsAutopilot.Simconnect;
using FsAutopilot.Util;
using Microsoft.FlightSimulator.SimConnect;
using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Timers;
using System.Windows.Threading;

namespace FsAutopilot.View
{
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
    public struct SimconnectText
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
        public string text;
    };

    // todo: get this working...
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
    public struct SimconnectLla
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
        public double lat;
        public double lon;
        public double alt;
    };

    public enum DEFINITION
    {
        Dummy = 0
    };

    public enum REQUEST
    {
        Dummy = 0
    };

    public class SimvarsViewModel : BaseViewModel, IBaseSimConnectWrapper
    {
        public string[] uiControllerNames
        {
            get { return PidControllerNames.CONTROLLER_NAMES; }
            private set { }
        }

        public string[] uiRollModes
        {
            get { return AutopilotModes.ROLL_MODES; }
            private set { }
        }

        public string[] uiPitchModes
        {
            get { return AutopilotModes.PITCH_MODES; }
            private set { }
        }

        public string[] uiThrottleModes
        {
            get { return AutopilotModes.THROTTLE_MODES; }
            private set { }
        }

        public string[] uiAircraftVariations
        {
            get
            {
                string[] result = new string[1];
                result[0] = "todo: aircraft variations";
                return result;
            }
            private set
            {
                // todo?
            }
        }

        #region IBaseSimConnectWrapper implementation

        /// User-defined win32 event
        public const int WM_USER_SIMCONNECT = 0x0402;

        /// Window handle
        private IntPtr m_hWnd = new IntPtr(0);

        /// SimConnect object
        private SimConnect m_oSimConnect = null;

        public bool bConnected
        {
            get { return m_bConnected; }
            private set { this.SetProperty(ref m_bConnected, value); }
        }

        private bool m_bConnected = false;

        private uint m_iCurrentDefinition = 0;
        private uint m_iCurrentRequest = 0;

        public int GetUserSimConnectWinEvent()
        {
            return WM_USER_SIMCONNECT;
        }

        public void ReceiveSimConnectMessage()
        {
            m_oSimConnect?.ReceiveMessage();
        }

        public void SetWindowHandle(IntPtr _hWnd)
        {
            m_hWnd = _hWnd;
        }

        public void Disconnect()
        {
            Console.WriteLine("Disconnect");

            ToggleMainTimer(false);
            bOddTick = false;

            Cleanup();

            sConnectButtonLabel = "Connect";
            bConnected = false;

            // Set all requests as pending
            foreach (SimvarRequest oSimvarRequest in lSimvarRequests)
            {
                oSimvarRequest.bPending = true;
                oSimvarRequest.bStillPending = true;
            }

            CustomOnDisconnect();
        }

        internal void Cleanup()
        {
            if (simconnectBridge != null)
            {
                simconnectBridge.Cleanup();
            }

            if (m_oSimConnect != null)
            {
                /// Dispose serves the same purpose as SimConnect_Close()
                Console.WriteLine("SimConnect_Close");
                m_oSimConnect.Dispose();
                m_oSimConnect = null;
            }
        }

        #endregion IBaseSimConnectWrapper implementation

        #region UI bindings

        public string sConnectButtonLabel
        {
            get { return m_sConnectButtonLabel; }
            private set { this.SetProperty(ref m_sConnectButtonLabel, value); }
        }

        private string m_sConnectButtonLabel = "Connect";

        public bool bObjectIDSelectionEnabled
        {
            get { return m_bObjectIDSelectionEnabled; }
            set { this.SetProperty(ref m_bObjectIDSelectionEnabled, value); }
        }

        private bool m_bObjectIDSelectionEnabled = false;

        public SIMCONNECT_SIMOBJECT_TYPE eSimObjectType
        {
            get { return m_eSimObjectType; }
            set
            {
                this.SetProperty(ref m_eSimObjectType, value);
                bObjectIDSelectionEnabled = (m_eSimObjectType != SIMCONNECT_SIMOBJECT_TYPE.USER);
                ClearResquestsPendingState();
            }
        }

        private SIMCONNECT_SIMOBJECT_TYPE m_eSimObjectType = SIMCONNECT_SIMOBJECT_TYPE.USER;
        public ObservableCollection<uint> lObjectIDs { get; private set; }

        public uint iObjectIdRequest
        {
            get { return m_iObjectIdRequest; }
            set
            {
                this.SetProperty(ref m_iObjectIdRequest, value);
                ClearResquestsPendingState();
            }
        }

        private uint m_iObjectIdRequest = 0;

        public string[] aSimvarNames
        {
            get { return SimVars.Names; }
            private set { }
        }

        public string sSimvarRequest
        {
            get { return m_sSimvarRequest; }
            set { this.SetProperty(ref m_sSimvarRequest, value); }
        }

        private string m_sSimvarRequest = null;

        public string[] aUnitNames
        {
            get { return Units.Names; }
            private set { }
        }

        public string sUnitRequest
        {
            get { return m_sUnitRequest; }
            set { this.SetProperty(ref m_sUnitRequest, value); }
        }

        private string m_sUnitRequest = null;

        public string sSetValue
        {
            get { return m_sSetValue; }
            set { this.SetProperty(ref m_sSetValue, value); }
        }

        private string m_sSetValue = null;

        public ObservableCollection<SimvarRequest> lSimvarRequests { get; private set; }

        public SimvarRequest oSelectedSimvarRequest
        {
            get { return m_oSelectedSimvarRequest; }
            set { this.SetProperty(ref m_oSelectedSimvarRequest, value); }
        }

        private SimvarRequest m_oSelectedSimvarRequest = null;

        public uint[] aIndices
        {
            get { return m_aIndices; }
            private set { }
        }

        private readonly uint[] m_aIndices = new uint[100] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
                                                            10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
                                                            20, 21, 22, 23, 24, 25, 26, 27, 28, 29,
                                                            30, 31, 32, 33, 34, 35, 36, 37, 38, 39,
                                                            40, 41, 42, 43, 44, 45, 46, 47, 48, 49,
                                                            50, 51, 52, 53, 54, 55, 56, 57, 58, 59,
                                                            60, 61, 62, 63, 64, 65, 66, 67, 68, 69,
                                                            70, 71, 72, 73, 74, 75, 76, 77, 78, 79,
                                                            80, 81, 82, 83, 84, 85, 86, 87, 88, 89,
                                                            90, 91, 92, 93, 94, 95, 96, 97, 98, 99 };

        public uint iIndexRequest
        {
            get { return m_iIndexRequest; }
            set { this.SetProperty(ref m_iIndexRequest, value); }
        }

        private uint m_iIndexRequest = 0;

        public bool bSaveValues
        {
            get { return m_bSaveValues; }
            set { this.SetProperty(ref m_bSaveValues, value); }
        }

        private bool m_bSaveValues = true;

        public bool bFSXcompatible
        {
            get { return m_bFSXcompatible; }
            set { this.SetProperty(ref m_bFSXcompatible, value); }
        }

        private bool m_bFSXcompatible = false;

        public bool bOddTick
        {
            get { return m_bOddTick; }
            set { this.SetProperty(ref m_bOddTick, value); }
        }

        private bool m_bOddTick = false;

        public ObservableCollection<string> lErrorMessages { get; private set; }

        public BaseCommand cmdToggleConnect { get; private set; }
        public BaseCommand cmdAddRequest { get; private set; }
        public BaseCommand cmdRemoveSelectedRequest { get; private set; }
        public BaseCommand cmdTrySetValue { get; private set; }
        public BaseCommand cmdResetTrim { get; private set; }
        public BaseCommand cmdResetI { get; private set; }
        public BaseCommand cmdLoadFiles { get; private set; }
        public BaseCommand cmdSaveFile { get; private set; }

        #endregion UI bindings

        #region Real time

        //private DispatcherTimer m_oTimer = new DispatcherTimer();  // this timer was inconsistent!
        private Timer mainLoopTimer2;

        #endregion Real time

        /// <summary>
        /// constructor
        /// 
        /// called once on boot
        /// </summary>
        public SimvarsViewModel(
            EventService eventService,
            ConfigService configService,
            FlightPlanManager flightPlanManager,
            CustomAutopilot customAutopilot,
            AircraftLogicService aircraftLogicService)
        {
            Console.WriteLine("SimvarsViewModel");

            this.aircraftLogicService = aircraftLogicService;
            this.configService = configService;
            this.customAutopilot = customAutopilot;
            this.eventService = eventService;
            this.flightPlanManager = flightPlanManager;

            joystickService = new JoystickService(eventService, configService);

            // pass references
            customAutopilot.SetJoystickService(joystickService);

            lObjectIDs = new ObservableCollection<uint>();
            lObjectIDs.Add(1);

            lSimvarRequests = new ObservableCollection<SimvarRequest>();
            lErrorMessages = new ObservableCollection<string>();

            cmdToggleConnect = new BaseCommand((p) => { ToggleConnect(); });
            cmdAddRequest = new BaseCommand((p) => { AddRequest(null, null); });
            cmdRemoveSelectedRequest = new BaseCommand((p) => { RemoveSelectedRequest(); });
            cmdTrySetValue = new BaseCommand((p) => { TrySetValue(); });
            cmdResetTrim = new BaseCommand((p) => { DoResetTrim(); });
            cmdResetI = new BaseCommand((p) => { ResetITerm(); });
            cmdLoadFiles = new BaseCommand((p) => { LoadFiles(); });
            cmdSaveFile = new BaseCommand((p) => { SaveFile(false); });

            // init main loop timer
            mainLoopTimer2 = new Timer(TimeUtil.DeltaTime.TotalMilliseconds);
            mainLoopTimer2.Elapsed += HandleTimer2;
            mainLoopTimer2.AutoReset = true;
            mainLoopTimer2.Enabled = false;

            // custom stuff
            eventService.AddHandler(EventTopic.DEV_CMD, HandleDevCmd);
            eventService.AddHandler(EventTopic.SIM_EVENT, SimEventHandler);
            eventService.AddHandler(EventTopic.JOYSTICK_EVENT, JoystickEventHandler);
            eventService.AddHandler(EventTopic.CHANGE_REQUEST, ChangeRequestHandler);
            eventService.AddHandler(EventTopic.TRIGGER_UI_UPDATE, HandleUiUpdateRequested);

            ChartManager = new ApChartManager(eventService);
        }

        private AircraftLogicService aircraftLogicService = null;
        private ArduinoBridge arduinoBridge = null;
        private ConfigService configService = null;
        private CustomAutopilot customAutopilot = null;
        private EventService eventService = null;
        private FlightPlanManager flightPlanManager = null;
        private JoystickService joystickService = null;
        private SimconnectBridge simconnectBridge = null;

        public ApChartManager ChartManager { get; set; }  // bound to the UI

        private Dispatcher uiDispatcher = null;
        internal void SetUiDispatcher(Dispatcher dispatcher)
        {
            uiDispatcher = dispatcher;
        }

        private bool autopilotEnableCheckbox_;
        public bool uiAutopilotEnableCheckbox
        {
            get
            {
                autopilotEnableCheckbox_ = customAutopilot.IsActive;
                return autopilotEnableCheckbox_;
            }
            set
            {
                customAutopilot.IsActive = value;
                this.SetProperty(ref autopilotEnableCheckbox_, value);
                afterApToggle(value);
            }
        }

        private bool autothrottleEnableCheckbox_;
        public bool uiAutothrottleEnableCheckbox
        {
            get
            {
                autothrottleEnableCheckbox_ = customAutopilot.IsAutothrottleActive;
                return autothrottleEnableCheckbox_;
            }
            set
            {
                customAutopilot.IsAutothrottleActive = value;
                this.SetProperty(ref autothrottleEnableCheckbox_, value);
            }
        }

        private bool useMaxAirspeedCheckbox_;
        public bool uiUseMaxAirspeedCheckbox
        {
            get { return useMaxAirspeedCheckbox_; }
            set
            {
                useMaxAirspeedCheckbox_ = value;
                Console.WriteLine($"UseMaxAirspeed: {useMaxAirspeedCheckbox_}");
            }
        }

        private bool internalInvertState_ = false;
        public bool uiInvertCheckbox
        {
            get
            {
                return internalInvertState_;
            }
            set
            {
                // called when checkbox is ticked
                if (value != internalInvertState_)
                {
                    Console.WriteLine($"uiInvertCheckbox: {value}");
                }

                this.SetProperty(ref internalInvertState_, value);
                customAutopilot.IsInverted = internalInvertState_;
            }
        }

        public string uiTargetAirspeed
        {
            get { return customAutopilot.targetAirspeed.ToString(); }
            set
            {
                // called when changed by the UI
                // can also be called manually

                var oldString = customAutopilot.targetAirspeed.ToString();
                Console.WriteLine($"uiTargetAirspeed: {oldString} -> {value}");

                double.TryParse(value, out double temp);
                customAutopilot.targetAirspeed = temp;

                this.SetProperty(ref oldString, value);
            }
        }

        private string vert_speed_;
        public string uiTargetVertSpeed
        {
            get { return customAutopilot.targetVertSpeed.ToString(); }
            set
            {
                double.TryParse(value, out double temp);
                customAutopilot.targetVertSpeed = temp;
                this.SetProperty(ref vert_speed_, value);
            }
        }

        private string target_pitch_;
        public string uiTargetPitch
        {
            get { return customAutopilot.targetPitch.ToString(); }
            set
            {
                double.TryParse(value, out double temp);
                customAutopilot.targetPitch = temp;
                this.SetProperty(ref target_pitch_, value);
            }
        }

        private string target_roll_;
        public string uiTargetRoll
        {
            get { return customAutopilot.targetRollDeg.ToString(); }
            set
            {
                double.TryParse(value, out double temp);
                customAutopilot.targetRollDeg = temp;
                this.SetProperty(ref target_roll_, value);
            }
        }

        public string uiControllerSelect
        {
            private get
            {
                return "";  // todo: is this needed?
            }
            set
            {
                if (value == null)
                {
                    return;
                }

                // update selected controller
                customAutopilot.SelectPidController(value);

                ChartManager.SelectControllers(value);

                UpdateUi();
            }
        }

        /// <summary>
        /// Update the UI w/ the latest config values for the connected aircraft/selected PID controller.
        /// Called when:
        ///     the UI starts up
        ///     an aircraft is connected
        ///     a new PID controller is selected
        /// </summary>
        private void UpdateUi()
        {
            Console.WriteLine("updating UI!");

            var selectedController = configService.SelectedController;

            if (selectedController != null)
            {
                // update PIDs in the UI
                uiSelectedControllerPidGains = configService.SelectedControllerYaml;

                uiMiscParams = configService.MiscAircraftParams;

                uiAircraftNotes = configService.AircraftNotes;

                uiAircraftData = configService.AircraftData;

                // update the UI to match the aircraft's defaultVertSpeed
                uiTargetVertSpeed = customAutopilot.targetVertSpeed.ToString();
            }
        }

        private string miscParams_;
        public string uiMiscParams
        {
            get
            {
                return configService.MiscAircraftParams;
            }
            set
            {
                try
                {
                    configService.MiscAircraftParams = value;
                    this.SetProperty(ref miscParams_, value);
                    eventService.FireEvent(EventTopic.CONFIG_UPDATE_FROM_UI);
                }
                catch
                {
                    Console.WriteLine($"invalid yaml!");
                    // reset to previous value
                    uiMiscParams = configService.MiscAircraftParams;
                }
            }
        }

        private string appConfig_;
        public string uiAppConfig
        {
            get
            {
                return configService.AppConfigStr;
            }
            set
            {
                try
                {
                    configService.AppConfigStr = value;
                    this.SetProperty(ref appConfig_, value);
                    eventService.FireEvent(EventTopic.CONFIG_UPDATE_FROM_UI);
                }
                catch
                {
                    Console.WriteLine($"invalid yaml!");
                    // reset to previous value
                    uiAppConfig = configService.AppConfigStr;
                }
            }
        }

        private string flightPlanConfig_;
        public string uiFlightPlanConfig
        {
            get
            {
                return configService.FlightPlanConfigStr;
            }
            set
            {
                try
                {
                    configService.FlightPlanConfigStr = value;
                    this.SetProperty(ref flightPlanConfig_, value);
                    eventService.FireEvent(EventTopic.CONFIG_UPDATE_FROM_UI);
                }
                catch
                {
                    Console.WriteLine($"invalid yaml!");
                    // reset to previous value
                    uiFlightPlanConfig = configService.FlightPlanConfigStr;
                }
            }
        }

        private string waypointFrequency_ = "";
        public string uiWaypointFrequency
        {
            get { return waypointFrequency_; }
            set { this.SetProperty(ref waypointFrequency_, value); }
        }

        private string waypointCrsIn_ = "";
        public string uiWaypointCrsIn
        {
            get { return waypointCrsIn_; }
            set { this.SetProperty(ref waypointCrsIn_, value); }
        }

        private string waypointCrsOut_ = "";
        public string uiWaypointCrsOut
        {
            get { return waypointCrsOut_; }
            set { this.SetProperty(ref waypointCrsOut_, value); }
        }

        private string aircraftNotes_;
        public string uiAircraftNotes
        {
            get
            {
                return configService.AircraftNotes;
            }
            set
            {
                configService.AircraftNotes = value;
                this.SetProperty(ref aircraftNotes_, value);
                eventService.FireEvent(EventTopic.CONFIG_UPDATE_FROM_UI);
            }
        }

        private string aircraftData_;
        public string uiAircraftData
        {
            get
            {
                return configService.AircraftData;
            }
            set
            {
                this.SetProperty(ref aircraftData_, value);
            }
        }

        private string selectedControllerPidGainsYaml_;
        public string uiSelectedControllerPidGains
        {
            get
            {
                return configService.SelectedControllerYaml;
            }
            set
            {
                try
                {
                    configService.SelectedControllerYaml = value;
                    this.SetProperty(ref selectedControllerPidGainsYaml_, value);
                    customAutopilot.UpdatePidGains();
                    eventService.FireEvent(EventTopic.CONFIG_UPDATE_FROM_UI);
                }
                catch
                {
                    Console.WriteLine($"invalid yaml!");
                    // reset to previous value
                    uiSelectedControllerPidGains = configService.SelectedControllerYaml;
                }
            }
        }

        private void HandleDevCmd(Event myEvent)
        {
            var devCmd = (DevCommand)myEvent.data;

            if (devCmd.command == "VS")
            {
                uiTargetVertSpeed = devCmd.args[0];
            }
        }

        private void SimEventHandler(Event myEvent)
        {
            Enum eventId = (SIM_EVENT)myEvent.data;
            switch (eventId)
            {
                case SIM_EVENT.THROTTLE_CUT:
                    if (!configService.GetAppCfg().bodieMode)
                    {
                        uiAutothrottleEnableCheckbox = false;
                    }
                    break;

                default:
                    Console.WriteLine($"{eventId} event not handled by SimvarsViewModel");
                    break;
            }
        }

        private void JoystickEventHandler(Event myEvent)
        {
            JoystickEvent joystickEvent = (JoystickEvent)myEvent.data;
            switch (joystickEvent.button)
            {
                case JoystickEvent.Button.AIRSPEED:
                    int airspeedDelta = configService.GetAppCfg().airspeedDelta;
                    if (joystickEvent.GetStateAsBool())
                    {
                        TryChangeTargetAirspeed($"+{airspeedDelta}");
                    }
                    else
                    {
                        TryChangeTargetAirspeed($"-{airspeedDelta}");
                    }
                    break;

                case JoystickEvent.Button.AUTOPILOT:
                    if (joystickEvent.IsStateToggle())
                    {
                        uiAutopilotEnableCheckbox = !uiAutopilotEnableCheckbox;
                    }
                    else
                    {
                        uiAutopilotEnableCheckbox = joystickEvent.GetStateAsBool();
                    }
                    break;

                case JoystickEvent.Button.AUTOTHROTTLE:
                    uiAutothrottleEnableCheckbox = joystickEvent.GetStateAsBool();
                    break;

                case JoystickEvent.Button.HIGH_RATES:
                    bool isHighRates = joystickEvent.GetStateAsBool();
                    customAutopilot.SetHighRates(isHighRates);

                    // check for auto cruise
                    if (customAutopilot.GetPitchMode() != PitchMode.AutoCruise)
                    {
                        if (!isHighRates)
                        {
                            eventService.FireEvent(EventTopic.CHANGE_PITCH_MODE, PitchMode.FlcMode);
                        }
                        else if (simconnectBridge.GetAltitudeAgl() < 100)
                        {
                            // while on ground, allow switch back to ALT mode
                            eventService.FireEvent(EventTopic.CHANGE_PITCH_MODE, PitchMode.AltHold);
                        }
                    }
                    break;

                default:
                    Console.WriteLine("unhandled event!");
                    break;
            }
        }

        private void ChangeRequestHandler(Event myEvent)
        {
            var request = (ChangeRequestEvent)myEvent.data;

            switch (request.key)
            {
                case ChangeRequestEvent.TargetKey.SET_TARGET_AIRSPEED:
                    {
                        TryChangeTargetAirspeed(request.value);
                        break;
                    }
                case ChangeRequestEvent.TargetKey.ADJUST_TARGET_AIRSPEED:
                    {
                        string delta = request.value;
                        if (!delta.StartsWith("-"))
                        {
                            delta = $"+{delta}";
                        }
                        TryChangeTargetAirspeed(delta);
                        break;
                    }
                default:
                    break;
            }
        }

        private void HandleUiUpdateRequested(Event myEvent)
        {
            // update app config
            uiAppConfig = configService.AppConfigStr;

            uiFlightPlanConfig = configService.FlightPlanConfigStr;

            uiAircraftData = configService.AircraftData;

            // todo: update other UI elements as needed
        }

        private void Connect()
        {
            Console.WriteLine("Connect");

            try
            {
                /// The constructor is similar to SimConnect_Open in the native API
                m_oSimConnect = new SimConnect("Simconnect - Simvar test", m_hWnd, WM_USER_SIMCONNECT, null, bFSXcompatible ? (uint)1 : 0);

                /// Listen to connect and quit msgs
                m_oSimConnect.OnRecvOpen += new SimConnect.RecvOpenEventHandler(SimConnect_OnRecvOpen);
                m_oSimConnect.OnRecvQuit += new SimConnect.RecvQuitEventHandler(SimConnect_OnRecvQuit);

                /// Listen to exceptions
                m_oSimConnect.OnRecvException += new SimConnect.RecvExceptionEventHandler(SimConnect_OnRecvException);

                /// Catch a simobject data request
                m_oSimConnect.OnRecvSimobjectDataBytype += new SimConnect.RecvSimobjectDataBytypeEventHandler(SimConnect_OnRecvSimobjectDataBytype);
            }
            catch (COMException ex)
            {
                Console.WriteLine("Connection to KH failed: " + ex.Message);
            }

            CustomOnConnect();
        }

        /// <summary>
        ///
        /// todo here:
        /// populate requests:
        /// altitude
        /// elev trim
        ///
        /// todo: only call this method once
        ///
        /// todo: get LLA struct working
        /// AddRequestLla(SimVars.NAV_GS_LATLONALT);
        /// AddRequestLla(SimVars.NAV_GS_LLAF_64);
        /// 
        /// see also:
        ///     SimconnectBridge.HandleAllVarsReady()
        ///
        /// </summary>
        private void CustomOnConnect()
        {
            Console.WriteLine("CustomOnConnect");

            // init vars
            LoadFromLines(ApSimvarsList.getInitialVarList());
            AddRequestString(SimVars.TITLE);

            // init bridge
            simconnectBridge = new SimconnectBridge(m_oSimConnect, eventService, configService, aircraftLogicService);
            simconnectBridge.SetSimvarRequestList(lSimvarRequests);

            aircraftLogicService.SetSimconnectBridge(simconnectBridge);
            configService.SetSimconnectBridge(simconnectBridge);
            customAutopilot.SetSimconnectBridge(simconnectBridge);
            flightPlanManager.SetSimconnectBridge(simconnectBridge);
            joystickService.SetSimconnectBridge(simconnectBridge);

            // init joysticks
            joystickService.ScanForDevices();
            // todo: load state of switches (eg: half bank, roll mode, etc)

            // Start serial service
            arduinoBridge = new ArduinoBridge(eventService, configService, simconnectBridge);
            arduinoBridge.Start();
        }

        private void CustomOnDisconnect()
        {
            Console.WriteLine("CustomOnDisconnect");

            // prevent freeze on exit to main menu
            uiDispatcher.Invoke(() =>
            {
                lSimvarRequests.Clear();
            });

            // cleanup
            simconnectBridge = null; // other classes will still have a pointer (todo: fix)
            customAutopilot.handleDisconnect();
            arduinoBridge.Stop();
            configService.Reset();
        }

        /// <summary>
        /// called once on connect
        /// </summary>
        private void SimConnect_OnRecvOpen(SimConnect sender, SIMCONNECT_RECV_OPEN data)
        {
            Console.WriteLine("SimConnect_OnRecvOpen");
            Console.WriteLine("Connected to KH");

            sConnectButtonLabel = "Disconnect";
            bConnected = true;

            // Register pending requests
            foreach (SimvarRequest oSimvarRequest in lSimvarRequests)
            {
                if (oSimvarRequest.bPending)
                {
                    oSimvarRequest.bPending = !RegisterToSimConnect(oSimvarRequest);
                    oSimvarRequest.bStillPending = oSimvarRequest.bPending;
                }
            }

            ToggleMainTimer(true);
            bOddTick = false;
        }

        /// The case where the user closes game
        private void SimConnect_OnRecvQuit(SimConnect sender, SIMCONNECT_RECV data)
        {
            Console.WriteLine("SimConnect_OnRecvQuit");
            Console.WriteLine("KH has exited");

            Disconnect();
        }

        private void SimConnect_OnRecvException(SimConnect sender, SIMCONNECT_RECV_EXCEPTION data)
        {
            SIMCONNECT_EXCEPTION eException = (SIMCONNECT_EXCEPTION)data.dwException;
            Console.WriteLine("SimConnect_OnRecvException: " + eException.ToString());

            lErrorMessages.Add("SimConnect : " + eException.ToString());
        }

        /// <summary>
        /// handle new data
        /// called when new data is received
        /// 
        /// todo: move this logic to simconnect bridge
        /// </summary>
        private void SimConnect_OnRecvSimobjectDataBytype(SimConnect sender, SIMCONNECT_RECV_SIMOBJECT_DATA_BYTYPE data)
        {
            if (simconnectBridge == null)
            {
                throw new ApplicationException("simconnectBridge should not be null");
            }

            // check object ID
            uint iObject = data.dwObjectID;
            if (!lObjectIDs.Contains(iObject))
            {
                lObjectIDs.Add(iObject);
            }
            bool isObjectIdValid = (!bObjectIDSelectionEnabled || iObject == m_iObjectIdRequest);
            if (!isObjectIdValid)
            {
                return;
            }

            // find the SimvarRequest
            uint requestId = data.dwRequestID; // this is basically the index in the list of simvars
            SimvarRequest oSimvarRequest = simconnectBridge.GetSimvarRequest(requestId);

            // handle the data
            string key = oSimvarRequest.sName;
            if (oSimvarRequest.sUnits == null)
            {
                // handle string
                SimconnectText scText = (SimconnectText)data.dwData[0];
                HandleSimconnectString(key, scText.text);
            }
            else
            {
                double dValue = simconnectBridge.parseDoubleValue(key, data);
                oSimvarRequest.dValue = dValue;
                oSimvarRequest.bPending = false;
                oSimvarRequest.bStillPending = false;

                // update the value
                simconnectBridge.HandleDataFromSim(key, dValue);
            }
        }

        private bool is_handling_aircraft_title_ = false;
        internal void HandleSimconnectString(string key, string value)
        {
            Console.WriteLine($"handle string: {key}, {value}");

            if (key == SimVars.TITLE)
            {
                simconnectBridge.SetTitle(value);

                is_handling_aircraft_title_ = true; // pause execution

                try
                {
                    // load config for the aircraft
                    configService.HandleAircraftTitle(value);

                    // init controllers
                    customAutopilot.HandleAircraftConnected();

                    // update UI
                    UpdateUi();
                }
                catch (Exception e)
                {
                    is_handling_aircraft_title_ = false;
                    throw new ApplicationException();
                }

                is_handling_aircraft_title_ = false;
            }
        }

        /// <summary>
        /// 
        /// [max checkbox]
        /// todo: move this?
        /// 
        /// </summary>
        private void DecideTargetAirspeed()
        {
            if (simconnectBridge.IsSimPaused)
            {
                return;
            }

            string previousTargetAirspeedStr = uiTargetAirspeed;
            double newTargetAirspeed = -1;

            if (ShouldUseMaxAirspeed())
            {
                // use max airspeed
                newTargetAirspeed = simconnectBridge.GetVarAirspeedBarberPole();

                // if descending, apply a buffer to prevent overspeeding
                double vertSpeed = simconnectBridge.GetCurrentVertSpeed();
                if (vertSpeed < 250)
                {
                    double buffer = Math.Abs(vertSpeed) / 500;  // 1 knot per 500 fpm
                    newTargetAirspeed -= buffer;
                }
            }
            else if (simconnectBridge.GetTargetSpeedIfAvailable() > 0)
            {
                // use target airspeed from the sim
                // NOTE: for some aircraft AUTOPILOT_AIRSPEED_HOLD_VAR is always 0 (eg: C152)
                newTargetAirspeed = simconnectBridge.GetTargetSpeedIfAvailable();
            }
            else if (previousTargetAirspeedStr == "0")
            {
                // use ~85% cruise speed
                newTargetAirspeed = 0.85 * simconnectBridge.GetCruiseSpeedIndicated();
                newTargetAirspeed = ApUtil.RoundTo(newTargetAirspeed, 5);
                newTargetAirspeed = Math.Min(newTargetAirspeed, 245);  // cap below 250
            }

            // set the new target airspeed
            if (newTargetAirspeed > 0)
            {
                string newTargetAirspeedStr = Math.Round(newTargetAirspeed).ToString();
                if (newTargetAirspeedStr != previousTargetAirspeedStr)
                {
                    Console.WriteLine($"target airspeed updated: {previousTargetAirspeedStr} -> {newTargetAirspeedStr}");
                    uiTargetAirspeed = newTargetAirspeedStr;
                }
            }
        }

        private bool ShouldUseMaxAirspeed()
        {
            if (uiUseMaxAirspeedCheckbox)
            {
                // this is to prevent overspeeding under FL100
                // also to mitigate me forgetting to uncheck the box
                double slowDownAlt = 10750;

                // if descending, calc transition point based on VS
                double vertSpeed = simconnectBridge.GetCurrentVertSpeed();
                if (vertSpeed < 250)
                {
                    // eg: -3000 fpm * 1 min -> 13000 ft
                    double minutesToSlow = 1.0;
                    double slowDownAlt2 = 10000 + (Math.Abs(vertSpeed) * minutesToSlow);
                    slowDownAlt = Math.Max(slowDownAlt, slowDownAlt2);
                }

                return simconnectBridge.GetCurrentAltitude() > slowDownAlt;
            }
            return false;
        }

        /// <summary>
        /// called when the increase/decrease airspeed buttons are pressed
        /// </summary>
        /// <param name="delta"> eg: 135, +5, -5 </param>
        private void TryChangeTargetAirspeed(string desiredAirspeed)
        {
            if (simconnectBridge.GetTargetSpeedIfAvailable() > 0)
            {
                return;  // airspeed handled by the sim
            }
            Console.WriteLine($"TryChangeTargetAirspeed: {desiredAirspeed}");

            if (desiredAirspeed.StartsWith("+") || desiredAirspeed.StartsWith("-"))
            {
                // adjust target
                int currentTarget = Int32.Parse(uiTargetAirspeed);
                int delta = Int32.Parse(desiredAirspeed);
                int newTarget = (int)ApUtil.RoundTo(currentTarget + delta, delta);
                uiTargetAirspeed = newTarget.ToString();
            }
            else
            {
                // set target
                uiTargetAirspeed = desiredAirspeed;
            }

            Console.WriteLine($"uiTargetAirspeed updated: {uiTargetAirspeed}");
        }

        private void ToggleMainTimer(bool state)
        {
            mainLoopTimer2.Enabled = state;
        }

        private readonly Timestamp previousTimestamp = Timestamp.Now();
        private void HandleTimer2(Object source, ElapsedEventArgs e)
        {
            if (IsMainLoopActive())
            {
                Console.WriteLine("\n\n\n");
                Console.WriteLine("t={0:HH:mm:ss.fff}, elapsed={1} ms", e.SignalTime, previousTimestamp.GetAgeMillis());
            }
            previousTimestamp.SetToNow();

            OnTick();
        }

        private bool IsMainLoopActive()
        {
            if (simconnectBridge == null || simconnectBridge.IsSimPaused || !simconnectBridge.AreVarsReady)
            {
                return false;
            }
            if (simconnectBridge.IsOnGroundBelowSpeed(5))
            {
                return false;
            }
            return customAutopilot.IsActive || customAutopilot.IsAutothrottleActive;
        }

        /// <summary>
        ///
        /// this is the main loop function
        /// called at reg interval after connect
        /// frequency slider is period in ms:
        ///  1000 = 1hz
        ///  100 = 10 hz
        ///
        /// May not be the best way to achive regular requests.
        /// See SimConnect.RequestDataOnSimObject
        ///
        /// </summary>
        private void OnTick()
        {
            if (is_handling_aircraft_title_)
            {
                return;  // skip while new aircraft dialog is active
            }

            bOddTick = !bOddTick;

            joystickService.Poll(); // todo: poll joystick more often

            foreach (SimvarRequest oSimvarRequest in lSimvarRequests)
            {
                if (!oSimvarRequest.bPending)
                {
                    m_oSimConnect?.RequestDataOnSimObjectType(oSimvarRequest.eRequest, oSimvarRequest.eDef, 0, m_eSimObjectType);
                    oSimvarRequest.bPending = true;
                }
                else
                {
                    oSimvarRequest.bStillPending = true;
                }
            }

            if (simconnectBridge.AreVarsReady)
            {
                if (simconnectBridge.AtMainMenu())
                {
                    Disconnect();
                    return;
                }

                DecideTargetAirspeed();

                CheckInverted();

                DoLandedChecks();

                try
                {
                    customAutopilot.OnTick();
                }
                catch (Exception e)
                {
                    Console.WriteLine("customAutopilot.OnTick() exception: " + e.ToString());
                }
            }
        }

        private void DoLandedChecks()
        {
            if (simconnectBridge.IsLanded() || simconnectBridge.GetAltitudeAgl() < 50)
            {
                // reset roll
                if (uiTargetRoll != "0")
                {
                    uiTargetRoll = "0";
                }
            }
        }

        // todo: make this better
        private void CheckInverted()
        {
            if (configService.GetAppCfg().bodieMode)
            {
                double absRollDeg = Math.Abs(simconnectBridge.GetVarRoll());
                if (absRollDeg < 85)
                {
                    // right side up
                    uiInvertCheckbox = false;
                }
                else if (absRollDeg > 95)
                {
                    // up side down
                    uiInvertCheckbox = true;
                }
            }
        }

        private void ToggleConnect()
        {
            if (m_oSimConnect == null)
            {
                try
                {
                    Connect();
                }
                catch (COMException ex)
                {
                    Console.WriteLine("Unable to connect to KH: " + ex.Message);
                }
            }
            else
            {
                Disconnect();
            }
        }

        private void ClearResquestsPendingState()
        {
            foreach (SimvarRequest oSimvarRequest in lSimvarRequests)
            {
                oSimvarRequest.bPending = false;
                oSimvarRequest.bStillPending = false;
            }
        }

        private bool RegisterToSimConnect(SimvarRequest _oSimvarRequest)
        {
            if (m_oSimConnect != null)
            {
                /// Define a data structure
                m_oSimConnect.AddToDataDefinition(_oSimvarRequest.eDef, _oSimvarRequest.sName, _oSimvarRequest.sUnits, SIMCONNECT_DATATYPE.FLOAT64, 0.0f, SimConnect.SIMCONNECT_UNUSED);
                /// IMPORTANT: Register it with the simconnect managed wrapper marshaller
                /// If you skip this step, you will only receive a uint in the .dwData field.
                m_oSimConnect.RegisterDataDefineStruct<double>(_oSimvarRequest.eDef);

                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// called when add request button clicked (both args are null)
        /// can call manually as well?
        /// </summary>
        private void AddRequest(string _sOverrideSimvarRequest, string _sOverrideUnitRequest)
        {
            //Console.WriteLine("AddRequest");

            string sNewSimvarRequest = _sOverrideSimvarRequest != null ? _sOverrideSimvarRequest : ((m_iIndexRequest == 0) ? m_sSimvarRequest : (m_sSimvarRequest + ":" + m_iIndexRequest));
            string sNewUnitRequest = _sOverrideUnitRequest != null ? _sOverrideUnitRequest : m_sUnitRequest;

            SimvarRequest oSimvarRequest = new SimvarRequest
            {
                eDef = (DEFINITION)m_iCurrentDefinition,
                eRequest = (REQUEST)m_iCurrentRequest,
                sName = sNewSimvarRequest,
                sUnits = sNewUnitRequest
            };

            oSimvarRequest.bPending = !RegisterToSimConnect(oSimvarRequest);
            oSimvarRequest.bStillPending = oSimvarRequest.bPending;

            lSimvarRequests.Add(oSimvarRequest);

            ++m_iCurrentDefinition;
            ++m_iCurrentRequest;
        }

        private void AddRequestString(string sNewSimvarRequest)
        {
            Console.WriteLine("AddRequestString");
            if (m_oSimConnect == null)
            {
                throw new ApplicationException();
            }

            SimvarRequest request = new SimvarRequest
            {
                eDef = (DEFINITION)m_iCurrentDefinition,
                eRequest = (REQUEST)m_iCurrentRequest,
                sName = sNewSimvarRequest,
                sUnits = null
            };

            m_oSimConnect.AddToDataDefinition(request.eDef, request.sName, request.sUnits, SIMCONNECT_DATATYPE.STRINGV, 0.0f, SimConnect.SIMCONNECT_UNUSED);
            m_oSimConnect.RegisterDataDefineStruct<SimconnectText>(request.eDef);

            request.bPending = false;
            request.bStillPending = false;

            lSimvarRequests.Add(request);

            ++m_iCurrentDefinition;
            ++m_iCurrentRequest;
        }

        private void RemoveSelectedRequest()
        {
            lSimvarRequests.Remove(oSelectedSimvarRequest);
        }

        /// <summary>
        /// called when "Try Set Value" button is clicked
        /// </summary>
        private void TrySetValue()
        {
            Console.WriteLine("TrySetValue");

            if (m_oSelectedSimvarRequest != null && m_sSetValue != null)
            {
                double dValue = 0.0;
                if (double.TryParse(m_sSetValue, NumberStyles.Any, null, out dValue))
                {
                    string key = m_oSelectedSimvarRequest.sName;
                    simconnectBridge.SetValue(key, dValue);
                }
            }
        }

        private void DoResetTrim()
        {
            simconnectBridge.SetValue(SimVars.ELEVATOR_TRIM_POSITION, 0);
            simconnectBridge.SetValue(SimVars.AILERON_TRIM_PCT, 0);
        }

        private void afterApToggle(bool isEnabled)
        {
            if (!isEnabled)
            {
                // on AP off: reset aileron trim
                simconnectBridge.SetValue(SimVars.AILERON_TRIM_PCT, 0);
            }
        }

        private void ResetITerm()
        {
            Console.WriteLine("ResetITerm");
            configService.SelectedController.resetIntegralTerm();
        }

        private void LoadFiles()
        {
            Microsoft.Win32.OpenFileDialog oOpenFileDialog = new Microsoft.Win32.OpenFileDialog();
            oOpenFileDialog.Multiselect = true;
            oOpenFileDialog.Filter = "Simvars files (*.simvars)|*.simvars";
            if (oOpenFileDialog.ShowDialog() == true)
            {
                foreach (string sFilename in oOpenFileDialog.FileNames)
                {
                    LoadFile(sFilename);
                }
            }
        }

        private void LoadFile(string _sFileName)
        {
            string[] aLines = System.IO.File.ReadAllLines(_sFileName);
            LoadFromLines(aLines);
        }

        private void LoadFromLines(string[] aLines)
        {
            for (uint i = 0; i < aLines.Length; ++i)
            {
                // Format : Simvar,Unit
                string[] aSubStrings = aLines[i].Split(',');
                if (aSubStrings.Length >= 2) // format check
                {
                    // values check
                    string[] aSimvarSubStrings = aSubStrings[0].Split(':'); // extract Simvar name from format Simvar:Index
                    string sSimvarName = Array.Find(SimVars.Names, s => s == aSimvarSubStrings[0]);
                    string sUnitName = Array.Find(Units.Names, s => s == aSubStrings[1]);
                    if (sSimvarName != null && sUnitName != null)
                    {
                        AddRequest(aSubStrings[0], aSubStrings[1]);
                    }
                    else
                    {
                        if (sSimvarName == null)
                        {
                            lErrorMessages.Add("l." + i.ToString() + " Wrong Simvar name : " + aSubStrings[0]);
                        }
                        if (sUnitName == null)
                        {
                            lErrorMessages.Add("l." + i.ToString() + " Wrong Unit name : " + aSubStrings[1]);
                        }
                    }
                }
                else
                {
                    lErrorMessages.Add("l." + i.ToString() + " Bad input format : " + aLines[i]);
                    lErrorMessages.Add("l." + i.ToString() + " Must be : SIMVAR,UNIT");
                }
            }
        }

        private void SaveFile(bool _bWriteValues)
        {
            Microsoft.Win32.SaveFileDialog oSaveFileDialog = new Microsoft.Win32.SaveFileDialog();
            oSaveFileDialog.Filter = "Simvars files (*.simvars)|*.simvars";
            if (oSaveFileDialog.ShowDialog() == true)
            {
                using (StreamWriter oStreamWriter = new StreamWriter(oSaveFileDialog.FileName, false))
                {
                    foreach (SimvarRequest oSimvarRequest in lSimvarRequests)
                    {
                        // Format : Simvar,Unit
                        string sFormatedLine = oSimvarRequest.sName + "," + oSimvarRequest.sUnits;
                        if (bSaveValues)
                        {
                            sFormatedLine += ",  " + oSimvarRequest.dValue.ToString();
                        }
                        oStreamWriter.WriteLine(sFormatedLine);
                    }
                }
            }
        }

        public void SetTickSliderValue(int _iValue)
        {
            Console.WriteLine($"SetTickSliderValue: not implemented! ({_iValue} ms)");
            //m_oTimer.Interval = new TimeSpan(0, 0, 0, 0, (int)(_iValue));
        }

        internal void HandleAddWaypointButton()
        {
            flightPlanManager.AddWaypoint(uiWaypointFrequency, uiWaypointCrsIn, uiWaypointCrsOut);

            // reset UI
            uiWaypointFrequency = "";
            uiWaypointCrsIn = uiWaypointCrsOut;
            uiWaypointCrsOut = "";
        }

        internal void HandleFlipSignsButton()
        {
            uiTargetVertSpeed = ApUtil.NegateString(uiTargetVertSpeed);
            uiTargetPitch = ApUtil.NegateString(uiTargetPitch);
            uiTargetRoll = ApUtil.NegateString(uiTargetRoll);

            //eventService.FireChangeRequestEvent(ChangeRequestEvent.TargetKey.ADJUST_HEADING_BUG, 180);
        }
    }
}
