﻿using System.Collections.Generic;

namespace FsAutopilot.Autopilot
{
    public class ChartPresets
    {
        public const string DEFAULT = "DEFAULT";

        private Dictionary<string, List<string>> presets = new Dictionary<string, List<string>>();

        public ChartPresets()
        {
            // define presets (groups of 3 charts)
            var altVsPitch = MakeList(
                PidControllerNames.ALTITUDE, PidControllerNames.VERT_SPEED, PidControllerNames.PITCH);
            var gsVsPitch = MakeList(
                PidControllerNames.GLIDE_SLOPE, PidControllerNames.VERT_SPEED, PidControllerNames.PITCH);
            var altFlcPitch = MakeList(
                PidControllerNames.ALTITUDE, PidControllerNames.FLC, PidControllerNames.PITCH);

            var navHdgRoll = MakeList(
                PidControllerNames.NAVIGATION, PidControllerNames.HEADING, PidControllerNames.ROLL);

            var airspeedVsPitch = MakeList(
                PidControllerNames.AIRSPEED, PidControllerNames.VERT_SPEED, PidControllerNames.PITCH);

            var vsPitchPr = MakeList(
                PidControllerNames.VERT_SPEED, PidControllerNames.PITCH, PidControllerNames.PITCH_RATE);

            // define mappings
            presets[DEFAULT] = altVsPitch;

            presets[PidControllerNames.GLIDE_SLOPE] = gsVsPitch;
            presets[PidControllerNames.FLC] = altFlcPitch;
            presets[PidControllerNames.VERT_SPEED] = airspeedVsPitch;
            presets[PidControllerNames.PITCH] = airspeedVsPitch;

            presets[PidControllerNames.PITCH_2] = vsPitchPr;
            presets[PidControllerNames.PITCH_RATE] = vsPitchPr;

            presets[PidControllerNames.NAVIGATION] = navHdgRoll;
            presets[PidControllerNames.HEADING] = navHdgRoll;
            presets[PidControllerNames.ROLL] = navHdgRoll;

            presets[PidControllerNames.AIRSPEED] = airspeedVsPitch;
        }

        private List<string> MakeList(string chart1, string chart2, string chart3)
        {
            var result = new List<string>
            {
                chart1,
                chart2,
                chart3
            };
            return result;
        }

        internal List<string> GetPresets(string key)
        {
            if (presets.ContainsKey(key))
            {
                return presets[key];
            }
            return presets[DEFAULT];
        }

    }
}
