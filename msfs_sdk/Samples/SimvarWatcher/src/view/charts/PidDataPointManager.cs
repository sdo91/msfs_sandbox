﻿using FsAutopilot.Events;
using FsAutopilot.Util;
using LiveCharts;
using System;

namespace FsAutopilot.View
{

    /// <summary>
    ///
    /// this class manages the data points in a PID chart
    ///
    /// </summary>
    public class PidDataPointManager
    {
        public ChartValues<PidDataPoint> endGoal = new ChartValues<PidDataPoint>();
        public ChartValues<PidDataPoint> commanded = new ChartValues<PidDataPoint>();
        public ChartValues<PidDataPoint> reported = new ChartValues<PidDataPoint>();
        public ChartValues<PidDataPoint> wildCard = new ChartValues<PidDataPoint>();

        internal void PlotData(DateTime now, ControllerEntry controlData)
        {
            if (!controlData.HasValidData())
            {
                return;
            }
            AddDataPoint(endGoal, now, controlData.endGoal);
            AddDataPoint(commanded, now, controlData.shortGoal);
            AddDataPoint(reported, now, controlData.current);
            if (controlData.wildCard != 0)
            {
                AddDataPoint(wildCard, now, controlData.wildCard);
            }
        }

        private void AddDataPoint(ChartValues<PidDataPoint> list, DateTime now, double value)
        {
            ApUtil.RoundTo(value, 0.01);
            list.Add(new PidDataPoint
            {
                DateTime = now,
                Value = value
            });
            TrimSize(list);
        }

        private void TrimSize(ChartValues<PidDataPoint> data)
        {
            while (data.Count > 2)
            {
                var age = TimeUtil.CalcElapsedTime(data[0].DateTime);
                if (age > PidResponseChart.SECONDS_HISTORY)
                {
                    data.RemoveAt(0);  // remove it
                }
                else
                {
                    //Console.WriteLine($"trimmed size to: {data.Count}");
                    return;  // we are done
                }
            }
        }
    }
}
