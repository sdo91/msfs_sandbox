﻿using FsAutopilot.Autopilot;
using FsAutopilot.Events;
using FsAutopilot.Util;
using LiveCharts;
using LiveCharts.Wpf;
using System;
using System.Windows.Media;

namespace FsAutopilot.View
{
    /// <summary>
    /// manage the data/functionality for one of the Pid Response Charts
    /// </summary>
    public class PidResponseChart
    {
        public static readonly double SECONDS_HISTORY = 15;

        private string pidControllerName;

        private readonly CartesianChart cartesianChart;  // owns the axes

        private readonly PidDataPointManager dataPointManager = new PidDataPointManager();

        private readonly Func<double, string> dateTimeFormatter;

        public PidResponseChart(string pidControllerName)
        {
            this.pidControllerName = pidControllerName;

            // set how to display the X Labels
            dateTimeFormatter = value => new DateTime((long)value).ToString(":ss");

            // init the chart
            cartesianChart = MakeChart();
        }

        /// <summary>
        /// todo: add button to reset to these zoom values
        /// </summary>
        /// <returns></returns>
        private int GetDefaultYAxisMax()
        {
            switch (pidControllerName)
            {
                case PidControllerNames.ALTITUDE:
                    return 1500;
                case PidControllerNames.FLC:
                    return 5;
                case PidControllerNames.VERT_SPEED:
                    return 500;
                case PidControllerNames.PITCH:
                    return 5;
                case PidControllerNames.PITCH_RATE:
                    return 5;
                case PidControllerNames.NAVIGATION:
                    return 50;
                case PidControllerNames.HEADING:
                    return 5;
                case PidControllerNames.ROLL:
                    return 15; // +/- 30
                case PidControllerNames.AIRSPEED:
                    return 5;
                default:
                    return 5;  // 1 unit markers
            }
        }

        public CartesianChart GetCartesianChart()
        {
            return cartesianChart;
        }

        private CartesianChart MakeChart()
        {
            var result = new CartesianChart
            {
                //AnimationsSpeed = TimeSpan.FromSeconds(0.5),
                DisableAnimations = true,
                Hoverable = false,
                DataTooltip = null
            };

            // NOTE: last color will display on top
            result.Series.Add(MakeSeries(dataPointManager.wildCard, Colors.Green));
            result.Series.Add(MakeSeries(dataPointManager.endGoal, Colors.Black));
            result.Series.Add(MakeSeries(dataPointManager.commanded, Colors.Red));
            result.Series.Add(MakeSeries(dataPointManager.reported, Colors.Blue));

            // init y/x axes
            result.AxisX.Add(MakeXAxis());
            result.AxisY.Add(MakeYAxis());

            // enable zoom on Y axis (mouse scroll wheel)
            result.Zoom = ZoomingOptions.Y;

            return result;
        }

        private LineSeries MakeSeries(ChartValues<PidDataPoint> dataPoints, Color color)
        {
            var result = new LineSeries
            {
                Values = dataPoints,
                PointGeometry = null,
                LineSmoothness = 1,
                StrokeThickness = 2,
                Stroke = new SolidColorBrush(color),
                Fill = new SolidColorBrush(Colors.Transparent)
            };
            return result;
        }

        private Axis MakeXAxis()
        {
            // axisUnit forces lets the axis know that we are plotting seconds
            // this is not always necessary, but it can prevent wrong labeling
            double axisUnit = TimeSpan.TicksPerSecond;

            // axisStep forces the distance between each separator in the X axis
            double axisStep = TimeSpan.FromSeconds(1).Ticks;

            // Note: MinValue/MaxValue will be set elsewhere
            Axis xAxis = new Axis
            {
                LabelFormatter = dateTimeFormatter,
                Unit = axisUnit
            };

            xAxis.Separator.Step = axisStep;

            return xAxis;
        }

        private Axis MakeYAxis()
        {
            Axis yAxis = new Axis();
            yAxis.MaxValue = GetDefaultYAxisMax();
            yAxis.MinValue = -yAxis.MaxValue;

            yAxis.Unit = 1;
            yAxis.Position = AxisPosition.RightTop;

            //yAxis.Separator.Step = ?;

            // limit decimal places on the Y axis
            yAxis.LabelFormatter = FormatYAxisLabel;

            return yAxis;
        }

        private string FormatYAxisLabel(double value)
        {
            if (IsName(PidControllerNames.HEADING))
            {
                value *= -1;
                value = ApUtil.WrapToMatch(value, 180);
            }
            return ApUtil.RoundTo(value, 0.1).ToString();
        }

        /// <summary>
        ///
        /// goals:
        /// center command (RED)
        /// keep blue in frame
        /// keep same range/zoom
        /// 
        /// NOTE: must run on the UI thread
        ///
        /// </summary>
        public void UpdateYAxisLimits(PidResponseData data)
        {
            ControllerEntry controllerEntry = data.GetControllerData(pidControllerName);
            double red = controllerEntry.shortGoal;
            double blue = controllerEntry.current;

            Axis yAxis = cartesianChart.AxisY[0];
            var newMax = yAxis.MaxValue;
            var newMin = yAxis.MinValue;

            // WIP: round red sometimes
            if (IsName(PidControllerNames.VERT_SPEED) ||
                IsName(PidControllerNames.HEADING))
            {
                double diff = Math.Abs(red - blue);
                if (diff < yAxis.Separator.Step)
                {
                    red = ApUtil.RoundTo(red, yAxis.Separator.Step);
                }
            }

            // move command (red) to center
            ApUtil.AdjustRangeToFitX(ref newMin, ref newMax, red, 0.5);

            // make sure current (blue) is still in frame
            ApUtil.AdjustRangeToFitX(ref newMin, ref newMax, blue, 0.1);

            // set the values
            yAxis.MaxValue = newMax;
            yAxis.MinValue = newMin;

            // set separator lines
            double yRange = Math.Abs(newMax - newMin);
            double heightPixels = cartesianChart.ActualHeight;
            double pixelsPerUnit = heightPixels / yRange;

            // set separator lines
            if (pixelsPerUnit > 10)
            {
                yAxis.Separator.Step = 1;
            }
            else if (pixelsPerUnit > 5)
            {
                yAxis.Separator.Step = 2;
            }
            else if (pixelsPerUnit > 2)
            {
                yAxis.Separator.Step = 5;
            }
            else if (pixelsPerUnit > 1)
            {
                yAxis.Separator.Step = 10;
            }
            else if (pixelsPerUnit > 0.13)
            {
                // threshold picked to get altitude step to 500 on ultrawide
                yAxis.Separator.Step = 100;
            }
            else
            {
                yAxis.Separator.Step = 500;
            }

            // log
            //if (IsName(PidControllerNames.ALTITUDE))
            //if (IsName(PidControllerNames.ROLL))
            //{
            //    Console.WriteLine(
            //        $"controller={pidControllerName}, yRange={yRange}, heightPixels={heightPixels}, " +
            //        $"pixelsPerUnit={pixelsPerUnit}, step={yAxis.Separator.Step}");
            //}
        }

        // NOTE: must run on the UI thread
        public void UpdateXAxisLimits(double xMin, double xMax)
        {
            Axis xAxis = cartesianChart.AxisX[0];

            xAxis.MinValue = xMin;
            xAxis.MaxValue = xMax;
        }

        public void HandlePidData(DateTime now, PidResponseData controlData)
        {
            ControllerEntry controllerData = controlData.GetControllerData(pidControllerName);

            dataPointManager.PlotData(now, controllerData);
        }

        internal bool IsName(string name)
        {
            return pidControllerName == name;
        }

    }
}
