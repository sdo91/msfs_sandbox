﻿using System;

namespace FsAutopilot.View
{
    public class PidDataPoint
    {
        public DateTime DateTime { get; set; }
        public double Value { get; set; }
    }
}
