﻿using FsAutopilot.Autopilot;
using FsAutopilot.Events;
using FsAutopilot.Util;
using LiveCharts;
using LiveCharts.Configurations;
using System;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using System.Windows.Controls;

namespace FsAutopilot.View
{
    /// <summary>
    ///
    /// colors:
    ///     red: desired (commanded)
    ///     black: limited command (ie: capped)
    ///     blue: current (reported)
    ///
    /// todo:
    ///     flip signs from simconnect (eg: pitch, roll)
    ///
    /// how to add a new chart:
    ///     add field to PidResponseData
    ///     populate new PidResponseData field
    ///     add/change presets in ChartPresets
    ///     add to GetDefaultYAxisMax
    ///
    /// </summary>
    public partial class ApChartManager : UserControl, INotifyPropertyChanged
    {
        /// <summary>
        /// this is kind of a hack, to access XAML elements
        /// see: https://stackoverflow.com/questions/13644114/how-can-i-access-a-control-in-wpf-from-another-class-or-window
        /// </summary>
        private readonly MainWindow Form = Application.Current.Windows[0] as MainWindow;

        private const int NUM_CHARTS = 3;
        private PidResponseChart[] pidCharts = new PidResponseChart[NUM_CHARTS];

        private readonly ChartPresets chartPresets = new ChartPresets();

        private string selectedPidController = ChartPresets.DEFAULT;

        public ApChartManager(EventService eventService)
        {
            // lets save the mapper globally.
            var mapper = Mappers.Xy<PidDataPoint>()
                .X(model => model.DateTime.Ticks)   //use DateTime.Ticks as X
                .Y(model => model.Value);           //use the value property as Y
            Charting.For<PidDataPoint>(mapper);

            // todo: remove this?
            IsReading = false;

            // custom
            InitCharts();

            eventService.AddHandler(EventTopic.PID_RESPONSE_DATA, HandlePidResponseData);
        }

        private string active_chart_names_;
        public string uiActiveChartNames
        {
            get
            {
                return active_chart_names_;
            }
            set
            {
                active_chart_names_ = value;
                OnPropertyChanged("uiActiveChartNames");
            }
        }

        /// <summary>
        ///
        /// todo:
        ///     subscribe to FLC/VS/GS/etc mode
        ///     make charts dynamic
        ///     eg:
        ///         while pitch is selected, show vs or flc
        /// 
        /// </summary>
        private void InitCharts()
        {
            var chartNames = chartPresets.GetPresets(selectedPidController);
            uiActiveChartNames = ApUtil.FormatList(chartNames);
            Console.WriteLine($"[ApChartManager] InitCharts: {selectedPidController} -> {uiActiveChartNames}");
            for (int i = 0; i < NUM_CHARTS; i++)
            {
                MakePidChart(i, chartNames[i]);
            }

            UpdateAxes(DateTime.Now, null);
        }

        private void MakePidChart(int i, string name)
        {
            // check if the chart is already init'd
            if (pidCharts[i] != null && pidCharts[i].IsName(name))
            {
                Console.WriteLine($"[ApChartManager] PID chart already good! (i={i}, name={name})");
                return;  // chart already good!
            }

            pidCharts[i] = new PidResponseChart(name);

            var children = GetChartGridChildren(i);
            children.Clear();
            children.Add(pidCharts[i].GetCartesianChart());

            Console.WriteLine($"[ApChartManager] PID chart created! (i={i}, name={name})");
        }

        private UIElementCollection GetChartGridChildren(int indexFrom0)
        {
            if (indexFrom0 == 0)
            {
                return Form.ChartGrid1.Children;
            }
            if (indexFrom0 == 1)
            {
                return Form.ChartGrid2.Children;
            }
            else
            {
                return Form.ChartGrid3.Children;
            }
        }

        /// <summary>
        /// plot new telemetry from the sim/autopilot
        /// </summary>
        private void HandlePidResponseData(Event myEvent)
        {
            try
            {
                PidResponseData controlData = (PidResponseData)myEvent.data;
                controlData.NormalizeData();

                var now = DateTime.Now;

                foreach (PidResponseChart chart in pidCharts)
                {
                    if (chart == null) {
                        // not sure how this happens, but I have seen it...
                        Console.WriteLine("[HandlePidResponseData] chart is null somehow...");
                        return;
                    }
                    chart.HandlePidData(now, controlData);
                }

                UpdateAxes(now, controlData);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void UpdateAxes(DateTime now, PidResponseData controlData)
        {
            AxisMax = now.Ticks + TimeSpan.FromSeconds(0.5).Ticks; // lets force the axis to be 1 second ahead
            AxisMin = now.Ticks - TimeSpan.FromSeconds(PidResponseChart.SECONDS_HISTORY).Ticks; // and X seconds behind

            foreach (PidResponseChart chart in pidCharts)
            {
                Dispatcher.Invoke(() =>
                {
                    // this logic needs to run on the UI thread
                    chart.UpdateXAxisLimits(AxisMin, AxisMax);
                    if (controlData != null)
                    {
                        chart.UpdateYAxisLimits(controlData);
                    }
                });
            }
        }

        private double _axisMax;
        public double AxisMax
        {
            get { return _axisMax; }
            set
            {
                _axisMax = value;
                OnPropertyChanged("AxisMax");
            }
        }

        private double _axisMin;
        public double AxisMin
        {
            get { return _axisMin; }
            set
            {
                _axisMin = value;
                OnPropertyChanged("AxisMin");
            }
        }

        public bool IsReading { get; set; }

        private void Read()
        {
            while (IsReading)
            {
                Thread.Sleep(100);
                PublishFakeData();
            }
        }

        private void PublishFakeData()
        {
            PidResponseData fakeData = new PidResponseData();
            fakeData.Randomize();

            Event fakeEvent = new Event(EventTopic.PID_RESPONSE_DATA, fakeData);
            HandlePidResponseData(fakeEvent);
        }

        /// <summary>
        /// todo: fix async bug
        /// can't update chart axis for some reason from a different thread
        /// </summary>
        internal void InjectRandomData()
        {
            Console.WriteLine("ApChartManager InjectStopOnClick");

            //IsReading = !IsReading;
            //if (IsReading)
            //{
            //    Task.Factory.StartNew(Read);
            //}
            //updateCharts();

            PublishFakeData();
        }

        #region INotifyPropertyChanged implementation

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName = null)
        {
            if (PropertyChanged != null)
            {
                // update property in the UI
                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion INotifyPropertyChanged implementation

        /// <summary>
        /// todo: add a UI box to say which controllers are active
        /// </summary>
        internal void SelectControllers(string controllerName)
        {
            selectedPidController = controllerName;
            Console.WriteLine($"[ApChartManager] SelectControllers: {selectedPidController}");
            InitCharts();
        }

        internal void HandleResetButton()
        {
            for (int i = 0; i < NUM_CHARTS; i++)
            {
                pidCharts[i] = null;
            }

            InitCharts();
        }

    } // end class ApChartManager
}
