﻿using System;

namespace FsAutopilot.Util
{
    public class Vector2D
    {
        public double x;
        public double y;

        public Vector2D() { }

        public static Vector2D CreateFromXy(double x, double y)
        {
            Vector2D result = new Vector2D
            {
                x = x,
                y = y
            };
            return result;
        }

        public static Vector2D CreateFromRadial(double radial_dist, double radial_angle_deg)
        {
            double angle_rad = radial_angle_deg * ApUtil.DEG_TO_RAD;
            double x = radial_dist * Math.Sin(angle_rad);
            double y = radial_dist * Math.Cos(angle_rad);
            return CreateFromXy(x, y);
        }

        public static Vector2D CreateFromRadialStr(string radial_str)
        {
            // eg: "6.5/154"
            var tokens = radial_str.Split('/');
            double radial_dist = Double.Parse(tokens[0]);
            double radial_angle_deg = Double.Parse(tokens[1]);
            return CreateFromRadial(radial_dist, radial_angle_deg);
        }

        public static Vector2D operator -(Vector2D a, Vector2D b)
        {
            return CreateFromXy(a.x - b.x, a.y - b.y);
        }

        public double Magnitude()
        {
            return Math.Sqrt(x * x + y * y);
        }

        public double AngleDeg()
        {
            double angle_rads = Math.Atan2(x, y);
            double angle_deg = angle_rads * ApUtil.RAD_TO_DEG;
            angle_deg = ApUtil.WrapToMatch(angle_deg, 180);
            return angle_deg;
        }

        public string toString()
        {
            double magnitude = ApUtil.RoundTo(Magnitude(), 0.01);
            double angle_deg = ApUtil.RoundTo(AngleDeg(), 0.01);
            return $"[Vector2D: {magnitude} NM @ {angle_deg} deg]";
        }
    }
}
