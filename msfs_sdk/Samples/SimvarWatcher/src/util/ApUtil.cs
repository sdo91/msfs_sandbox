﻿using FsAutopilot.Simconnect;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace FsAutopilot.Util
{
    public class ApUtil
    {
        public static readonly double DEG_TO_RAD = Math.PI / 180;
        public static readonly double RAD_TO_DEG = 180 / Math.PI;

        public static readonly double FPM_TO_FPS = 1 / 60.0;
        public static readonly double KNOTS_TO_FPM = 101.269;
        public static readonly double KNOTS_TO_MACH = 1 / 666.739;

        public static readonly double NM_TO_FT = 6076.12;
        public static readonly double FT_TO_NM = 1 / NM_TO_FT;

        public static readonly double G_FPMPS = 9.8 * 3.28 * 60;  // 1G in fpm/sec
        public static readonly double G_KFPMPS = G_FPMPS / 1000.0;  // 1G in kfpm/sec

        public static void ConsoleLog(string text, bool verbose)
        {
            if (verbose)
            {
                Console.WriteLine(text);
            }
        }

        public static string GetProjectPath()
        {
            string path = Path.GetFullPath(@".");
            // example: C:\Users\sdo91\base\code\msfs_sandbox\msfs_sdk\Samples\SimvarsTests\bin\Debug

            // navigate up to msfs_sdk
            while (!path.EndsWith("msfs_sdk"))
            {
                path = Directory.GetParent(path).ToString();
            }

            // add relative path to project
            path = path.Replace("\\", "/");
            path += "/Samples/SimvarWatcher";
            return path;
        }

        public static string ExpandPath(string relativePath)
        {
            return $"{GetProjectPath()}/{relativePath}";
        }

        /// <summary>
        /// wrap currentDegrees to be within 180 degrees of desiredDegrees
        /// </summary>
        public static double WrapToMatch(double degreesToWrap, double desiredDegrees)
        {
            while (true)
            {
                var delta = desiredDegrees - degreesToWrap;
                if (Math.Abs(delta) <= 180)
                {
                    break;  // we are done
                }
                // else, we need to wrap
                if (delta < 0)
                {
                    degreesToWrap -= 360;
                }
                else
                {
                    degreesToWrap += 360;
                }
            }
            return degreesToWrap;
        }

        public static double AbsAngleDiff(double theta1, double theta2)
        {
            double diff = theta1 - theta2;
            diff = WrapToMatch(diff, 0);
            return Math.Abs(diff);
        }

        public static double RoundTo(double value, double roundNumber)
        {
            roundNumber = Math.Abs(roundNumber);
            return Math.Round(value / roundNumber) * roundNumber;
        }

        public static double RoundUp(double value, double roundNumber)
        {
            double rounded = RoundTo(value, roundNumber);
            if (rounded < value)
            {
                rounded += roundNumber;
            }
            return rounded;
        }

        /// <summary>
        /// given a desired decent rate and current speed, this will help me know when to descend
        /// </summary>
        /// <param name="targetVertSpeedFpm"></param>
        /// <param name="groundSpeedKnots"></param>
        /// <returns></returns>
        public static double CalcDescentAngle(double targetVertSpeedFpm, double groundSpeedKnots)
        {
            if (groundSpeedKnots == 0)
            {
                return -1;
            }
            double oppositeSideFpm = Math.Abs(targetVertSpeedFpm);
            double adjacentSideFpm = groundSpeedKnots * KNOTS_TO_FPM;
            double result_rad = Math.Atan2(oppositeSideFpm, adjacentSideFpm);
            double result_deg = RoundTo(result_rad * RAD_TO_DEG, 0.01);
            Console.WriteLine($"  descent angle from here: {result_deg} (deg)");
            return result_rad;
        }

        internal static double CalcVertSpeedOfDescent(double groundSpeedKnots, int descentAngle)
        {
            double groundSpeedFpm = groundSpeedKnots * KNOTS_TO_FPM;
            double descentSpeedFpm = groundSpeedFpm * Math.Tan(descentAngle * DEG_TO_RAD);
            return -descentSpeedFpm;
        }

        public static bool IsNeoflyRunning()
        {
            return IsProcessRunning("NeoFly");
        }

        public static bool IsProcessRunning(string name)
        {
            var matches = Process.GetProcessesByName(name);
            bool isRunning = matches.Length > 0;
            return isRunning;
        }

        public static List<string> GetMatchingProcessNames(string name)
        {
            Process[] processes = Process.GetProcesses();
            string nameLower = name.ToLower();
            List<string> result = new List<string>();
            foreach (Process p in processes)
            {
                if (p.ProcessName.ToLower().Contains(nameLower))
                {
                    result.Add(p.ProcessName);
                }
            }
            return result;
        }

        public static double CalcXtkLeft(double radialErrorDeg, double dme)
        {
            if (dme < 0.01)
            {
                // sometime DME is not available... (example: ILS @ OAKN)
                dme = 10;
                Console.WriteLine($"DME INVALID! Using {dme}nm to calc xtk left.");
            }

            double nmLeftOfRadial = dme * Math.Sin(radialErrorDeg * DEG_TO_RAD);
            //Console.WriteLine($"nmLeftOfRadial: {nmLeftOfRadial}");
            return nmLeftOfRadial;
        }

        /// <summary>
        /// NOTE:
        /// let the Radial Component Distance (RCD) be the length of leg that is on the radial
        /// in the right triangle formed with current ownship position and some point on the radial
        /// </summary>
        public static double CalcNavInterceptAngle(double radialErrorDegLeft, double dmeNm, double approachDistanceNm)
        {
            var xtkLeft = CalcXtkLeft(radialErrorDegLeft, dmeNm);

            // d2 is the RCD to the runway/VOR
            var d2 = dmeNm * Math.Cos(radialErrorDegLeft * DEG_TO_RAD);

            // d3 is the RCD to the buffer point
            var d3 = d2 - approachDistanceNm;

            var interceptAngleDeg = Math.Atan2(xtkLeft, d3) * RAD_TO_DEG;
            interceptAngleDeg = Math.Abs(interceptAngleDeg);

            // round result
            double result = ApUtil.RoundTo(interceptAngleDeg, 0.1);
            return result;
        }

        /// <summary>
        /// NOTE: this isn't very accurate...
        /// </summary>
        public static double CalcAirfieldElevation(double indicatedAlt, double rawGlideSlope, double degAboveGlideSlope, double dmeDist)
        {
            // elev = alt - DME * sin(gs + error)
            double thetaRad = (rawGlideSlope + degAboveGlideSlope) * DEG_TO_RAD;
            double altAboveFieldFt = dmeDist * Math.Sin(thetaRad) * NM_TO_FT;
            return indicatedAlt - altAboveFieldFt;
        }

        /// <summary>
        /// test at KCGZ
        /// ILS 05 freq = 111.15
        /// </summary>
        public static double CalcFeetAboveGlideslope(double degAboveGlideSlope, double dmeNm)
        {
            double ftAbove = dmeNm * NM_TO_FT * Math.Tan(degAboveGlideSlope * DEG_TO_RAD);
            return ftAbove;
        }

        /// <summary>
        ///
        /// some ILS don't have DME
        /// examples: KPMD
        ///
        /// </summary>
        public static double GetBestIlsDist(Dictionary<string, double> telemetry, int index)
        {
            // return best
            bool hasDme = telemetry[$"{SimVars.NAV_HAS_DME}:{index}"] != 0;
            if (hasDme)
            {
                return telemetry[$"{SimVars.NAV_DME}:{index}"];
            }
            else
            {
                // collect values
                double altitudeAgl = telemetry[SimVars.PLANE_ALT_ABOVE_GROUND];
                double rawGlideSlope = telemetry[$"{SimVars.NAV_RAW_GLIDE_SLOPE}:{index}"];
                double degAboveGlideSlope = telemetry[$"{SimVars.NAV_GLIDE_SLOPE_ERROR}:{index}"];

                if (rawGlideSlope < 1)
                {
                    // don't have glide slope lock yet
                    Console.WriteLine($"NO GLIDE SLOPE LOCK YET: 15 NM");
                    return 15;
                }

                // do math
                double heightAglNm = altitudeAgl * FT_TO_NM;
                double thetaDeg = rawGlideSlope + degAboveGlideSlope;
                double estimatedDmeNm = heightAglNm / Math.Sin(thetaDeg * DEG_TO_RAD);
                Console.WriteLine($"estimatedDmeNm={estimatedDmeNm:f2}, heightAglNm={heightAglNm:f2}, thetaDeg={thetaDeg:f2}");
                return estimatedDmeNm;
            }
        }

        /// <summary>
        /// 109.50 -> 0x0950
        /// https://www.fsdeveloper.com/forum/threads/setting-nav-frequency.5573/
        /// </summary>
        public static uint EncodeFreqToHex(double freqMhz)
        {
            freqMhz -= 100;
            string digits = Math.Round(freqMhz * 100).ToString();
            uint result = 0;
            foreach (char c in digits)
            {
                result <<= 4;
                result += (uint)Char.GetNumericValue(c);
            }
            return result;
        }

        /// <summary>
        /// scale from X to Y
        /// </summary>
        /// <param name="x">input</param>
        /// <param name="xMin">min input</param>
        /// <param name="xMax">max input</param>
        /// <param name="yMin">min output</param>
        /// <param name="yMax">max output</param>
        /// <returns>scaled value</returns>
        public static double Scale(double x, double xMin, double xMax, double yMin, double yMax)
        {
            if (xMax < xMin)
            {
                return Scale(x, xMax, xMin, yMax, yMin);
            }
            if (x < xMin)
            {
                return yMin;
            }
            else if (x > xMax)
            {
                return yMax;
            }
            else
            {
                // do the math
                var pctAboveMin = (x - xMin) / (xMax - xMin);
                var yDiff = yMax - yMin;
                var result = yMin + (pctAboveMin * yDiff);
                Console.WriteLine($"Scale result: {(int)result}");
                return result;
            }
        }

        /// <summary>
        /// 
        /// NOTE: result can be negative
        /// 
        /// </summary>
        public static double CalcInterceptTime(double distNm, double speedKnots)
        {
            if (speedKnots < 1)
            {
                Console.WriteLine("Floor airspeed to avoid NaN!");
                speedKnots = 1;
            }
            double resultSeconds = distNm * 3600 / speedKnots;
            return resultSeconds;
        }

        /// <summary>
        /// v = d / t
        /// d = t * v
        /// nm = hours * knots
        /// </summary>
        public static double CalcIlsDistance(double groundSpeedKnots, double minutes)
        {
            double hours = minutes / 60.0;
            return hours * groundSpeedKnots;
        }

        public static void LogXtk(double currentXtkNmLeft)
        {
            // calc side
            string side = (currentXtkNmLeft > 0) ? "left" : "right";

            // calc XTK
            double xtkNm = Math.Abs(currentXtkNmLeft);

            // log
            if (xtkNm < 0.5)
            {
                int xtkFt = (int)Math.Round(xtkNm * NM_TO_FT);
                if (xtkFt == 0)
                {
                    side = "";
                }
                Console.WriteLine($"XTK: {xtkFt} ft {side}");
            }
            else
            {
                xtkNm = RoundTo(xtkNm, 0.01);
                Console.WriteLine($"XTK: {xtkNm} NM {side}");
            }
        }

        internal static double AddNoise(double percent)
        {
            Random random = new Random();
            double noise = random.NextDouble() - 0.5;
            return percent + noise;
        }

        /// <summary>
        /// 
        /// given:
        ///     radial from VOR to ownship
        ///     radial from VOR to destiation
        ///     
        /// calc:
        ///     vector from ownship to destination
        /// 
        /// </summary>
        /// <returns></returns>
        public static Vector2D CalcVectorToVorOffsetPoint(Vector2D vor_to_ownship, Vector2D vor_to_destination)
        {
            Vector2D ownship_to_destination = vor_to_destination - vor_to_ownship;
            return ownship_to_destination;
        }

        public static string FormatHoursAndMinutes(double raw_minutes)
        {
            int hours = (int)(raw_minutes / 60);
            int minutes = (int)(raw_minutes % 60);

            string result = "";
            if (hours > 0)
            {
                result += $"{hours} hour";
                if (hours > 1)
                {
                    result += "s";
                }
                result += " ";
            }

            result += $"{minutes} minute";
            if (minutes != 1)
            {
                result += "s";
            }
            return result;
        }

        public static string NegateString(string number)
        {
            if (number.StartsWith("-"))
            {
                return number.Substring(1);
            }
            else
            {
                return "-" + number;
            }
        }

        public static bool StartsWithDigit(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return false;
            }
            return Char.IsDigit(str[0]);
        }

        public static string FormatList<T>(List<T> items)
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.Append("[");
            for (int i = 0; i < items.Count; i++)
            {
                if (i > 0)
                {
                    stringBuilder.Append(", ");
                }
                stringBuilder.Append(items[i].ToString());
            }
            stringBuilder.Append("]");
            return stringBuilder.ToString();
        }

        public static void AdjustRangeToFitX(
            ref double min, ref double max, double x, double edgeBufferPct)
        {
            // handle case of inverted axis
            if (max < min)
            {
                AdjustRangeToFitX(ref max, ref min, x, edgeBufferPct);
                return;
            }

            double range = max - min;
            double edgeBuffer = edgeBufferPct * range;

            // calc adjusted min/max
            double adjustedMin = x - edgeBuffer;
            double adjustedMax = x + edgeBuffer;

            if (adjustedMin < min)
            {
                // need to move down
                min = adjustedMin;
                max = min + range;
            }
            else if (adjustedMax > max)
            {
                // need to move up
                max = adjustedMax;
                min = max - range;
            }
        }

        public static bool HasCrossedUnder(double prevX, double currentX, double threshold, double buffer)
        {
            if (prevX < threshold)
            {
                return false;  // prev was too low
            }
            else if (prevX > (threshold + buffer))
            {
                return false;  // prev was too high
            }
            else if (currentX > threshold)
            {
                return false;  // current is too high
            }
            else if (currentX < (threshold - buffer))
            {
                return false;  // current is too low
            }
            else
            {
                return true;   // just crossed threshold
            }
        }
    }
}
