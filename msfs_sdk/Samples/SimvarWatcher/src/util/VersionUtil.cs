﻿using System;

namespace FsAutopilot.Util
{
    public class VersionUtil
    {
        public static readonly string VERSION_2020 = "2020";
        public static readonly string VERSION_2024 = "2024";
        public static readonly string VERSION_AUTO = "auto";

        private static string currentVersion = VERSION_AUTO;

        public static void Set2020()
        {
            currentVersion = VERSION_2020;
        }

        public static void Set2024()
        {
            currentVersion = VERSION_2024;
        }

        public static string GetVersion()
        {
            if (currentVersion == VERSION_AUTO)
            {
                if (IsFlightSim2024Running())
                {
                    return VERSION_2024;
                }
                else
                {
                    return VERSION_2020;
                }
            }
            return currentVersion;
        }

        public static bool IsFlightSim2024Running()
        {
            return ApUtil.IsProcessRunning("FlightSimulator2024");
        }
    }
}
