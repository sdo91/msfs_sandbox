﻿using System;
using System.Windows;

namespace FsAutopilot.Util
{

    public class Timestamp
    {
        DateTime stamp;

        private Timestamp(DateTime stamp)
        {
            this.stamp = stamp;
        }

        public static Timestamp Zero()
        {
            return new Timestamp(DateTime.MinValue);
        }

        public static Timestamp Now()
        {
            return new Timestamp(DateTime.Now);
        }

        public static Timestamp FutureMinutes(double minutes)
        {
            var t = DateTime.Now.AddMinutes(minutes);
            return new Timestamp(t);
        }

        public void SetToNow()
        {
            stamp = DateTime.Now;
        }

        public void SetToZero()
        {
            stamp = DateTime.MinValue;
        }

        public bool IsWithinSec(double seconds)
        {
            double diff = Math.Abs(GetAgeSeconds());
            return diff < seconds;
        }

        public double GetMinutesToPause()
        {
            double result = (stamp - DateTime.Now).TotalMinutes;
            result = ApUtil.RoundTo(result, 0.1);
            return result;
        }

        public bool IsTimedOut(double timeoutSec)
        {
            return GetAgeSeconds() > timeoutSec;
        }

        public bool IsRecent(double timeoutSec)
        {
            return !IsTimedOut(timeoutSec);
        }

        public double GetAgeSeconds()
        {
            return (DateTime.Now - stamp).TotalSeconds;
        }

        public int GetAgeMillis()
        {
            return (int)(GetAgeSeconds() * 1000);
        }

        public bool IsZero()
        {
            return stamp == DateTime.MinValue;
        }
    }
}
