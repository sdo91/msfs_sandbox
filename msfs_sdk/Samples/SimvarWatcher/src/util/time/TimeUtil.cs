﻿using System;
using System.Threading;

namespace FsAutopilot.Util
{
    public class TimeUtil
    {
        public static readonly int UPDATE_RATE = 5;
        public static readonly TimeSpan DeltaTime = TimeSpan.FromSeconds(1.0 / UPDATE_RATE);

        public static double CalcElapsedTime(DateTime timestamp)
        {
            return (DateTime.Now - timestamp).TotalSeconds;
        }

        public static bool IsTimedOut(DateTime timestamp, double timeout)
        {
            return CalcElapsedTime(timestamp) > timeout;
        }

        public static bool IsRecent(DateTime timestamp, double timeout)
        {
            return CalcElapsedTime(timestamp) < timeout;
        }

        public static DateTime GetOldTimestamp()
        {
            return new DateTime(2020, 1, 1);
        }

        public static void SleepSec(double sec)
        {
            int millis = (int)(sec * 1000);
            Thread.Sleep(millis);
        }
    }
}
