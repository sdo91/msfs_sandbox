﻿using System;

namespace FsAutopilot.Util
{

    public class StateTracker<T>
    {
        private string name = null;

        private T savedState;
        private bool hasFirstUpdate = false;
        private Timestamp stateChangeTimestamp = Timestamp.Zero();

        public StateTracker()
        {
            // todo?
        }

        public void ConfigureLogging(string name)
        {
            this.name = name;
        }

        public void Update(T state)
        {
            if (!state.Equals(savedState) || !hasFirstUpdate)
            {
                if (name != null)
                {
                    Console.WriteLine($"{name} changed: {savedState} -> {state}");
                }
                savedState = state;
                stateChangeTimestamp.SetToNow();
            }
            hasFirstUpdate = true;
        }

        public double GetSecInState(T state)
        {
            if (state.Equals(savedState))
            {
                return stateChangeTimestamp.GetAgeSeconds();
            }
            else
            {
                return 0;
            }
        }

        public bool IsRecentlyUpdated(double timeout)
        {
            return stateChangeTimestamp.IsRecent(timeout);
        }
    }
}
