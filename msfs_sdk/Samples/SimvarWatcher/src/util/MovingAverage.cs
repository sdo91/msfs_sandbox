﻿namespace FsAutopilot.Util
{
    /// <summary>
    /// 
    /// option 1:
    /// keep 90% old, 10% new
    /// 
    /// option 2:
    /// keep a deque of 10 entries, avg them all
    /// 
    /// </summary>
    public class MovingAverage
    {
        private double alpha;
        private double movingAverage;

        public MovingAverage(double alpha)
        {
            this.alpha = alpha;
            movingAverage = double.PositiveInfinity;
        }

        public double Update(double rawValue)
        {
            if (movingAverage == double.PositiveInfinity || movingAverage == double.NegativeInfinity)
            {
                movingAverage = rawValue;
            }
            else
            {
                double beta = 1 - alpha;
                movingAverage = (beta * movingAverage) + (alpha * rawValue);
            }
            return movingAverage;
        }
    }
}
