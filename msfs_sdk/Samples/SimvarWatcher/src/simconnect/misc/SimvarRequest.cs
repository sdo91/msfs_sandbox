﻿using FsAutopilot.View;

namespace FsAutopilot.Simconnect
{
    public class SimvarRequest : ObservableObject
    {
        public DEFINITION eDef = DEFINITION.Dummy;
        public REQUEST eRequest = REQUEST.Dummy;

        public string sName { get; set; }

        public double dValue
        {
            get { return m_dValue; }
            set { this.SetProperty(ref m_dValue, value); }
        }

        private double m_dValue = 0.0;

        public string sUnits { get; set; }

        public bool bPending = true;

        public bool bStillPending
        {
            get { return m_bStillPending; }
            set { this.SetProperty(ref m_bStillPending, value); }
        }

        private bool m_bStillPending = false;
    };
}