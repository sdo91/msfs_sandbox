﻿using FsAutopilot.AircraftLogic;
using FsAutopilot.Config;
using FsAutopilot.Events;
using FsAutopilot.Util;
using Microsoft.FlightSimulator.SimConnect;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace FsAutopilot.Simconnect
{
    /// <summary>
    /// in sim docs:
    /// Programming Tools -> SimConnect SDK -> API Status -> SimEvents
    ///
    /// online:
    /// http://www.prepar3d.com/SDKv3/LearningCenter/utilities/variables/event_ids.html
    /// 
    /// NOTE:
    /// events added here will be auto-mapped w/ simconnect in initSimEvents()
    /// </summary>
    public enum SIM_EVENT
    {
        ALL_LIGHTS_TOGGLE,
        AP_SPD_VAR_SET,
        BAROMETRIC,
        ELEV_TRIM_UP,
        FUEL_SELECTOR_SET,
        HEADING_BUG_DEC,
        HEADING_BUG_INC,
        HEADING_BUG_SET,
        LANDING_LIGHTS_OFF,
        LANDING_LIGHTS_ON,
        LANDING_LIGHTS_SET,
        LANDING_LIGHTS_TOGGLE,
        MIXTURE_SET_BEST,
        NAV1_RADIO_SET,
        NAV1_RADIO_SWAP,
        NAV2_RADIO_SET,
        NAV2_RADIO_SWAP,
        PANEL_LIGHTS_OFF,
        PANEL_LIGHTS_ON,
        PANEL_LIGHTS_SET,
        PANEL_LIGHTS_TOGGLE,
        PARKING_BRAKES,
        THROTTLE_CUT,
        TOGGLE_GPS_DRIVES_NAV1,
        TOGGLE_RECOGNITION_LIGHTS,
        TOGGLE_TAXI_LIGHTS,
        VOR1_SET,
        VOR2_SET,
    };

    public enum GROUP_ID
    {
        ZERO = 0
    };

    /// <summary>
    ///
    /// todo:
    /// create SimconnectService that is just instantiated once
    /// it should handle creation of SimconnectBridge
    /// 
    /// WIP:
    /// move sim connect logic into this class
    ///
    /// todo:
    /// detect joystick movements:
    ///     see: MapInputEventToClientEvent
    ///
    /// </summary>
    public class SimconnectBridge
    {
        private readonly Dictionary<string, double> Telemetry = new Dictionary<string, double>();
        private string aircraftLivery = null;

        private SimConnect simConnect;
        private Dictionary<string, SimvarRequest> simvarRequestDict;
        private Dictionary<uint, string> requestIdDict;

        private EventService eventService;
        private ConfigService configService;
        private AircraftLogicService aircraftLogicService;

        private double offsetHeadingToTrackDeg = 0;

        private static readonly int DEFAULT_TAKEOFF_ALT = -9999;
        private int takeoffAltitudeFt = DEFAULT_TAKEOFF_ALT;

        private bool is_fuel_flowing = true;
        private readonly StateTracker<double> fuelQuantityTracker = new StateTracker<double>();

        public SimconnectBridge(
            SimConnect simConnect,
            EventService eventService,
            ConfigService configService,
            AircraftLogicService aircraftLogicService)
        {
            Console.WriteLine("Constructing SimconnectBridge");

            if (simConnect == null)
            {
                throw new ApplicationException("should not happen...");
            }
            this.simConnect = simConnect;

            // init EventService
            this.eventService = eventService;
            eventService.AddHandler(EventTopic.DEV_CMD, HandleDevCmd);
            eventService.AddHandler(EventTopic.CHANGE_REQUEST, ChangeRequestHandler);

            // save other services
            this.aircraftLogicService = aircraftLogicService;
            this.configService = configService;

            // init sim events
            initSimEvents();
        }

        internal void Cleanup()
        {
            // todo?
        }

        /// <summary>
        /// see:
        /// http://www.mindstaraviation.com/SimConnectExample_SendEvents.html
        /// example code to receive events:
        /// https://www.prepar3d.com/SDKv5/sdk/simconnect_api/samples/managed_client_events.html
        /// in sim docs:
        /// Programming Tools -> SimConnect SDK -> API Reference -> General -> MapClientEventToSimEvent
        /// </summary>
        private void initSimEvents()
        {
            // map each SIM_EVENT
            foreach (SIM_EVENT eventId in Enum.GetValues(typeof(SIM_EVENT)))
            {
                string event_str = eventId.ToString();
                simConnect.MapClientEventToSimEvent(eventId, event_str);
            }

            // subscribe to specific sim events here
            simConnect.OnRecvEvent += new SimConnect.RecvEventEventHandler(simEventHandler);
            simConnect.AddClientEventToNotificationGroup(GROUP_ID.ZERO, SIM_EVENT.THROTTLE_CUT, false);

            // this is also needed
            simConnect.SetNotificationGroupPriority(GROUP_ID.ZERO, SimConnect.SIMCONNECT_GROUP_PRIORITY_HIGHEST);
        }

        internal bool IsBitSet(string simVar, uint mask)
        {
            uint state = GetUnsigned(simVar);
            uint masked = state & mask;
            return masked != 0;
        }

        private void simEventHandler(SimConnect sender, SIMCONNECT_RECV_EVENT recEvent)
        {
            Enum eventId = (SIM_EVENT)recEvent.uEventID;
            Console.WriteLine($"handle sim event: {eventId}");
            eventService.FireEvent(EventTopic.SIM_EVENT, eventId);

            // todo: handle some events here?
        }

        internal void SetSimvarRequestList(Collection<SimvarRequest> simvarRequestList)
        {
            simvarRequestDict = new Dictionary<string, SimvarRequest>();
            requestIdDict = new Dictionary<uint, string>();
            foreach (SimvarRequest entry in simvarRequestList)
            {
                string key = entry.sName;
                simvarRequestDict[key] = entry;

                uint requestId = (uint)entry.eRequest;
                requestIdDict[requestId] = key;
            }
            Console.WriteLine("SimvarRequest dictionaries built!");
        }

        /// <summary>
        /// send the specified event to the sim
        ///
        /// in sim docs:
        /// Programming Tools -> SimConnect SDK -> API Reference -> General -> MapClientEventToSimEvent
        /// </summary>
        public void fireSimEvent(SIM_EVENT eventId, uint data)
        {
            // todo: check if connected here?

            // not sure what these do, but these numbers seem to work...
            uint objectId = 1;
            Enum groupId = GROUP_ID.ZERO;

            simConnect.TransmitClientEvent(objectId, eventId, data, groupId, SIMCONNECT_EVENT_FLAG.DEFAULT);
            Console.WriteLine($"fired event: {eventId} (data={data}, hex=0x{data.ToString("X")})");
        }

        public void fireSimEvent(SIM_EVENT eventId)
        {
            fireSimEvent(eventId, 0);
        }

        private void HandleDevCmd(Event myEvent)
        {
            var devCmd = (DevCommand)myEvent.data;

            if (devCmd.command == "NAV2")
            {
                SetNav2Frequency(devCmd.GetArgAsDouble(0));
            }
            else if (devCmd.command == "HDG")
            {
                fireSimEvent(SIM_EVENT.HEADING_BUG_SET, devCmd.GetArgAsUInt(0));
            }
            else if (devCmd.command == "HI")
            {
                fireSimEvent(SIM_EVENT.HEADING_BUG_INC);
            }
            else if (devCmd.command == "HD")
            {
                fireSimEvent(SIM_EVENT.HEADING_BUG_DEC);
            }
            else if (devCmd.command == "SPEED")
            {
                SetTargetSpeed(devCmd.GetArgAsUInt(0));
            }
            else if (devCmd.command == "BRAKE")
            {
                SetParkingBrake(devCmd.GetArgAsBool(0));
            }
            else if (devCmd.command == "CDI")
            {
                fireSimEvent(SIM_EVENT.TOGGLE_GPS_DRIVES_NAV1);
            }
            else if (devCmd.command == "MIX")
            {
                fireSimEvent(SIM_EVENT.MIXTURE_SET_BEST);
            }
        }

        private void ChangeRequestHandler(Event myEvent)
        {
            var request = (ChangeRequestEvent)myEvent.data;

            switch (request.key)
            {
                case ChangeRequestEvent.TargetKey.SET_TARGET_ALTITUDE:
                    {
                        int desired = Int32.Parse(request.value);
                        SetTargetAltitude(desired);
                        break;
                    }
                case ChangeRequestEvent.TargetKey.SET_HEADING_BUG:
                    {
                        uint desired = UInt32.Parse(request.value);
                        SetHeadingBug(desired);
                        break;
                    }
                case ChangeRequestEvent.TargetKey.SET_TARGET_AIRSPEED:
                    {
                        uint desired = UInt32.Parse(request.value);
                        SetTargetSpeed(desired);
                        break;
                    }
                case ChangeRequestEvent.TargetKey.ADJUST_TARGET_ALTITUDE:
                    {
                        int delta = Int32.Parse(request.value);
                        int oldValue = GetAltitudeBug();
                        int newValue = oldValue + delta;
                        SetTargetAltitude(newValue);
                        break;
                    }
                case ChangeRequestEvent.TargetKey.ADJUST_HEADING_BUG:
                    {
                        int delta = Int32.Parse(request.value);
                        bool roundToDelta = (delta <= 90);
                        AdjustHeadingBug(delta, roundToDelta);
                        break;
                    }
                case ChangeRequestEvent.TargetKey.ADJUST_TARGET_AIRSPEED:
                    {
                        double delta = Double.Parse(request.value);
                        double oldValue = GetTargetSpeedIfAvailable();
                        double newValue = ApUtil.RoundTo(oldValue + delta, delta);
                        if (newValue <= 0)
                        {
                            Console.WriteLine($"invalid target airspeed: {newValue}");
                        }
                        else
                        {
                            SetTargetSpeed((uint)newValue);
                        }
                        break;
                    }
                default:
                    break;
            }

        }

        internal void AdjustHeadingBug(double delta, bool roundToDelta)
        {
            double oldValue = GetHeadingBug();
            double newValue = oldValue + delta;
            if (roundToDelta)
            {
                newValue = ApUtil.RoundTo(newValue, delta);
            }
            newValue = ApUtil.WrapToMatch(newValue, 180);
            SetHeadingBug(newValue);
        }

        public bool IsSimPaused { get; private set; } = false;

        internal SimvarRequest GetSimvarRequest(uint requestId)
        {
            string key = requestIdDict[requestId];
            return simvarRequestDict[key];
        }

        internal void SetTitle(string value)
        {
            aircraftLivery = value;
            Console.WriteLine($"[SimconnectBridge] SetTitle: {aircraftLivery}");

            CheckForVarsReady();
        }

        /// <summary>
        /// NOTE: these don't work to detect pause (they come to rest when landed):
        ///     AIRSPEED_INDICATED
        ///     INDICATED_ALTITUDE
        ///     VERTICAL_SPEED
        ///     AIRSPEED_MACH
        ///     AMBIENT_WIND_*
        /// </summary>
        internal void HandleDataFromSim(string key, double value)
        {
            if (AreVarsReady)
            {
                CheckBeforeUpdate(key, value);
            }

            Telemetry[key] = value;

            CheckForVarsReady();
        }

        /// <summary>
        /// 
        /// handle some special cases here
        /// 
        /// </summary>
        private void CheckBeforeUpdate(string key, double value)
        {
            if (!AreVarsReady)
            {
                return;
            }

            if (key == $"{SimVars.GENERAL_ENG_RPM}:1")
            {
                // check if sim is paused
                IsSimPaused = (GetValue(key) == value);
            }

            if (key == $"{SimVars.ENG_FUEL_FLOW_PPH_1}")
            {
                if (!IsSimPaused)
                {
                    is_fuel_flowing = (GetValue(key) != value);
                }
            }

            if (key == SimVars.FUEL_TOTAL_QUANTITY_WEIGHT)
            {
                fuelQuantityTracker.Update(value);
            }

            if (key == SimVars.ELEVATOR_TRIM_PCT)
            {
                UpdateElevatorLimits(value);
            }

            // update takeoff altitude
            if (key == SimVars.INDICATED_ALTITUDE)
            {
                int newTakeoffAltitude = takeoffAltitudeFt;
                if (GetGroundSpeed() < 1)
                {
                    newTakeoffAltitude = (int)value;
                }
                else if (takeoffAltitudeFt == DEFAULT_TAKEOFF_ALT)
                {
                    newTakeoffAltitude = (int)GetGroundAltitude();
                }

                if (newTakeoffAltitude != takeoffAltitudeFt)
                {
                    takeoffAltitudeFt = newTakeoffAltitude;
                    Console.WriteLine($"takeoffAltitudeFt: {takeoffAltitudeFt}");
                }
            }

            if (key == SimVars.GPS_GROUND_MAGNETIC_TRACK)
            {
                UpdateTrackOffset(value);
            }

            if (key == SimVars.SIM_ON_GROUND)
            {
                if (GetValue(key) != value)  // value changed
                {
                    if (value == 0)
                    {
                        Console.WriteLine("SIM_ON_GROUND: airborne");
                    }
                    else
                    {
                        Console.WriteLine("SIM_ON_GROUND: landed");
                        Telemetry[key] = value;  // update early for InitTargetAirspeed
                        InitTargetAirspeed();
                    }
                }
            }
        }

        private void UpdateElevatorLimits(double elevatorTrimPercent)
        {
            if (elevatorTrimPercent == 0)
            {
                return;  // can't divide by 0
            }

            if (!configService.IsAircraftLoaded())
            {
                return;  // wait for aircraft config to load
            }
            double currentSavedValue = configService.GetAircraftCfg().simData.maxElevatorTrimDegrees;

            double prevElevatorPct = GetValue(SimVars.ELEVATOR_TRIM_PCT);
            if (elevatorTrimPercent == prevElevatorPct && currentSavedValue != 0)
            {
                return;  // no need to recalculate value
            }

            // todo: prevent this logic from running all the time when AP is active...
            // eg: only run while on the ground

            // calc max elevator trim (degrees)
            double elevatorDeg = GetValue(SimVars.ELEVATOR_TRIM_POSITION);
            // current trim / max trim = pct
            double maxElevatorTrimDeg = elevatorDeg / (elevatorTrimPercent / 100.0);
            //Console.WriteLine($"maxElevatorTrim: {maxElevatorTrimDeg} (current={elevatorDeg} deg, {elevatorTrimPercent} %)");

            // update value in config
            double diff = Math.Abs(currentSavedValue - maxElevatorTrimDeg);
            if (diff > 0.01)
            {
                Console.WriteLine($"updating maxElevatorTrimDegrees: {currentSavedValue} -> {maxElevatorTrimDeg}");
                configService.GetAircraftCfg().simData.maxElevatorTrimDegrees = maxElevatorTrimDeg;
                configService.SaveConfig();
            }
        }


        /// <summary>
        /// NOTE: for a while there was an issue where the track from the sim was only updating at 1hz
        /// update: this issue appears to be fixed now
        /// update 2: it's faster than 1hz, but still slow in some aircraft
        /// </summary>
        private void UpdateTrackOffset(double currentTrackDeg)
        {
            if (GetGroundSpeed() < 1)
            {
                return; // too slow
            }

            if (currentTrackDeg != GetValue(SimVars.GPS_GROUND_MAGNETIC_TRACK))
            {
                // track updated!
                // hdg + offset = track -> offset = track - hdg
                offsetHeadingToTrackDeg = currentTrackDeg - GetCurrentHeading();
            }
            //Console.WriteLine($"offsetHeadingToTrackDeg: {offsetHeadingToTrackDeg}");
        }

        public bool AreVarsReady { get; private set; } = false;

        public bool CheckForVarsReady()
        {
            // check if all vars are now ready
            if (AreVarsReady)
            {
                return false;
            }
            if (Telemetry.Count != ApSimvarsList.getNumVars())
            {
                return false;
            }
            if (aircraftLivery == null)
            {
                return false;
            }

            HandleAllVarsReady();
            return true;
        }

        /// <summary>
        /// note: this gets called before HandleSimconnectString
        /// </summary>
        private void HandleAllVarsReady()
        {
            Console.WriteLine($"HandleAllVarsReady: all {Telemetry.Count} vars are ready!");
            Console.WriteLine($"aircraftLivery={aircraftLivery}");

            // init vars here (NOTE: aircraft config might not have loaded yet)
            InitTargetAltitude();

            // this is to allow us to calculate max elevator trim (degrees)
            if (GetValue(SimVars.ELEVATOR_TRIM_PCT) == 0)
            {
                fireSimEvent(SIM_EVENT.ELEV_TRIM_UP);
            }

            AreVarsReady = true;
            eventService.FireEvent(EventTopic.SIMVARS_READY);
        }

        public double CalcDistToLanding(bool verbose)
        {
            // use a large default value, and assume we'll get a valid distance enroute
            double DEFAULT_RESULT = 9999;

            // use GPS distance if available
            double gpsDistToLandingNm = GetGpsDistanceToLanding();
            if (gpsDistToLandingNm > 0.1)
            {
                ApUtil.ConsoleLog($"gpsDistToLandingNm: {gpsDistToLandingNm:F2}", verbose);
                return gpsDistToLandingNm;
            }

            // otherwise try DME
            double dmeNm = GetDmeDistanceNm(1);
            if (IsToVor(1))
            {
                // this is to prevent auto cruise from exiting shortly after takeoff
                double minVorDistance = configService.GetAutoCruiseCfg().approachDistanceNm
                    + configService.GetFlightPlanConfig().autoCruiseDescentBufferNm - 1;
                if (dmeNm < minVorDistance)
                {
                    ApUtil.ConsoleLog($"distToFinalNm: {DEFAULT_RESULT} (below minVorDistance)", verbose);
                    return DEFAULT_RESULT;
                }

                // VOR nav
                double distToFinalNm = ApUtil.RoundTo(dmeNm, 0.1);
                ApUtil.ConsoleLog($"distToFinalNm: {distToFinalNm} (DME)", verbose);
                return distToFinalNm;
            }

            // no GPS or VOR yet
            ApUtil.ConsoleLog($"distToFinalNm: {DEFAULT_RESULT} (no distance yet)", verbose);
            return DEFAULT_RESULT;
        }

        private void InitTargetAltitude()
        {
            //var airspeed = GetValue(SimVars.AIRSPEED_INDICATED);
            //if (airspeed > 10)
            //{
            //    return;  // don't set if moving
            //}

            var currentAltBug = GetAltitudeBug();

            if (currentAltBug == 99900)
            {
                // V35B Bonanza starts at 99900 for some reason...
                currentAltBug = 0;
            }

            if (currentAltBug > 0)
            {
                return;  // already set
            }

            // set to a bit above ground
            var groundAlt = GetGroundAltitude();
            int desiredAlt = (int)ApUtil.RoundUp(groundAlt + 1000, 1000);
            SetTargetAltitude(desiredAlt);
        }

        public void InitTargetAirspeed()
        {
            Console.WriteLine("InitTargetAirspeed");

            if (!AreVarsReady)
            {
                Console.WriteLine("vars not ready!");
            }

            if (IsOnGround() || true)
            {
                int desiredSpeed = configService.GetMiscCfg().defaultTargetSpeed;
                if (desiredSpeed > 0)
                {
                    SetTargetSpeed((uint)desiredSpeed);
                }
            }
        }

        public void SetValue(string key, double value)
        {
            if (!simvarRequestDict.ContainsKey(key))
            {
                throw new ApplicationException($"missing key: {key}");
            }

            SimvarRequest simVar = simvarRequestDict[key];
            simConnect.SetDataOnSimObject(simVar.eDef, SimConnect.SIMCONNECT_OBJECT_ID_USER, SIMCONNECT_DATA_SET_FLAG.DEFAULT, value);

            // also update local value
            Telemetry[key] = value;
        }

        private void SetBool(string key, bool value)
        {
            SetValue(key, value ? 1 : 0);
        }

        public Dictionary<string, double> GetTelemDict()
        {
            return Telemetry;
        }

        // todo: make private
        internal double GetValue(string key)
        {
            try
            {
                return Telemetry[key];
            }
            catch
            {
                Console.WriteLine($"GetValue failed: {key}");
                throw new ApplicationException();
            }
        }

        public double GetValueWithIndex(string key, int index)
        {
            return GetValue($"{key}:{index}");
        }

        public bool GetBool(string key)
        {
            return GetValue(key) != 0;
        }

        public bool GetBoolWithIndex(string key, int index)
        {
            return GetValueWithIndex(key, index) != 0;
        }

        public int GetInteger(string key)
        {
            return (int)Math.Round(GetValue(key));
        }

        public uint GetUnsigned(string key)
        {

            int value = GetInteger(key);
            if (value < 0)
            {
                Console.WriteLine($"GetUnsigned failed: {key}, {value}");
                throw new ApplicationException();
            }
            return (uint)value;
        }

        public double GetCurrentAltitude()
        {
            double result = GetValue(SimVars.INDICATED_ALTITUDE);
            if (result > 60000)
            {
                result = GetValue(SimVars.PLANE_ALTITUDE);
            }
            return result;
        }

        public int GetAltitudeBug()
        {
            if (configService.GetAircraftCfg().name == "ANTONOV_AIRLINES")
            {
                // hack: knob to adjust altitude in the Antonov is really slow
                return GetInteger(SimVars.AUTOPILOT_ALTITUDE_LOCK_VAR) * 10;
            }
            return GetInteger(SimVars.AUTOPILOT_ALTITUDE_LOCK_VAR);
        }

        public void SetTargetAltitude(int altitude)
        {
            SetValue(SimVars.AUTOPILOT_ALTITUDE_LOCK_VAR, altitude);
        }

        /// <summary>
        /// 
        /// get VOR/ILS CRS for NAV1 or NAV2
        /// 
        /// todo:
        ///     return double for ILS?
        /// 
        /// </summary>
        public double GetNavCourse(int navNumber)
        {
            bool hasLocalizer = GetBool($"{SimVars.NAV_HAS_LOCALIZER}:{navNumber}");
            if (hasLocalizer)
            {
                // If we have the LOC, use that
                double localizer = GetValueWithIndex(SimVars.NAV_LOCALIZER, navNumber);
                localizer = ApUtil.RoundTo(localizer, 0.1);
                return localizer;
            }
            else
            {
                // otherwise, use CRS
                return GetValueWithIndex(SimVars.NAV_OBS, navNumber);
            }
        }

        public double GetNavRadial(int navNumber)
        {
            return GetValueWithIndex(SimVars.NAV_RADIAL, navNumber);
        }

        /// <summary>
        ///
        /// NOTE:
        /// on the inbound radial, this will approach 0 degrees (and can be exactly 0)
        /// on the outbound radial, this will approach +/-180 degrees
        ///
        /// </summary>
        public double GetNavRadialError(int index)
        {
            return GetValueWithIndex(SimVars.NAV_RADIAL_ERROR, index);
        }

        public bool HasLocalizer(int navIndex)
        {
            return GetBoolWithIndex(SimVars.NAV_HAS_LOCALIZER, navIndex);
        }

        public bool HasDme(int navIndex)
        {
            // sometimes distance check is necessary, just after locking on
            return GetBoolWithIndex(SimVars.NAV_HAS_DME, navIndex) && GetDmeDistanceNm(navIndex) > 0.01;
        }

        public bool IsConnectedVor(int navIndex)
        {
            return IsNavConnected(navIndex) && !HasLocalizer(navIndex);
        }

        public double GetIndicatedAirspeed()
        {
            return GetValue(SimVars.AIRSPEED_INDICATED);
        }

        public double GetTrueAirspeed()
        {
            return GetValue(SimVars.AIRSPEED_TRUE);
        }

        public double GetMachAirspeed()
        {
            return GetValue(SimVars.AIRSPEED_MACH);
        }

        public int GetCruiseSpeedIndicated()
        {
            // todo: clean this up?
            int design_cruise = GetInteger(SimVars.DESIGN_SPEED_VC);
            int estimated_cruise = GetInteger(SimVars.ESTIMATED_CRUISE_SPEED);
            return Math.Min(estimated_cruise, design_cruise);
        }

        public int GetCruiseSpeedTrue()
        {
            // todo: clean this up?
            int design_cruise = GetInteger(SimVars.DESIGN_SPEED_VC);
            int estimated_cruise = GetInteger(SimVars.ESTIMATED_CRUISE_SPEED);
            return Math.Max(estimated_cruise, design_cruise);
        }

        public double GetCruiseSpeedMach()
        {
            double cruise_speed_mach = GetCruiseSpeedTrue() * ApUtil.KNOTS_TO_MACH;
            return ApUtil.RoundTo(cruise_speed_mach, 0.001);
        }

        internal bool IsHypersonicAircraft()
        {
            return GetCruiseSpeedMach() > 2;
        }

        /// <summary>
        ///
        /// WARNING:
        ///     this value is not available for some aircraft (eg: C152)
        ///     you may want: AirspeedController.TargetAirspeed
        ///
        /// NOTE: 
        /// purchased ATR doesn't let you adjust target airspeed 
        /// (until you touch the knob in the cockpit)
        ///
        /// </summary>
        public double GetTargetSpeedIfAvailable()
        {
            if (configService.GetAppCfg().ignoreSimTargetAirspeed)
            {
                return 0;
            }
            return GetValue(SimVars.AUTOPILOT_AIRSPEED_HOLD_VAR);
        }

        public void SetTargetSpeed(uint knots)
        {
            Console.WriteLine($"SetTargetSpeed: {knots}");
            fireSimEvent(SIM_EVENT.AP_SPD_VAR_SET, knots);
        }

        public double GetVarRoll()
        {
            return GetValue(SimVars.PLANE_BANK_DEGREES);
        }

        public double GetVarAileronPosition()
        {
            return GetValue(SimVars.AILERON_POSITION);
        }

        public double GetGroundSpeed()
        {
            return GetValue(SimVars.GROUND_VELOCITY);
        }

        public double ToKnotsIndicatedAirspeed(double airspeedTrueOrMach)
        {
            if (airspeedTrueOrMach > 10)
            {
                // true airspeed
                double airspeedTrue = airspeedTrueOrMach;
                double ratio = GetIndicatedAirspeed() / GetTrueAirspeed();
                double result = ratio * airspeedTrue;
                result = ApUtil.RoundTo(result, 0.1);
                Console.WriteLine($"{ratio} * {airspeedTrue} KTAS = {result} KIAS");
                return result;
            }
            else
            {
                // mach number
                double machNumber = airspeedTrueOrMach;
                double speedOfSoundKnots = GetIndicatedAirspeed() / GetMachAirspeed();
                double result = machNumber * speedOfSoundKnots;
                Console.WriteLine("{0:f3} M * {1:f1} speedOfSoundKnots = {2:f1} KIAS", machNumber, speedOfSoundKnots, result);
                return result;
            }
        }

        public double GetVarAirspeedBarberPole()
        {
            return GetValue(SimVars.AIRSPEED_BARBER_POLE);
        }

        public double GetDmeDistanceNm(int index)
        {
            // NOTE: some VOR/ILS don't have DME
            double distanceNm = GetValueWithIndex(SimVars.NAV_DME, index);
            return distanceNm;
        }

        public double GetBestIlsDist(int index)
        {
            // NOTE: some ILS don't have DME
            return ApUtil.GetBestIlsDist(GetTelemDict(), index);
        }

        public bool HasNavSignal(int index)
        {
            double value = GetValueWithIndex(SimVars.NAV_SIGNAL, index);
            return value > 0;
        }

        /// <summary>
        /// True if the nav at the given index is connected (green navigation line).
        /// Can be a VOR or ILS/localizer.
        /// Returns false if only DME is available.
        /// </summary>
        public bool IsNavConnected(int index)
        {
            return GetNavRadial(index) != 0 || GetNavRadialError(index) != 0;
        }

        /// <summary>
        /// 
        /// Nav TO/FROM flag:
        /// 0 = Off (disconnected)
        /// 1 = TO
        /// 2 = FROM
        /// 
        /// from: http://www.prepar3d.com/SDKv3/LearningCenter/utilities/variables/simulation_variables.html
        /// 
        /// </summary>
        public bool IsFromVor(int index)
        {
            double value = GetValueWithIndex(SimVars.NAV_TOFROM, index);
            return value == 2;
        }

        public bool IsToVor(int index)
        {
            double value = GetValueWithIndex(SimVars.NAV_TOFROM, index);
            return value == 1;
        }

        public void SetNavRadial(int index, uint degrees)
        {
            if (index == 1)
            {
                fireSimEvent(SIM_EVENT.VOR1_SET, degrees);
            }
            else if (index == 2)
            {
                fireSimEvent(SIM_EVENT.VOR2_SET, degrees);
            }
        }

        public int GetRotationSpeed()
        {
            if (configService.GetSpeeds().rotate > 0)
            {
                return configService.GetSpeeds().rotate;
            }

            int vRotate = GetInteger(SimVars.DESIGN_SPEED_MIN_ROTATION);
            int vTakeoff = GetInteger(SimVars.DESIGN_TAKEOFF_SPEED);
            if (vRotate < 5)
            {
                return vTakeoff;
            }
            else
            {
                return Math.Min(vRotate, vTakeoff);
            }
        }

        public double GetGpsXtkLeft()
        {
            return GetValue(SimVars.GPS_WP_CROSS_TRK);
        }

        public double GetGpsDesiredTrackDeg()
        {
            return GetValue(SimVars.GPS_WP_DESIRED_TRACK);
        }

        public double GetGpsHeadingToSteer()
        {
            return GetValue(SimVars.GPS_COURSE_TO_STEER);
        }

        public double GetGpsTrackToSteer()
        {
            double heading = GetGpsHeadingToSteer();
            if (heading == 0)
            {
                return 0;
            }
            double track = heading + offsetHeadingToTrackDeg;
            return ApUtil.WrapToMatch(track, 180);
        }

        public uint GetCruiseAltitude()
        {
            return GetUnsigned(SimVars.DESIGN_CRUISE_ALT);
        }

        public double GetAltitudeAgl()
        {
            return GetValue(SimVars.PLANE_ALT_ABOVE_GROUND);
        }

        public double GetAltitudeAboveTakeoff()
        {
            if (GetCurrentAltitude() < takeoffAltitudeFt)
            {
                // this can happen in MSFS 2024 after a crash
                takeoffAltitudeFt = (int)GetCurrentAltitude();
            }
            double result = GetCurrentAltitude() - takeoffAltitudeFt;
            if (result < 1000)
            {
                Console.WriteLine($"takeoffAltitudeFt={takeoffAltitudeFt}, aboveTakeoff={result}");
            }
            return result;
        }

        public double GetGroundAltitude()
        {
            // NOTE: GROUND_ALTITUDE is not super reliable
            // I have seen it be off by 100+ ft
            // so I calculate it this way
            double groundAlt = GetCurrentAltitude() - GetAltitudeAgl();
            //Console.WriteLine($"groundAlt: {groundAlt}");
            return groundAlt;
        }

        public double GetHeadingBug()
        {
            return GetValue(SimVars.AUTOPILOT_HEADING_LOCK_DIR);
        }

        public void SetHeadingBug(double degrees)
        {
            // todo: get from config service
            bool canSetHdgBug = true;

            if (canSetHdgBug)
            {
                SetValue(SimVars.AUTOPILOT_HEADING_LOCK_DIR, degrees);
            }
            else
            {
                // only set locally
                Telemetry[SimVars.AUTOPILOT_HEADING_LOCK_DIR] = degrees;
            }
        }

        public void SyncHeadingBug()
        {
            double headingDeg = Math.Round(GetCurrentHeading());
            double currentBug = GetHeadingBug();
            if (headingDeg != currentBug)
            {
                SetHeadingBug(headingDeg);
                Console.WriteLine($"Sync'd HDG to {headingDeg}");
            }
        }

        public double GetCurrentHeading()
        {
            return GetValue(SimVars.PLANE_HEADING_DEGREES_MAGNETIC);
        }

        public double GetCurrentTrack()
        {
            if (configService.GetMiscCfg().calcTrackFromHdg)
            {
                // track needs to be smoothed for some aircraft (eg: antonov 225)
                double track = GetCurrentHeading() + offsetHeadingToTrackDeg;
                return ApUtil.WrapToMatch(track, 180);
            }
            return GetValue(SimVars.GPS_GROUND_MAGNETIC_TRACK);
        }

        public double GetFuelCapacityLbs()
        {
            return GetValue(SimVars.FUEL_TOTAL_CAPACITY) * GetValue(SimVars.FUEL_WEIGHT_PER_GALLON);
        }

        public double GetFuelLbsPerGallon()
        {
            return GetValue(SimVars.FUEL_WEIGHT_PER_GALLON);
        }

        public bool GetParkingBrake()
        {
            return GetBool(SimVars.BRAKE_PARKING_POSITION);
        }

        /// <summary>
        /// NOTE: this doesn't seem to work w/ CRJ
        /// use VJoyFeeder.ToggleParkingBrake();
        /// </summary>
        /// <param name="desiredState"></param>
        public void SetParkingBrake(bool desiredState)
        {
            if (GetParkingBrake() != desiredState)
            {
                uint stateInt = desiredState ? 1u : 0u;
                fireSimEvent(SIM_EVENT.PARKING_BRAKES, stateInt);
            }
        }

        public double GetNavFrequency(int index)
        {
            return GetValueWithIndex(SimVars.NAV_ACTIVE_FREQUENCY, index);
        }

        public bool IsNavFrequency(int index, double desiredFrequency)
        {
            double current = GetNavFrequency(index);
            double diff = Math.Abs(current - desiredFrequency);
            return diff < 0.001;
        }

        public void SetNav1Frequency(double navFreqMhz)
        {
            // save current to standby
            fireSimEvent(SIM_EVENT.NAV1_RADIO_SWAP);

            // set frequency
            uint encodedFrequency = ApUtil.EncodeFreqToHex(navFreqMhz);
            fireSimEvent(SIM_EVENT.NAV1_RADIO_SET, encodedFrequency);
        }

        public void SetNav2Frequency(double navFreqMhz)
        {
            // save current to standby
            fireSimEvent(SIM_EVENT.NAV2_RADIO_SWAP);

            // set frequency
            uint encodedFrequency = ApUtil.EncodeFreqToHex(navFreqMhz);
            fireSimEvent(SIM_EVENT.NAV2_RADIO_SET, encodedFrequency);
        }

        public double EstimateTopOfDescentDistance(double vertSpeed)
        {
            // assume airport at sea level for now
            double altBuffer = 3000;

            double currentAltAboveBuffer = GetCurrentAltitude() - altBuffer;
            double descentTimeMin = currentAltAboveBuffer / vertSpeed;

            double groundSpeed = GetGroundSpeed();

            double descentDistanceNm = groundSpeed * descentTimeMin / 60;

            descentTimeMin = ApUtil.RoundTo(descentTimeMin, 0.1);
            descentDistanceNm = ApUtil.RoundTo(descentDistanceNm, 0.1);
            Console.WriteLine($"EstimateTopOfDescentDistance: {descentTimeMin} min, {descentDistanceNm} NM");

            return descentDistanceNm;
        }

        private double savedDistanceDiffNm = 0;
        public double GetGpsDistanceToLanding()
        {
            // descent smoothing hack
            double timeDiffMinutes = Math.Abs(GetMinutesToLastWaypoint() - GetMinutesToNextWaypoint());
            bool isNextLast = timeDiffMinutes < 0.1;

            if (isNextLast || configService.GetFlightPlanConfig().isLandingAtNextWaypoint)
            {
                return GetValue(SimVars.GPS_WP_DISTANCE); // dist to next GPS WP
            }
            else
            {
                // descent smoothing hack #2
                double timeDiffHours = timeDiffMinutes / 60.0;
                double distanceDiffNm = GetGroundSpeed() * timeDiffHours;
                Console.WriteLine($"distanceDiffNm: {distanceDiffNm:f2}");
                if (Math.Abs(distanceDiffNm - savedDistanceDiffNm) > 1)
                {
                    savedDistanceDiffNm = distanceDiffNm;
                }

                return GetValue(SimVars.GPS_WP_DISTANCE) + savedDistanceDiffNm;

                // old method: calc dist from time (not smooth)
                //double hoursToFinal = GetMinutesToLastWaypoint() / 60.0;
                //double distToFinalNm = GetGroundSpeed() * hoursToFinal; // d = vt
                //return distToFinalNm;
            }
        }

        public double GetGpsMinutesToLanding()
        {
            // NOTE: update rate on these is slow
            if (configService.GetFlightPlanConfig().isLandingAtNextWaypoint)
            {
                return GetMinutesToNextWaypoint(); // time to next WP
            }
            else
            {
                return GetMinutesToLastWaypoint(); // time to last WP
            }
        }

        public double GetMinutesToNextWaypoint()
        {
            // NOTE: this has a slow update rate
            return GetValue(SimVars.GPS_WP_ETE);
        }

        public double GetMinutesToLastWaypoint()
        {
            // todo: improve update rate
            return GetValue(SimVars.GPS_ETE);
        }

        public double GetCurrentVertSpeed()
        {
            return GetValue(SimVars.VERTICAL_SPEED);
        }

        public bool HasGpsFlightPlan()
        {
            // Note: I have seen GPS_WP_DESIRED_TRACK be non-zero when the was no flight plan
            return GetValue(SimVars.GPS_WP_DISTANCE) > 0;
        }

        public static bool IsReady(SimconnectBridge simconnectBridge)
        {
            return simconnectBridge != null && simconnectBridge.AreVarsReady;
        }

        public void SetCdi(string desiredSource)
        {
            if (desiredSource.ToUpper().StartsWith("G"))
            {
                // GPS
                if (!GpsDrivesCdi())
                {
                    fireSimEvent(SIM_EVENT.TOGGLE_GPS_DRIVES_NAV1);
                }
            }
            else
            {
                // VOR
                if (GpsDrivesCdi())
                {
                    fireSimEvent(SIM_EVENT.TOGGLE_GPS_DRIVES_NAV1);
                }
            }
        }

        internal bool GpsDrivesCdi()
        {
            return GetBool(SimVars.GPS_DRIVES_NAV_1);
        }

        internal int GetDesignSpeedClimb()
        {
            return GetInteger(SimVars.DESIGN_SPEED_CLIMB);
        }

        internal int GetDesignSpeedStallClean()
        {
            return GetInteger(SimVars.DESIGN_SPEED_VS_1);
        }

        internal bool AtMainMenu()
        {
            return AreVarsReady && GetGroundAltitude() == 0;
        }

        public bool IsOnGround()
        {
            return GetBool(SimVars.SIM_ON_GROUND);
        }

        internal bool IsOnGroundBelowSpeed(double groundSpeedKnots)
        {
            return IsOnGround() && GetGroundSpeed() < groundSpeedKnots;
        }

        public bool IsEngineLost()
        {
            if (IsOnGround() || IsSimPaused || is_fuel_flowing)
            {
                return false;
            }

            // todo: check if recently paused

            if (fuelQuantityTracker.IsRecentlyUpdated(3))
            {
                // note: in some aircraft (cessna 337), fuel flow freezes at idle throttle...
                // this is a workaround
                return false;
            }

            return GetValueWithIndex(SimVars.PROP_THRUST, 1) < 0; // prop drag
        }

        internal bool IsLanded()
        {
            // todo: make this smarter?
            return GetGroundSpeed() < 1;
        }

        internal double calcBufferDistanceNm(double buffer_seconds)
        {
            double hours = buffer_seconds / 3600.0;
            return GetGroundSpeed() * hours; // d=vt
        }

        internal double parseDoubleValue(string key, SIMCONNECT_RECV_SIMOBJECT_DATA_BYTYPE data)
        {
            double value = (double)data.dwData[0];
            if (ShouldFlipValue(key))
            {
                value *= -1;
            }
            return value;
        }

        private bool ShouldFlipValue(string key)
        {
            if (key == SimVars.PLANE_PITCH_DEGREES ||
                key == SimVars.ROTATION_VELOCITY_BODY_X ||
                key == SimVars.PLANE_BANK_DEGREES)
            {
                return true;
            }
            return false;
        }
    }
}
