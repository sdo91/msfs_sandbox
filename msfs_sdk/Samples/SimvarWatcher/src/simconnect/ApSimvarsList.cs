﻿using System;

namespace FsAutopilot.Simconnect
{
    public class ApSimvarsList
    {
        private ApSimvarsList()
        {
            Console.WriteLine("Don't init this");
        }

        private static readonly string[] desiredSimVars = new string[] {
            // inputs
            $"{SimVars.AUTOPILOT_ALTITUDE_LOCK_VAR},{Units.FEET}",  // target altitude
            $"{SimVars.AUTOPILOT_HEADING_LOCK_DIR},{Units.DEGREES}",  // heading bug

            // only on some aircraft
            $"{SimVars.AUTOPILOT_VERTICAL_HOLD_VAR},{Units.FEET_PER_MINUTE}",  // target VS
            $"{SimVars.AUTOPILOT_AIRSPEED_HOLD_VAR},{Units.KNOTS}",

            // speed
            $"{SimVars.AIRSPEED_INDICATED},{Units.KNOTS}",
            $"{SimVars.AIRSPEED_TRUE},{Units.KNOTS}",
            $"{SimVars.GROUND_VELOCITY},{Units.KNOTS}",
            $"{SimVars.AIRSPEED_MACH},{Units.NUMBER}",

            // altitude
            $"{SimVars.INDICATED_ALTITUDE},{Units.FEET}",
            $"{SimVars.PLANE_ALT_ABOVE_GROUND},{Units.FEET}",
            $"{SimVars.GROUND_ALTITUDE},{Units.FEET}",

            // todo: use this above 60k ft
            $"{SimVars.PLANE_ALTITUDE},{Units.FEET}",

            // V speeds
            $"{SimVars.DESIGN_SPEED_VS_0},{Units.KNOTS}",
            $"{SimVars.DESIGN_SPEED_VS_1},{Units.KNOTS}",
            $"{SimVars.DESIGN_SPEED_MIN_ROTATION},{Units.KNOTS}",
            $"{SimVars.DESIGN_TAKEOFF_SPEED},{Units.KNOTS}",
            $"{SimVars.DESIGN_SPEED_CLIMB},{Units.KNOTS}",
            $"{SimVars.DESIGN_SPEED_VC},{Units.KNOTS}",
            $"{SimVars.ESTIMATED_CRUISE_SPEED},{Units.KNOTS}",
            $"{SimVars.DESIGN_CRUISE_ALT},{Units.FEET}",

            // wind
            $"{SimVars.AMBIENT_WIND_DIRECTION},{Units.DEGREES}",
            $"{SimVars.AMBIENT_WIND_VELOCITY},{Units.KNOTS}",

            // Nav
            $"{SimVars.GPS_COURSE_TO_STEER},{Units.DEGREES}",
            $"{SimVars.GPS_DRIVES_NAV_1},{Units.BOOLEAN}",
            $"{SimVars.GPS_WP_CROSS_TRK},{Units.NAUTICAL_MILES}",
            $"{SimVars.GPS_WP_DESIRED_TRACK},{Units.DEGREES}",  // GPS CRS
            $"{SimVars.GPS_WP_DISTANCE},{Units.NAUTICAL_MILES}",
            $"{SimVars.GPS_WP_ETE},{Units.MINUTES}",  // time to waypoint
            $"{SimVars.GPS_ETE},{Units.MINUTES}",  // time to dest

            $"{SimVars.NAV_ACTIVE_FREQUENCY}:1,{Units.MHZ}",
            $"{SimVars.NAV_ACTIVE_FREQUENCY}:2,{Units.MHZ}",
            $"{SimVars.NAV_DME}:1,{Units.NAUTICAL_MILES}",
            $"{SimVars.NAV_HAS_DME}:1,{Units.BOOLEAN}",
            $"{SimVars.NAV_HAS_LOCALIZER_1},{Units.BOOLEAN}",
            $"{SimVars.NAV_LOCALIZER_1},{Units.DEGREES}",
            $"{SimVars.NAV_OBS_1},{Units.DEGREES}",  // NAV1 CRS
            $"{SimVars.NAV_OBS_2},{Units.DEGREES}",  // NAV2 CRS
            $"{SimVars.NAV_RADIAL}:1,{Units.DEGREES}",
            $"{SimVars.NAV_RADIAL}:2,{Units.DEGREES}",
            $"{SimVars.NAV_RADIAL_ERROR}:1,{Units.DEGREES}",
            $"{SimVars.NAV_RADIAL_ERROR}:2,{Units.DEGREES}",
            $"{SimVars.NAV_SIGNAL}:1,{Units.NUMBER}",
            $"{SimVars.NAV_SIGNAL}:2,{Units.NUMBER}",
            $"{SimVars.NAV_TOFROM}:1,{Units.ENUM}",

            $"{SimVars.NAV_GLIDE_SLOPE_ERROR}:1,{Units.DEGREES}",
            $"{SimVars.NAV_RAW_GLIDE_SLOPE}:1,{Units.DEGREES}",

            // PITCH CONTROL
            $"{SimVars.VERTICAL_SPEED},{Units.FEET_PER_MINUTE}",
            $"{SimVars.PLANE_PITCH_DEGREES},{Units.DEGREES}",
            $"{SimVars.ALIAS_PITCH_RATE},{Units.DEGREES_PER_SECOND}",
            $"{SimVars.ELEVATOR_TRIM_POSITION},{Units.DEGREES}",
            $"{SimVars.ELEVATOR_TRIM_PCT},{Units.PERCENT}",
            $"{SimVars.ELEVATOR_POSITION},{Units.PERCENT}",

            // ROLL CONTROL
            $"{SimVars.PLANE_HEADING_DEGREES_MAGNETIC},{Units.DEGREES}",
            $"{SimVars.GPS_GROUND_MAGNETIC_TRACK},{Units.DEGREES}",
            $"{SimVars.PLANE_BANK_DEGREES},{Units.DEGREES}",
            $"{SimVars.AILERON_TRIM_PCT},{Units.PERCENT}",
            $"{SimVars.AILERON_POSITION},{Units.PERCENT}",

            //// todo: use this for aircraft w/o roll trim?
            //$"{SimVars.RUDDER_POSITION},{Units.DEGREES}",
            //$"{SimVars.RUDDER_TRIM_PCT},{Units.PERCENT}",

            // AUTOTHROTTLE
            $"{SimVars.AIRSPEED_BARBER_POLE},{Units.KNOTS}",
            $"{SimVars.NUMBER_OF_ENGINES},{Units.SCALER}",
            $"{SimVars.GENERAL_ENG_THROTTLE_LEVER_POSITION}:1,{Units.PERCENT}",
            $"{SimVars.GENERAL_ENG_THROTTLE_LEVER_POSITION}:2,{Units.PERCENT}",
            $"{SimVars.GENERAL_ENG_THROTTLE_LEVER_POSITION}:3,{Units.PERCENT}",
            $"{SimVars.GENERAL_ENG_THROTTLE_LEVER_POSITION}:4,{Units.PERCENT}",
            $"{SimVars.GENERAL_ENG_THROTTLE_LEVER_POSITION}:5,{Units.PERCENT}",
            $"{SimVars.GENERAL_ENG_THROTTLE_LEVER_POSITION}:6,{Units.PERCENT}",
            $"{SimVars.GENERAL_ENG_RPM}:1,{Units.RPM}",
            $"{SimVars.GENERAL_ENG_PCT_MAX_RPM}:1,{Units.PERCENT}",
            $"{SimVars.GENERAL_ENG_MAX_REACHED_RPM}:1,{Units.RPM}",
            $"{SimVars.TURB_ENG_MAX_TORQUE_PERCENT}:1,{Units.PERCENT}",

            // Buit-in AP
            $"{SimVars.AUTOPILOT_MASTER},{Units.BOOLEAN}",
            $"{SimVars.AUTOPILOT_FLIGHT_LEVEL_CHANGE},{Units.BOOLEAN}",

            // FUEL
            $"{SimVars.ENG_FUEL_FLOW_PPH_1},{Units.POUNDS_PER_HOUR}",
            $"{SimVars.FUEL_TOTAL_QUANTITY_WEIGHT},{Units.POUNDS}",
            $"{SimVars.FUEL_TOTAL_CAPACITY},{Units.GALLONS}",
            $"{SimVars.FUEL_WEIGHT_PER_GALLON},{Units.POUNDS}",
            $"{SimVars.FUEL_TANK_SELECTOR_1},{Units.ENUM}",
            //$"{SimVars.FUEL_SELECTED_QUANTITY},{Units.GALLONS}",
            $"{SimVars.FUEL_LEFT_QUANTITY},{Units.GALLONS}",
            $"{SimVars.FUEL_RIGHT_QUANTITY},{Units.GALLONS}",
            $"{SimVars.FUEL_TANK_CENTER_QUANTITY},{Units.GALLONS}",
            $"{SimVars.FUEL_TANK_CENTER_2_QUANTITY},{Units.GALLONS}",
            $"{SimVars.FUEL_TANK_CENTER_3_QUANTITY},{Units.GALLONS}",

            // misc
            $"{SimVars.ALIAS_FLAPS_DEG},{Units.DEGREES}",
            $"{SimVars.BRAKE_PARKING_POSITION},{Units.NUMBER}",
            $"{SimVars.GEAR_TOTAL_PCT_EXTENDED},{Units.PERCENT}",
            $"{SimVars.LIGHT_STATES},{Units.NUMBER}",
            $"{SimVars.SIM_ON_GROUND},{Units.BOOLEAN}",

            // sandbox
            $"{SimVars.RUDDER_TRIM},{Units.DEGREES}",
            $"{SimVars.RUDDER_TRIM_PCT},{Units.PERCENT}",

            $"{SimVars.PROP_THRUST}:1,{Units.POUNDS}",

            //$"{SimVars.ALIAS_YAW_RATE},{Units.DEGREES_PER_SECOND}",
            //$"{SimVars.ALIAS_ROLL_RATE},{Units.DEGREES_PER_SECOND}",
        };

        /// <summary>
        /// todo: move to simvar manager
        /// </summary>
        /// <returns></returns>
        public static string[] getInitialVarList()
        {
            return desiredSimVars;
        }

        public static int getNumVars()
        {
            return getInitialVarList().Length;
        }
    }
}
