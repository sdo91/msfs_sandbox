﻿using FsAutopilot.Config;
using FsAutopilot.Simconnect;
using System;

namespace FsAutopilot.AircraftLogic
{
    /// <summary>
    /// 
    /// todo: we'll need to construct a new Logic class whenever an aircraft is connected/disconnected
    /// 
    /// todo: create MicroserviceLifecycleManager (mlm)
    ///     manages creation and distribution of different services
    /// 
    /// </summary>
    public class BaseAirplaneLogic
    {
        protected ConfigService configService = null;
        protected SimconnectBridge simconnectBridge = null;

        public BaseAirplaneLogic()
        {
            Console.WriteLine("[BaseAirplane] constructing");
        }

        public void InjectServices(ConfigService configService, SimconnectBridge simconnectBridge)
        {
            this.configService = configService;
            this.simconnectBridge = simconnectBridge;
        }

        virtual internal double GetClimbSpeedKias()
        {
            // get climb speed
            double vClimbIndicated = configService.GetSpeeds().climbAirspeedKias;
            if (vClimbIndicated < 0)
            {
                // climb speed: auto
                vClimbIndicated = simconnectBridge.GetDesignSpeedClimb();
            }
            return vClimbIndicated;
        }

        virtual internal bool ShouldForceCruiseSettings()
        {
            return false;
        }

    }
}
