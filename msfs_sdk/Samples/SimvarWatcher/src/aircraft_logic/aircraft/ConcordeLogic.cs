﻿using FsAutopilot.Util;
using System;

namespace FsAutopilot.AircraftLogic
{
    /// <summary>
    /// 
    /// climb speeds:
    ///     250 to 11k
    ///     scale up to 400 at 28kish
    /// 
    /// cruise goals:
    ///     set constant throttle: 90%
    ///     cruise at 640 indicated, what ever alt that is?
    ///
    /// descent:
    ///     
    /// 
    /// todo before next flight:
    ///     make super cruise a real thing (instead of just perpetual climb...)
    ///     this will fix issues with start of descent
    /// 
    /// </summary>
    public class ConcordeLogic : BaseAirplaneLogic
    {

        public ConcordeLogic()
        {
            Console.WriteLine("[ConcordeLogic] constructing");
        }

        internal static bool MatchesName(string name)
        {
            return name.Contains("CONCORDE");
        }

        override internal double GetClimbSpeedKias()
        {
            // todo: make const
            int LOW_CLIMB_SPEED = 400;
            int HIGH_CLIMB_SPEED = (int)configService.GetSpeeds().climbAirspeedKias;

            // calculate climb speed
            double currentAltitude = simconnectBridge.GetCurrentAltitude();
            double vClimbKias = ApUtil.Scale(currentAltitude, 11000, 50000, LOW_CLIMB_SPEED, HIGH_CLIMB_SPEED);
            return vClimbKias;
        }

        override internal bool ShouldForceCruiseSettings()
        {
            double currentAltitude = simconnectBridge.GetCurrentAltitude();
            if (currentAltitude < 20000)
            {
                return simconnectBridge.GetIndicatedAirspeed() > 230;
            }
            else
            {
                return simconnectBridge.GetMachAirspeed() > 1.7;
            }
        }
    }
}
