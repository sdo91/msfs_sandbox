﻿using FsAutopilot.Config;
using FsAutopilot.Simconnect;
using System;

namespace FsAutopilot.AircraftLogic
{

    public class AircraftLogicService
    {
        private ConfigService configService = null;
        protected SimconnectBridge simconnectBridge = null;

        BaseAirplaneLogic currentAircraft;

        public AircraftLogicService()
        {
            Console.WriteLine("[AircraftLogicService] constructing");
        }

        internal void SetConfigService(ConfigService configService)
        {
            this.configService = configService;
        }

        internal void SetSimconnectBridge(SimconnectBridge simconnectBridge)
        {
            this.simconnectBridge = simconnectBridge;
        }

        internal void HandleAircraftConnection(string name)
        {
            currentAircraft = AircraftFactory(name);
        }

        private BaseAirplaneLogic AircraftFactory(string name)
        {
            BaseAirplaneLogic result;
            if (ConcordeLogic.MatchesName(name))
            {
                result = new ConcordeLogic();
            }
            else
            {
                result = new BaseAirplaneLogic();
            }

            // simconnectBridge should be valid when we get here
            if (name != ConfigService.DEFAULT_AIRCRAFT_TITLE && simconnectBridge == null)
            {
                throw new NullReferenceException("simconnectBridge");
            }
            result.InjectServices(configService, simconnectBridge);
            return result;
        }

        internal BaseAirplaneLogic GetAircraft()
        {
            return currentAircraft;
        }

    }
}
