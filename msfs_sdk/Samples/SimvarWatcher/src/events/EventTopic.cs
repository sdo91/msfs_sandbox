﻿namespace FsAutopilot.Events
{
    public enum EventTopic
    {
        TEST,
        CHANGE_PITCH_MODE,
        CHANGE_REQUEST,
        CHANGE_ROLL_MODE,
        CONFIG_UPDATE_FROM_UI,  // config was updated via the UI
        DEV_CMD,
        IR_REMOTE_COMMAND,
        JOYSTICK_EVENT,  // a button was pressed
        PID_RESPONSE_DATA,  // data to be plotted
        SIM_EVENT,
        SIMVARS_READY,
        TRIGGER_UI_UPDATE,  // cfg has changed internally and UI needs to be updated
        VJOY_BUTTON_REQUEST,
    }
}
