﻿namespace FsAutopilot.Events
{
    public class JoystickEvent
    {
        public enum Button
        {
            AIRSPEED,
            AUTOPILOT,
            AUTOTHROTTLE,
            ROLL_MODE,
            HIGH_RATES,
        }

        public static readonly int STATE_TOGGLE = -1;
        public static readonly int STATE_OFF = 0;
        public static readonly int STATE_ON = 1;

        public Button button;
        public int state;

        public JoystickEvent(Button button, int state)
        {
            this.button = button;
            this.state = state;
        }

        public bool GetStateAsBool()
        {
            return state != 0;
        }

        internal bool IsStateToggle()
        {
            return state == STATE_TOGGLE;
        }
    }
}