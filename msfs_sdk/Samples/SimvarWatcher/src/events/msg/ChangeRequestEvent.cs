﻿using System;

namespace FsAutopilot.Events
{
    public class ChangeRequestEvent
    {
        public enum TargetKey
        {
            ADJUST_HEADING_BUG,
            ADJUST_TARGET_AIRSPEED,
            ADJUST_TARGET_ALTITUDE,
            SET_HEADING_BUG,
            SET_TARGET_AIRSPEED,
            SET_TARGET_ALTITUDE,
        }

        public TargetKey key;
        public string value;

        public ChangeRequestEvent(TargetKey key, string value)
        {
            Init(key, value);
        }

        private void Init(TargetKey key, string value)
        {
            this.key = key;
            this.value = value;
            Console.WriteLine($"[ChangeRequestEvent] {key}: {value}");
        }
    }
}
