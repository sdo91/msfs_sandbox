﻿using System;

namespace FsAutopilot.Events
{
    public class DevCommand
    {
        public string command;  // ALL CAPS
        public string[] args;  // ALL CAPS

        public DevCommand(string rawCommand)
        {
            rawCommand = rawCommand.ToUpper();
            rawCommand = rawCommand.Replace(" ", "");
            var tokens = rawCommand.Split(':');

            command = tokens[0];

            if (tokens.Length > 1)
            {
                // handle args
                args = tokens[1].Split(',');
            }
            else
            {
                args = new string[0];
            }
        }

        public override string ToString()
        {
            return $"{{DevCommand: cmd={command}, args=[{string.Join(", ", args)}]}}";
        }

        public int GetArgAsInt(int index)
        {
            return Int32.Parse(args[index]);
        }

        public uint GetArgAsUInt(int index)
        {
            return UInt32.Parse(args[index]);
        }

        public bool GetArgAsBool(int index)
        {
            string arg = args[index].ToUpper();
            return arg.StartsWith("T") || arg == "1";
        }

        public double GetArgAsDouble(int index)
        {
            return Double.Parse(args[index]);
        }
    }
}
