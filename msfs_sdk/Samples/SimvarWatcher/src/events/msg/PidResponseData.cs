﻿using FsAutopilot.Autopilot;
using FsAutopilot.Util;
using System;

namespace FsAutopilot.Events
{
    public class ControllerEntry
    {
        private static readonly Random random = new Random();

        public double endGoal = 0;    // BLACK: long-term goal
        public double shortGoal = 0;  // RED: short-term goal
        public double current = 0;    // BLUE: current vehicle state
        public double wildCard = 0;   // GREEN: wild-card (eg: GPS CRS to steer)

        internal void Reset()
        {
            endGoal = 0;
            shortGoal = 0;
            current = 0;
            wildCard = 0;
        }

        internal void FlipSigns()
        {
            endGoal *= -1.0;
            shortGoal *= -1.0;
            current *= -1.0;
            wildCard *= -1.0;
        }

        internal void Randomize(int min, int max)
        {
            endGoal = random.Next(min, max);
            shortGoal = random.Next(min, max);
            current = random.Next(min, max);
        }

        internal void WrapAngles()
        {
            // NOTE: red line is centered
            endGoal = ApUtil.WrapToMatch(endGoal, shortGoal);
            current = ApUtil.WrapToMatch(current, shortGoal);
            if (wildCard != 0)
            {
                wildCard = ApUtil.WrapToMatch(wildCard, shortGoal);
            }
        }

        internal void Log(string name)
        {
            Console.WriteLine($"[ControllerEntry] {name}: current={current}, short={shortGoal}, end={endGoal}");
        }

        internal bool HasValidData()
        {
            return (endGoal != 0 || shortGoal != 0 || current != 0);
        }
    };

    public class PidResponseData
    {
        public ControllerEntry altitude = new ControllerEntry();
        public ControllerEntry glideSlope = new ControllerEntry();
        public ControllerEntry flc = new ControllerEntry();
        public ControllerEntry vertSpeed = new ControllerEntry();
        public ControllerEntry pitch = new ControllerEntry();
        public ControllerEntry pitchRate = new ControllerEntry();

        public ControllerEntry navigation = new ControllerEntry();
        public ControllerEntry heading = new ControllerEntry();
        public ControllerEntry roll = new ControllerEntry();

        public ControllerEntry airspeed = new ControllerEntry();

        internal void NormalizeData()
        {
            roll.FlipSigns();

            heading.FlipSigns();

            heading.WrapAngles();

            //Log(PidControllerNames.NAVIGATION);
        }

        private void Log(string key)
        {
            GetControllerData(key).Log(key);
        }

        internal void Randomize()
        {
            altitude.Randomize(9000, 11000);
            glideSlope.Randomize(0, 100);
            flc.Randomize(100, 150);
            vertSpeed.Randomize(-1000, 1000);
            pitch.Randomize(-5, 5);
            pitchRate.Randomize(-1, 1);

            navigation.Randomize(-10, 10);
            heading.Randomize(0, 360);
            roll.Randomize(-30, 30);

            airspeed.Randomize(100, 150);
        }

        internal ControllerEntry GetControllerData(string key)
        {
            switch (key)
            {
                case PidControllerNames.ALTITUDE:
                    return altitude;
                case PidControllerNames.GLIDE_SLOPE:
                    return glideSlope;
                case PidControllerNames.FLC:
                    return flc;
                case PidControllerNames.VERT_SPEED:
                    return vertSpeed;
                case PidControllerNames.PITCH:
                    return pitch;
                case PidControllerNames.PITCH_RATE:
                    return pitchRate;
                case PidControllerNames.NAVIGATION:
                    return navigation;
                case PidControllerNames.HEADING:
                    return heading;
                case PidControllerNames.ROLL:
                    return roll;
                case PidControllerNames.AIRSPEED:
                    return airspeed;
                default:
                    return null;
            }
        }

        internal void Reset()
        {
            foreach (string name in PidControllerNames.CONTROLLER_NAMES)
            {
                ControllerEntry controllerData = GetControllerData(name);
                if (controllerData != null)
                {
                    controllerData.Reset();
                }
            }
        }
    };
}
