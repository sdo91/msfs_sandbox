﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace FsAutopilot.Events
{
    public class EventService
    {
        public delegate void EventHandler(Event myEvent);

        private static ConcurrentDictionary<EventTopic, List<EventHandler>> handlersByTopic = null;

        private EventService(bool isTest)
        {
            Console.WriteLine("constructing EventService");

            if (!isTest)
            {
                if (handlersByTopic != null)
                {
                    throw new ApplicationException("there should only be 1 EventService");
                }
            }
            handlersByTopic = new ConcurrentDictionary<EventTopic, List<EventHandler>>();
        }

        public static EventService Create(bool isTest)
        {
            return new EventService(isTest);
        }

        public void AddHandler(EventTopic topic, EventHandler handler)
        {
            Console.WriteLine($"Adding event listener: topic={topic}");
            if (!handlersByTopic.ContainsKey(topic))
            {
                handlersByTopic[topic] = new List<EventHandler>();
            }
            handlersByTopic[topic].Add(handler);
        }

        public void FireEvent(EventTopic topic)
        {
            FireEvent(topic, null);
        }

        public void FireEvent(EventTopic topic, object data)
        {
            if (topic != EventTopic.PID_RESPONSE_DATA)
            {
                Console.WriteLine($"Firing event on topic: {topic}");
            }

            if (!handlersByTopic.ContainsKey(topic))
            {
                return;
            }

            foreach (EventHandler handler in handlersByTopic[topic])
            {
                try
                {
                    handler(new Event(topic, data));
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }

        internal void FireChangeRequestEvent(ChangeRequestEvent.TargetKey key, string value)
        {
            var request = new ChangeRequestEvent(key, value);
            FireEvent(EventTopic.CHANGE_REQUEST, request);
        }

        internal void FireChangeRequestEvent(ChangeRequestEvent.TargetKey key, int value)
        {
            FireChangeRequestEvent(key, value.ToString());
        }

        internal void TriggerUiUpdate()
        {
            // cfg has changed internally and UI needs to be updated
            FireEvent(EventTopic.TRIGGER_UI_UPDATE);
        }
    }
}
