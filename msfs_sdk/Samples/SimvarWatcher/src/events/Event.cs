﻿namespace FsAutopilot.Events
{
    public class Event
    {
        public EventTopic topic;
        public object data;

        public Event(EventTopic topic, object data)
        {
            this.topic = topic;
            this.data = data;
        }
    };
}