﻿using FsAutopilot.Config;
using FsAutopilot.Events;
using FsAutopilot.Simconnect;
using FsAutopilot.Util;
using OpenTK.Input;
using System;
using System.Collections.Generic;

namespace FsAutopilot.Hardware
{
    public class FsJoystick
    {
        private class ButtonState
        {
            public bool isPressed;
            public int count;
            public DateTime stamp;

            public override string ToString()
            {
                return $"[ButtonState: {isPressed}, {count}, {stamp}]";
            }
        }

        private readonly int joystickIndex;
        private JoystickCapabilities capabilities;
        private readonly int minButtonNumber;
        private readonly int maxButtonNumber;

        private readonly Dictionary<int, ButtonState> previousButtonStates = new Dictionary<int, ButtonState>();
        private readonly Dictionary<int, ButtonState> currentButtonStates = new Dictionary<int, ButtonState>();

        private readonly DateTime startTimestamp;

        protected ConfigService configService = null;
        protected EventService eventService = null;
        protected SimconnectBridge simconnectBridge = null;
        protected VJoyFeeder vJoyFeeder = null;

        public FsJoystick(int joystickIndex, JoystickCapabilities capabilities, int minButtonNumber, int maxButtonNumber)
        {
            this.joystickIndex = joystickIndex;
            this.capabilities = capabilities;
            this.minButtonNumber = minButtonNumber;
            this.maxButtonNumber = maxButtonNumber;
            if (maxButtonNumber < 0)
            {
                this.maxButtonNumber = capabilities.ButtonCount;
            }

            startTimestamp = DateTime.Now;
        }

        virtual internal void Process()
        {
            Console.WriteLine("todo: override");
        }

        internal void InjectServices(EventService eventService, ConfigService configService, SimconnectBridge simconnectBridge, VJoyFeeder vJoyFeeder)
        {
            this.configService = configService;
            this.eventService = eventService;
            this.simconnectBridge = simconnectBridge;
            this.vJoyFeeder = vJoyFeeder;
        }

        public void Poll(bool verbose)
        {
            JoystickState state = Joystick.GetState(joystickIndex);

            // update buttons
            for (int buttonNumber = minButtonNumber; buttonNumber <= maxButtonNumber; buttonNumber++)
            {
                int buttonIndex = buttonNumber - 1;  // 0 indexed
                ButtonState newState = new ButtonState
                {
                    isPressed = state.IsButtonDown(buttonIndex)
                };

                bool isSameState = false;
                if (currentButtonStates.ContainsKey(buttonNumber))
                {
                    previousButtonStates[buttonNumber] = currentButtonStates[buttonNumber];
                    ButtonState prevState = previousButtonStates[buttonNumber];

                    if (prevState.isPressed == newState.isPressed)
                    {
                        // same state as before: increment
                        isSameState = true;
                        newState.count = Math.Min(prevState.count + 1, 1000000);
                        newState.stamp = prevState.stamp;
                    }
                }

                if (!isSameState)
                {
                    // new/different state: reset
                    newState.count = 1;
                    newState.stamp = DateTime.Now;
                }
                currentButtonStates[buttonNumber] = newState;
            }
            if (verbose)
            {
                LogState();
            }
        }

        private void LogState()
        {
            Console.WriteLine($"FsJoystick: {capabilities} @ {joystickIndex}:");
            foreach (var buttonNumber in currentButtonStates.Keys)
            {
                Console.WriteLine($"\tbutton {buttonNumber}: {currentButtonStates[buttonNumber]}");
            }
        }

        private bool IsValidButton(int buttonNumber)
        {
            bool result = previousButtonStates.ContainsKey(buttonNumber) && currentButtonStates.ContainsKey(buttonNumber);
            if (!result)
            {
                Console.WriteLine($"buttonNumber not found: {buttonNumber}");
            }
            return result;
        }

        public bool JustConnected()
        {
            return TimeUtil.IsRecent(startTimestamp, 2.0);
        }

        public bool WasButtonChanged(int buttonNumber)
        {
            if (!IsValidButton(buttonNumber))
            {
                return false;
            }

            // wait a bit to avoid false events
            if (JustConnected())
            {
                return false;
            }

            return currentButtonStates[buttonNumber].isPressed != previousButtonStates[buttonNumber].isPressed;
        }

        public bool WasJustPressed(int buttonNumber)
        {
            if (!WasButtonChanged(buttonNumber))
            {
                return false;
            }
            return currentButtonStates[buttonNumber].isPressed;
        }

        public bool IsPressed(int buttonNumber)
        {
            if (!IsValidButton(buttonNumber))
            {
                return false;
            }
            return currentButtonStates[buttonNumber].isPressed;
        }

        public int GetState(int buttonNumber)
        {
            return IsPressed(buttonNumber) ? 1 : 0;
        }

        /// <summary>
        /// true if the button was just pressed, or has been held down for a while
        /// </summary>
        public bool JustPressedOrHeld(int buttonNumber)
        {
            if (!IsValidButton(buttonNumber))
            {
                return false;
            }
            ButtonState state = currentButtonStates[buttonNumber];
            if (!state.isPressed)
            {
                return false;  // not currently pressed
            }
            if (state.count == 1)
            {
                return true;  // just pressed
            }
            return TimeUtil.IsTimedOut(state.stamp, 0.5);  // held
        }

        public bool IsHeld(int buttonNumber)
        {
            if (!IsValidButton(buttonNumber))
            {
                return false;
            }
            ButtonState state = currentButtonStates[buttonNumber];
            if (!state.isPressed)
            {
                return false;  // not currently pressed
            }
            return TimeUtil.IsTimedOut(state.stamp, 0.5);  // held
        }

        /// <summary>
        /// NOTE: this WILL trigger on connect
        /// </summary>
        public bool WasJustHeld(int buttonNumber)
        {
            if (!IsValidButton(buttonNumber))
            {
                return false;
            }
            ButtonState state = currentButtonStates[buttonNumber];
            if (!state.isPressed)
            {
                return false;
            }
            return state.count == 5;  // todo: fix magic number
        }

    }
}
