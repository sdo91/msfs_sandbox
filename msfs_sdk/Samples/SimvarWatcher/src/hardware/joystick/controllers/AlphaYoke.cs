﻿using FsAutopilot.Autopilot;
using FsAutopilot.Config;
using FsAutopilot.Events;
using FsAutopilot.Util;
using OpenTK.Input;
using System;

namespace FsAutopilot.Hardware
{

    /// <summary>
    /// 
    /// alpha yoke button numbers:
    /// https://fsxtimes.files.wordpress.com/2020/09/msfs_controlsetting_00.jpg
    /// 
    /// </summary>
    public class AlphaYoke : FsJoystick
    {
        // alpha switches: top row
        private static readonly int ALPHA_YOKE_RATES_BUTTON = 15;
        private static readonly int ALPHA_YOKE_AUTOPILOT_BUTTON = 17;
        private static readonly int AUTOTHROTTLE_BUTTON = 19;

        // alpha switches: bottom row
        // 1st switch: reserved for cabin lights
        private static readonly int ALPHA_YOKE_BRAKE_ON = 23;  // 2nd switch: parking brake
        private static readonly int ALPHA_YOKE_BRAKE_OFF = 24;
        private static readonly int ALPHA_YOKE_AIRSPEED_SRC_KTAS = 25;  // 3rd switch: KIAS/KTAS

        // candidates:
        // glide slope/alt bug toggle
        // FLC/Altitude toggle
        // deadstick/autostart actions?

        public static readonly int ALPHA_YOKE_DEV_FEATURE_ON = 29;

        private static readonly int BUTTON_ROLL_MODE_OFF = 31;
        private static readonly int BUTTON_ROLL_MODE_ROLL = 32;
        private static readonly int BUTTON_ROLL_MODE_HDG = 33;
        private static readonly int BUTTON_ROLL_MODE_GPS = 34;
        private static readonly int BUTTON_ROLL_MODE_VOR = 35;

        private static readonly int MIN_BUTTON = 15;
        private static readonly int MAX_BUTTON = 35;

        private readonly StateTracker<bool> weirdBrakeStateTracker;

        public AlphaYoke(int joystickIndex, JoystickCapabilities capabilities)
            : base(joystickIndex, capabilities, MIN_BUTTON, MAX_BUTTON)
        {
            Console.WriteLine($"  [AlphaYoke] joystickIndex: {joystickIndex}");

            weirdBrakeStateTracker = new StateTracker<bool>();
            weirdBrakeStateTracker.ConfigureLogging("weirdBrakeState");
        }

        /// <summary>
        ///
        /// button map:
        /// https://fsxtimes.files.wordpress.com/2020/09/msfs_controlsetting_00.jpg
        ///
        /// 4 switches, left to right (1 indexed): 13, 15, 17, 19
        ///
        /// </summary>
        internal static bool IsMatch(JoystickCapabilities joystickCapabilities)
        {
            return joystickCapabilities.AxisCount == 2
                && joystickCapabilities.ButtonCount == 35
                && joystickCapabilities.HatCount == 1;
        }

        override internal void Process()
        {

            Poll(false);

            // check switches
            if (WasButtonChanged(ALPHA_YOKE_AUTOPILOT_BUTTON))
            {
                eventService.FireEvent(EventTopic.JOYSTICK_EVENT, new JoystickEvent(
                    JoystickEvent.Button.AUTOPILOT, GetState(ALPHA_YOKE_AUTOPILOT_BUTTON)));
            }
            if (WasButtonChanged(AUTOTHROTTLE_BUTTON))
            {
                eventService.FireEvent(EventTopic.JOYSTICK_EVENT, new JoystickEvent(
                    JoystickEvent.Button.AUTOTHROTTLE, GetState(AUTOTHROTTLE_BUTTON)));
            }
            if (WasButtonChanged(ALPHA_YOKE_RATES_BUTTON))
            {
                eventService.FireEvent(EventTopic.JOYSTICK_EVENT, new JoystickEvent(
                    JoystickEvent.Button.HIGH_RATES, GetState(ALPHA_YOKE_RATES_BUTTON)));
            }
            CheckParkingBrake();
            CheckAirspeedMode();

            // check knob for AP roll mode
            if (WasJustHeld(BUTTON_ROLL_MODE_OFF))
            {
                eventService.FireEvent(EventTopic.CHANGE_ROLL_MODE, RollMode.Off);
            }
            else if (WasJustHeld(BUTTON_ROLL_MODE_ROLL))
            {
                eventService.FireEvent(EventTopic.CHANGE_ROLL_MODE, RollMode.RollHold);
            }
            else if (WasJustHeld(BUTTON_ROLL_MODE_HDG))
            {
                eventService.FireEvent(EventTopic.CHANGE_ROLL_MODE, RollMode.HeadingHold);
            }
            else if (WasJustHeld(BUTTON_ROLL_MODE_GPS))
            {
                eventService.FireEvent(EventTopic.CHANGE_ROLL_MODE, RollMode.Gps);
            }
            else if (WasJustHeld(BUTTON_ROLL_MODE_VOR))
            {
                eventService.FireEvent(EventTopic.CHANGE_ROLL_MODE, RollMode.Vor);
            }
        }

        /// <summary>
        /// use jVoy to get this to work in the CRJ
        /// </summary>
        private void CheckParkingBrake()
        {
            if (!simconnectBridge.AreVarsReady || JustConnected() || simconnectBridge.IsSimPaused)
            {
                return;
            }

            bool isBrakeOn = simconnectBridge.GetParkingBrake();
            if (!isBrakeOn && IsHeld(ALPHA_YOKE_BRAKE_ON))
            {
                Console.WriteLine("Parking brake should be on!");
                weirdBrakeStateTracker.Update(true);
            }
            else if (isBrakeOn && IsHeld(ALPHA_YOKE_BRAKE_OFF))
            {
                Console.WriteLine("Parking brake should be off!");
                weirdBrakeStateTracker.Update(true);
            }
            else
            {
                weirdBrakeStateTracker.Update(false);
            }

            if (weirdBrakeStateTracker.GetSecInState(true) > 1)
            {
                // state has been weird for a while
                vJoyFeeder.ToggleParkingBrake();
                weirdBrakeStateTracker.Update(false);
            }
        }

        private void CheckAirspeedMode()
        {
            if (JustConnected())
            {
                return;
            }

            bool isKtasModeSelected = IsPressed(ALPHA_YOKE_AIRSPEED_SRC_KTAS);
            AppConfig appCfg = configService.GetAppCfg();

            if (isKtasModeSelected && appCfg.speedSource == "I")
            {
                appCfg.speedSource = "T";
                configService.SaveConfig();
                eventService.TriggerUiUpdate();
            }
            else if (!isKtasModeSelected && appCfg.speedSource == "T")
            {
                appCfg.speedSource = "I";
                configService.SaveConfig();
                eventService.TriggerUiUpdate();
            }
        }
    }
}
