﻿using FsAutopilot.Events;
using OpenTK.Input;
using System;

namespace FsAutopilot.Hardware
{
    public class LogitechThrottleQuadrant : FsJoystick
    {
        private static readonly int THROTTLE_AIRSPEED_UP_BUTTON = 1;
        private static readonly int THROTTLE_AIRSPEED_DOWN_BUTTON = 2;

        private static readonly int MIN_BUTTON = 1;
        private static readonly int MAX_BUTTON = 7;

        public LogitechThrottleQuadrant(int joystickIndex, JoystickCapabilities capabilities)
            : base(joystickIndex, capabilities, MIN_BUTTON, MAX_BUTTON)
        {
            Console.WriteLine($"  [LogitechThrottleQuadrant] joystickIndex: {joystickIndex}");
        }

        internal static bool IsMatch(JoystickCapabilities joystickCapabilities)
        {
            return joystickCapabilities.AxisCount == 3
                && joystickCapabilities.ButtonCount == 9
                && joystickCapabilities.HatCount == 0;
        }

        override internal void Process()
        {
            Poll(false);

            // check switches
            if (JustPressedOrHeld(THROTTLE_AIRSPEED_UP_BUTTON))
            {
                eventService.FireEvent(EventTopic.JOYSTICK_EVENT, new JoystickEvent(
                    JoystickEvent.Button.AIRSPEED, 1));
            }
            else if (JustPressedOrHeld(THROTTLE_AIRSPEED_DOWN_BUTTON))
            {
                eventService.FireEvent(EventTopic.JOYSTICK_EVENT, new JoystickEvent(
                    JoystickEvent.Button.AIRSPEED, 0));
            }
        }

    }
}