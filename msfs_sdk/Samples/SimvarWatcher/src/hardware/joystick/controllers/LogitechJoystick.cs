﻿using OpenTK.Input;
using System;

namespace FsAutopilot.Hardware
{
    public class LogitechJoystick : FsJoystick
    {
        //private static readonly int THROTTLE_AIRSPEED_UP_BUTTON = 1;
        //private static readonly int THROTTLE_AIRSPEED_DOWN_BUTTON = 2;

        private static readonly int MIN_BUTTON = 7;
        private static readonly int MAX_BUTTON = 12;

        public LogitechJoystick(int joystickIndex, JoystickCapabilities capabilities)
            : base(joystickIndex, capabilities, MIN_BUTTON, MAX_BUTTON)
        {
            Console.WriteLine($"  [LogitechJoystick] joystickIndex: {joystickIndex}");
        }

        internal static bool IsMatch(JoystickCapabilities joystickCapabilities)
        {
            return joystickCapabilities.AxisCount == 4
                && joystickCapabilities.ButtonCount == 12
                && joystickCapabilities.HatCount == 1;
        }

        override internal void Process()
        {
            Poll(false);

            // todo
        }

    }
}