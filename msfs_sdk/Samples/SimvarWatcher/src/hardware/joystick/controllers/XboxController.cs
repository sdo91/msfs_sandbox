﻿using FsAutopilot.Events;
using OpenTK.Input;
using System;

namespace FsAutopilot.Hardware
{
    public class XboxController : FsJoystick
    {
        private static readonly int XBOX_BUTTON_A = 1;
        private static readonly int XBOX_BUTTON_B = 2;
        private static readonly int XBOX_BUTTON_X = 3;
        private static readonly int XBOX_BUTTON_Y = 4;

        private static readonly int MIN_BUTTON = 1;
        private static readonly int MAX_BUTTON = 4;

        public XboxController(int joystickIndex, JoystickCapabilities capabilities)
            : base(joystickIndex, capabilities, MIN_BUTTON, MAX_BUTTON)
        {
            Console.WriteLine($"  [XboxController] joystickIndex: {joystickIndex}");
        }

        /// <summary>
        /// button map:
        /// https://joytokey.net/en/posts/button-mapping-for-xbox-controller/
        /// </summary>
        internal static bool IsMatch(JoystickCapabilities joystickCapabilities)
        {
            return joystickCapabilities.AxisCount == 6
                && joystickCapabilities.ButtonCount == 14
                && joystickCapabilities.HatCount == 1;
        }

        override internal void Process()
        {
            Poll(false);

            // check switches
            if (WasJustHeld(XBOX_BUTTON_X))
            {
                // toggle AP
                eventService.FireEvent(EventTopic.JOYSTICK_EVENT, new JoystickEvent(
                    JoystickEvent.Button.AUTOPILOT, JoystickEvent.STATE_TOGGLE));
            }
            else if (WasJustPressed(XBOX_BUTTON_B))
            {
                // disable AT
                eventService.FireEvent(EventTopic.JOYSTICK_EVENT, new JoystickEvent(
                    JoystickEvent.Button.AUTOTHROTTLE, JoystickEvent.STATE_OFF));
            }
        }

    }
}