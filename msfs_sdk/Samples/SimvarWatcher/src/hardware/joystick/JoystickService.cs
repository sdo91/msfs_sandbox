﻿using FsAutopilot.Config;
using FsAutopilot.Events;
using FsAutopilot.Simconnect;
using OpenTK.Input;
using System;
using System.Collections.Generic;

namespace FsAutopilot.Hardware
{
    /// <summary>
    /// 
    /// http://www.siltutorials.com/opentkbasics/3
    /// 
    /// todo:
    /// make folder for joysticks
    /// make class for yoke,TQ,xbox
    /// add class for Logitech joystick
    ///
    /// </summary>
    public class JoystickService
    {
        private static readonly int MAX_SCAN = 10;

        private readonly EventService eventService;
        private readonly ConfigService configService;
        private SimconnectBridge simconnectBridge;
        private readonly VJoyFeeder vJoyFeeder = new VJoyFeeder(null);

        private FsJoystick alphaYoke;
        private List<FsJoystick> joysticks = new List<FsJoystick>();

        public JoystickService(EventService eventService, ConfigService configService)
        {
            this.eventService = eventService;
            this.configService = configService;

            Reset();

            eventService.AddHandler(EventTopic.DEV_CMD, HandleDevCmd);
            eventService.AddHandler(EventTopic.SIM_EVENT, HandleSimEvent);
            eventService.AddHandler(EventTopic.IR_REMOTE_COMMAND, HandleIrRemoteCommand);
            eventService.AddHandler(EventTopic.VJOY_BUTTON_REQUEST, HandleVjoyButtonRequest);
        }

        private void Reset()
        {
            alphaYoke = null;
            joysticks.Clear();
        }

        private void HandleDevCmd(Event myEvent)
        {
            var devCmd = (DevCommand)myEvent.data;

            if (devCmd.command == "VJT")
            {
                int throttlePercent = devCmd.GetArgAsInt(0);
                vJoyFeeder.SetThrottlePercent(throttlePercent);
            }
            else if (devCmd.command == "VJE")
            {
                int percent = devCmd.GetArgAsInt(0);
                vJoyFeeder.SetElevator(percent);
            }
            else if (devCmd.command == "VJB")
            {
                uint buttonNum = devCmd.GetArgAsUInt(0);
                vJoyFeeder.AsyncPressButton(buttonNum);
            }
        }

        private void HandleSimEvent(Event myEvent)
        {
            SIM_EVENT eventId = (SIM_EVENT)myEvent.data;
            Console.WriteLine($"[JoystickService] HandleSimEvent: {eventId}");
            switch (eventId)
            {
                case SIM_EVENT.THROTTLE_CUT:
                    vJoyFeeder.SetThrottlePercent(0);
                    break;

                default:
                    Console.WriteLine($"{eventId} event not handled by JoystickService");
                    break;
            }
        }

        private void HandleIrRemoteCommand(Event myEvent)
        {
            string command = (string)myEvent.data;
            Console.WriteLine($"HandleIrRemoteCommand: {command}");

            if (command == "PAUSE_SIM")
            {
                vJoyFeeder.ToggleSimPause();
            }
            else if (command == "COCKPIT_EXTERNAL_TOGGLE")
            {
                vJoyFeeder.ToggleCockpitExternalView();
            }
        }

        private void HandleVjoyButtonRequest(Event myEvent)
        {
            uint buttonNum = (uint)myEvent.data;
            Console.WriteLine($"HandleVjoyButtonRequest: buttonNum={buttonNum}");
            vJoyFeeder.AsyncPressButton(buttonNum);
        }

        /// <summary>
        /// 
        /// simconnectBridge should be set before this is called
        /// 
        /// </summary>
        public void ScanForDevices()
        {
            Reset();

            for (int i = 0; i <= MAX_SCAN; i++)
            {
                try
                {
                    JoystickCapabilities potential = Joystick.GetCapabilities(i);
                    LogJoystick(i, potential);
                    FsJoystick joystick = null;
                    if (AlphaYoke.IsMatch(potential))
                    {
                        joystick = new AlphaYoke(i, potential);
                        alphaYoke = joystick;
                    }
                    else if (LogitechThrottleQuadrant.IsMatch(potential))
                    {
                        joystick = new LogitechThrottleQuadrant(i, potential);
                    }
                    else if (XboxController.IsMatch(potential))
                    {
                        joystick = new XboxController(i, potential);
                    }
                    else if (LogitechJoystick.IsMatch(potential))
                    {
                        joystick = new LogitechJoystick(i, potential);
                    }
                    else
                    {
                        // no match found
                        Console.WriteLine($"  unrecognized controller at index: {i}");
                    }

                    if (joystick != null)
                    {
                        joystick.InjectServices(eventService, configService, simconnectBridge, vJoyFeeder);
                        joysticks.Add(joystick);
                    }
                }
                catch (Exception)
                {
                    Console.WriteLine($"ScanForDevices error (i={i})");
                }
            }
            Console.WriteLine("Device scan complete");
        }

        private void LogJoystick(int index, JoystickCapabilities potential)
        {
            if (potential.ButtonCount > 0)
            {
                Console.WriteLine($"Joystick found at index {index}: {potential}");
            }
        }

        /// <summary>
        ///
        /// fire event if:
        ///     AP switch has changed
        ///     AT switch has changed
        ///     airspeed buttons have been pressed
        ///
        /// </summary>
        public void Poll()
        {
            if (configService.GetAppCfg().bodieMode)
            {
                return;
            }

            foreach (FsJoystick joystick in joysticks)
            {
                joystick.Process();
            }
        }

        internal void SetThrottlePercent(double throttlePercent)
        {
            vJoyFeeder.SetThrottlePercent(throttlePercent);
        }

        internal double GetThrottle()
        {
            return vJoyFeeder.GetThrottle();
        }

        internal void SetElevator(double percent)
        {
            vJoyFeeder.SetElevator(percent);
        }

        internal void SetSimconnectBridge(SimconnectBridge simconnectBridge)
        {
            this.simconnectBridge = simconnectBridge;
        }

        internal bool IsDevFeatureEnabled()
        {
            if (alphaYoke == null)
            {
                return false;
            }
            return alphaYoke.IsHeld(AlphaYoke.ALPHA_YOKE_DEV_FEATURE_ON);
        }
    }
}