﻿using System;
using System.Threading;
using System.Threading.Tasks;
using vJoyInterfaceWrap;  // Don't forget to add this

namespace FsAutopilot.Hardware
{
    public class VJoyFeeder
    {
        // vjoy button assignments
        public static readonly uint BUTTON_PARKING_BRAKE = 1;
        public static readonly uint BUTTON_TOGGLE_PAUSE = 2;
        public static readonly uint BUTTON_TOGGLE_COCKPIT_EXTERNAL_VIEW = 3;
        public static readonly uint BUTTON_FUEL_SELECTOR_1_LEFT = 4;
        // 5 reserved for now
        public static readonly uint BUTTON_FUEL_SELECTOR_1_RIGHT = 6;

        // Declaring one joystick (Device id 1) and a position structure. 
        private vJoy joystick;
        private vJoy.JoystickState iReport;
        private uint id = 1;

        private int ContPovNumber;
        private int DiscPovNumber;

        private long maxval = 0;

        // custom
        double currentThrottlePercent = -1;

        public VJoyFeeder(string[] args)
        {
            Console.WriteLine("Constructing VJoyFeeder");

            // Create one joystick object and a position structure.
            joystick = new vJoy();
            iReport = new vJoy.JoystickState();

            // Device ID can only be in the range 1-16
            if (args != null && args.Length > 0 && !string.IsNullOrEmpty(args[0]))
            {
                id = Convert.ToUInt32(args[0]);
            }
            if (id <= 0 || id > 16)
            {
                Console.WriteLine("Illegal device ID {0}\nExit!", id);
                return;
            }

            // Get the driver attributes (Vendor ID, Product ID, Version Number)
            if (!joystick.vJoyEnabled())
            {
                Console.WriteLine("vJoy driver not enabled: Failed Getting vJoy attributes.");
                return;
            }
            Console.WriteLine("Vendor: {0}\nProduct :{1}\nVersion Number:{2}", joystick.GetvJoyManufacturerString(), joystick.GetvJoyProductString(), joystick.GetvJoySerialNumberString());

            // Get the state of the requested device
            VjdStat status = joystick.GetVJDStatus(id);
            switch (status)
            {
                case VjdStat.VJD_STAT_OWN:
                    Console.WriteLine("vJoy Device {0} is already owned by this feeder", id);
                    break;
                case VjdStat.VJD_STAT_FREE:
                    Console.WriteLine("vJoy Device {0} is free", id);
                    break;
                case VjdStat.VJD_STAT_BUSY:
                    Console.WriteLine("vJoy Device {0} is already owned by another feeder\nCannot continue", id);
                    return;
                case VjdStat.VJD_STAT_MISS:
                    Console.WriteLine("vJoy Device {0} is not installed or disabled\nCannot continue", id);
                    return;
                default:
                    Console.WriteLine("vJoy Device {0} general error\nCannot continue", id);
                    return;
            };

            // Check which axes are supported
            bool AxisX = joystick.GetVJDAxisExist(id, HID_USAGES.HID_USAGE_X);
            bool AxisY = joystick.GetVJDAxisExist(id, HID_USAGES.HID_USAGE_Y);
            bool AxisZ = joystick.GetVJDAxisExist(id, HID_USAGES.HID_USAGE_Z);
            bool AxisRX = joystick.GetVJDAxisExist(id, HID_USAGES.HID_USAGE_RX);
            bool AxisRZ = joystick.GetVJDAxisExist(id, HID_USAGES.HID_USAGE_RZ);

            // Get the number of buttons and POV Hat switchessupported by this vJoy device
            int nButtons = joystick.GetVJDButtonNumber(id);
            ContPovNumber = joystick.GetVJDContPovNumber(id);
            DiscPovNumber = joystick.GetVJDDiscPovNumber(id);

            joystick.GetVJDAxisMax(id, HID_USAGES.HID_USAGE_X, ref maxval);

            // Print results
            Console.WriteLine("vJoy Device {0} capabilities:", id);
            Console.WriteLine("\tNumber of buttons: {0}", nButtons);
            Console.WriteLine("\tNumber of Continuous POVs: {0}", ContPovNumber);
            Console.WriteLine("\tNumber of Descrete POVs: {0}", DiscPovNumber);
            Console.WriteLine("\tAxis X: {0}", AxisX ? "Yes" : "No");
            Console.WriteLine("\tAxis Y: {0}", AxisX ? "Yes" : "No");
            Console.WriteLine("\tAxis Z: {0}", AxisX ? "Yes" : "No");
            Console.WriteLine("\tAxis Rx: {0}", AxisRX ? "Yes" : "No");
            Console.WriteLine("\tAxis Rz: {0}", AxisRZ ? "Yes" : "No");
            Console.WriteLine($"\tmaxval: {maxval}");

            // Test if DLL matches the driver
            UInt32 DllVer = 0, DrvVer = 0;
            bool match = joystick.DriverMatch(ref DllVer, ref DrvVer);
            if (match)
            {
                Console.WriteLine("Version of Driver Matches DLL Version ({0:X})", DllVer);
            }
            else
            {
                Console.WriteLine("Version of Driver ({0:X}) does NOT match DLL Version ({1:X})", DrvVer, DllVer);
            }

            // Acquire the target
            if ((status == VjdStat.VJD_STAT_OWN) || ((status == VjdStat.VJD_STAT_FREE) && (!joystick.AcquireVJD(id))))
            {
                Console.WriteLine("Failed to acquire vJoy device number {0}.", id);
                return;
            }
            Console.WriteLine("Acquired: vJoy device number {0}.", id);

            // Reset this device to default values
            joystick.ResetVJD(id);
            SetThrottlePercent(0);
            SetElevator(0);
            Console.WriteLine($"vJoy device {id} ready!");
            Console.WriteLine();
        }

        /// <summary>
        /// Set the vjoy throttle from the given throttle value
        /// 
        /// The vjoy throttle range is similar the the logitech throttle.
        ///     logitech: [-16384, 16384]
        ///     vjoy    : [-16383, 16384]
        /// 
        /// [bug] atr drops way off between 88-87%
        ///     this happens with both logitech and vjoy
        /// 
        /// </summary>
        public void SetThrottlePercent(double throttlePercent)
        {
            // input range: [0, 100]
            currentThrottlePercent = throttlePercent;
            SetAxis(HID_USAGES.HID_USAGE_X, throttlePercent);
        }

        public double GetThrottle()
        {
            return currentThrottlePercent;
        }

        public void SetElevator(double percent)
        {
            // input range: [-100, 100]
            percent /= 2; // scale to [-50, 50]
            percent += 50; // shift to [0, 100]

            SetAxis(HID_USAGES.HID_USAGE_Y, percent);
            Console.WriteLine($"SetElevator: {percent}");
        }

        private void SetAxis(HID_USAGES axis, double percent)
        {
            int value = (int)Math.Ceiling(percent * maxval / 100.0);
            value = Math.Max(1, value);
            joystick.SetAxis(value, id, axis);
        }

        public void ToggleParkingBrake()
        {
            Console.WriteLine("[VJoyFeeder] ToggleParkingBrake");
            AsyncPressButton(BUTTON_PARKING_BRAKE);
        }

        public void ToggleSimPause()
        {
            Console.WriteLine("[VJoyFeeder] ToggleSimPause");
            AsyncPressButton(BUTTON_TOGGLE_PAUSE);
        }

        public void ToggleCockpitExternalView()
        {
            Console.WriteLine("[VJoyFeeder] ToggleCockpitExternalView");
            AsyncPressButton(BUTTON_TOGGLE_COCKPIT_EXTERNAL_VIEW);
        }

        public void AsyncPressButton(uint buttonNum)
        {
            Console.WriteLine($"[VJoyFeeder] AsyncPressButton: buttonNum={buttonNum}");
            Task.Run(() =>
            {
                joystick.SetBtn(true, id, buttonNum);
                Thread.Sleep(100);
                joystick.SetBtn(false, id, buttonNum);
            });
        }
    }
}
