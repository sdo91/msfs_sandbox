﻿using System;

namespace FsAutopilot.Hardware
{
    public class ButtonPressEvent
    {
        public DateTime timestamp;
        public IrRemoteButton button;

        public ButtonPressEvent(IrRemoteButton button)
        {
            timestamp = DateTime.Now;
            this.button = button;
        }
    };
}