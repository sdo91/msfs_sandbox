﻿namespace FsAutopilot.Hardware
{
    public enum IrRemoteButton
    {
        ARROW_DOWN,
        ARROW_UP,
        EQ,
        FUNC_STOP,
        NUM_0,
        NUM_1,
        NUM_2,
        NUM_3,
        NUM_4,
        NUM_5,
        NUM_6,
        NUM_7,
        NUM_8,
        NUM_9,
        PLAY_PAUSE,
        POWER,
        SKIP_BACK,
        SKIP_FORWARD,
        ST_REPT,
        VOLUME_DOWN,
        VOLUME_UP,
    }
}