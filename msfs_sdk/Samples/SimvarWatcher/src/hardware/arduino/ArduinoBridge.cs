﻿using FsAutopilot.Config;
using FsAutopilot.Events;
using FsAutopilot.Simconnect;
using FsAutopilot.Util;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Runtime.CompilerServices;

namespace FsAutopilot.Hardware
{

    public class ArduinoBridge
    {
        private EventService eventService;
        private ConfigService configService;
        private SimconnectBridge simconnectBridge;

        private SerialPort port;

        private string buffer = "";

        private readonly Dictionary<string, IrRemoteButton> buttonMap;

        private readonly LinkedList<ButtonPressEvent> buttonPressEvents = new LinkedList<ButtonPressEvent>();

        public ArduinoBridge(EventService eventService, ConfigService configService, SimconnectBridge simconnectBridge)
        {
            this.eventService = eventService;
            this.configService = configService;
            this.simconnectBridge = simconnectBridge;

            // mapping
            buttonMap = new Dictionary<string, IrRemoteButton>();

            buttonMap["0x45"] = IrRemoteButton.POWER;
            buttonMap["0x46"] = IrRemoteButton.VOLUME_UP;
            buttonMap["0x47"] = IrRemoteButton.FUNC_STOP;

            buttonMap["0x44"] = IrRemoteButton.SKIP_BACK;
            buttonMap["0x40"] = IrRemoteButton.PLAY_PAUSE;
            buttonMap["0x43"] = IrRemoteButton.SKIP_FORWARD;

            buttonMap["0x07"] = IrRemoteButton.ARROW_DOWN;
            buttonMap["0x15"] = IrRemoteButton.VOLUME_DOWN;
            buttonMap["0x09"] = IrRemoteButton.ARROW_UP;

            buttonMap["0x16"] = IrRemoteButton.NUM_0;
            buttonMap["0x19"] = IrRemoteButton.EQ;
            buttonMap["0x0D"] = IrRemoteButton.ST_REPT;

            buttonMap["0x0C"] = IrRemoteButton.NUM_1;
            buttonMap["0x18"] = IrRemoteButton.NUM_2;
            buttonMap["0x5E"] = IrRemoteButton.NUM_3;

            buttonMap["0x08"] = IrRemoteButton.NUM_4;
            buttonMap["0x1C"] = IrRemoteButton.NUM_5;
            buttonMap["0x5A"] = IrRemoteButton.NUM_6;

            buttonMap["0x42"] = IrRemoteButton.NUM_7;
            buttonMap["0x52"] = IrRemoteButton.NUM_8;
            buttonMap["0x4A"] = IrRemoteButton.NUM_9;
        }

        public void Start()
        {
            for (int i = 0; i < 10; i++)
            {
                string comPort = $"COM{i}";
                try
                {
                    // Try to create the serial port with basic settings
                    port = new SerialPort(comPort, 115200, Parity.None, 8, StopBits.One);
                    port.Open();
                    break;
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"failed to connect: {comPort}");
                }
            }
            port.DataReceived += new SerialDataReceivedEventHandler(HandleSerialData);
            Console.WriteLine("serial port open");
        }

        public void Stop()
        {
            if (port != null)
            {
                port.Close();
            }
            Console.WriteLine("serial port closed");
        }

        private void HandleSerialData(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                buffer += port.ReadExisting();

                if (buffer.EndsWith("\n"))
                {
                    // handle serial command
                    string text = buffer.Trim();
                    text = text.Replace("cmd: ", "");

                    IrRemoteButton button = buttonMap[text];
                    Console.WriteLine($"got serial data: text={text}, button={button}");
                    HandleButtonPress(button);

                    buffer = "";
                }
            }
            catch (Exception)
            {
                Console.WriteLine($"HandleSerialData Exception:\nbuffer={buffer}");
                buffer = "";
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        private void HandleButtonPress(IrRemoteButton button)
        {
            // clear if timed out
            if (buttonPressEvents.Count > 0)
            {
                ButtonPressEvent last = buttonPressEvents.Last();
                if (TimeUtil.IsTimedOut(last.timestamp, 15))
                {
                    buttonPressEvents.Clear();
                }
            }

            if (button == IrRemoteButton.POWER)
            {
                // handle reset
                buttonPressEvents.Clear();
                Console.WriteLine($"buttons cleared!");
            }
            else if (button == IrRemoteButton.VOLUME_UP || button == IrRemoteButton.VOLUME_DOWN)
            {
                string numbers = ParseNumbers();
                if (numbers.Length > 0)
                {
                    // set altitude
                    string desiredAlt = numbers + "00";
                    eventService.FireChangeRequestEvent(ChangeRequestEvent.TargetKey.SET_TARGET_ALTITUDE, desiredAlt);
                }
                else
                {
                    // adjust altitude
                    string delta = (button == IrRemoteButton.VOLUME_UP) ? "500" : "-500";
                    eventService.FireChangeRequestEvent(ChangeRequestEvent.TargetKey.ADJUST_TARGET_ALTITUDE, delta);
                }
                buttonPressEvents.Clear();
            }
            else if (button == IrRemoteButton.SKIP_FORWARD || button == IrRemoteButton.SKIP_BACK)
            {
                string numbers = ParseNumbers();
                if (numbers.Length > 0)
                {
                    // set heading
                    string desiredHdg = numbers;
                    eventService.FireChangeRequestEvent(ChangeRequestEvent.TargetKey.SET_HEADING_BUG, desiredHdg);
                }
                else
                {
                    // adjust heading
                    string headingDelta = configService.GetAppCfg().headingDelta.ToString();
                    if (button == IrRemoteButton.SKIP_BACK)
                    {
                        headingDelta = $"-{headingDelta}";
                    }
                    eventService.FireChangeRequestEvent(ChangeRequestEvent.TargetKey.ADJUST_HEADING_BUG, headingDelta);
                }
                buttonPressEvents.Clear();
            }
            else if (button == IrRemoteButton.PLAY_PAUSE)
            {
                // sync heading bug
                double currentHdg = simconnectBridge.GetCurrentHeading();
                currentHdg = ApUtil.RoundTo(currentHdg, 5);
                eventService.FireChangeRequestEvent(ChangeRequestEvent.TargetKey.SET_HEADING_BUG, currentHdg.ToString());
            }
            else if (button == IrRemoteButton.ARROW_UP || button == IrRemoteButton.ARROW_DOWN || button == IrRemoteButton.ST_REPT)
            {
                string numbers = ParseNumbers();
                if (numbers.Length > 0)
                {
                    // set airspeed
                    string desiredAirpeed = numbers;
                    eventService.FireChangeRequestEvent(ChangeRequestEvent.TargetKey.SET_TARGET_AIRSPEED, desiredAirpeed);
                }
                else
                {
                    // adjust airspeed
                    string delta = configService.GetAppCfg().airspeedDelta.ToString();
                    if (button != IrRemoteButton.ARROW_UP)
                    {
                        delta = "-" + delta;
                    }
                    eventService.FireChangeRequestEvent(ChangeRequestEvent.TargetKey.ADJUST_TARGET_AIRSPEED, delta);
                }
                buttonPressEvents.Clear();
            }
            else if (button == IrRemoteButton.FUNC_STOP)
            {
                string numbers = ParseNumbers();
                HandleFuncButton(numbers);
                buttonPressEvents.Clear();
            }
            else
            {
                // add button to queue (eg: a number)
                buttonPressEvents.AddLast(new ButtonPressEvent(button));
            }

        }

        private string ParseNumbers()
        {
            string result = "";
            foreach (ButtonPressEvent buttonPressEvent in buttonPressEvents)
            {
                string text = buttonPressEvent.button.ToString();
                if (!text.StartsWith("NUM_"))
                {
                    Console.WriteLine($"Error: NaN: {text}");
                    return "";
                }
                text = text.Replace("NUM_", "");
                result += text;
            }
            return result;
        }

        /// <summary>
        /// 
        /// handle a numbered function command
        /// 
        /// </summary>
        private void HandleFuncButton(string numbers)
        {
            if (numbers.Length == 0)
            {
                Console.WriteLine("[HandleFuncButton] no numbers");
                return;
            }

            int commandId = Int32.Parse(numbers);
            string command = "";

            switch (commandId)
            {
                // todo: line these numbers up with vjoy button numbers
                case 1:
                    command = "PAUSE_SIM";
                    break;
                case 2:
                    command = "COCKPIT_EXTERNAL_TOGGLE";
                    break;
                case 7:
                    Console.WriteLine("[HandleFuncButton] left 90");
                    simconnectBridge.AdjustHeadingBug(-90, false);
                    return;
                case 8:
                    Console.WriteLine("[HandleFuncButton] 180");
                    simconnectBridge.AdjustHeadingBug(180, false);
                    return;
                case 9:
                    Console.WriteLine("[HandleFuncButton] right 90");
                    simconnectBridge.AdjustHeadingBug(90, false);
                    return;
                default:
                    Console.WriteLine($"[HandleFuncButton] invalid command: {commandId}");
                    break;
            }

            if (command.Length > 0)
            {
                eventService.FireEvent(EventTopic.IR_REMOTE_COMMAND, command);
            }
        }

    }
}
