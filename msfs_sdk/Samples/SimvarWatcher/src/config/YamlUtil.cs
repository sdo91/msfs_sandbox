﻿using FsAutopilot.Util;
using System;
using System.Collections.Generic;
using System.IO;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace FsAutopilot.Config
{
    /// <summary>
    /// https://github.com/aaubry/YamlDotNet
    /// </summary>
    public class YamlUtil
    {
        private static readonly string CONFIG_DIR = ApUtil.ExpandPath("resources/config");
        private static readonly string AIRCRAFT_DIR = $"{CONFIG_DIR}/aircraft";

        public static readonly string CONFIG_FILE_PATH = $"{CONFIG_DIR}/base_config.yaml";

        public static string ObjToStr(object data)
        {
            var serializer = new SerializerBuilder()
                .WithNamingConvention(CamelCaseNamingConvention.Instance)
                .Build();
            string yamlText = serializer.Serialize(data);
            yamlText = TrimAllDoubles(yamlText);
            return yamlText;
        }

        public static T StrToObj<T>(string yamlText)
        {
            var deserializer = new DeserializerBuilder()
                .WithNamingConvention(CamelCaseNamingConvention.Instance)
                .IgnoreUnmatchedProperties()
                .Build();
            var result = deserializer.Deserialize<T>(yamlText);
            return result;
        }

        public static void ObjToFile(object data, string fileName)
        {
            Console.WriteLine($"Writing YAML to: {fileName}");
            string text = ObjToStr(data);
            File.WriteAllText(fileName, text);
        }

        public static T FileToObj<T>(string configFilePath)
        {
            string yamlText = File.ReadAllText(configFilePath);
            return StrToObj<T>(yamlText);
        }

        /// <summary>
        /// trim double if possible
        /// if not, return empty string
        /// </summary>
        public static string TrimDouble(string valueRaw)
        {
            double valueParsed;
            bool success = Double.TryParse(valueRaw, out valueParsed);
            if (success)
            {
                string valueTrimmed = valueParsed.ToString();
                if (valueTrimmed != valueRaw)
                {
                    return valueTrimmed;
                }
            }
            return "";
        }

        /// <summary>
        /// algo:
        ///     split into tokens
        ///     make a map of tokens to trim
        ///     replace in string using map
        /// </summary>
        public static string TrimAllDoubles(string yamlText)
        {
            // split into tokens
            string[] tokens = yamlText.Split(new char[0], StringSplitOptions.RemoveEmptyEntries);

            // make a map of tokens to trim
            HashSet<string> tokensChecked = new HashSet<string>();
            Dictionary<string, string> tokensToTrim = new Dictionary<string, string>();
            foreach (string token in tokens)
            {
                if (token.Length > 0 && !tokensChecked.Contains(token))
                {
                    // haven't seen this token yet
                    if (token.Contains("."))  // handles the case of "Livery 01"
                    {
                        string trimmed = TrimDouble(token);
                        if (trimmed.Length > 0)
                        {
                            tokensToTrim[token] = trimmed;
                        }
                    }
                }
                tokensChecked.Add(token);
            }

            foreach (var entry in tokensToTrim)
            {
                //Console.WriteLine($"replacing: {entry.Key} -> {entry.Value}");
                yamlText = yamlText.Replace(entry.Key, entry.Value);
            }

            return yamlText;
        }

        internal static bool IsObjEqualToFile(Object obj, string filePath)
        {
            // read from file to string
            string fileText = File.ReadAllText(filePath);

            // convert obj to string
            string objectText = ObjToStr(obj);

            // compare
            return fileText == objectText;
        }

        public static string GetFullAircraftPath(string aircraftFileBasename)
        {
            string dir = GetAircraftConfigDirPath();
            return $"{dir}/{aircraftFileBasename}";
        }

        public static string[] GetAllAircraftConfigPaths()
        {
            string dir = GetAircraftConfigDirPath();
            return Directory.GetFiles(dir);
        }

        public static string GetAircraftConfigDirPath()
        {
            return $"{AIRCRAFT_DIR}/msfs{VersionUtil.GetVersion()}";
        }
    }
}
