﻿namespace FsAutopilot.Config
{
    public class RollConfig
    {
        public int maxAileronTrim;
        public int maxAileronTrimRate;
    }
}
