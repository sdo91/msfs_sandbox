﻿namespace FsAutopilot.Config
{
    public class AirspeedConfig
    {
        public int maxThrottleTakeoff = 100; // eg: for vertigo
        public int maxThrottleClimb = 95;
        public int maxThrottleCruise = 90;

        public int minThrottleGearUp = 1;  // to prevent landing gear warnings

        public int maxThrottleChangeRate;  // (%/sec) limit how fast throttle input can change
    }
}
