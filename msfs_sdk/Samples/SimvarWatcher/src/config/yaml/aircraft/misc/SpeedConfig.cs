﻿namespace FsAutopilot.Config
{
    public class SpeedConfig
    {
        public int rotate = -1;                    // rotation speed (-1 = auto)
        public int climbAirspeedKias = -1;         // climb speed (-1 = auto)
        public double highAltitudeClimbMach = -1;  // (-1 = disabled)
        public double cruiseSpeed = -1;            // mach number or true airspeed (-1 = auto)
    }
}
