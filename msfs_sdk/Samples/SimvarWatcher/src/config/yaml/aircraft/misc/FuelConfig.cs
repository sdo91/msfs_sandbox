﻿namespace FsAutopilot.Config
{
    public class FuelConfig
    {
        public int fuelCutoffPercentage = 0;
        public int fuelFudgeFactor = 5;  // percent to add to fuel consumption
        public bool balanceFuel = false;
        public bool useVjoyFuelSwitch = false;
    }
}
