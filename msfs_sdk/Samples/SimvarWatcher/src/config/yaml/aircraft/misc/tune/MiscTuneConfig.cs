﻿namespace FsAutopilot.Config
{
    public class MiscTuneConfig
    {
        public AirspeedConfig airspeed = new AirspeedConfig();

        public PitchConfig pitch = new PitchConfig();
        public VertSpeedConfig vertSpeed = new VertSpeedConfig();
        public FlcConfig flc = new FlcConfig();
        public GlideSlopeConfig glideSlope = new GlideSlopeConfig();

        public RollConfig roll = new RollConfig();
        public HeadingConfig heading = new HeadingConfig();
    }
}
