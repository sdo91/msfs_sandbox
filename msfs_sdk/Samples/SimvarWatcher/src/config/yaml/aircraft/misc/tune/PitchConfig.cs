﻿namespace FsAutopilot.Config
{
    public class PitchConfig
    {
        public int maxElevatorTrim;  // degrees for now (todo: use percent?)
        public double maxElevatorTrimRate;
    }
}
