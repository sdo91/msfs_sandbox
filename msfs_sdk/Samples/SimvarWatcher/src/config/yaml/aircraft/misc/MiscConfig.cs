﻿namespace FsAutopilot.Config
{
    /// <summary>
    ///
    /// todo: move to PID?
    /// MaxControlVarRate
    /// setMinMax
    ///
    /// </summary>
    public class MiscConfig
    {
        public SpeedConfig speeds = new SpeedConfig();

        public AutoCruiseConfig autoCruise = new AutoCruiseConfig();

        public MiscTuneConfig tune = new MiscTuneConfig();

        // sim compatablity (eg: dc6/crj throttle, auto trim)
        public bool useCustomGpsNav = false;  // since G1000 is now broken
        public bool useVjoyThrottle = false;  // DC-6 and some other 3rd party aircraft
        public bool useElevatorPosition = false;  // set true for fly-by-wire aircraft
        public bool usePitchRate = false; // todo: deprecate?
        public bool calcTrackFromHdg = false;

        public bool scaleRollWithSpeed = false; // scale roll gainswith speed
        public bool scalePitchWithSpeed = false; // scale pitch gains with speed

        // fuel
        public FuelConfig fuelConfig = new FuelConfig();
        
        // startup values
        public int defaultVertSpeed = 500;  // ft/min
        public int defaultTargetSpeed = 150;  // knots (todo: rename: defaultTargetAirspeed)
        // vr
        // cruise altitude buffer
    }
}
