﻿namespace FsAutopilot.Config
{
    public class AutoCruiseConfig
    {
        // todo: change this to takeoffToClimbRateKpkf
        // eg:
        // 208: 25 knots in 250 ft = 10 ft/knot = 100 knots/kiloFt
        // TBM: rotate @ 90, climb at 110 or 190 knots in 2000 ft = 20 ft/ 1 knot = 50 kpkf
        public int climbSpeedThresholdAgl = 200;  // below this, tgt speed will interpolate between vr and climb speed

        public int cruiseAlt = 20000;  // todo: change to flight level
        public int cruiseAltBuffer = 1000;  // when to switch from FLC to AltHold

        public double descentSlopeDeg = 3.0;
        public double maxDescentRate = 3000;

        public int approachDistanceNm = 10;  // distance for ILS approach
    }
}
