﻿using System.Collections.Generic;

namespace FsAutopilot.Config
{

    public class AircraftConfig
    {
        public string name;

        public SortedSet<string> liveries = new SortedSet<string>();

        public SimData simData = new SimData();

        public MiscConfig misc = new MiscConfig();

        public string notes = "";

        // slow gains
        public PidControlConfig pidControlConfig = new PidControlConfig();

        // todo: fast gains
        //public PidControlConfig fastGains = null;

        /// <summary>
        /// 
        /// can reset bad PID defaults here
        /// 
        /// todo: move to PidControlConfig
        /// 
        /// </summary>
        public void OnLoad()
        {
            if (pidControlConfig.glideSlopeGains.p == 0.5 && pidControlConfig.glideSlopeGains.i == 0.2)
            {
                pidControlConfig.glideSlopeGains.p = 5;
                pidControlConfig.glideSlopeGains.i = 0.2;
            }

            if (pidControlConfig.navGains.i == 0 && pidControlConfig.navGains.maxErrForI == 0)
            {
                pidControlConfig.navGains.i = 0.3;
                pidControlConfig.navGains.maxErrForI = 3;
            }

            if (pidControlConfig.headingGains.i == 0 && pidControlConfig.headingGains.maxErrForI == 0)
            {
                pidControlConfig.headingGains.i = 0.1;
                pidControlConfig.headingGains.maxErrForI = 5;
            }

            if (pidControlConfig.navGains.maxErrForI > 0 && pidControlConfig.navGains.maxErrForI <= 10)
            {
                pidControlConfig.navGains.maxErrForI = 50;
            }

            //fastGains = null;
        }

    }
}
