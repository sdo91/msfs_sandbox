﻿namespace FsAutopilot.Config
{
    public class PidGains
    {
        public double p = 0.1;
        public double i = 0.01;
        public double d = 0.0;

        // reset I if we are too far from set point
        // example: reset roll windup if heading is off by 10+ degress
        public double maxErrForI = 0;

        // prevent set point from being too far from current value
        // example: keep target roll/pitch close to current roll/pitch
        public double maxSetPointError = 0;

        // rate at which set point can change
        // eg: limit roll rate in roll controller
        public double maxSetPointChangeRate = 0;

        // todo: add MaxControlVarRate

        public PidGains()
        {
        }

        public PidGains(double p, double i, double d, double maxErrForI, double maxSetPointError, double maxSetPointChangeRate)
        {
            this.p = p;
            this.i = i;
            this.d = d;

            this.maxErrForI = maxErrForI;
            this.maxSetPointError = maxSetPointError;
            this.maxSetPointChangeRate = maxSetPointChangeRate;
        }

        public PidGains Copy()
        {
            return new PidGains(p, i, d, maxErrForI, maxSetPointError, maxSetPointChangeRate);
        }
    }
}