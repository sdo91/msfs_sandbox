﻿namespace FsAutopilot.Config
{
    public class PidControlConfig
    {
        public PidGains altGains;
        public PidGains glideSlopeGains;
        public PidGains vsGains;
        public PidGains flcGains;
        public PidGains pitchGains;

        public PidGains pitch2Gains;  // uses pitch rate
        public PidGains pitchRateGains;

        public PidGains navGains;
        public PidGains headingGains;
        public PidGains rollGains;

        public PidGains airspeedGains;
    }
}