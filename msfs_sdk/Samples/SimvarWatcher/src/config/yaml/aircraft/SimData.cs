﻿namespace FsAutopilot.Config
{
    public class SimData
    {
        public int designSpeedStallLanding = 0;
        public int designSpeedStallClean = 0;

        // NOTE: these 2 are not consistent between aircraft...
        public int designSpeedMinRotation = 0;
        public int designSpeedTakeoff = 0;

        public int designSpeedClimb = 0;

        public int cruiseSpeedIndicated = 0;
        public int cruiseSpeedTrue = 0;
        public double cruiseSpeedMach = 0;

        public int designCruiseAltitude = 0;

        public double maxElevatorTrimDegrees = 0;

        // NOTE: these 2 are not consistent between aircraft...
        public int designSpeedCruise = 0;
        public int estimatedCruiseSpeed = 0;
    }
}
