﻿using FsAutopilot.Util;
using System;

namespace FsAutopilot.Config
{
    public class YamlVorWaypoint
    {
        public static readonly string RADIAL_INVALID = "x";
        public static readonly string RADIAL_ILS = "ils";

        public double frequency = 0;

        /// <summary>
        /// values:
        ///     x: don't go inbound (eg: already accomplished) 
        ///     <int>: go into VOR on the indicated radial
        ///
        /// todo:
        ///     auto: go into point on the current radial (direct to VOR)
        /// </summary>
        public string radialInbound = RADIAL_INVALID;

        /// <summary>
        /// this can also be used for "outbound" heading from VOR offset point
        ///     eg: when we reach VOR offset point, we set HDG bug to this value (and ignore freq)
        /// </summary>
        public string radialOutbound = RADIAL_INVALID;

        /// <summary>
        /// Used to mark a waypoint as reached early. It will then be removed from the queue.
        /// example usage:
        ///     allows us to use both VOR and ILS at destination airport
        ///     mark VOR done early, to allow to switch to ILS
        /// </summary>
        public int inboundArrivalDistance = -1;  // -1: auto

        // todo: add a way to change altitude
        // eg: descend to X MSL by 10 miles DME inbound

        public static YamlVorWaypoint CreateWaypoint(string frequency, string crsInDeg, string crsOutDeg)
        {
            YamlVorWaypoint result = new YamlVorWaypoint();

            result.frequency = Double.Parse(frequency);
            if (result.frequency < 100)
            {
                result.frequency += 100;
            }

            if (crsInDeg.Length > 0)
            {
                result.radialInbound = crsInDeg;
            }
            if (crsOutDeg.Length > 0)
            {
                result.radialOutbound = crsOutDeg;
            }

            return result;
        }

        internal int GetDesiredRadial()
        {
            if (IsInboundLegActive())
            {
                return Int32.Parse(radialInbound);
            }
            else if (IsRadialValid(radialOutbound))
            {
                return Int32.Parse(radialOutbound);
            }
            else
            {
                return -1;  // eg: ILS
            }
        }

        internal bool IsInboundLegActive()
        {
            return IsRadialValid(radialInbound);
        }

        internal void MarkInboundLegDone()
        {
            radialInbound = RADIAL_INVALID;
        }

        internal bool MarkIls()
        {
            if (radialInbound != RADIAL_ILS || IsOutboundRadialValid())
            {
                radialInbound = RADIAL_ILS;
                radialOutbound = RADIAL_INVALID;
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool IsRadialValid(string radial)
        {
            return ApUtil.StartsWithDigit(radial);
        }

        internal bool HasLocalizer()
        {
            //Console.WriteLine($"radialInbound: {radialInbound}");
            return radialInbound == RADIAL_ILS;
        }

        internal bool IsOutboundRadialValid()
        {
            return IsRadialValid(radialOutbound);
        }
    }
}
