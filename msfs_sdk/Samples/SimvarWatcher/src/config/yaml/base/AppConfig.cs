﻿namespace FsAutopilot.Config
{
    /// <summary>
    ///
    /// application level settings
    ///
    /// </summary>
    public class AppConfig
    {

        /// <summary>
        /// reset certain values to their defaults
        /// todo: make this time based (eg: reset on launch after 6 hours)
        /// </summary>
        public void Reset()
        {
            ignoreSimTargetAirspeed = false;
            airspeedDelta = 5;
            speedSource = "I";

            headingDelta = 10;
            neoflyMaxRollDeg = 25;

            desiredXtkRight = 0;
            maxNavInterceptAngleDeg = 30;
            noDmeNavInterceptAngleDeg = -1;
        }

        public bool resetOnStart = true;

        public bool bodieMode = false;

        public bool ignoreSimTargetAirspeed = false;

        // amount to inc/dec airspeed for AT
        // NOTE: only affects aircaft w/o built-in airspeed control
        public int airspeedDelta = 5;

        public string speedSource = "I";  // options {"I", "T", "G"}
        public int airspeedUnder10k = 240;

        public int headingDelta = 10;
        public bool useTrackInHeadingMode = true;
        public int neoflyMaxRollDeg = 25;

        public double desiredXtkRight = 0.0;  // 0-99 NM or 100+ feet

        // VOR/ILS
        public int maxNavInterceptAngleDeg = 30;
        public int noDmeNavInterceptAngleDeg = -1;
        public bool syncNav2 = true;  // to show DME on logitech FIP
        public bool useCrsIfNoVorLock = true;
    }
}
