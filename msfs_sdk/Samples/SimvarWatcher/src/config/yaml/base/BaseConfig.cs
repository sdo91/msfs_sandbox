﻿using System;

namespace FsAutopilot.Config
{
    /// <summary>
    /// this is the base class for the config file
    /// </summary>
    public class BaseConfig
    {
        public int configFormatVersion = 2;
        public AppConfig appConfig = new AppConfig();

        public FlightPlanConfig flightPlan = new FlightPlanConfig();

        public void Reset()
        {
            if (appConfig.resetOnStart)
            {
                appConfig.Reset();

                flightPlan.Reset();

                Console.WriteLine("BaseConfig reset!");
            }
            else
            {
                Console.WriteLine("BaseConfig reset skipped!");
            }
        }

        internal void AfterDeserialize()
        {
            flightPlan.AfterDeserialize();
        }
    }
}
