﻿using System.Collections.Generic;

namespace FsAutopilot.Config
{
    /// <summary>
    ///
    /// eg: change CRS on arrival at VOR
    ///
    /// </summary>
    public class FlightPlanConfig
    {
        public bool isLandingAtNextWaypoint = false;  // otherwise, use last waypoint

        public int autoCruiseDescentBufferNm = 2; // try to reach to target altitude this many miles early

        public bool pauseOnApproach = false;  // for auto-cruise
        public int pauseAtEte = -1;           // for Neofly GA flights
        public double pauseOnFreqLock = -1;   // for VOR only flights
        public bool pauseOnEngineLoss = true;
        public bool pauseOnIlsLock = true;

        // todo: implement
        public int pauseAtAgl = 1000;

        /// <summary>
        /// see FlightWaypoint for acceptable waypoint formats
        /// </summary>
        public bool enableVorWaypoints = true;
        public int vorArrivalDist = 3;
        public List<YamlVorWaypoint> vorWaypoints = new List<YamlVorWaypoint>();

        public string vorToDestinationVector = "";  // todo: document

        internal void Reset()
        {
            isLandingAtNextWaypoint = false;

            enableVorWaypoints = true;

            vorToDestinationVector = "";

            pauseOnEngineLoss = true;
        }

        public void AfterDeserialize()
        {
            if (vorWaypoints == null)
            {
                vorWaypoints = new List<YamlVorWaypoint>();
            }
        }
    }
}
