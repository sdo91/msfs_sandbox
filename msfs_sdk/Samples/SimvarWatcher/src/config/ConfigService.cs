﻿using FsAutopilot.AircraftLogic;
using FsAutopilot.Autopilot;
using FsAutopilot.Events;
using FsAutopilot.Simconnect;
using FsAutopilot.View;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;

namespace FsAutopilot.Config
{
    /// <summary>
    ///
    /// v3 config plans:
    ///     make PID controllers a dict
    ///         eg:
    ///         pidControllers:
    ///             ALT:
    ///                 name: ALT
    ///                 p: 5
    ///                 i: 0.5
    ///                 d: 0
    ///
    /// </summary>
    public class ConfigService
    {
        /// <summary>
        ///
        /// sdo91@SCOTT-PC MINGW64 /d/MSFS_INSTALL/Official/OneStore
        /// $ grep -r 'title =' asobo-aircraft-e330*
        /// asobo-aircraft-e330/SimObjects/Airplanes/Asobo_E330/aircraft.cfg:title =
        ///     "Extra 330 Asobo" ; Variation name
        /// asobo-aircraft-e330-livery-01/SimObjects/Airplanes/Asobo_E330_Livery_01/aircraft.cfg:title =
        ///     "Extra 330 01 Livery" ; Variation name
        /// asobo-aircraft-e330-livery-aviators/SimObjects/Airplanes/Asobo_E330_LiveryAviators/aircraft.cfg:title =
        ///     "Extra 330 Aviators Club Livery" ; Variation name
        /// asobo-aircraft-e330-livery-kenmore/SimObjects/Airplanes/Asobo_E330_Livery_Kenmore/aircraft.cfg:title =
        ///     "Extra 330 Kenmore Livery" ; Variation name
        /// asobo-aircraft-e330-livery-xbox-aviators/SimObjects/Airplanes/Asobo_E330_Livery_Xbox_Aviators/aircraft.cfg:title =
        ///     "Extra 330 Xbox Aviators Club Livery" ; Variation name
        ///
        /// sdo91@SCOTT-PC MINGW64 /d/MSFS_INSTALL/Official/OneStore
        /// $ grep -r 'title =' asobo-aircraft-longitude*
        /// asobo-aircraft-longitude-livery-01/SimObjects/Airplanes/Asobo_Longitude_Livery_01/aircraft.cfg:title =
        ///     "Cessna Longitude 01 Livery" ; Variation name
        /// asobo-aircraft-longitude-livery-aviators/SimObjects/Airplanes/Asobo_Longitude_LiveryAviators/aircraft.cfg:title =
        ///     "Cessna Longitude Aviators Club Livery" ; Variation name
        /// asobo-aircraft-longitude-livery-global/SimObjects/Airplanes/Asobo_Longitude_Livery_Global/aircraft.cfg:title =
        ///     "Cessna Longitude Global Livery" ; Variation name
        /// asobo-aircraft-longitude-livery-kenmore/SimObjects/Airplanes/Asobo_Longitude_Livery_Kenmore/aircraft.cfg:title =
        ///     "Cessna Longitude Kenmore Livery" ; Variation name
        /// asobo-aircraft-longitude-livery-orbit/SimObjects/Airplanes/Asobo_Longitude_Livery_Orbit/aircraft.cfg:title =
        ///     "Cessna Longitude Orbit Livery" ; Variation name
        /// asobo-aircraft-longitude-livery-pacifica/SimObjects/Airplanes/Asobo_Longitude_Livery_Pacifica/aircraft.cfg:title =
        ///     "Cessna Longitude Pacifica Livery" ; Variation name
        /// asobo-aircraft-longitude-livery-xbox-aviators/SimObjects/Airplanes/Asobo_Longitude_Livery_Xbox_Aviators/aircraft.cfg:title =
        ///     "Cessna Longitude Xbox Aviators Club Livery" ; Variation name
        ///
        /// </summary>
        private static readonly string[] LIVERY_NAMES = {
            "01 Livery",
            "Asobo",
            "Aviators Club Livery",
            "Global Livery",
            "Kenmore Livery",
            "Livery 01",
            "Orbit Livery",
            "Pacifica Livery",
            "Xbox Aviators Club Livery",
        };

        public static readonly string DEFAULT_AIRCRAFT_TITLE = "DEFAULT";
        public static readonly string DEFAULT_AIRCRAFT_BASENAME = DEFAULT_AIRCRAFT_TITLE + ".yaml";

        private AircraftLogicService aircraftLogicService = null;
        private SimconnectBridge simconnectBridge = null;

        private string rawLiveryTitle;  // the livery name
        private string formattedAircraftTitle;
        private string currentAircraftFileBasename;  // eg: DCDESIGNS_CONCORDE.yaml?

        private BaseConfig baseConfig;
        private AircraftConfig currentAircraftConfig;

        internal PidController SelectedController;

        private readonly Mutex configFileMutex = new Mutex();

        /// <summary>
        /// 
        /// Note:
        /// this service is constructed when the app starts
        /// (not when an aircraft is connected)
        /// and the same instance is used throught the lifetime of the app
        /// 
        /// </summary>
        public ConfigService(EventService eventService, AircraftLogicService aircraftLogicService)
        {
            this.aircraftLogicService = aircraftLogicService;

            // load default config
            Reset();

            // reset when app starts
            baseConfig.Reset();

            // init events
            eventService.AddHandler(EventTopic.CONFIG_UPDATE_FROM_UI, HandleConfigUpdate);
            eventService.AddHandler(EventTopic.DEV_CMD, HandleDevCmd);
        }

        internal void SetSimconnectBridge(SimconnectBridge simconnectBridge)
        {
            this.simconnectBridge = simconnectBridge;
        }

        public void Reset()
        {
            Console.WriteLine("[ConfigService] Reset");
            HandleAircraftTitle(DEFAULT_AIRCRAFT_TITLE);
            SelectedController = new PidController(0, 0, 0, 0, 0);
        }

        public AircraftConfig GetAircraftCfg()
        {
            return currentAircraftConfig;
        }

        public MiscConfig GetMiscCfg()
        {
            return currentAircraftConfig.misc;
        }

        public MiscTuneConfig GetMiscTuneCfg()
        {
            return currentAircraftConfig.misc.tune;
        }

        public SpeedConfig GetSpeeds()
        {
            return currentAircraftConfig.misc.speeds;
        }

        public AutoCruiseConfig GetAutoCruiseCfg()
        {
            return currentAircraftConfig.misc.autoCruise;
        }

        public FuelConfig GetFuelCfg()
        {
            return currentAircraftConfig.misc.fuelConfig;
        }

        public PidControlConfig GetPidControlCfg()
        {
            return currentAircraftConfig.pidControlConfig;
        }

        public SimData GetSimData()
        {
            return currentAircraftConfig.simData;
        }

        public AppConfig GetAppCfg()
        {
            return baseConfig.appConfig;
        }

        public FlightPlanConfig GetFlightPlanConfig()
        {
            return baseConfig.flightPlan;
        }

        /// <summary>
        /// this is called when:
        ///     Reset is called:
        ///         the app starts
        ///         we exit back to the MSFS main menu
        ///     a new aircraft is connected
        ///     supersonic toggle
        /// </summary>
        public void HandleAircraftTitle(string rawLiveryTitle)
        {
            Console.WriteLine();
            Console.WriteLine($"rawLiveryTitle: {rawLiveryTitle}");
            this.rawLiveryTitle = rawLiveryTitle;

            formattedAircraftTitle = FormatAircraftTitle(rawLiveryTitle);
            Console.WriteLine($"formattedAircraftTitle: {formattedAircraftTitle}");

            LoadAllConfig();

            // if we get here, the aircraft config was sucessfully loaded (may be default)
            if (aircraftLogicService != null)
            {
                aircraftLogicService.HandleAircraftConnection(GetAircraftCfg().name);
            }

            if (simconnectBridge != null && rawLiveryTitle != DEFAULT_AIRCRAFT_TITLE)
            {
                simconnectBridge.InitTargetAirspeed();
            }
        }

        private static string FormatAircraftTitle(string rawLiveryTitle)
        {
            // remove some livery info
            string result = rawLiveryTitle.ToUpper();
            foreach (var livery in LIVERY_NAMES)
            {
                result = result.Replace(livery.ToUpper(), "");
            }
            result = result.Trim();

            // remove invalid chars
            result = SanitizeChars(result);

            // handle special cases
            result = HandleSpecialLiveryCases(result);

            return result;
        }

        /// <summary>
        /// examples of invalid liveries:
        /// F/A 18
        /// Spirit Of St. Louis
        /// SIAI-Marchetti S.205
        /// Short SC.7 Skyvan (Skydive) Clown Fish Livery
        /// </summary>
        public static string SanitizeChars(string liveryTitle)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in liveryTitle)
            {
                if (Char.IsLetterOrDigit(c) || c == '-' || c == '_')
                {
                    sb.Append(c); // valid chars
                }
                else if (c == ' ')
                {
                    sb.Append("_"); // convert spaces
                }
            }
            return sb.ToString();
        }

        private static string HandleSpecialLiveryCases(string liveryTitle)
        {
            // Note: liveryTitle will be SCREAMING_SNAKE_CASE

            // handle any special cases here
            if (liveryTitle == "SR_22")
            {
                return "SR22";
            }
            if (liveryTitle == "DV_20")
            {
                return "DV20";
            }
            if (liveryTitle.Contains("DA_40"))
            {
                return liveryTitle.Replace("DA_40", "DA40");
            }
            if (liveryTitle.Contains("B787"))
            {
                return "BOEING_787";
            }
            if (liveryTitle.Contains("JU52"))
            {
                return liveryTitle.Replace("JU52", "JU_52");
            }
            else
            {
                return liveryTitle;
            }
        }

        /// <summary>
        /// 
        /// called when config was updated via the UI
        /// 
        /// </summary>
        /// <param name="myEvent"></param>
        private void HandleConfigUpdate(Event myEvent)
        {
            SaveConfig();
        }

        private void HandleDevCmd(Event myEvent)
        {
            var devCmd = (DevCommand)myEvent.data;

            if (devCmd.command == "FAST")
            {
                bool desiredState = devCmd.GetArgAsBool(0);
                ToggleFastTune(desiredState);
            }
        }

        /// <summary>
        ///
        /// plan:
        ///     add 2nd set of gains to config file
        ///     add thresholds to switch between the gains
        ///     
        /// </summary>
        /// <param name="desiredState"></param>
        private void ToggleFastTune(bool desiredState)
        {
            Console.WriteLine($"toggle FAST tune: {desiredState}");

            string aircraftName = currentAircraftConfig.name;
            if (aircraftName == DEFAULT_AIRCRAFT_TITLE)
            {
                return;
            }

            // todo: switch gains
        }

        public string SelectedControllerYaml
        {
            get
            {
                string formatted = YamlUtil.ObjToStr(SelectedController.getPidGains());
                return formatted;
            }
            set
            {
                // called when PID gains are changed in the UI
                Console.WriteLine($"new gains: {value}");
                PidGains gains = YamlUtil.StrToObj<PidGains>(value);
                SelectedController.LoadGains(gains);
            }
        }

        public string MiscAircraftParams
        {
            get
            {
                string formatted = YamlUtil.ObjToStr(currentAircraftConfig.misc);
                return formatted;
            }
            set
            {
                // called when params are changed in the UI
                Console.WriteLine($"new MiscAircraftParams: {value}");
                currentAircraftConfig.misc = YamlUtil.StrToObj<MiscConfig>(value);
            }
        }

        // for the UI
        public string AircraftNotes
        {
            get
            {
                return FormatNotes(currentAircraftConfig.notes);
            }
            set
            {
                // called when notes are changed in the UI
                string notes = FormatNotes(value);
                Console.WriteLine($"new AircraftNotes: {notes}");
                currentAircraftConfig.notes = FormatNotes(notes);
            }
        }

        // for the UI
        public string AircraftData
        {
            get
            {
                SimData simData = currentAircraftConfig.simData;
                return YamlUtil.ObjToStr(simData);
            }
            set
            {
                Console.WriteLine("todo: set AircraftData?");
            }
        }

        private static string FormatNotes(string notes)
        {
            notes = notes.Trim();
            notes = notes.Replace("\r\n", "\n");
            while (notes.Contains("\n\n\n"))
            {
                notes = notes.Replace("\n\n\n", "\n\n");
            }
            notes += "\n";
            return notes;
        }

        public string AppConfigStr
        {
            get
            {
                string formatted = YamlUtil.ObjToStr(baseConfig.appConfig);
                return formatted;
            }
            set
            {
                // called when params are changed in the UI
                Console.WriteLine($"new AppConfigStr: {value}");
                baseConfig.appConfig = YamlUtil.StrToObj<AppConfig>(value);
                baseConfig.AfterDeserialize();
            }
        }

        public string FlightPlanConfigStr
        {
            get
            {
                string formatted = YamlUtil.ObjToStr(baseConfig.flightPlan);
                return formatted;
            }
            set
            {
                // called when params are changed in the UI
                Console.WriteLine($"new FlightPlanConfigStr: {value}");
                baseConfig.flightPlan = YamlUtil.StrToObj<FlightPlanConfig>(value);
                baseConfig.AfterDeserialize();
            }
        }

        /// <summary>
        /// This should be called after updating config in memory.
        /// This will write the changes from memory into the YAML files.
        /// </summary>
        internal void SaveConfig()
        {
            Console.WriteLine("saving config...");
            lock (configFileMutex)
            {
                YamlUtil.ObjToFile(baseConfig, YamlUtil.CONFIG_FILE_PATH);
                YamlUtil.ObjToFile(currentAircraftConfig, YamlUtil.GetFullAircraftPath(currentAircraftFileBasename));

                Console.WriteLine($"config saved:\n  {currentAircraftFileBasename}");
            }
        }

        private void LoadBaseConfigFromFile()
        {
            Console.WriteLine("reading base config...");
            lock (configFileMutex)
            {
                baseConfig = YamlUtil.FileToObj<BaseConfig>(YamlUtil.CONFIG_FILE_PATH);
                Console.WriteLine("base config read from file!");
            }
        }

        private AircraftConfig LoadAircraftYaml(string aircraftFileBasename)
        {
            string fullPath = YamlUtil.GetFullAircraftPath(aircraftFileBasename);
            Console.WriteLine($"reading aircraft config: {fullPath}");
            AircraftConfig result;
            lock (configFileMutex)
            {
                result = YamlUtil.FileToObj<AircraftConfig>(fullPath);
                Console.WriteLine($"aircraft config read from: {fullPath}");
            }
            return result;
        }

        /// <summary>
        /// 
        /// note:
        /// formattedAircraftTitle will be set before this is called
        /// 
        /// </summary>
        private void LoadAllConfig()
        {
            // load base config
            LoadBaseConfigFromFile();

            // load aircraft config
            currentAircraftFileBasename = GetMatchingAircraftFileBasename();
            if (currentAircraftFileBasename != null)
            {
                // match found!
                Console.WriteLine($"found matching aircraft config file: {currentAircraftFileBasename}");
                currentAircraftConfig = LoadAircraftYaml(currentAircraftFileBasename);

                // reset bad defaults, etc
                currentAircraftConfig.OnLoad();

                // default notes
                if (currentAircraftConfig.notes.Length == 0)
                {
                    AircraftConfig defaultAircraft = LoadAircraftYaml(DEFAULT_AIRCRAFT_BASENAME);
                    currentAircraftConfig.notes = defaultAircraft.notes;
                    Console.WriteLine($"using default notes");
                }

                // save name/livery 
                currentAircraftConfig.name = currentAircraftFileBasename.Replace(".yaml", "");
                currentAircraftConfig.liveries.Remove(DEFAULT_AIRCRAFT_TITLE);
                currentAircraftConfig.liveries.Add(rawLiveryTitle);

                // get sim data
                LoadSimData(currentAircraftConfig.simData);

                // write to file
                SaveConfig();
            }
            else
            {
                // match not found, we'll need to add a new entry
                HandleNewAircraft();

                throw new ApplicationException();  // hack: this is so I know a new aircraft was added
            }
        }

        private void HandleNewAircraft()
        {
            // prompt user for name
            var dialog = new NewAircraftDialog();
            dialog.ResponseText = formattedAircraftTitle;
            if (dialog.ShowDialog() == true)
            {
                formattedAircraftTitle = dialog.ResponseText.ToUpper();
            }
            else
            {
                throw new ApplicationException();  // user clicked X
            }

            // load default from file
            currentAircraftConfig = LoadAircraftYaml(DEFAULT_AIRCRAFT_BASENAME);

            // save name/livery
            currentAircraftFileBasename = formattedAircraftTitle + ".yaml";
            currentAircraftConfig.name = formattedAircraftTitle;
            currentAircraftConfig.liveries.Clear();
            currentAircraftConfig.liveries.Add(rawLiveryTitle);

            // get sim data
            LoadSimData(currentAircraftConfig.simData);

            // write to file
            SaveConfig();
            Console.WriteLine($"ADDED NEW AIRCRAFT CONFIG FILE: {formattedAircraftTitle}");
            throw new ApplicationException();  // hack: this is so I know a new aircraft was added
        }

        private void LoadSimData(SimData simData)
        {
            string aircraftName = GetAircraftCfg().name;
            Console.WriteLine($"LoadSimData: {aircraftName}");
            if (aircraftName == DEFAULT_AIRCRAFT_TITLE)
            {
                return;
            }

            if (simconnectBridge == null || !simconnectBridge.AreVarsReady)
            {
                Console.WriteLine($"vars not ready");
                return;
            }

            simData.designSpeedStallLanding = simconnectBridge.GetInteger(SimVars.DESIGN_SPEED_VS_0);
            simData.designSpeedStallClean = simconnectBridge.GetDesignSpeedStallClean();
            simData.designSpeedMinRotation = simconnectBridge.GetInteger(SimVars.DESIGN_SPEED_MIN_ROTATION);
            simData.designSpeedTakeoff = simconnectBridge.GetInteger(SimVars.DESIGN_TAKEOFF_SPEED);
            simData.designSpeedClimb = simconnectBridge.GetInteger(SimVars.DESIGN_SPEED_CLIMB);

            simData.cruiseSpeedIndicated = simconnectBridge.GetCruiseSpeedIndicated();
            simData.cruiseSpeedTrue = simconnectBridge.GetCruiseSpeedTrue();
            simData.cruiseSpeedMach = simconnectBridge.GetCruiseSpeedMach();

            simData.designCruiseAltitude = simconnectBridge.GetInteger(SimVars.DESIGN_CRUISE_ALT);

            simData.designSpeedCruise = simconnectBridge.GetInteger(SimVars.DESIGN_SPEED_VC);
            simData.estimatedCruiseSpeed = simconnectBridge.GetInteger(SimVars.ESTIMATED_CRUISE_SPEED);
        }

        public static List<string> GetAllAircraftConfigBasenames()
        {
            List<string> result = new List<string>();
            foreach (string path in YamlUtil.GetAllAircraftConfigPaths())
            {
                string basename = Path.GetFileName(path);
                result.Add(basename);
            }
            return result;
        }

        private string GetMatchingAircraftFileBasename()
        {
            Console.WriteLine(
                $"[ConfigService] GetMatchingAircraftFile (formattedAircraftTitle={formattedAircraftTitle})");

            // get list of all available files
            List<string> aircraftConfigBasenames = GetAllAircraftConfigBasenames();

            // look for aircraft in config file
            foreach (string basename in aircraftConfigBasenames)
            {
                if (IsAircraftMatch(basename))
                {
                    Console.WriteLine($"[ConfigService] basename={basename}");
                    return basename;
                }
            }
            Console.WriteLine("[ConfigService] no match found!");
            return null;
        }

        public bool IsAircraftMatch(string potentialAircraftFileBasename)
        {
            string potentialConfigMatch = potentialAircraftFileBasename.Replace(".yaml", "");

            if (!formattedAircraftTitle.Contains(potentialConfigMatch))
            {
                return false;  // name does not match
            }
            if (is152Aero(formattedAircraftTitle) != is152Aero(potentialConfigMatch))
            {
                return false;  // AERO does not match
            }
            return true;
        }

        private bool is152Aero(string potentialConfigMatch)
        {
            return potentialConfigMatch.Contains("152") && potentialConfigMatch.Contains("AERO");
        }

        public bool IsAircraftLoaded()
        {
            return rawLiveryTitle != DEFAULT_AIRCRAFT_TITLE;
        }
    }
}
