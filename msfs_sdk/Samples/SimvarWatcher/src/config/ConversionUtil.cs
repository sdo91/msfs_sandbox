﻿
using System;
using System.IO;

namespace FsAutopilot.Config
{
    public class ConversionUtil
    {

        public static void RewriteAllAircraftFiles()
        {
            // get list of all available files
            string[] aircraftPaths = YamlUtil.GetAllAircraftConfigPaths();

            foreach (string path in aircraftPaths)
            {
                RewriteAircraftFile(path);
            }
        }

        public static void RewriteDefaultAircraftFile()
        {
            string fullPath = YamlUtil.GetFullAircraftPath(ConfigService.DEFAULT_AIRCRAFT_BASENAME);
            RewriteAircraftFile(fullPath);
        }

        public static void ResetDefaultAircraftConfigFile()
        {
            string c152Path = YamlUtil.GetFullAircraftPath("CESSNA_152.yaml");
            AircraftConfig cfg = YamlUtil.FileToObj<AircraftConfig>(c152Path);

            cfg.name = ConfigService.DEFAULT_AIRCRAFT_TITLE;
            cfg.liveries.Clear();
            cfg.liveries.Add(cfg.name);

            string defaultAircraftPath = YamlUtil.GetFullAircraftPath(ConfigService.DEFAULT_AIRCRAFT_BASENAME);
            YamlUtil.ObjToFile(cfg, defaultAircraftPath);
        }

        private static void RewriteAircraftFile(string aircraftConfigPath)
        {
            Console.WriteLine($"rewriting: {Path.GetFileName(aircraftConfigPath)}");

            // load from file
            AircraftConfig config = YamlUtil.FileToObj<AircraftConfig>(aircraftConfigPath);
            config.OnLoad();

            // process aircraft
            ProcessAircraft(config);

            // write back to file
            YamlUtil.ObjToFile(config, aircraftConfigPath);
        }

        /// <summary>
        /// 
        /// edit this method to do the conversions as needed!
        /// 
        /// </summary>
        private static void ProcessAircraft(AircraftConfig aircraftConfig)
        {
            aircraftConfig.name = ConfigService.SanitizeChars(aircraftConfig.name);

            // todo: add conversion logic here as needed
            // example:
            // aircraftConfig.misc.autoCruise.cruiseAlt = aircraftConfig.misc.cruiseAlt;

            //aircraftConfig.misc.fuelConfig.fuelFudgeFactor = aircraftConfig.misc.fuelFudgeFactor;
            //aircraftConfig.misc.fuelConfig.fuelCutoffPercentage = aircraftConfig.misc.fuelCutoffPercentage;
            //aircraftConfig.misc.fuelConfig.balanceFuel = aircraftConfig.misc.balanceFuel;
        }
    }
}
