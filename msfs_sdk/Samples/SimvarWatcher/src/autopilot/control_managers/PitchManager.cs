﻿using FsAutopilot.AircraftLogic;
using FsAutopilot.Config;
using FsAutopilot.Events;
using FsAutopilot.Hardware;
using FsAutopilot.Simconnect;
using FsAutopilot.Util;
using System;

namespace FsAutopilot.Autopilot
{
    public class PitchManager
    {
        internal PitchMode CurrentPitchMode = PitchMode.AltHold;

        internal PidController AltHoldController { get; private set; }
        internal PidController GlideSlopeController { get; private set; }
        internal PidController VertSpeedController { get; private set; }
        internal PidController FlcController { get; private set; }
        internal PidController PitchController { get; private set; }

        private bool usePitchRateController = true;
        internal PidController Pitch2Controller { get; private set; }  // uses pitch rate
        internal PidController PitchRateController { get; private set; }

        internal bool IsActive { private get; set; }

        private AircraftLogicService aircraftLogicService = null;
        private ConfigService configService = null;
        private EventService eventService = null;
        private JoystickService joystickService = null;
        private SimconnectBridge simconnectBridge = null;

        private bool isInverted_ = false;

        // these targets are from the UI?
        internal double TargetVertSpeed;
        internal double TargetPitch;

        private ThrottleManager throttleManager = null;

        private PidResponseData pidResponseData;

        private DateTime baroCheckTimestamp = TimeUtil.GetOldTimestamp();

        private AutoCruiseManager cruiseControl;

        public PitchManager(
            SimconnectBridge simconnectBridge,
            ConfigService configService,
            EventService eventService,
            JoystickService joystickService,
            AircraftLogicService aircraftLogicService)
        {
            Console.WriteLine("Constructing PitchManager");

            this.aircraftLogicService = aircraftLogicService;
            this.configService = configService;
            this.eventService = eventService;
            this.joystickService = joystickService;
            this.simconnectBridge = simconnectBridge;

            cruiseControl = new AutoCruiseManager(simconnectBridge, configService, eventService, aircraftLogicService);

            CurrentPitchMode = PitchMode.AltHold;

            AircraftConfig aircraftConfig = configService.GetAircraftCfg();
            PidControlConfig pidConfig = configService.GetPidControlCfg();
            MiscTuneConfig miscTuneConfig = configService.GetMiscTuneCfg();

            // use VS to control altitude
            AltHoldController = new PidController(pidConfig.altGains, 500);
            AltHoldController.logSettings("ALT controller", true, "ft/min", "ft");

            // use VS to control glide slope
            GlideSlopeController = new PidController(pidConfig.glideSlopeGains, 0);
            GlideSlopeController.logSettings("GlideSlope controller", true, "ft/min", "ft (above GS)");

            // use pitch to control vertical speed
            VertSpeedController = new PidController(pidConfig.vsGains);
            VertSpeedController.logSettings("VS controller", true, "degrees pitch", "ft/min");
            VertSpeedController.ScaleFactor = 0.001;

            // use pitch to control airspeed
            FlcController = new PidController(pidConfig.flcGains, GetMaxPitch());
            FlcController.logSettings("FLC controller", true, "degrees pitch", "knots");
            FlcController.invertOutput();

            // use elevator trim to control pitch
            PitchController = new PidController(pidConfig.pitchGains, miscTuneConfig.pitch.maxElevatorTrim);
            PitchController.logSettings("Pitch controller", true, "% elevator trim", "deg pitch");

            // use pitch rate to control pitch
            Pitch2Controller = new PidController(pidConfig.pitch2Gains, pidConfig.pitchGains.maxSetPointChangeRate);
            Pitch2Controller.logSettings("Pitch 2 controller", true, "deg/sec", "degrees pitch");
            Pitch2Controller.MaxSetPointChangeRate = 5;

            // use elevator trim to control pitch rate
            PitchRateController = new PidController(pidConfig.pitchRateGains, miscTuneConfig.pitch.maxElevatorTrim);
            PitchRateController.logSettings("Pitch Rate controller", true, "degrees trim", "deg/sec");
            PitchRateController.MaxSetPointChangeRate = 0.5;

            // apply params
            applyMiscParams();

            // init events
            eventService.AddHandler(EventTopic.CONFIG_UPDATE_FROM_UI, HandleConfigUpdate);
        }

        private void applyMiscParams()
        {
            MiscConfig miscConfig = configService.GetMiscCfg();
            MiscTuneConfig miscTuneConfig = configService.GetMiscTuneCfg();

            usePitchRateController = miscConfig.usePitchRate;

            VertSpeedController.SetControlVarMinMax(GetMaxPitch());
            //VertSpeedController.MaxSetPointChangeRate = miscTuneConfig.vertSpeed.maxDeltaG * ApUtil.G_FPMPS;

            PitchController.SetControlVarMinMax(miscTuneConfig.pitch.maxElevatorTrim);
            PitchController.MaxControlVarRate = miscTuneConfig.pitch.maxElevatorTrimRate;
            //PitchController.MaxSetPointChangeRate = miscTuneConfig.pitch.maxPitchRate;

            Pitch2Controller.SetControlVarMinMax(configService.GetPidControlCfg().pitchGains.maxSetPointChangeRate);
            PitchRateController.SetControlVarMinMax(miscTuneConfig.pitch.maxElevatorTrim);
            PitchRateController.MaxControlVarRate = miscTuneConfig.pitch.maxElevatorTrimRate;
        }

        private void HandleConfigUpdate(Event myEvent)
        {
            applyMiscParams();
        }

        internal void SetThrottleManager(ThrottleManager throttleManager)
        {
            this.throttleManager = throttleManager;
        }

        internal void SetPidResponseData(PidResponseData pidResponseData)
        {
            this.pidResponseData = pidResponseData;
        }

        private void CheckInverted()
        {
            double rollDeg = simconnectBridge.GetValue(SimVars.PLANE_BANK_DEGREES);
            bool inverted = Math.Abs(rollDeg) > 90;
            if (inverted != isInverted_)
            {
                isInverted_ = inverted;
                PitchController.invertOutput(inverted);
                PitchController.resetIntegralTerm();

                // todo: add support for pitch2
            }
        }

        internal void SetPitchMode(string selectedMode)
        {
            // todo: kick out if invalid
            // eg: AutoCruise invalid if flight plan destination isn't set

            CurrentPitchMode = AutopilotModes.ToPitchMode(selectedMode);
        }

        private double RunVertSpeedController()
        {
            VertSpeedController.SetControlVarMinMax(GetMaxPitch());

            VertSpeedController.SetPoint = CalcDesiredVertSpeed();
            VertSpeedController.ProcessVariable = simconnectBridge.GetCurrentVertSpeed();
            var desiredPitchDegs = VertSpeedController.ControlVariable(TimeUtil.DeltaTime);

            VertSpeedController.PopulateResponseData(pidResponseData.vertSpeed);

            Console.WriteLine();
            return desiredPitchDegs;
        }

        private double GetMaxPitch()
        {
            double maxPitchDegrees = configService.GetMiscTuneCfg().vertSpeed.maxPitch;
            if (ApUtil.IsNeoflyRunning())
            {
                double neoflyMax = 30;
                maxPitchDegrees = Math.Min(maxPitchDegrees, neoflyMax);
            }
            return maxPitchDegrees;
        }

        /// <summary>
        /// use pitch rate to control pitch
        /// </summary>
        private double RunPitch2Controller()
        {
            Pitch2Controller.SetPoint = calcDesiredPitchDeg();
            Pitch2Controller.ProcessVariable = simconnectBridge.GetValue(SimVars.PLANE_PITCH_DEGREES);
            var desiredPitchRate = Pitch2Controller.ControlVariable(TimeUtil.DeltaTime);

            Pitch2Controller.PopulateResponseData(pidResponseData.pitch);

            Console.WriteLine();
            return desiredPitchRate;
        }

        /// <summary>
        /// use elevator trim rate to control pitch rate
        /// </summary>
        private double RunPitchRateController(double desiredPitchRate)
        {
            PitchRateController.SetPoint = desiredPitchRate;
            PitchRateController.ProcessVariable = simconnectBridge.GetValue(SimVars.ALIAS_PITCH_RATE);
            var trimDegrees = PitchRateController.ControlVariable(TimeUtil.DeltaTime);

            PitchRateController.PopulateResponseData(pidResponseData.pitchRate);

            Console.WriteLine();
            return trimDegrees;
        }

        /// <summary>
        /// use VS to control altitude
        /// </summary>
        private double RunAltHoldController()
        {
            AltHoldController.SetPoint = GetTargetAltitude();
            AltHoldController.ProcessVariable = simconnectBridge.GetCurrentAltitude();
            var desiredVertSpeed = AltHoldController.ControlVariable(TimeUtil.DeltaTime);

            AltHoldController.PopulateResponseData(pidResponseData.altitude);
            CheckIfFlcNeeded();

            Console.WriteLine();
            return desiredVertSpeed;
        }

        /// <summary>
        /// this is to prevent the aircraft from stalling if it can't maintain speed
        /// 
        /// todo: use aircraft stall speed? (vs1)
        /// </summary>
        private void CheckIfFlcNeeded()
        {
            if (CurrentPitchMode != PitchMode.AltHold)
            {
                // this is to prevent switching from auto-cruise mode
                return;
            }

            if (simconnectBridge.IsHypersonicAircraft())
            {
                return; // for darkstar
            }

            double ftBelowTarget = pidResponseData.altitude.endGoal - pidResponseData.altitude.current;
            if (ftBelowTarget > 300) // todo: make this number configurable
            {
                double climbSpeed = GetDesiredFlcAirspeed();
                double knotsBelowClimbSpeed = climbSpeed - simconnectBridge.GetIndicatedAirspeed();
                if (knotsBelowClimbSpeed > 10)
                {
                    // need to switch to FLC
                    Console.WriteLine($"need to switch to FLC: speed={simconnectBridge.GetIndicatedAirspeed()}, alt={pidResponseData.altitude.current}");
                    eventService.FireEvent(EventTopic.CHANGE_PITCH_MODE, PitchMode.FlcMode);
                }
            }
        }

        private double RunGlideSlopeController()
        {
            GlideSlopeController.OutputMax = 100;  // no need to climb
            var estimatedGlideSlopeVertSpeed = ApUtil.CalcVertSpeedOfDescent(simconnectBridge.GetGroundSpeed(), 3);
            GlideSlopeController.OutputMin = 4 * estimatedGlideSlopeVertSpeed;

            int index = 1;

            double dme = ApUtil.GetBestIlsDist(simconnectBridge.GetTelemDict(), index);

            double degAboveGs = simconnectBridge.GetValueWithIndex(SimVars.NAV_GLIDE_SLOPE_ERROR, index);
            double ftAboveGs = ApUtil.CalcFeetAboveGlideslope(degAboveGs, dme);

            GlideSlopeController.SetPoint = configService.GetMiscTuneCfg().glideSlope.targetFtAboveGs;
            GlideSlopeController.ProcessVariable = ftAboveGs;

            var desiredVertSpeed = GlideSlopeController.ControlVariable(TimeUtil.DeltaTime);

            GlideSlopeController.PopulateResponseData(pidResponseData.glideSlope);

            // apply feed forward term
            double feedForwardTerm = ApUtil.Scale(ftAboveGs, -500, -150, 0, estimatedGlideSlopeVertSpeed);
            Console.WriteLine($"  feedForwardTerm={estimatedGlideSlopeVertSpeed:f1} FPM");
            desiredVertSpeed += estimatedGlideSlopeVertSpeed;

            Console.WriteLine();
            return desiredVertSpeed;
        }

        /// <summary>
        /// return target altitude in ft above MSL
        /// </summary>
        private double GetTargetAltitude()
        {
            double result = simconnectBridge.GetAltitudeBug();
            if (result < 0)
            {
                // if target alt is negative, invert and divide by 10
                // this way I can fly really close to the ground :)
                result *= -0.1;
                Console.WriteLine($"flipping target alt: {result}");
            }

            if (CurrentPitchMode == PitchMode.AglMode)
            {
                double groundAlt = simconnectBridge.GetGroundAltitude();
                result += groundAlt;  // convert AGL to MSL
            }
            if (CurrentPitchMode == PitchMode.AutoCruise)
            {
                result = cruiseControl.CalcAutoCruiseAltitude(TargetVertSpeed, true);
            }
            return result;
        }

        internal double CalcDesiredVertSpeed()
        {
            double desiredVertSpeed;
            switch (CurrentPitchMode)
            {
                case PitchMode.AltHold:
                case PitchMode.AglMode:
                    if (ShouldDoGlideSlope())
                    {
                        desiredVertSpeed = RunGlideSlopeController();
                    }
                    else
                    {
                        AltHoldController.SetControlVarMinMax(TargetVertSpeed);
                        desiredVertSpeed = RunAltHoldController();
                    }
                    break;
                case PitchMode.AutoCruise:
                    AltHoldController.SetControlVarMinMax(TargetVertSpeed);
                    if (cruiseControl.IsDescending())
                    {
                        AltHoldController.OutputMin = -Math.Abs(configService.GetAutoCruiseCfg().maxDescentRate);
                    }
                    desiredVertSpeed = RunAltHoldController();
                    break;
                case PitchMode.FlcMode:
                    // I think this was used before I auto-switched out of FLC mode
                    // not sure if it is needed anymore...
                    AltHoldController.SetControlVarMinMax(500);
                    desiredVertSpeed = RunAltHoldController();
                    break;
                default:
                    desiredVertSpeed = TargetVertSpeed;
                    break;
            }
            return desiredVertSpeed;
        }

        /// <summary>
        /// when to use GS mode:
        ///
        /// for now:
        /// ILS connected NAV_RAW_GLIDE_SLOPE not zero
        /// inside 10 nm
        ///
        /// maybe later:
        /// when above GS?
        /// after intercepting GS from below?
        /// when alt of gs point is lower than target altitude
        ///     will need to estimate estimate airfield alt
        /// must be close to localizer?
        /// use a joystick switch?
        ///
        ///
        /// </summary>
        private bool ShouldDoGlideSlope()
        {
            int index = 1;  // todo: add NAV2 support?

            if (!simconnectBridge.GetBoolWithIndex(SimVars.NAV_RAW_GLIDE_SLOPE, index))
            {
                return false;  // ILS not connected
            }

            double radialError = Math.Abs(simconnectBridge.GetValueWithIndex(SimVars.NAV_RADIAL_ERROR, index));
            if (radialError > 20)
            {
                return false;  // radial error too large
            }

            // check hdg angle
            double headingDeg = simconnectBridge.GetCurrentHeading();
            double locDeg = simconnectBridge.GetValueWithIndex(SimVars.NAV_LOCALIZER, index);
            double headingDiff = ApUtil.AbsAngleDiff(headingDeg, locDeg);
            if (headingDiff > 90)
            {
                return false;  // flying in the wrong direction
            }

            double dme = ApUtil.GetBestIlsDist(simconnectBridge.GetTelemDict(), index);
            double ilsDist = Math.Max(10, configService.GetAutoCruiseCfg().approachDistanceNm);
            return dme < ilsDist;
        }

        /// <summary>
        /// 
        /// NOTE: also used for auto climb
        /// 
        /// </summary>
        private double DoFlcMode()
        {
            double altAboveTarget = simconnectBridge.GetCurrentAltitude() - GetTargetAltitude();

            if (CurrentPitchMode == PitchMode.FlcMode)
            {
                if (IsFlcModeDone(altAboveTarget))
                {
                    // kick to ALT HOLD mode
                    eventService.FireEvent(EventTopic.CHANGE_PITCH_MODE, PitchMode.AltHold);

                    // todo: add logic for cleaner transition

                    // Once alt is reached, use vert speed
                    double desiredPitchDegrees = RunVertSpeedController();
                    return desiredPitchDegrees;
                }
            }

            // we should use FLC controller to change altitude

            // get airspeeds
            double desiredAirspeed = GetDesiredFlcAirspeed();

            double currentAirspeed;
            if (throttleManager.CurrentThrottleMode == ThrottleMode.IndicatedAirspeedHold)
            {
                currentAirspeed = simconnectBridge.GetValue(SimVars.AIRSPEED_INDICATED);
            }
            else
            {
                currentAirspeed = simconnectBridge.GetValue(SimVars.AIRSPEED_TRUE);
            }

            // set min/max pitch
            MiscTuneConfig miscTuneConfig = configService.GetMiscTuneCfg();
            if (altAboveTarget < 0)
            {
                // need to climb
                if (desiredAirspeed - currentAirspeed > 5)
                {
                    Console.WriteLine("Need to gain speed to climb");
                }
                FlcController.OutputMin = -GetMaxPitch();  // high angle
                FlcController.OutputMax = -miscTuneConfig.flc.flcGainSpeedAngle;  // low angle
            }
            else
            {
                // need to descend
                if (currentAirspeed - desiredAirspeed > 5)
                {
                    Console.WriteLine($"Need to lose speed to descend: currentSpeed={currentAirspeed}, desired={desiredAirspeed}");
                }
                FlcController.OutputMin = 0;  // high angle
                FlcController.OutputMax = GetMaxPitch();  // low angle
            }

            // calc pitch to control airspeed
            FlcController.SetPoint = desiredAirspeed;
            FlcController.ProcessVariable = currentAirspeed;
            var desiredPitch = FlcController.ControlVariable(TimeUtil.DeltaTime);

            FlcController.PopulateResponseData(pidResponseData.flc);

            Console.WriteLine();
            return desiredPitch;
        }

        /// <summary>
        /// cases to handle:
        /// descending
        /// climbing:
        ///     slower than VS
        ///     faster than VS
        /// 
        /// </summary>
        private bool IsFlcModeDone(double altAboveTarget)
        {
            if (altAboveTarget > 250)
            {
                // still descending
                return false;
            }

            double currentVertSpeed = simconnectBridge.GetCurrentVertSpeed();
            if (currentVertSpeed > TargetVertSpeed)
            {
                // climbing quickly
                return altAboveTarget > -500;
            }
            else
            {
                // climbing slowly
                return altAboveTarget > -50;
            }
        }

        private double GetDesiredFlcAirspeed()
        {
            Console.WriteLine("GetDesiredClimbAirspeed");

            // if need to decend, use target airspeed
            if (CurrentPitchMode == PitchMode.FlcMode)
            {
                if (GetTargetAltitude() < simconnectBridge.GetCurrentAltitude())
                {
                    // need to descend
                    double descentSpeed = throttleManager.TargetAirspeed;
                    Console.WriteLine($"  need to descend: using descentSpeed={descentSpeed}");
                    return descentSpeed;
                }
            }

            // todo: if target airspeed is lower, use target airspeed?

            // get climb speed
            double vClimbIndicated = aircraftLogicService.GetAircraft().GetClimbSpeedKias();

            // check highAltitudeClimbMach (for high altitude climb in jets)
            double vHighAltMach = configService.GetSpeeds().highAltitudeClimbMach;
            if (vHighAltMach > 0)
            {
                double vHighAltIndicated = simconnectBridge.ToKnotsIndicatedAirspeed(vHighAltMach);
                // NOTE: as I climb, vHighAltIndicated goes down
                if (vHighAltIndicated < vClimbIndicated)
                {
                    int vClimbDesigned = simconnectBridge.GetDesignSpeedClimb();
                    if (vHighAltIndicated > vClimbDesigned)
                    {
                        Console.WriteLine($"above mach threshold: {vHighAltMach} -> {(int)vHighAltIndicated} knots");
                        vClimbIndicated = vHighAltIndicated;
                    }
                    else
                    {
                        // ensure we climb above DESIGN_SPEED_CLIMB
                        Console.WriteLine($"using designSpeedClimb: {vClimbDesigned}");
                        vClimbIndicated = vClimbDesigned;
                    }
                }
            }

            // cap airspeed under 10k
            if (simconnectBridge.GetCurrentAltitude() < 11000)
            {
                var maxAirspeedUnder10k = configService.GetAppCfg().airspeedUnder10k;
                if (vClimbIndicated > maxAirspeedUnder10k)
                {
                    vClimbIndicated = maxAirspeedUnder10k;
                    Console.WriteLine($"airspeed limited under 10k: {vClimbIndicated}");
                }
            }

            // don't overspeed
            var barberPoleSpeed = simconnectBridge.GetVarAirspeedBarberPole();
            if (vClimbIndicated > barberPoleSpeed)
            {
                Console.WriteLine($"airspeed limited to barberPoleSpeed: {barberPoleSpeed}");
                vClimbIndicated = barberPoleSpeed;
            }

            // scale climb speed just after takeoff
            double altAboveTakeoff = simconnectBridge.GetAltitudeAboveTakeoff();
            int vRotate = simconnectBridge.GetRotationSpeed();
            double vScaled = ApUtil.Scale(
                altAboveTakeoff, 0, configService.GetMiscCfg().autoCruise.climbSpeedThresholdAgl, vRotate, vClimbIndicated);
            Console.WriteLine();
            return vScaled;
        }

        internal double calcDesiredPitchDeg()
        {
            if (CurrentPitchMode == PitchMode.PitchHold)
            {
                cruiseControl.CalcAutoCruiseAltitude(TargetVertSpeed, true);  // log autocruise info during climb
                return TargetPitch;
            }
            else if (CurrentPitchMode == PitchMode.FlcMode || cruiseControl.ShouldAutoClimb(CurrentPitchMode, TargetVertSpeed))
            {
                double desiredPitchDegrees = DoFlcMode();
                return desiredPitchDegrees;
            }
            else  // VS hold/ALT hold/etc
            {
                double desiredPitchDegrees = RunVertSpeedController();
                return desiredPitchDegrees;
            }
        }

        /// <summary>
        /// for large aircraft: uses pitch rate
        /// </summary>
        private double calcElevatorTrim2()
        {
            // first calc pitch rate
            double desiredPitchRate = RunPitch2Controller();

            // then elevator trim deg
            double elevatorTrimPct = RunPitchRateController(desiredPitchRate);
            return elevatorTrimPct;
        }

        internal void DoElevatorTrim()
        {
            if (CurrentPitchMode == PitchMode.Off)
            {
                return;
            }

            // takeoff check
            if (simconnectBridge.IsOnGroundBelowSpeed(simconnectBridge.GetRotationSpeed() * 0.75))
            {
                Console.WriteLine("[DoElevatorTrim] below rotation speed: skipping");
                return;
            }

            double elevatorTrimPct;
            if (usePitchRateController)
            {
                elevatorTrimPct = calcElevatorTrim2();
            }
            else
            {
                CheckInverted();

                // calc dynamic scale factor based on speed
                PitchController.ScaleFactor = 1;
                if (configService.GetMiscCfg().scalePitchWithSpeed)
                {
                    double currentSpeed = simconnectBridge.GetIndicatedAirspeed();
                    if (currentSpeed > 100)
                    {
                        PitchController.ScaleFactor = (100 / currentSpeed);
                    }
                }

                PitchController.SetPoint = calcDesiredPitchDeg();
                PitchController.ProcessVariable = simconnectBridge.GetValue(SimVars.PLANE_PITCH_DEGREES);
                elevatorTrimPct = PitchController.ControlVariable(TimeUtil.DeltaTime);

                PitchController.PopulateResponseData(pidResponseData.pitch);

                Console.WriteLine();
            }

            // set the value
            if (configService.GetMiscCfg().useElevatorPosition)
            {
                if (configService.GetMiscCfg().useVjoyThrottle)
                {
                    // A310
                    joystickService.SetElevator(elevatorTrimPct);
                }
                else
                {
                    // fly-by-wire aircraft
                    simconnectBridge.SetValue(SimVars.ELEVATOR_POSITION, elevatorTrimPct); // in percent
                }
            }
            else
            {
                // convert to degrees
                double elevatorTrimDeg = configService.GetSimData().maxElevatorTrimDegrees * (elevatorTrimPct / 100.0);
                simconnectBridge.SetValue(SimVars.ELEVATOR_TRIM_POSITION, elevatorTrimDeg); // in degrees
            }
        }

        internal bool isFlcActive()
        {
            if (!IsActive)
            {
                return false;
            }
            else if (CurrentPitchMode == PitchMode.FlcMode)
            {
                return true;
            }
            else if (CurrentPitchMode == PitchMode.AutoCruise)
            {
                return cruiseControl.ShouldAutoClimb(CurrentPitchMode, TargetVertSpeed);
            }
            else
            {
                return false;
            }
        }

        internal bool IsAutoCruising()
        {
            if (CurrentPitchMode != PitchMode.AutoCruise)
            {
                return false;
            }
            return cruiseControl.IsCruising();
        }

        internal bool IsCruising()
        {
            if (CurrentPitchMode == PitchMode.AutoCruise)
            {
                return cruiseControl.IsCruising();
            }
            else if (CurrentPitchMode == PitchMode.AltHold)
            {
                // compare current alt with buffer
                double diff = Math.Abs(GetTargetAltitude() - simconnectBridge.GetCurrentAltitude());
                return diff < configService.GetAutoCruiseCfg().cruiseAltBuffer;
            }
            else
            {
                return false;
            }
        }

        public void CheckBaro()
        {
            if (TimeUtil.IsTimedOut(baroCheckTimestamp, 5))
            {
                // sync baro
                baroCheckTimestamp = DateTime.Now;
                Console.WriteLine("setting baro!");
                simconnectBridge.fireSimEvent(SIM_EVENT.BAROMETRIC);

                // todo: put this on its own timer?
                simconnectBridge.fireSimEvent(SIM_EVENT.MIXTURE_SET_BEST);
            }
        }

        internal PitchMode GetPitchMode()
        {
            return CurrentPitchMode;
        }

        internal void PlanDescent()
        {
            if (CurrentPitchMode != PitchMode.AutoCruise)
            {
                simconnectBridge.EstimateTopOfDescentDistance(TargetVertSpeed);
            }
        }
    }
}
