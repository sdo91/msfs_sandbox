﻿using FsAutopilot.AircraftLogic;
using FsAutopilot.Config;
using FsAutopilot.Events;
using FsAutopilot.Hardware;
using FsAutopilot.Simconnect;
using FsAutopilot.Util;
using System;

namespace FsAutopilot.Autopilot
{
    public class ThrottleManager
    {
        internal PidController AirspeedController { get; private set; }

        internal ThrottleMode CurrentThrottleMode { get; private set; } = ThrottleMode.IndicatedAirspeedHold;

        // services
        private AircraftLogicService aircraftLogicService = null;
        private ConfigService configService = null;
        private JoystickService joystickService = null;
        private PitchManager pitchManager = null;
        private SimconnectBridge simconnectBridge = null;

        private bool isActive_ = false;

        private PidResponseData pidResponseData;

        public ThrottleManager(
            SimconnectBridge simconnectBridge,
            ConfigService configService,
            EventService eventService,
            PitchManager pitchManager,
            JoystickService joystickService,
            AircraftLogicService aircraftLogicService)
        {
            Console.WriteLine("Constructing ThrottleManager");

            if (joystickService == null)
            {
                throw new ApplicationException("joystickService is null");
            }

            this.aircraftLogicService = aircraftLogicService;
            this.configService = configService;
            this.joystickService = joystickService;
            this.pitchManager = pitchManager;
            this.simconnectBridge = simconnectBridge;

            AircraftConfig aircraftConfig = configService.GetAircraftCfg();
            PidControlConfig pidConfig = configService.GetPidControlCfg();
            MiscTuneConfig miscTuneCfg = configService.GetMiscTuneCfg();

            // use throttle to control airspeed
            AirspeedController = new PidController(
                pidConfig.airspeedGains.p,
                pidConfig.airspeedGains.i,
                pidConfig.airspeedGains.d,
                1,
                100);
            AirspeedController.logSettings("Airspeed Controller", true, "% throttle", "knots");
            AirspeedController.MaxControlVarRate = miscTuneCfg.airspeed.maxThrottleChangeRate;
            //airspeedController.UnlockIntegrator();

            // init events
            eventService.AddHandler(EventTopic.DEV_CMD, HandleDevCmd);
            eventService.AddHandler(EventTopic.CONFIG_UPDATE_FROM_UI, HandleConfigUpdate);
        }

        // todo: clean up
        internal double TargetAirspeed { get; set; }  // knots
        private double GetTargetAirspeed()
        {
            if (pitchManager.IsAutoCruising())
            {
                // auto cruise at alt
                double cruiseSpeed = configService.GetSpeeds().cruiseSpeed;

                if (cruiseSpeed < 0)
                {
                    cruiseSpeed = simconnectBridge.GetCruiseSpeedMach();
                }

                Console.WriteLine($"auto cruise @ {cruiseSpeed}", cruiseSpeed);
                cruiseSpeed = simconnectBridge.ToKnotsIndicatedAirspeed(cruiseSpeed);
                return cruiseSpeed;
            }

            return TargetAirspeed;
        }

        internal void SetPidResponseData(PidResponseData pidResponseData)
        {
            this.pidResponseData = pidResponseData;
        }

        private void HandleDevCmd(Event myEvent)
        {
            var devCmd = (DevCommand)myEvent.data;

            if (devCmd.command.StartsWith("THR"))  // THROTTLE
            {
                double throttlePercent = devCmd.GetArgAsDouble(0);
                SetThrottle(throttlePercent);
            }
        }

        private void HandleConfigUpdate(Event myEvent)
        {
            MiscTuneConfig miscTuneConfig = configService.GetMiscTuneCfg();

            AirspeedController.MaxControlVarRate = miscTuneConfig.airspeed.maxThrottleChangeRate;
        }

        public bool IsActive
        {
            get { return isActive_; }
            set
            {
                isActive_ = value;
                if (isActive_)
                {
                    Console.WriteLine("AutoThrottle ON");
                }
                else
                {
                    Console.WriteLine("AutoThrottle OFF");
                }
            }
        }

        internal void calcThrottlePosition()
        {
            if (!IsActive)
            {
                throw new ApplicationException();  // shouldn't happen
            }

            if (isFlcActive())
            {
                int targetAlt;
                if (pitchManager.CurrentPitchMode == PitchMode.FlcMode)
                {
                    targetAlt = simconnectBridge.GetAltitudeBug();
                }
                else
                {
                    targetAlt = configService.GetMiscCfg().autoCruise.cruiseAlt;
                }

                var currentAlt = simconnectBridge.GetCurrentAltitude();
                var miscConfig = configService.GetMiscCfg();
                var miscTuneCfg = configService.GetMiscTuneCfg();
                if (currentAlt < targetAlt)
                {
                    // need to climb
                    double altAboveTakeoff = simconnectBridge.GetAltitudeAboveTakeoff();
                    if (altAboveTakeoff < miscConfig.autoCruise.climbSpeedThresholdAgl)
                    {
                        // takeoff phase
                        SetThrottle(miscTuneCfg.airspeed.maxThrottleTakeoff);
                    }
                    else if (aircraftLogicService.GetAircraft().ShouldForceCruiseSettings())
                    {
                        SetThrottle(miscTuneCfg.airspeed.maxThrottleCruise);
                    }
                    else
                    {
                        SetThrottle(miscTuneCfg.airspeed.maxThrottleClimb);
                    }
                }
                else
                {
                    // need to descend
                    SetThrottle(GetMinThrottle());
                }
            }
            else
            {
                var currentSpeed = GetCurrentSpeed();
                RunAirspeedController(currentSpeed);
            }

            Console.WriteLine();
        }

        private double GetCurrentSpeed()
        {
            double knotsIndicated = simconnectBridge.GetValue(SimVars.AIRSPEED_INDICATED);

            if (knotsIndicated < 40)
            {
                // on ground
                return simconnectBridge.GetValue(SimVars.GROUND_VELOCITY);
            }

            // check for flaps
            if (simconnectBridge.GetValue(SimVars.ALIAS_FLAPS_DEG) > 0)
            {
                // use indicated
                return knotsIndicated;
            }

            // check for slow speed
            if (TargetAirspeed < 100)
            {
                // todo: make this less arbitrary?
                return knotsIndicated;
            }

            // use configured source
            string speedSource = configService.GetAppCfg().speedSource.ToUpper();
            if (speedSource.StartsWith("T"))
            {
                return simconnectBridge.GetValue(SimVars.AIRSPEED_TRUE);
            }
            else if (speedSource.StartsWith("G"))
            {
                return simconnectBridge.GetValue(SimVars.GROUND_VELOCITY);
            }
            else
            {
                return knotsIndicated;
            }
        }

        private void RunAirspeedController(double currentAirspeed)
        {
            var desiredAirspeed = GetTargetAirspeed();

            AirspeedController.OutputMin = GetMinThrottle();
            if (pitchManager.IsCruising() || aircraftLogicService.GetAircraft().ShouldForceCruiseSettings())
            {
                // use maxThrottleCruise when holding altitude
                AirspeedController.OutputMax = configService.GetMiscTuneCfg().airspeed.maxThrottleCruise;
            }
            else
            {
                // use maxThrottleClimb
                AirspeedController.OutputMax = configService.GetMiscTuneCfg().airspeed.maxThrottleClimb;
            }

            AirspeedController.SetPoint = desiredAirspeed;

            AirspeedController.ProcessVariable = currentAirspeed;

            double currentThrottle = GetThrottle();
            AirspeedController.CurrentControlVariable = currentThrottle;

            var throttlePercent = AirspeedController.ControlVariable(TimeUtil.DeltaTime);
            AirspeedController.PopulateResponseData(pidResponseData.airspeed);
            SetThrottle(throttlePercent);
        }

        private int GetMinThrottle()
        {
            bool isGearExtended = simconnectBridge.GetValue(SimVars.GEAR_TOTAL_PCT_EXTENDED) >= 1;
            if (isGearExtended)
            {
                return 0;
            }
            else
            {
                // to prevent LANDING GEAR audio
                return configService.GetMiscTuneCfg().airspeed.minThrottleGearUp;
            }
        }

        public double GetThrottle()
        {
            if (configService.GetMiscCfg().useVjoyThrottle)
            {
                // NOTE: for CRJ, GENERAL_ENG_THROTTLE_LEVER_POSITION does not reflect the joystick state
                if (IsActive)
                {
                    return joystickService.GetThrottle();
                }
                else
                {
                    // todo: get from throttle lever
                    return simconnectBridge.GetValueWithIndex(SimVars.GENERAL_ENG_THROTTLE_LEVER_POSITION, 1);
                }
            }
            else
            {
                return simconnectBridge.GetValueWithIndex(SimVars.GENERAL_ENG_THROTTLE_LEVER_POSITION, 1);
            }
        }

        private void SetThrottle(double throttlePercent)
        {
            if (configService.GetMiscCfg().useVjoyThrottle)
            {
                joystickService.SetThrottlePercent(throttlePercent);
            }
            else
            {
                int numEngines = simconnectBridge.GetInteger(SimVars.NUMBER_OF_ENGINES);
                for (int i = 1; i <= numEngines; i++)
                {
                    simconnectBridge.SetValue($"{SimVars.GENERAL_ENG_THROTTLE_LEVER_POSITION}:{i}", throttlePercent);
                }
            }
        }

        private bool isFlcActive()
        {
            if (pitchManager.isFlcActive())
            {
                return true;
            }

            // check if sim's FLC is active
            bool isSimFlcActive = simconnectBridge.GetBool(SimVars.AUTOPILOT_FLIGHT_LEVEL_CHANGE);
            isSimFlcActive &= simconnectBridge.GetBool(SimVars.AUTOPILOT_MASTER);
            return isSimFlcActive;
        }

        internal void SetThrottleMode(string selectedMode)
        {
            CurrentThrottleMode = AutopilotModes.ToThrottleMode(selectedMode);
            Console.WriteLine($"CurrentThrottleMode: {CurrentThrottleMode}");
        }
    }
}
