﻿using FsAutopilot.Config;
using FsAutopilot.Events;
using FsAutopilot.Simconnect;
using FsAutopilot.Util;
using System;

namespace FsAutopilot.Autopilot
{
    public class RollManager
    {
        private ConfigService configService;

        private PidResponseData pidResponseData;

        public RollManager(SimconnectBridge simconnectBridge, ConfigService configService, EventService eventService)
        {
            Console.WriteLine("Constructing RollManager");

            this.simconnectBridge = simconnectBridge;
            this.configService = configService;

            CurrentRollMode = RollMode.RollHold;

            AircraftConfig aircraftConfig = configService.GetAircraftCfg();
            PidControlConfig pidConfig = configService.GetPidControlCfg();
            MiscTuneConfig miscTuneConfig = configService.GetMiscTuneCfg();

            // calc heading needed to control Cross Track Error
            NavController = new PidController(pidConfig.navGains, configService.GetAppCfg().maxNavInterceptAngleDeg);
            NavController.logSettings("NAV controller", true, "degrees intercept", "ft XTK left");
            NavController.invertOutput();

            // calc roll needed to control heading
            HeadingController = new PidController(pidConfig.headingGains, miscTuneConfig.heading.maxRoll);
            HeadingController.logSettings("HDG controller", true, "degrees roll right", "degrees heading");
            HeadingController.SetShouldWrapSetPoint(true);

            // calc trim needed to control roll
            RollController = new PidController(pidConfig.rollGains, miscTuneConfig.roll.maxAileronTrim);
            RollController.logSettings("Roll controller", true, "% aileron right", "degrees roll right");

            // default
            TargetRollDeg = 0;

            // init events
            eventService.AddHandler(EventTopic.CONFIG_UPDATE_FROM_UI, configUpdateHandler);
        }

        internal void SetPidResponseData(PidResponseData pidResponseData)
        {
            this.pidResponseData = pidResponseData;
        }

        private RollMode CurrentRollMode;
        internal PidController NavController { get; private set; }
        internal PidController HeadingController { get; private set; }
        internal PidController RollController { get; private set; }

        private SimconnectBridge simconnectBridge;

        internal double TargetRollDeg { get; set; }

        private bool isHighRates = true;

        private bool isInvertRollLocked = false;

        // checkbox
        private bool isInverted_ = false;
        internal bool IsInverted
        {
            get
            {
                return isInverted_;
            }
            set
            {
                isInverted_ = value;
                isInvertRollLocked = true;
            }
        }

        private void configUpdateHandler(Event myEvent)
        {
            UpdateMaxInterceptAngle();

            updateMaxRoll();

            MiscTuneConfig miscTuneConfig = configService.GetMiscTuneCfg();
            RollController.SetControlVarMinMax(miscTuneConfig.roll.maxAileronTrim);
            RollController.MaxControlVarRate = miscTuneConfig.roll.maxAileronTrimRate;
            // maxRollRate set elsewhere
        }

        /// <summary>
        /// 
        /// just ILS for now
        /// 
        /// </summary>
        private void UpdateMaxInterceptAngle()
        {
            bool hasLocalizer = simconnectBridge.HasLocalizer(1);
            double radialErrorDeg = simconnectBridge.GetNavRadialError(1);
            double maxInterceptAngle = configService.GetAppCfg().maxNavInterceptAngleDeg;
            if (hasLocalizer && radialErrorDeg != 0)
            {
                // ILS available
                // calc angle needed to intercept with buffer
                double potentialInterceptAngle = CalcIlsInterceptAngle(radialErrorDeg);
                maxInterceptAngle = Math.Max(potentialInterceptAngle, maxInterceptAngle);
            }
            NavController.SetControlVarMinMax(maxInterceptAngle);
        }

        private double CalcIlsInterceptAngle(double radialErrorDegLeft)
        {
            var dmeNm = simconnectBridge.GetDmeDistanceNm(1);
            if (dmeNm < 0.01)
            {
                int fallback = configService.GetAppCfg().noDmeNavInterceptAngleDeg;
                if (fallback > 0)
                {
                    Console.WriteLine($"DME INVALID! Using {fallback} deg intercept angle.");
                    return fallback;
                }
                else
                {
                    // use distance estimate
                    dmeNm = simconnectBridge.GetBestIlsDist(1);
                    Console.WriteLine($"BEST ILS DIST: {dmeNm:f2}");
                }
            }

            return ApUtil.CalcNavInterceptAngle(radialErrorDegLeft, dmeNm, configService.GetAutoCruiseCfg().approachDistanceNm);
        }

        private void updateMaxRoll()
        {
            double maxRollDegrees = configService.GetMiscTuneCfg().heading.maxRoll;
            if (!isHighRates)
            {
                maxRollDegrees = Math.Min(maxRollDegrees, 15);
            }
            else if (ApUtil.IsNeoflyRunning())
            {
                double neoflyMax = configService.GetAppCfg().neoflyMaxRollDeg;
                maxRollDegrees = Math.Min(maxRollDegrees, neoflyMax);
            }
            Console.WriteLine($"maxRollDegrees: {maxRollDegrees}");
            HeadingController.SetControlVarMinMax(maxRollDegrees);
        }

        internal void SetHighRates(bool isHigh)
        {
            isHighRates = isHigh;
            updateMaxRoll();
        }

        /// <summary>
        /// called when roll mode dropdown changes
        /// (eg: can be triggered by knob)
        /// </summary>
        internal void SetRollMode(string selectedMode)
        {
            // update roll mode
            CurrentRollMode = AutopilotModes.ToRollMode(selectedMode);

            // follow-up actions:
            if (SimconnectBridge.IsReady(simconnectBridge))
            {
                if (CurrentRollMode == RollMode.Off)
                {
                    simconnectBridge.SetValue(SimVars.AILERON_TRIM_PCT, 0);
                    Console.WriteLine("reset aileron trim");
                }
                else if (CurrentRollMode == RollMode.Gps)
                {
                    CurrentRollMode = RollMode.Gps;
                    simconnectBridge.SetCdi("G");
                }
                else if (CurrentRollMode == RollMode.Vor)
                {
                    CurrentRollMode = RollMode.Vor;
                    simconnectBridge.SetCdi("V");
                }
            }
        }

        /// <summary>
        /// 
        /// This will auto-spin the HSI to desired heading. Inspired by HSI on the Mooney Ovation.
        /// 
        /// goal:
        ///     sync mooney's HSI HDG w/ GPS/ILS
        ///     works w/ Logitech FIP too
        ///
        /// todo: support nav2?
        ///
        /// in general:
        ///     I want to set CRS to reflect GPS_DRIVES_NAV_1
        ///     This is because the yellow arrow on the Logitech HSI reflects GPS_DRIVES_NAV_1
        ///
        /// Aircraft limitations:
        ///     CJ4:
        ///         CDI button doesn't fully work
        ///         can switch the VLOC1 (green)
        ///         can NOT switch back to GPS (magenta)
        ///         (workaround: use PFD MENU button, then knobs to switch back to GPS)
        ///     GPS_DRIVES_NAV_1 works in pitts/C152/etc (using GTN 750)
        ///         todo: what does works mean?
        /// 
        /// </summary>
        public void AutoAdjustNavCourse(bool is_active)
        {
            if (simconnectBridge.IsSimPaused)
            {
                return;
            }

            int navToAdjust = 1;  // this used to be in misc config

            // get desired course
            // todo: figure out how to do this with both AP(mooney) and non-AP(pitts) aircraft
            int desiredCourse = -1;
            if (simconnectBridge.HasGpsFlightPlan() && simconnectBridge.GpsDrivesCdi())
            {
                if (CurrentRollMode == RollMode.Vor)
                {
                    // exception: in VOR mode, we don't want to auto adjust CRS
                    ApUtil.ConsoleLog("AutoAdjustNavCourse: can't use GPS track: VOR is active", is_active);
                }
                else
                {
                    // use GPS
                    desiredCourse = (int)Math.Round(simconnectBridge.GetGpsDesiredTrackDeg());
                    ApUtil.ConsoleLog($"AutoAdjustNavCourse: using GPS track: {desiredCourse}", is_active);
                }
            }
            else if (simconnectBridge.HasLocalizer(1) && !simconnectBridge.GpsDrivesCdi())
            {
                // use localizer
                desiredCourse = simconnectBridge.GetInteger(SimVars.NAV_LOCALIZER_1);
                ApUtil.ConsoleLog($"AutoAdjustNavCourse: using localizer: {desiredCourse}", is_active);
            }

            if (desiredCourse < 0)
            {
                return;  // don't sync
            }

            // fix: sync nav1 crs: 0 -> 360
            if (desiredCourse == 360)
            {
                desiredCourse = 0;
            }

            // sync
            uint currentCourse = simconnectBridge.GetUnsigned($"{SimVars.NAV_OBS}:{navToAdjust}");
            if (currentCourse != desiredCourse)
            {
                Console.WriteLine($"sync nav{navToAdjust} crs: {currentCourse} -> {desiredCourse}");
                simconnectBridge.SetNavRadial(navToAdjust, (uint)desiredCourse);
            }
        }

        /// <summary>
        /// NOTE: caller should check HasGpsFlightPlan()
        /// </summary>
        private double CalcGpsCourseToSteer()
        {
            double currentXtkNmLeft = simconnectBridge.GetGpsXtkLeft();

            if (configService.GetMiscCfg().useCustomGpsNav)
            {
                // run custom NAV controller
                NavController.SetControlVarMinMax(configService.GetAppCfg().maxNavInterceptAngleDeg);
                double gpsDesiredTrack = simconnectBridge.GetGpsDesiredTrackDeg();
                double gpsCrsToSteer = RunNavController(currentXtkNmLeft, gpsDesiredTrack);
                return gpsCrsToSteer;
            }
            else
            {
                // use provided CRS to steer (INOP on some aircraft)
                double gpsCrsToSteer = simconnectBridge.GetGpsHeadingToSteer();
                Console.WriteLine($"gpsCrsToSteer: {gpsCrsToSteer:F2}");
                ApUtil.LogXtk(currentXtkNmLeft);
                return gpsCrsToSteer;
            }
        }

        private double RunNavController(double currentXtkNmLeft, double targetCrsDeg)
        {
            // do some Integrator hacks
            PreventRollOscillation();
            DoNavAlignmentCheck();

            // calc intercept angle
            NavController.SetPoint = GetDesiredXtkFtLeft();
            NavController.ProcessVariable = currentXtkNmLeft * ApUtil.NM_TO_FT;
            double interceptAngleDeg = NavController.ControlVariable(TimeUtil.DeltaTime);
            NavController.PopulateResponseData(pidResponseData.navigation);

            // calc dynamic scale factor based on speed
            double groundSpeed = simconnectBridge.GetGroundSpeed();
            NavController.ScaleFactor = (1 / groundSpeed);

            // calc desired heading
            double desiredHeadingDeg = targetCrsDeg + interceptAngleDeg;
            desiredHeadingDeg = ApUtil.WrapToMatch(desiredHeadingDeg, 180);

            // log summary
            Console.WriteLine($"[CalcDesiredNavTrack] ScaleFactor={NavController.ScaleFactor:F4}");
            Console.WriteLine($"{targetCrsDeg:F2} (goal CRS) " +
                $"+ {interceptAngleDeg:F2} (intercept) " +
                $"= {desiredHeadingDeg:F2} (CRS to steer)");
            ApUtil.LogXtk(currentXtkNmLeft);

            return desiredHeadingDeg;
        }

        private double GetDesiredXtkFtLeft()
        {
            // don't apply while using ILS
            if (CurrentRollMode == RollMode.Vor && simconnectBridge.HasLocalizer(1))
            {
                Console.WriteLine("[CalcDesiredInterceptTimeSec] skip for ILS");
                return 0;
            }

            // get desired offset in NM
            var desiredXtkLeft = -configService.GetAppCfg().desiredXtkRight;
            if (Math.Abs(desiredXtkLeft) < 100)
            {
                desiredXtkLeft *= ApUtil.NM_TO_FT;  // convert from NM to ft
            }
            return desiredXtkLeft;
        }

        private void DoNavAlignmentCheck()
        {
            // XTK should be small for several seconds before we start applying I
            double secCloseToNavLine = NavController.GetSecondsInsideMaxErrorForI();
            if (secCloseToNavLine > 0 && secCloseToNavLine < 10) // todo: add to config
            {
                NavController.resetIntegralTerm();
                Console.WriteLine($"[DoNavAlignmentCheck] resetting I: secCloseToNavLine={secCloseToNavLine}");
            }
        }

        private void PreventRollOscillation()
        {
            // hack: this is to prevent roll oscillation while at high speed/altitude (eg: TBM)

            // I term needed for ILS
            if (CurrentRollMode == RollMode.Vor && simconnectBridge.HasLocalizer(1))
            {
                return;
            }

            // I term needed for VOR?
            if (CurrentRollMode == RollMode.Vor && simconnectBridge.IsNavConnected(1))
            {
                return;
            }

            // I term OK when going slow
            if (simconnectBridge.GetIndicatedAirspeed() < 152.5)  // todo: move to config?
            {
                return;
            }

            // disable I term for GPS/ILS nav for fast aircraft (eg: TBM)
            NavController.resetIntegralTerm();
        }

        internal void UpdateHeadingBug()
        {
            if (simconnectBridge.IsSimPaused)
            {
                return;
            }
            Console.WriteLine("UpdateHeadingBug");

            if (CurrentRollMode == RollMode.Gps)
            {
                if (simconnectBridge.HasGpsFlightPlan())
                {
                    // calc desired course
                    var gpsCrsToSteer = CalcGpsCourseToSteer();

                    // update HDG bug
                    gpsCrsToSteer = ApUtil.RoundTo(gpsCrsToSteer, 0.01);
                    gpsCrsToSteer = ApUtil.WrapToMatch(gpsCrsToSteer, 180);
                    simconnectBridge.SetHeadingBug(gpsCrsToSteer);
                }
                else
                {
                    Console.WriteLine("no GPS flight plan!");
                }
            }
            else if (CurrentRollMode == RollMode.Vor)
            {
                UpdateHeadingBugForVorMode();
            }
            else if (CurrentRollMode == RollMode.RollHold)
            {
                if (simconnectBridge != null)
                {
                    simconnectBridge.SyncHeadingBug();
                }
            }
            else
            {
                // round the heading bug's value
                var currentHdg = simconnectBridge.GetHeadingBug();
                var roundedHdg = Math.Round(currentHdg);
                if (currentHdg != roundedHdg)
                {
                    simconnectBridge.SetHeadingBug(roundedHdg);
                }
            }
            Console.WriteLine();
        }

        private void UpdateHeadingBugForVorMode()
        {
            FlightPlanConfig flightPlanCfg = configService.GetFlightPlanConfig();
            string vorToDestinationVector = flightPlanCfg.vorToDestinationVector;
            if (vorToDestinationVector.Length > 0)
            {
                UpdateHeadingBugToVorOffset(vorToDestinationVector);
            }
            else
            {
                UpdateHeadingBugToOrFromVor();
            }
        }

        private void UpdateHeadingBugToOrFromVor()
        {
            // NOTE: for VOR this isn't exact, but it should be within a degree or two
            // The necessary track will change as we fly along the radial
            double targetCrsDeg = simconnectBridge.GetNavCourse(1);

            var radialErrorDeg = simconnectBridge.GetNavRadialError(1);
            if (simconnectBridge.IsNavConnected(1))
            {
                // calc intercept angle
                UpdateMaxInterceptAngle();

                // calc current Cross Track Error
                var dmeDistance = simconnectBridge.GetDmeDistanceNm(1);
                var currentXtkError = ApUtil.CalcXtkLeft(radialErrorDeg, dmeDistance);

                // calc desired track
                double desiredVorTrack = RunNavController(currentXtkError, targetCrsDeg);

                // set heading bug
                simconnectBridge.SetHeadingBug(desiredVorTrack);
            }
            else if (configService.GetAppCfg().useCrsIfNoVorLock)
            {
                simconnectBridge.SetHeadingBug(targetCrsDeg);
            }
            Console.WriteLine($"CRS={targetCrsDeg}, radialErrorDeg={radialErrorDeg}");
        }

        private void UpdateHeadingBugToVorOffset(string vorToDestinationVector)
        {
            if (!simconnectBridge.IsNavConnected(1))
            {
                Console.WriteLine("waiting for VOR signal...");
                return;
            }

            Vector2D vor_to_destination = Vector2D.CreateFromRadialStr(vorToDestinationVector);
            Console.WriteLine($"vor_to_destination: {vor_to_destination.toString()}");

            double radial_vor_to_ownship = simconnectBridge.GetNavRadial(1);
            double distance_vor_to_ownship = simconnectBridge.GetDmeDistanceNm(1);
            Vector2D vor_to_ownship = Vector2D.CreateFromRadial(distance_vor_to_ownship, radial_vor_to_ownship);
            Console.WriteLine($"vor_to_ownship: {vor_to_ownship.toString()}");

            Vector2D ownship_to_destination = ApUtil.CalcVectorToVorOffsetPoint(vor_to_ownship, vor_to_destination);
            double angle_deg = ownship_to_destination.AngleDeg();
            angle_deg = ApUtil.RoundTo(angle_deg, 0.01);
            Console.WriteLine($"ownship_to_destination: {ownship_to_destination.toString()}");

            simconnectBridge.SetHeadingBug(angle_deg);
            Console.WriteLine();
        }

        /// <summary>
        /// 
        /// this is to prevent the AP from changing its mind about direction mid-roll
        /// 
        /// algo:
        /// on invert: lock desired roll to 0 or 180
        /// once roll is within max roll, unlock
        /// 
        /// eg: 
        /// assume max is 30
        /// on invert, lock to 180, plane gets to 150, unlock
        /// 
        /// todo: also wait for positive rate of climb?
        /// 
        /// </summary>
        private bool CheckInvertRollLock()
        {
            if (isInvertRollLocked)
            {
                // need to check if we can unlock

                // get effective bank angle
                var effectiveBankDeg = simconnectBridge.GetValue(SimVars.PLANE_BANK_DEGREES);
                if (IsInverted)
                {
                    // flip and wrap
                    effectiveBankDeg = ApUtil.WrapToMatch(effectiveBankDeg + 180, 0);
                }
                effectiveBankDeg = Math.Abs(effectiveBankDeg);

                // compare
                var maxRollDeg = configService.GetMiscTuneCfg().heading.maxRoll;
                if (effectiveBankDeg < maxRollDeg)
                {
                    // we can unlock!
                    isInvertRollLocked = false;
                }
            }
            return isInvertRollLocked;
        }

        private bool ShouldUseTrack()
        {
            if (CurrentRollMode == RollMode.Gps)
            {
                // maintain track for custom NAV, heading otherwise
                return configService.GetMiscCfg().useCustomGpsNav;
            }
            else if (CurrentRollMode == RollMode.Vor)
            {
                return true;
            }
            else
            {
                return configService.GetAppCfg().useTrackInHeadingMode;
            }
        }

        private double CalcDesiredRollDeg()
        {
            if (CheckInvertRollLock())
            {
                // roll is locked
                double lockedRoll = IsInverted ? 180 : 0;
                Console.WriteLine($"lockedRoll: {lockedRoll}");
                return lockedRoll;
            }

            // todo: auto calc maxSetPointError based on maxRoll

            var desiredRoll = TargetRollDeg;
            if (CurrentRollMode != RollMode.RollHold)
            {
                // heading/nav mode

                var desiredHeadingDeg = simconnectBridge.GetHeadingBug();
                HeadingController.SetPoint = desiredHeadingDeg;

                double currentHeadingDeg;
                if (ShouldUseTrack())  // control track instead of heading
                {
                    HeadingController.SetName("Track controller");
                    currentHeadingDeg = simconnectBridge.GetCurrentTrack();
                }
                else
                {
                    HeadingController.SetName("HDG controller");
                    currentHeadingDeg = simconnectBridge.GetCurrentHeading();
                }
                currentHeadingDeg = ApUtil.WrapToMatch(currentHeadingDeg, desiredHeadingDeg);
                HeadingController.ProcessVariable = currentHeadingDeg;

                HeadingController.CurrentControlVariable = simconnectBridge.GetVarRoll();

                desiredRoll = HeadingController.ControlVariable(TimeUtil.DeltaTime);

                HeadingController.PopulateResponseData(pidResponseData.heading);
                pidResponseData.heading.wildCard = simconnectBridge.GetGpsTrackToSteer();
            }

            // check if inverted
            if (IsInverted)
            {
                // NOTE: Don't wrap the angle here! It will cause issues!
                // The wrapping is done in CalcAileronTrim()
                desiredRoll += 180;
            }

            Console.WriteLine();
            return desiredRoll;
        }

        internal void DoAileronTrim()
        {
            if (CurrentRollMode == RollMode.Off)
            {
                return;
            }

            // if invert locked, allow quick roll
            var maxRollRateDeg = configService.GetPidControlCfg().rollGains.maxSetPointChangeRate;
            if (isInvertRollLocked)
            {
                maxRollRateDeg = Math.Max(maxRollRateDeg, 360);
                Console.WriteLine($"maxRollRateDeg: {maxRollRateDeg}");
            }
            RollController.MaxSetPointChangeRate = maxRollRateDeg;

            // calc desired roll
            var desiredRoll = CalcDesiredRollDeg();
            RollController.SetPoint = desiredRoll;

            // calc dynamic scale factor based on speed
            RollController.ScaleFactor = 1;
            if (configService.GetMiscCfg().scaleRollWithSpeed)
            {
                double currentSpeed = simconnectBridge.GetIndicatedAirspeed();
                if (currentSpeed > 100)
                {
                    RollController.ScaleFactor = (100 / currentSpeed);
                }
                Console.WriteLine($"[CalcAileronTrim] ScaleFactor={RollController.ScaleFactor:f4}");
            }

            // calc desired aileron
            var currentBankDeg = simconnectBridge.GetValue(SimVars.PLANE_BANK_DEGREES);
            currentBankDeg = ApUtil.WrapToMatch(currentBankDeg, desiredRoll);
            RollController.ProcessVariable = currentBankDeg;

            RollController.CurrentControlVariable = simconnectBridge.GetVarAileronPosition();

            var aileronPct = RollController.ControlVariable(TimeUtil.DeltaTime);

            RollController.PopulateResponseData(pidResponseData.roll);

            // set values
            simconnectBridge.SetValue(SimVars.AILERON_POSITION, aileronPct);
            simconnectBridge.SetValue(SimVars.AILERON_TRIM_PCT, 0);
            Console.WriteLine();
        }

        internal RollMode GetCurrentRollMode()
        {
            return CurrentRollMode;
        }
    }
}
