﻿using FsAutopilot.Config;
using FsAutopilot.Events;
using FsAutopilot.Simconnect;
using FsAutopilot.Util;
using System;

namespace FsAutopilot.Autopilot
{

    public class PauseManager
    {

        private SimconnectBridge simconnectBridge = null;
        private ConfigService configService = null;
        private EventService eventService = null;

        private Timestamp timeToPause = Timestamp.Zero();
        private double pauseMinutesBeforeLanding = -1;

        public PauseManager(ConfigService configService, EventService eventService)
        {
            Console.WriteLine("Constructing PauseManager");

            this.configService = configService;
            this.eventService = eventService;

            // events
            eventService.AddHandler(EventTopic.DEV_CMD, HandleDevCmd);
        }

        internal void SetSimconnectBridge(SimconnectBridge simconnectBridge)
        {
            this.simconnectBridge = simconnectBridge;
        }

        private void HandleDevCmd(Event myEvent)
        {
            var devCmd = (DevCommand)myEvent.data;

            if (devCmd.command == "PAUSE")
            {
                handlePauseCmd(devCmd);
            }
        }

        // pause:15 (pause in 15 minutes)
        private void handlePauseCmd(DevCommand devCmd)
        {
            try
            {
                // queue pause
                int minutes = Int32.Parse(devCmd.args[0]);
                if (minutes > 0)
                {
                    Console.WriteLine($"will pause in {minutes} min");
                    timeToPause = Timestamp.FutureMinutes(minutes);
                    pauseMinutesBeforeLanding = -1;
                }
                else
                {
                    timeToPause = Timestamp.Zero();
                    pauseMinutesBeforeLanding = Math.Abs(minutes);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"[handlePauseCmd] ex: ${devCmd.ToString()}");
                // todo
            }
        }

        private void PauseSim()
        {
            eventService.FireEvent(EventTopic.IR_REMOTE_COMMAND, "PAUSE_SIM");
        }

        public void CheckPause(bool isActive, RollMode currentRollMode)
        {
            // todo: check if landed
            // if so, clear queue'd pauses

            if (!isActive)
            {
                return;
            }

            if (timeToPause.IsWithinSec(3600))
            {
                Console.WriteLine($"will pause in {timeToPause.GetMinutesToPause()} min");
            }

            // check if que'd pause has arrived
            if (timeToPause.IsWithinSec(5))
            {
                timeToPause = Timestamp.Zero();
                Console.WriteLine("time to pause 1");
                PauseSim();
            }

            // check if negative pause has arrived
            if (pauseMinutesBeforeLanding > 0)
            {
                double eteLanding = simconnectBridge.GetGpsMinutesToLanding();
                Console.WriteLine($"will pause @ {pauseMinutesBeforeLanding} min before landing (ete={eteLanding:F1})");
                if (eteLanding < pauseMinutesBeforeLanding)
                {
                    if (eteLanding <= 0)
                    {
                        // invalid ETE to destination (eg: we are on the ground, or just modified the flight plan)
                        Console.WriteLine($"invalid ETE to destination: {eteLanding}");
                        pauseMinutesBeforeLanding = -1;
                    }
                    else
                    {
                        Console.WriteLine("time to pause before landing");
                        Console.WriteLine($"eteLanding={eteLanding} < pauseMinutesBeforeLanding={pauseMinutesBeforeLanding}");
                        pauseMinutesBeforeLanding = -1;
                        PauseSim();
                    }
                }
            }

            QueueNeoflyAutoPause();

            CheckNavFreq();
            CheckForIlsLock(currentRollMode);

            // todo
            CheckAgl();
        }

        private void QueueNeoflyAutoPause()
        {
            // todo: why neofly?
            // should I do this always?

            int pauseMinutes = configService.GetFlightPlanConfig().pauseAtEte;
            if (pauseMinutes < 1)
            {
                return; // disabled
            }

            // check if we should queue auto-pause
            if (pauseMinutesBeforeLanding == pauseMinutes)
            {
                return; // already queue'd
            }

            if (!ApUtil.IsNeoflyRunning())
            {
                return; // neofly not active
            }

            if (simconnectBridge.GetGroundSpeed() < 15)
            {
                return;
            }

            double eteLanding = simconnectBridge.GetGpsMinutesToLanding();
            if (eteLanding > (pauseMinutes + 1))
            {
                // do it
                Console.WriteLine($"Neofly Auto-Pause q'd! eteLanding={eteLanding}, gndSpeed={simconnectBridge.GetGroundSpeed()}");
                pauseMinutesBeforeLanding = pauseMinutes;
            }
        }

        /// <summary>
        ///
        /// goal:
        ///     don't CFIT while autopilot/autocruise is active
        ///
        /// pause if:
        ///     we are close to the ground
        ///     we have flaps up + gear up
        ///     we didn't just take off (check climbing? FLC mode?)
        /// 
        /// </summary>
        private void CheckAgl()
        {
            if (simconnectBridge.GetAltitudeAgl() < configService.GetFlightPlanConfig().pauseAtAgl)
            {
                // we are below pauseAtAgl
                // todo: something
            }
        }

        private void CheckNavFreq()
        {
            double pauseOnFreqLock = configService.GetFlightPlanConfig().pauseOnFreqLock;
            if (pauseOnFreqLock < 0)
            {
                return;
            }

            double nav1 = simconnectBridge.GetNavFrequency(1);
            Console.WriteLine($"pauseOnFreqLock={pauseOnFreqLock}, current={nav1}");

            double diff = Math.Abs(nav1 - pauseOnFreqLock);
            if (diff < 0.001)
            {
                configService.GetFlightPlanConfig().pauseOnFreqLock = -1;
                eventService.TriggerUiUpdate();
                PauseSim();
            }
        }

        /// <summary>
        ///
        /// goal:
        ///     pause on ILS lock (for VOR flights)
        ///
        /// conditions:
        ///     vor mode active
        ///     ils locked
        ///     dme crosses under approachDistanceNm
        ///
        /// bonus:
        ///     use dme
        ///     only when no gps flight plan?
        ///     on crs
        ///     use time
        ///
        /// </summary>
        private void CheckForIlsLock(RollMode currentRollMode)
        {
            if (!configService.GetFlightPlanConfig().pauseOnIlsLock)
            {
                return;
            }

            // check if VOR mode is active
            if (currentRollMode != RollMode.Vor)
            {
                return;
            }

            // check if ILS is locked
            if (!simconnectBridge.HasLocalizer(1))
            {
                return;
            }

            // check DME
            double dmeNm = simconnectBridge.GetBestIlsDist(1);
            double bufferDistanceNm = simconnectBridge.calcBufferDistanceNm(10);

            bool shouldPause = ApUtil.HasCrossedUnder(
                prev_ils_dist_nm,
                dmeNm,
                configService.GetAutoCruiseCfg().approachDistanceNm + 1,
                bufferDistanceNm);
            prev_ils_dist_nm = dmeNm;

            Console.WriteLine($"[CheckForIlsLock] dmeNm={dmeNm:f2}, bufferDistanceNm={bufferDistanceNm:f2}");
            if (shouldPause)
            {
                PauseSim();
            }
        }
        private double prev_ils_dist_nm = 0;

    }
}
