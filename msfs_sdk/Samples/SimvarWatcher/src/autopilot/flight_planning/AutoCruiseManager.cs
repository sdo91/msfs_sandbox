﻿using FsAutopilot.AircraftLogic;
using FsAutopilot.Config;
using FsAutopilot.Events;
using FsAutopilot.Simconnect;
using FsAutopilot.Util;
using System;

namespace FsAutopilot.Autopilot
{
    public class AutoCruiseManager
    {
        private AircraftLogicService aircraftLogicService = null;
        private ConfigService configService = null;
        private EventService eventService = null;
        private SimconnectBridge simconnectBridge = null;

        private bool is_descending = false;

        public AutoCruiseManager(
            SimconnectBridge simconnectBridge,
            ConfigService configService,
            EventService eventService,
            AircraftLogicService aircraftLogicService)
        {
            Console.WriteLine("Constructing AutoCruiseManager");

            this.aircraftLogicService = aircraftLogicService;
            this.configService = configService;
            this.eventService = eventService;
            this.simconnectBridge = simconnectBridge;
        }

        /// <summary>
        /// 
        /// calc target altitude for auto cruise mode
        /// for most of the flight this is 'cruiseAlt'
        /// at the end of the flight we need to calculate the altitude to manage the descent
        /// 
        /// let "descent target point" (DTP) be the point:
        ///     approachDistanceNm away from airport
        ///     @ target alt (not cruise alt)
        ///     
        /// let "descent start point" (DSP) be the point:
        ///     where we need to start the descent to arrive at DTP
        ///     
        /// let the descent triangle be the triangle:
        ///     +               <-- DSP
        ///     |   \
        ///     |       \
        ///     +-----------+   <-- DTP
        /// 
        /// </summary>
        internal double CalcAutoCruiseAltitude(double target_vert_speed_fpm, bool verbose)
        {
            ApUtil.ConsoleLog("CalcAutoCruiseAltitude", verbose);

            // setup
            AutoCruiseConfig cruiseCfg = configService.GetAutoCruiseCfg();
            double distance_buffer = configService.GetFlightPlanConfig().autoCruiseDescentBufferNm;
            if (configService.GetFlightPlanConfig().isLandingAtNextWaypoint)
            {
                ApUtil.ConsoleLog("  landing at next: not applying approachDistanceNm", verbose);
            }
            else
            {
                distance_buffer += cruiseCfg.approachDistanceNm;
            }
            int altitude_bug = simconnectBridge.GetAltitudeBug();

            // calculate descent triangle
            double dt_y_leg_nm = (cruiseCfg.cruiseAlt - altitude_bug) * ApUtil.FT_TO_NM;
            double slope_rads = cruiseCfg.descentSlopeDeg * ApUtil.DEG_TO_RAD;
            double dt_x_leg_nm = dt_y_leg_nm / Math.Tan(slope_rads);

            // calc horizontal distances
            double distance_to_airport = simconnectBridge.CalcDistToLanding(verbose);
            if (distance_to_airport < 30)
            {
                ApUtil.ConsoleLog($"  distance_to_airport: {distance_to_airport}, distance_buffer: {distance_buffer}", verbose);
            }
            if (distance_to_airport > 0 && distance_to_airport < distance_buffer)
            {
                ExitAutoCruise(distance_to_airport);
                return altitude_bug;
            }
            double distance_to_target = distance_to_airport - distance_buffer;
            double distance_to_descent = distance_to_target - dt_x_leg_nm;

            // check if we have reached DSP
            if (distance_to_descent > 0)
            {
                // don't start descent yet
                is_descending = false;
                if (verbose)
                {
                    // t=d/v
                    double min_to_descent = (distance_to_descent / simconnectBridge.GetGroundSpeed()) * 60.0;
                    Console.WriteLine("  to descent: {0:f1} NM ({1})", distance_to_descent, ApUtil.FormatHoursAndMinutes(min_to_descent));
                    Console.WriteLine();
                }
                return cruiseCfg.cruiseAlt;
            }
            else
            {
                // on descent slope
                is_descending = true;

                // calc altitudes
                double altitude_y_ft = distance_to_target * ApUtil.NM_TO_FT * Math.Tan(slope_rads);
                double altitude_goal = altitude_bug + altitude_y_ft;

                // log
                if (verbose)
                {
                    ApUtil.CalcDescentAngle(target_vert_speed_fpm, simconnectBridge.GetValue(SimVars.GROUND_VELOCITY));
                    Console.WriteLine($"  auto cruise alt (pre cap): {altitude_goal}");
                }

                // do some capping
                altitude_goal = Math.Min(altitude_goal, cruiseCfg.cruiseAlt);  // don't go above cruise
                altitude_goal = Math.Min(altitude_goal, 60000);  // for concorde
                altitude_goal = Math.Max(altitude_goal, altitude_bug);  // don't go below alt bug

                // handle goal above current altitude
                int currentAlt = (int)simconnectBridge.GetCurrentAltitude();
                if (altitude_goal > currentAlt)
                {
                    currentAlt = (int)ApUtil.RoundTo(currentAlt, 1000);
                    ApUtil.ConsoleLog($"  hold at current alt: {currentAlt}", verbose);
                    altitude_goal = currentAlt;
                    is_descending = false;
                }

                // finish up
                if (verbose)
                {
                    Console.WriteLine($"  auto cruise alt (after cap): {altitude_goal}");
                    Console.WriteLine();
                }
                return altitude_goal;
            }
        }

        private void ExitAutoCruise(double distance_to_airport)
        {
            Console.WriteLine($"distance_to_airport: {distance_to_airport}");

            // kick to ALT mode
            eventService.FireEvent(EventTopic.CHANGE_PITCH_MODE, PitchMode.AltHold);

            // kick to ILS mode
            if (simconnectBridge.HasLocalizer(1))
            {
                eventService.FireEvent(EventTopic.CHANGE_ROLL_MODE, RollMode.Vor);
            }

            // pause if configured
            if (configService.GetFlightPlanConfig().pauseOnApproach)
            {
                eventService.FireEvent(EventTopic.IR_REMOTE_COMMAND, "PAUSE_SIM");
            }
        }

        /// <summary>
        /// 
        /// reqs:
        /// auto cruise
        /// before descend point
        /// below target altitude
        /// 
        /// todo:
        /// handle the case of a short flight where I don't have enought time to climb to cruise altitude
        ///
        /// </summary>
        internal bool ShouldAutoClimb(PitchMode current_pitch_mode, double target_vert_speed_fpm)
        {
            if (current_pitch_mode != PitchMode.AutoCruise)
            {
                return false;  // wrong mode
            }

            MiscConfig config = configService.GetMiscCfg();
            double currentAlt = simconnectBridge.GetCurrentAltitude();
            double distBelowCruiseAlt = config.autoCruise.cruiseAlt - currentAlt;
            if (distBelowCruiseAlt < config.autoCruise.cruiseAltBuffer)
            {
                return false;  // already at cruise alt
            }

            double autoCruiseAltitude = CalcAutoCruiseAltitude(target_vert_speed_fpm, false);
            if (autoCruiseAltitude < config.autoCruise.cruiseAlt)
            {
                return false;  // we are in the descent phase
            }

            if (simconnectBridge.IsHypersonicAircraft())
            {
                return false;
            }

            return true;  // auto climb!
        }

        // used to set descent rate
        internal bool IsDescending()
        {
            return is_descending;
        }

        internal bool IsCruising()
        {
            double distBelowCruiseAlt = configService.GetAutoCruiseCfg().cruiseAlt - simconnectBridge.GetCurrentAltitude();
            int cruiseAltBuffer = configService.GetAutoCruiseCfg().cruiseAltBuffer;

            // NOTE: this is why my speed drops after the DSP
            // todo: fix concorde supercruise
            //Console.WriteLine($"distBelowCruiseAlt={distBelowCruiseAlt:F1}, cruiseAltBuffer={cruiseAltBuffer}");
            return distBelowCruiseAlt < cruiseAltBuffer;
        }
    }
}
