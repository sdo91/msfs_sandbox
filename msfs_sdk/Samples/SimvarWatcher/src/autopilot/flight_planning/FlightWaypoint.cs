﻿using System;

namespace FsAutopilot.Autopilot
{

    public class FlightWaypoint
    {
        public double frequency;
        public int radial = -1;

        public string raw;

        /// <summary>
        /// 
        /// formats:
        ///     '112.3'     // use current radial
        ///     '115.9@90'
        ///     '117.9@90'
        ///     '117.9@44'
        /// future:
        ///     '117.9@90,44' // into 117.9 at 90, then out at 44
        ///     
        /// in (default):
        ///     don't pop inbound waypoints from the queue until they are reached
        ///     switch to it as soon as it is in range
        ///     (this is what I have now?)
        ///     
        /// out:
        ///     switch to it as soon as it is in range
        ///     
        /// theory:
        ///     I don't think I need in/out
        ///     just keep the current waypoint in the queue, until waypoint is reached
        ///     
        /// reached:
        ///     outbound from vor
        ///     bonus: close to vor? (in distance or time)
        /// 
        /// </summary>
        /// <param name="text"></param>
        public FlightWaypoint(string text)
        {
            text = text.Replace(" ", "");  // remove spaces

            // alternate splitters
            text = text.Replace("*", "@");
            text = text.Replace("/", "@");
            text = text.Replace("+", "@");
            text = text.Replace("-", "@");

            raw = text;
            string[] tokens = text.Split('@');

            frequency = Double.Parse(tokens[0]);

            if (tokens.Length > 1)
            {
                radial = Int32.Parse(tokens[1]);
            }

            //Console.WriteLine($"[FlightWaypoint: freq={frequency}, hdg={heading}]");
        }
    }
}