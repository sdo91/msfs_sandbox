﻿using FsAutopilot.Config;
using FsAutopilot.Events;
using FsAutopilot.Simconnect;
using FsAutopilot.Util;
using System;
using System.Collections.Generic;

namespace FsAutopilot.Autopilot
{

    /// <summary>
    /// 
    /// ideas:
    ///     switch to VOR when xtk is small
    ///         use case: fly in hdg mode until we intercept the VOR
    /// 
    /// </summary>
    public class FlightPlanManager
    {

        private SimconnectBridge simconnectBridge = null;
        private ConfigService configService = null;
        private EventService eventService = null;
        private PauseManager pauseManager = null;

        private double previousNav1FreqMhz = 0;

        private Timestamp waypointCheckTimestamp = Timestamp.Now();

        public FlightPlanManager(ConfigService configService, EventService eventService)
        {
            Console.WriteLine("Constructing FlightPlanManager");

            this.configService = configService;
            this.eventService = eventService;

            pauseManager = new PauseManager(configService, eventService);
        }

        internal void SetSimconnectBridge(SimconnectBridge simconnectBridge)
        {
            this.simconnectBridge = simconnectBridge;

            pauseManager.SetSimconnectBridge(simconnectBridge);
        }

        internal void CheckFlightPlan(bool isActive, RollMode currentRollMode)
        {
            if (configService.GetFlightPlanConfig().enableVorWaypoints)
            {
                CheckVorWaypoints();
            }

            // estimate time to VOR
            double dme_dist = simconnectBridge.GetDmeDistanceNm(1);
            double ground_speed = simconnectBridge.GetGroundSpeed();
            if (dme_dist > 1 && ground_speed > 1)
            {
                Console.WriteLine($"DME 1 ({simconnectBridge.GetNavFrequency(1)}): {dme_dist:f2} NM");
                if (!simconnectBridge.IsFromVor(1))
                {
                    double minutes = (dme_dist / ground_speed) * 60;
                    minutes = ApUtil.RoundTo(minutes, 0.1);
                    Console.WriteLine($"minutes to VOR: {minutes}");
                }

                double nmPerMinute = simconnectBridge.GetGroundSpeed() / 60;
                nmPerMinute = ApUtil.RoundTo(nmPerMinute, 0.01);
                Console.WriteLine($"nmPerMinute={nmPerMinute} (t = d / v)");
            }

            pauseManager.CheckPause(isActive, currentRollMode);

            if (isActive)
            {
                Console.WriteLine();
            }
        }

        private bool IsNav1Reached(YamlVorWaypoint currentWaypoint)
        {
            if (!simconnectBridge.IsNavConnected(1))
            {
                return false;
            }

            if (simconnectBridge.IsFromVor(1))
            {
                Console.WriteLine("IsNav1Reached: heading away");
                return true;
            }

            if (!simconnectBridge.HasDme(1))
            {
                return false;  // DME required
            }

            // get arrival distance
            int arrivalDistance = currentWaypoint.inboundArrivalDistance;
            if (arrivalDistance < 0)
            {
                arrivalDistance = configService.GetFlightPlanConfig().vorArrivalDist;
            }

            double dmeDist = simconnectBridge.GetDmeDistanceNm(1);
            if (dmeDist < arrivalDistance)
            {
                Console.WriteLine($"IsNav1Reached: close enough (DME={dmeDist}, arrivalDistance={arrivalDistance})");
                return true;
            }

            return false;
        }

        /// <summary>
        /// 
        /// the logitech FIP HSI screen shows DME2 for some reason
        /// so sometimes when nav 1 changes, I want nav 2 to change as well
        /// 
        /// </summary>
        public void SyncNav2()
        {
            // get NAV1
            double nav1FreqMhz = simconnectBridge.GetNavFrequency(1);
            if (nav1FreqMhz == previousNav1FreqMhz)
            {
                return; // frequency hasn't changed
            }
            Console.WriteLine("nav1 has changed!");

            // update previous
            if (previousNav1FreqMhz == 0)
            {
                previousNav1FreqMhz = nav1FreqMhz;
                return;  // don't count it as a change if previous wasn't set
            }
            previousNav1FreqMhz = nav1FreqMhz;

            // exit if sync is disabled
            if (!configService.GetAppCfg().syncNav2)
            {
                Console.WriteLine("nav2 sync disabled");
                return;
            }

            // check if nav2 is already set
            if (simconnectBridge.IsNavFrequency(2, nav1FreqMhz))
            {
                Console.WriteLine("nav2 already sync'd");
                return;
            }

            // set the frequency
            simconnectBridge.SetNav2Frequency(nav1FreqMhz);
        }

        /// <summary>
        /// 
        /// algo:
        ///     check next waypoint in the queue
        ///     if in range, switch to it
        ///     keep the current waypoint in the queue, until waypoint is reached
        ///     
        /// </summary>
        private void CheckVorWaypoints()
        {
            // limit the rate
            int TIMEOUT_SEC = 2;
            if (!waypointCheckTimestamp.IsTimedOut(TIMEOUT_SEC))
            {
                return;
            }

            // are there any waypoints?
            FlightPlanConfig flightPlanCfg = configService.GetFlightPlanConfig();
            if (flightPlanCfg.vorWaypoints.Count == 0)
            {
                Console.WriteLine($"CheckVorWaypoints: no waypoints");
                return; // no waypoints
            }

            // parse waypoint
            YamlVorWaypoint currentWaypoint = flightPlanCfg.vorWaypoints[0];

            // ensure current waypoint is loaded into NAV2
            if (!simconnectBridge.IsNavFrequency(2, currentWaypoint.frequency))
            {
                Console.WriteLine("CheckVorWaypoints: setting NAV2!");
                simconnectBridge.SetNav2Frequency(currentWaypoint.frequency);
                waypointCheckTimestamp.SetToNow();
                return;
            }

            // check if NAV2 frequency is in range
            if (!simconnectBridge.IsNavConnected(2))
            {
                if (simconnectBridge.HasNavSignal(2))
                {
                    Console.WriteLine("CheckVorWaypoints: NAV2 is DME only!");
                    // todo: do something?
                }
                else
                {
                    Console.WriteLine($"CheckVorWaypoints: NAV2 not yet in range! ({simconnectBridge.GetNavFrequency(2)})");
                }
                return;
            }

            // check for localizer
            // NOTE: this check needs to be before the freq/radial check below
            if (IsWaypointIls(currentWaypoint))
            {
                Console.WriteLine("CheckVorWaypoints: localizer detected, marking ILS!");
                if (currentWaypoint.MarkIls())
                {
                    eventService.TriggerUiUpdate();
                    return;
                }
            }

            // todo: implement method: ShouldSwitchToNav2()
            // if nav2 is ils, and there is no DME, don't switch until xtk is <5

            // in range, time to switch NAV2 -> NAV1
            bool doesFreqMatch = simconnectBridge.IsNavFrequency(1, currentWaypoint.frequency);
            int desiredRadial = currentWaypoint.GetDesiredRadial();
            bool doesRadialMatch = DoesNav1RadialMatch(desiredRadial);
            if (!doesFreqMatch || !doesRadialMatch)
            {
                Console.WriteLine("CheckVorWaypoints: freq. in range, setting NAV1: " +
                    $"{currentWaypoint.frequency} @ {desiredRadial} deg");
                simconnectBridge.SetNav1Frequency(currentWaypoint.frequency);
                if (desiredRadial >= 0)
                {
                    simconnectBridge.SetNavRadial(1, (uint)desiredRadial);
                }
                waypointCheckTimestamp.SetToNow();
                return;
            }

            // check if waypoint is reached
            if (IsNav1Reached(currentWaypoint))
            {
                if (currentWaypoint.IsInboundLegActive())
                {
                    // this will allow the outbound CRS to activate
                    Console.WriteLine("CheckVorWaypoints: marking inbound leg done!");
                    currentWaypoint.MarkInboundLegDone();
                }
                else if (currentWaypoint.HasLocalizer())
                {
                    // can pop ILS waypoints even while landed
                    Console.WriteLine("CheckVorWaypoints: popping ILS waypoint from queue!");
                    flightPlanCfg.vorWaypoints.RemoveAt(0);
                }
                else if (ShouldFollowVorOutbound(currentWaypoint))
                {
                    Console.WriteLine("CheckVorWaypoints: ShouldFollowVorOutbound");
                    return;
                }
                else if (!simconnectBridge.IsLanded())
                {
                    // only pop VOR waypoints while airborne
                    Console.WriteLine("CheckVorWaypoints: popping VOR waypoint from queue!");
                    flightPlanCfg.vorWaypoints.RemoveAt(0);
                }
                eventService.TriggerUiUpdate();
                waypointCheckTimestamp.SetToNow();
                return;
            }

            Console.WriteLine();
        }

        /// <summary>
        ///
        /// this is called when a VOR waypoint is reached.
        /// in some cases, it makes sense to follow the outbound radial for a bit.
        /// example:
        ///     current waypoint is an airport VOR
        ///     next waypoint is the ILSat the same airport
        ///     in this case, we should follow the outbound radial to set up the ILS
        ///
        /// assumption: the current waypoint is already loaded into NAV1
        /// 
        /// </summary>
        private bool ShouldFollowVorOutbound(YamlVorWaypoint currentWaypoint)
        {
            if (!currentWaypoint.IsOutboundRadialValid())
            {
                Console.WriteLine("[ShouldFollowVorOutbound] no outbound radial to follow");
                return false;
            }

            if (currentWaypoint.inboundArrivalDistance > 0)
            {
                Console.WriteLine("[ShouldFollowVorOutbound] inboundReachedDistance is set");
                return false;
            }

            if (!simconnectBridge.IsNavConnected(1))
            {
                Console.WriteLine("[ShouldFollowVorOutbound] we might be directly over the VOR");
                return true;
            }

            if (!simconnectBridge.HasDme(1))
            {
                Console.WriteLine("[ShouldFollowVorOutbound] DME is required");
                return false;
            }

            // all preconditions met: check to see if we have reached outbound distance
            double dmeDist = simconnectBridge.GetDmeDistanceNm(1);
            double buffer = configService.GetAutoCruiseCfg().approachDistanceNm + 2;
            if (dmeDist > buffer)
            {
                Console.WriteLine("[ShouldFollowVorOutbound] outbound distance reached");
                return false;  // done
            }
            else
            {
                Console.WriteLine($"[ShouldFollowVorOutbound] following outbound (DME={dmeDist:f2}, buffer={buffer})");
                return true;
            }
        }

        private bool IsWaypointIls(YamlVorWaypoint waypoint)
        {
            bool doesFreqMatch = simconnectBridge.IsNavFrequency(1, waypoint.frequency);
            bool result = doesFreqMatch && simconnectBridge.HasLocalizer(1);
            if (result)
            {
                Console.WriteLine($"[IsWaypointIls] ILS detected on NAV1: {simconnectBridge.GetNavFrequency(1)}");
            }
            return result;
        }

        /// <summary>
        ///     
        /// todo:
        ///     auto switch to ILS
        ///     
        /// algo:
        ///     if ils is pending && we are inbound to VOR && DME < 30 
        ///         set nav2 to watch ILS
        ///         pop first waypoint when ILS is locked
        ///     else
        ///         set nav 2 to watch DME
        /// 
        /// </summary>
        private bool IsLocalizerPending(List<YamlVorWaypoint> vorWaypoints)
        {
            if (vorWaypoints.Count < 2)
            {
                return false;
            }

            YamlVorWaypoint potentialLoc = vorWaypoints[1];
            bool result = potentialLoc.HasLocalizer();
            return result;
        }

        private bool DoesNav1RadialMatch(int waypointRadial)
        {
            if (waypointRadial < 0)
            {
                return true;  // don't need to change radial
            }

            int currentRadial = (int)simconnectBridge.GetNavCourse(1);
            return currentRadial == waypointRadial;
        }

        internal void AddWaypoint(string frequency, string crsInDeg, string crsOutDeg)
        {
            Console.WriteLine($"HandleAddWaypointButton: freq={frequency}, in={crsInDeg}, out={crsOutDeg}");

            YamlVorWaypoint vorWaypoint = YamlVorWaypoint.CreateWaypoint(frequency, crsInDeg, crsOutDeg);

            // add the new waypoint
            FlightPlanConfig flightPlanCfg = configService.GetFlightPlanConfig();
            flightPlanCfg.vorWaypoints.Add(vorWaypoint);
            eventService.TriggerUiUpdate();
        }
    }
}
