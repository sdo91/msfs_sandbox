﻿using FsAutopilot.Config;
using FsAutopilot.Events;
using FsAutopilot.Util;
using System;

namespace FsAutopilot.Autopilot
{
    /// <summary>
    /// A (P)roportional, (I)ntegral, (D)erivative Controller
    ///
    /// terminology:
    /// Set Point:
    ///     the desired value of the variable we want to control
    ///     eg: target airspeed
    /// Process Variable:
    ///     the current value of the variable we want to control
    ///     eg: current airspeed
    /// Control Variable:
    ///     the value we manipulate to achieve the desired Set Point
    ///     eg: throttle
    ///
    /// </summary>
    /// <remarks>
    /// The controller should be able to control any process with a
    /// measureable value, a known ideal value and an input to the
    /// process that will affect the measured value.
    /// </remarks>
    /// <see cref="https://en.wikipedia.org/wiki/PID_controller"/>
    public sealed class PidController
    {
        private double processVariable = 0;
        private bool isInverted = false;
        private bool doPreventOverwind = true;

        private string controllerName = "call logSettings";
        private string controlVarUnits = "call logSettings";
        private string setPointUnits = "call logSettings";

        private bool shouldWrapSetPoint = false;

        private double previousOutputValue = 0;

        private Timestamp latestTimestamp = Timestamp.Zero();

        public bool verbose { get; private set; } = false;

        StateTracker<bool> insideMaxErrorForITracker = new StateTracker<bool>();

        public PidController(double GainProportional, double GainIntegral, double GainDerivative, double OutputMin, double OutputMax)
        {
            this.GainDerivative = GainDerivative;
            this.GainIntegral = GainIntegral;
            this.GainProportional = GainProportional;
            this.OutputMin = OutputMin;
            this.OutputMax = OutputMax;
            resetIntegralTerm();
        }

        public PidController(PidGains pidGains, double outputMinMax)
        {
            LoadGains(pidGains);
            SetControlVarMinMax(outputMinMax);
            resetIntegralTerm();
        }

        public PidController(PidGains pidGains)
        {
            LoadGains(pidGains);
            // NOTE: min/max must be set elsewhere
        }

        internal void SetName(string name)
        {
            controllerName = name;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="controllerName"></param>
        /// <param name="verbose"></param>
        /// <param name="controlVarUnits">output units</param>
        /// <param name="setPointUnits">input units</param>
        public void logSettings(string controllerName, bool verbose, string controlVarUnits, string setPointUnits)
        {
            this.controllerName = controllerName;
            this.verbose = verbose;
            this.controlVarUnits = controlVarUnits;
            this.setPointUnits = setPointUnits;
        }

        private string FormatUnits()
        {
            string scale = "";
            if (ScaleFactor != 1)
            {
                scale = $"{ApUtil.RoundTo(1 / ScaleFactor, 0.1)} ";
            }
            string formattedUnits = $"units={controlVarUnits} per {scale}{setPointUnits} error";
            formattedUnits = formattedUnits.Replace(" left", "").Replace(" right", "").Replace(" up", "").Replace(" down", "");
            return formattedUnits;
        }

        internal void resetIntegralTerm()
        {
            if (controllerName.ToUpper().Contains("AIRSPEED"))
            {
                IntegralTerm = (OutputMin + OutputMax) / 2;
            }
            else
            {
                IntegralTerm = 0;
            }

            Console.WriteLine($"[{controllerName}] resetting I");

            // todo: why?
            //previousOutputValue = IntegralTerm;
        }

        /// <summary>
        /// call this if increasing the ControlVariable will decrease the ProcessVariable
        /// </summary>
        internal void invertOutput()
        {
            isInverted = true;
        }

        internal void invertOutput(bool state)
        {
            isInverted = state;
        }

        internal void UnlockIntegrator()
        {
            doPreventOverwind = false;
        }

        /// <summary>
        /// 
        /// init I term if necessary
        /// 
        /// Integrator = (out - (P * error))
        /// 
        /// </summary>
        private bool InitIntegrator(double proportionalTerm, double derivativeTerm)
        {
            if (GainIntegral == 0)
            {
                return false;
            }
            if (CurrentControlVariable == 0)
            {
                return false;
            }
            if (latestTimestamp.IsRecent(1.0))
            {
                return false;
            }

            double estimatedI = CurrentControlVariable - proportionalTerm + derivativeTerm;
            if (verbose)
            {
                Console.WriteLine("estimatedI: {0:f2} ({1:f2} - {2:f2} + {3:f2})",
                    estimatedI, CurrentControlVariable, proportionalTerm, derivativeTerm);
            }
            IntegralTerm = estimatedI;
            return true;
        }

        /// <summary>
        /// The controller output
        ///
        /// todo:
        /// if 1 sec has passed since this was called
        /// then I need to calc the integrator value
        ///
        /// P + I - D = out
        /// so
        /// I = out - P + D
        ///
        /// </summary>
        /// <param name="timeSinceLastUpdate">timespan of the elapsed time
        /// since the previous time that ControlVariable was called</param>
        /// <returns>Value of the variable that needs to be controlled</returns>
        public double ControlVariable(TimeSpan timeSinceLastUpdate)
        {
            double pGain = getScaledP();
            double iGain = getScaledI();
            double dGain = getScaledD();

            AdjustSetPoint(timeSinceLastUpdate);

            if (verbose)
            {
                Console.WriteLine("[{0:S}] goal={1:f} {2:S}, {3:S}:",
                    controllerName, damped_set_point_, setPointUnits, FormatUnits());
            }

            double error = damped_set_point_ - ProcessVariable;

            // proportional term calculation
            double proportionalTerm = pGain * error;

            // derivative term calculation
            double dInput = processVariable - ProcessVariableLast;
            double derivativeTerm = dGain * (dInput / timeSinceLastUpdate.TotalSeconds);

            // integral term calculation
            bool wasIntegratorInitialized = InitIntegrator(proportionalTerm, derivativeTerm);
            double newIntegralTerm = IntegralTerm + (iGain * error * timeSinceLastUpdate.TotalSeconds);
            newIntegralTerm = Clamp(newIntegralTerm);

            // check MaxErrorForI
            if (MaxErrorForI > 0 && Math.Abs(error) > MaxErrorForI)
            {
                // reset I
                IntegralTerm = 0;
                newIntegralTerm = 0;
                insideMaxErrorForITracker.Update(false);
            }
            else
            {
                insideMaxErrorForITracker.Update(true);
            }

            // combine the terms
            double output = proportionalTerm + newIntegralTerm - derivativeTerm;

            // decide if we should wind the integrator or not
            var shouldWind = true;
            string clampWarning = "";
            if (doPreventOverwind)
            {
                // doesn't work with AirspeedController...
                //if (shouldClamp(proportionalTerm))
                //{
                //    clampWarning = " Clamped! (P)";
                //    shouldWind = false;
                //}
                if (shouldClamp(output))
                {
                    clampWarning = $" Clamped! (P={proportionalTerm}, I={newIntegralTerm}, D={derivativeTerm})";
                    shouldWind = false;
                }
            }

            if (shouldWind)
            {
                IntegralTerm = newIntegralTerm;  // wind it, baby!
            }
            // todo: allow wind down?

            double clampedOutput = Clamp(output);

            if (verbose)
            {
                Console.WriteLine("  current value: {0:F} {1:S}", ProcessVariable, setPointUnits);

                Console.WriteLine(
                    $"  {proportionalTerm:f2} + {IntegralTerm:f2} - {derivativeTerm:f2} = {output:f2} " +
                    $"({controlVarUnits}){clampWarning}");

                if (!shouldWind)
                {
                    Console.WriteLine("  NOT winding integrator!");
                }
            }

            if (isInverted)
            {
                clampedOutput *= -1;
            }

            if (CurrentControlVariable != 0)
            {
                // instead of last commanded value, I want current value if available
                Console.WriteLine("  using CurrentControlVariable: {0:f2} {1:s}", CurrentControlVariable, controlVarUnits);
                previousOutputValue = CurrentControlVariable;
            }

            // limit rate of change
            if (MaxControlVarRate != 0 && !wasIntegratorInitialized)
            {
                var maxDelta = MaxControlVarRate * timeSinceLastUpdate.TotalSeconds;
                var delta = clampedOutput - previousOutputValue;
                if (delta > maxDelta)
                {
                    clampedOutput = previousOutputValue + maxDelta;
                    if (verbose)
                    {
                        Console.WriteLine("  delta={0:f} is being limited to maxDelta={1:f}", delta, maxDelta);
                    }
                }
                else if (delta < -maxDelta)
                {
                    clampedOutput = previousOutputValue - maxDelta;
                    if (verbose)
                    {
                        Console.WriteLine("  delta={0:f} is being limited to maxDelta={1:f}", delta, maxDelta);
                    }
                }
                //else
                //{
                //    if (verbose)
                //    {
                //        Console.WriteLine("{0:f} is OK", delta);
                //    }
                //}
            }

            // finish up
            previousOutputValue = clampedOutput;
            latestTimestamp.SetToNow();
            return clampedOutput;
        }

        /// <summary>
        /// called when I change a gain from the UI (for the selected controller)
        /// </summary>
        public void LoadGains(PidGains pidGains)
        {
            GainProportional = pidGains.p;
            GainIntegral = pidGains.i;
            GainDerivative = pidGains.d;
            MaxErrorForI = pidGains.maxErrForI;
            MaxSetPointError = pidGains.maxSetPointError;
            MaxSetPointChangeRate = pidGains.maxSetPointChangeRate;
        }

        /// <summary>
        /// set min/max of control var
        /// todo: rename
        /// </summary>
        internal void SetControlVarMinMax(double outputMinMax)
        {
            outputMinMax = Math.Abs(outputMinMax);
            OutputMin = -outputMinMax;
            OutputMax = outputMinMax;
        }

        /// <summary>
        /// The derivative term is proportional to the rate of
        /// change of the error
        /// </summary>
        public double GainDerivative { get; set; } = 0;

        /// <summary>
        /// The integral term is proportional to both the magnitude
        /// of the error and the duration of the error
        /// </summary>
        public double GainIntegral { get; set; } = 0;

        /// <summary>
        /// The proportional term produces an output value that
        /// is proportional to the current error value
        /// </summary>
        /// <remarks>
        /// Tuning theory and industrial practice indicate that the
        /// proportional term should contribute the bulk of the output change.
        /// </remarks>
        public double GainProportional { get; set; } = 0;

        /// <summary>
        /// If error is larger than this, reset I windup
        /// 
        /// disabled if 0
        /// </summary>
        public double MaxErrorForI { get; set; } = 0;

        // todo: add this to PidGains
        public double MaxControlVarRate { get; set; } = 0;

        public double MaxSetPointChangeRate { get; set; } = 0;

        public double MaxSetPointError { get; set; } = 0;

        public double ScaleFactor { get; set; } = 1;
        public double MachScale { get; set; } = 1;

        private double getScaledP()
        {
            return GainProportional * ScaleFactor * MachScale;
        }
        private double getScaledI()
        {
            return GainIntegral * ScaleFactor;
        }
        private double getScaledD()
        {
            return GainDerivative * ScaleFactor;
        }

        /// <summary>
        /// The max output value the control device can accept.
        /// </summary>
        public double OutputMax { get; set; } = 0;

        /// <summary>
        /// The minimum ouput value the control device can accept.
        /// </summary>
        public double OutputMin { get; set; } = 0;

        /// <summary>
        /// Adjustment made by considering the accumulated error over time
        /// </summary>
        /// <remarks>
        /// An alternative formulation of the integral action, is the
        /// proportional-summation-difference used in discrete-time systems
        /// </remarks>
        public double IntegralTerm { get; private set; } = 0;

        /// <summary>
        /// The current control variable value
        /// </summary>
        public double CurrentControlVariable { get; set; } = 0;

        /// <summary>
        /// The current value
        /// </summary>
        public double ProcessVariable
        {
            get { return processVariable; }
            set
            {
                ProcessVariableLast = processVariable;
                processVariable = value;
            }
        }

        /// <summary>
        /// The last reported value (used to calculate the rate of change)
        /// </summary>
        public double ProcessVariableLast { get; private set; } = 0;

        private double damped_set_point_ { get; set; } = 0;  // the short-term goal

        /// <summary>
        /// The desired value
        /// </summary>
        public double SetPoint
        {
            get { return true_set_point_; }
            set { true_set_point_ = value; }
        }
        private double true_set_point_ = 0;  // the long-term goal

        /// <summary>
        /// todo: if the controller has timed out, reset the damped set point to the current process variable value?
        /// </summary>
        private void AdjustSetPoint(TimeSpan deltaT)
        {
            // apply MaxSetPointChangeRate
            if (MaxSetPointChangeRate == 0)
            {
                // no rate cap
                damped_set_point_ = true_set_point_;
            }
            else
            {
                ApplyMaxSetPointChangeRate(deltaT);
            }

            // apply MaxSetPointError
            if (MaxSetPointError != 0)
            {
                ApplyMaxSetPointError();
            }
        }

        private void ApplyMaxSetPointChangeRate(TimeSpan deltaT)
        {
            var maxSetPointChange = Math.Abs(MaxSetPointChangeRate * deltaT.TotalSeconds);

            // wrap damped set point (if necessary)
            if (shouldWrapSetPoint)
            {
                damped_set_point_ = ApUtil.WrapToMatch(damped_set_point_, true_set_point_);
            }

            // move the damped SP towards the true SP
            var diff = Math.Abs(true_set_point_ - damped_set_point_);
            if (diff < maxSetPointChange)
            {
                // we are there
                damped_set_point_ = true_set_point_;
            }
            else
            {
                // we aren't there yet
                int sign = (damped_set_point_ < true_set_point_) ? 1 : -1;
                damped_set_point_ += (sign * maxSetPointChange);
                Console.WriteLine($"new damped_set_point_: {damped_set_point_} (delta={maxSetPointChange})");
            }
        }

        private void ApplyMaxSetPointError()
        {
            // eg: for AirspeedController
            // don't let set point get too far from current speed
            // SetPoint: true set point, long term goal, eg: target airspeed on LCD
            // damped_set_point_: short term goal
            // ProcessVariable: current airspeed

            if (damped_set_point_ > ProcessVariable + MaxSetPointError)
            {
                damped_set_point_ = ProcessVariable + MaxSetPointError;
                Console.WriteLine($"using MaxSetPointError={MaxSetPointError}: damped_set_point_={damped_set_point_}");
            }
            else if (damped_set_point_ < ProcessVariable - MaxSetPointError)
            {
                damped_set_point_ = ProcessVariable - MaxSetPointError;
                Console.WriteLine($"using MaxSetPointError={MaxSetPointError}: damped_set_point_={damped_set_point_}");
            }
        }

        /// <summary>
        /// Limit a variable to the set OutputMax and OutputMin properties
        /// </summary>
        /// <returns>
        /// A value that is between the OutputMax and OutputMin properties
        /// </returns>
        /// <remarks>
        /// Inspiration from http://stackoverflow.com/questions/3176602/how-to-force-a-number-to-be-in-a-range-in-c
        /// </remarks>
        private double Clamp(double variableToClamp)
        {
            if (variableToClamp <= OutputMin) { return OutputMin; }
            if (variableToClamp >= OutputMax) { return OutputMax; }
            return variableToClamp;
        }

        private bool shouldClamp(double variableToClamp)
        {
            bool result = (variableToClamp <= OutputMin) || (variableToClamp >= OutputMax);
            if (result)
            {
                Console.WriteLine($"  variableToClamp={variableToClamp}, min={OutputMin}, max={OutputMax}");
            }
            return result;
        }

        internal PidGains getPidGains()
        {
            return new PidGains(GainProportional, GainIntegral, GainDerivative, MaxErrorForI, MaxSetPointError, MaxSetPointChangeRate);
        }

        public void PopulateResponseData(ControllerEntry entry)
        {
            entry.endGoal = SetPoint;
            entry.shortGoal = damped_set_point_;
            entry.current = ProcessVariable;
        }

        internal double GetSecondsInsideMaxErrorForI()
        {
            return insideMaxErrorForITracker.GetSecInState(true);
        }

        internal void SetShouldWrapSetPoint(bool shouldWrap)
        {
            shouldWrapSetPoint = shouldWrap;
        }
    }
}
