﻿using FsAutopilot.AircraftLogic;
using FsAutopilot.Config;
using FsAutopilot.Events;
using FsAutopilot.Hardware;
using FsAutopilot.Simconnect;
using FsAutopilot.Util;
using System;

namespace FsAutopilot.Autopilot
{
    /// <summary>
    ///
    /// intended for GA aircraft
    /// especially those that don't have built in AP systems
    ///
    /// periodic:
    /// tank switch
    /// auto mix
    /// gear/flaps check
    ///
    /// </summary>
    public class CustomAutopilot
    {
        private AircraftLogicService aircraftLogicService = null;
        private ConfigService configService = null;
        private EventService eventService = null;
        private JoystickService joystickService = null;
        private SimconnectBridge simconnectBridge = null;

        private string selectedControllerName = null;

        private PitchManager pitchManager;
        private RollManager rollManager;
        private ThrottleManager throttleManager;

        private FuelManager fuelManager;
        private LightManager lightManager;
        private FlightPlanManager flightPlanManager;

        private bool isInverted_ = false;
        private bool isActive_ = false;

        private PidResponseData pidResponseData = new PidResponseData();

        public bool IsActive
        {
            get { return isActive_; }
            set
            {
                isActive_ = value;
                pitchManager.IsActive = value;
                if (isActive_)
                {
                    Console.WriteLine("Autopilot ON");
                }
                else
                {
                    Console.WriteLine("Autopilot OFF");
                }
            }
        }

        internal bool IsAutothrottleActive
        {
            get
            {
                if (throttleManager != null)
                {
                    return throttleManager.IsActive;
                }
                return false;
            }
            set
            {
                if (throttleManager != null)
                {
                    throttleManager.IsActive = value;
                }
            }
        }

        internal bool IsInverted
        {
            get { return isInverted_; }
            set
            {
                isInverted_ = value;
                rollManager.IsInverted = value;
            }
        }

        public double targetVertSpeed  // ft/min
        {
            get
            {
                if (pitchManager != null)
                {
                    return pitchManager.TargetVertSpeed;
                }
                return 500;
            }
            set
            {
                if (pitchManager != null)
                {
                    pitchManager.TargetVertSpeed = value;
                }
            }
        }

        public double targetAirspeed  // knots
        {
            get
            {
                if (throttleManager != null)
                {
                    return throttleManager.TargetAirspeed;
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                Console.WriteLine($"targetAirspeed set: {value}");
                if (throttleManager != null)
                {
                    // todo: https://gitlab.com/sdo91/msfs_sandbox/-/issues/67
                    throttleManager.TargetAirspeed = value;
                }
            }
        }

        public double targetPitch  // degrees
        {
            get
            {
                if (pitchManager != null)
                {
                    return pitchManager.TargetPitch;
                }
                return 1;
            }
            set
            {
                if (pitchManager != null)
                {
                    pitchManager.TargetPitch = value;
                }
            }
        }

        public double targetRollDeg
        {
            get
            {
                if (rollManager != null)
                {
                    return rollManager.TargetRollDeg;
                }
                return 0;
            }
            set
            {
                if (rollManager != null)
                {
                    Console.WriteLine($"targetRollDeg: {value}");
                    rollManager.TargetRollDeg = value;
                }
            }
        }

        /// <summary>
        ///
        /// this is called once on boot
        ///
        /// tune philosophy:
        ///     error on the side of smooth, not fast
        ///     smooth for normal operations (eg: -2000 to 500 VS for c152)
        ///
        /// tune process:
        ///     lower I and D (near 0)
        ///     tweak P until I get a reasonable response
        ///         may not reach setpoint, that's OK
        ///         should feel smooth
        ///         eg: 0 to 500 (VS)
        ///     tweak I gain until we just overshoot the setpoint
        ///         smooth for normal ops
        ///
        /// </summary>
        internal CustomAutopilot(
            EventService eventService,
            ConfigService configService,
            FlightPlanManager flightPlanManager,
            AircraftLogicService aircraftLogicService)
        {
            Console.WriteLine($"constructing CustomAutopilot");

            // don't call init here anymore
            this.aircraftLogicService = aircraftLogicService;
            this.configService = configService;
            this.eventService = eventService;
            this.flightPlanManager = flightPlanManager;
        }

        internal void SetJoystickService(JoystickService joystickService)
        {
            this.joystickService = joystickService;
            Console.WriteLine("JoystickService set!");
        }

        /// <summary>
        /// called:
        ///     on boot
        ///     when a new aircraft is connected
        /// </summary>
        public void HandleAircraftConnected()
        {
            initControllers();

            // init controllers first
            targetVertSpeed = configService.GetMiscCfg().defaultVertSpeed;
            targetPitch = -1;

            // make sure the correct controller is selected on connection
            if (selectedControllerName != null)
            {
                SelectPidController(selectedControllerName);
            }

            // todo: set roll/pitch modes here too?
        }

        private void initControllers()
        {
            // init PitchManager
            pitchManager = new PitchManager(simconnectBridge, configService, eventService, joystickService, aircraftLogicService);
            pitchManager.SetPidResponseData(pidResponseData);

            // init RollManager
            rollManager = new RollManager(simconnectBridge, configService, eventService);
            rollManager.SetPidResponseData(pidResponseData);

            // use throttle to control airspeed
            throttleManager = new ThrottleManager(simconnectBridge, configService, eventService, pitchManager, joystickService, aircraftLogicService);
            throttleManager.TargetAirspeed = targetAirspeed;
            throttleManager.SetPidResponseData(pidResponseData);

            // pass some references
            pitchManager.SetThrottleManager(throttleManager);

            fuelManager = new FuelManager(simconnectBridge, configService, eventService);
            lightManager = new LightManager(simconnectBridge, configService, eventService);
        }

        internal void OnTick()
        {
            // logic to run even when paused
            flightPlanManager.SyncNav2();

            // check if paused
            if (simconnectBridge.IsSimPaused)
            {
                return;
            }

            if (simconnectBridge.IsOnGroundBelowSpeed(5))
            {
                return;
            }

            pidResponseData.Reset();

            if (IsAutothrottleActive)
            {
                throttleManager.calcThrottlePosition();
            }

            rollManager.AutoAdjustNavCourse(IsActive);
            if (IsActive)
            {
                rollManager.UpdateHeadingBug();
                rollManager.DoAileronTrim();

                pitchManager.DoElevatorTrim();
                pitchManager.PlanDescent();

                // publish PID response data
                eventService.FireEvent(EventTopic.PID_RESPONSE_DATA, pidResponseData);
            }

            fuelManager.Process(IsActive);
            lightManager.CheckLights();
            flightPlanManager.CheckFlightPlan(IsActive, rollManager.GetCurrentRollMode());
            pitchManager.CheckBaro();

            // show throttle lever position (for DC6/CRJ/ATR/etc)
            if (configService.GetMiscCfg().useVjoyThrottle && throttleManager.IsActive)
            {
                double throttlePercent = throttleManager.GetThrottle();
                if (simconnectBridge.GetGroundSpeed() > 1 || throttlePercent > 1)
                {
                    Console.WriteLine("throttle lever: {0:f1}%", throttlePercent);
                }
            }
        }

        internal void SetSimconnectBridge(SimconnectBridge simconnectBridge)
        {
            this.simconnectBridge = simconnectBridge;
        }

        /// <summary>
        /// select which PID controller to tune
        /// </summary>
        internal void SelectPidController(string controllerName)
        {
            selectedControllerName = controllerName;
            Console.WriteLine($"Selected Controller: {selectedControllerName}");
            if (simconnectBridge == null)
            {
                return;  // no aircraft connected yet
            }

            configService.SelectedController = MapControllerName(selectedControllerName);
        }

        private PidController MapControllerName(string controllerName)
        {
            switch (controllerName)
            {
                case PidControllerNames.ALTITUDE:
                    return pitchManager.AltHoldController;
                case PidControllerNames.GLIDE_SLOPE:
                    return pitchManager.GlideSlopeController;
                case PidControllerNames.FLC:
                    return pitchManager.FlcController;
                case PidControllerNames.VERT_SPEED:
                    return pitchManager.VertSpeedController;
                case PidControllerNames.PITCH:
                    return pitchManager.PitchController;
                case PidControllerNames.PITCH_2:
                    return pitchManager.Pitch2Controller;
                case PidControllerNames.PITCH_RATE:
                    return pitchManager.PitchRateController;

                case PidControllerNames.HEADING:
                    return rollManager.HeadingController;
                case PidControllerNames.NAVIGATION:
                    return rollManager.NavController;
                case PidControllerNames.ROLL:
                    return rollManager.RollController;

                case PidControllerNames.AIRSPEED:
                    return throttleManager.AirspeedController;

                default:
                    throw new ApplicationException($"[MapControllerName] error: {controllerName}");
            }
        }

        internal void SyncAltitudeBug()
        {
            if (simconnectBridge == null)
            {
                Console.WriteLine("not connected");
                return;
            }

            // get/set altitude
            int currentAltitudeFt = (int)ApUtil.RoundTo(simconnectBridge.GetCurrentAltitude(), 100);
            simconnectBridge.SetTargetAltitude(currentAltitudeFt);
            Console.WriteLine($"Sync'd ALT to {currentAltitudeFt}");
        }

        internal void SyncHeadingBug()
        {
            if (simconnectBridge != null)
            {
                simconnectBridge.SyncHeadingBug();
            }
        }

        internal void SetRollMode(string selectedMode)
        {
            if (rollManager != null)
            {
                rollManager.SetRollMode(selectedMode);
            }
        }

        internal void SetPitchMode(string selectedMode)
        {
            if (pitchManager != null)
            {
                pitchManager.SetPitchMode(selectedMode);
            }
        }

        internal void SetThrottleMode(string selectedMode)
        {
            if (throttleManager != null)
            {
                throttleManager.SetThrottleMode(selectedMode);
            }
        }

        internal void handleDisconnect()
        {
            simconnectBridge = null;
            // todo: more?
        }

        /// <summary>
        /// reload the PID gains from config
        /// this way when the config updated event is fired, the values will be saved to file
        ///
        /// todo: there is probably a better way to do this...
        /// </summary>
        internal void UpdatePidGains()
        {
            if (simconnectBridge == null)
            {
                return;  // no aircraft connected yet
            }
            PidControlConfig pidConfig = configService.GetPidControlCfg();

            pidConfig.altGains = pitchManager.AltHoldController.getPidGains();
            pidConfig.glideSlopeGains = pitchManager.GlideSlopeController.getPidGains();
            pidConfig.vsGains = pitchManager.VertSpeedController.getPidGains();
            pidConfig.flcGains = pitchManager.FlcController.getPidGains();
            pidConfig.pitchGains = pitchManager.PitchController.getPidGains();
            pidConfig.pitch2Gains = pitchManager.Pitch2Controller.getPidGains();
            pidConfig.pitchRateGains = pitchManager.PitchRateController.getPidGains();

            pidConfig.navGains = rollManager.NavController.getPidGains();
            pidConfig.headingGains = rollManager.HeadingController.getPidGains();
            pidConfig.rollGains = rollManager.RollController.getPidGains();

            pidConfig.airspeedGains = throttleManager.AirspeedController.getPidGains();
        }

        internal void SetHighRates(bool isHigh)
        {
            rollManager.SetHighRates(isHigh);
        }

        internal PitchMode GetPitchMode()
        {
            return pitchManager.GetPitchMode();
        }
    }
}
