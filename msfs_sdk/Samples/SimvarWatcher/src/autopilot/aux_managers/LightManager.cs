﻿using FsAutopilot.Config;
using FsAutopilot.Events;
using FsAutopilot.Simconnect;
using FsAutopilot.Util;
using System;

namespace FsAutopilot.Autopilot
{
    public class LightManager
    {
        private readonly uint MASK_NAV = 0x0001;
        private readonly uint MASK_BEACON = 0x0002;
        private readonly uint MASK_LANDING = 0x0004;
        private readonly uint MASK_TAXI = 0x0008;
        private readonly uint MASK_STROBE = 0x0010;
        private readonly uint MASK_PANEL = 0x0020;
        private readonly uint MASK_RECOGNITION = 0x0040;
        private readonly uint MASK_WING = 0x0080;
        private readonly uint MASK_LOGO = 0x0100;
        private readonly uint MASK_CABIN = 0x0200;

        private readonly int CHECK_PERIOD = 10;
        private DateTime latestCheckTimestamp = TimeUtil.GetOldTimestamp();

        private SimconnectBridge simconnectBridge = null;
        private ConfigService configService = null;
        private EventService eventService = null;

        public LightManager(SimconnectBridge simconnectBridge, ConfigService configService, EventService eventService)
        {
            Console.WriteLine("Constructing LightManager");

            this.simconnectBridge = simconnectBridge;
            this.configService = configService;
            this.eventService = eventService;

            // events
            eventService.AddHandler(EventTopic.DEV_CMD, HandleDevCmd);

            // set panel lights on when we connect
            simconnectBridge.fireSimEvent(SIM_EVENT.PANEL_LIGHTS_ON);
        }

        private void HandleDevCmd(Event myEvent)
        {
            var devCmd = (DevCommand)myEvent.data;

            if (devCmd.command.StartsWith("LIGHT"))
            {
                if (devCmd.args[0].StartsWith("X"))
                {
                    PrintStates();
                }
                else if (devCmd.args[0] == "ALL")
                {
                    simconnectBridge.fireSimEvent(SIM_EVENT.ALL_LIGHTS_TOGGLE);
                }
                else if (devCmd.args[0].StartsWith("R"))
                {
                    simconnectBridge.fireSimEvent(SIM_EVENT.TOGGLE_RECOGNITION_LIGHTS);
                }
                else if (devCmd.args[0].StartsWith("T"))
                {
                    simconnectBridge.fireSimEvent(SIM_EVENT.TOGGLE_TAXI_LIGHTS);
                }
                else if (devCmd.args[0].StartsWith("P"))
                {
                    string panel_arg_str = devCmd.args[1];
                    if (panel_arg_str.StartsWith("T"))
                    {
                        simconnectBridge.fireSimEvent(SIM_EVENT.PANEL_LIGHTS_TOGGLE);
                    }
                    else if (panel_arg_str == "ON")
                    {
                        simconnectBridge.fireSimEvent(SIM_EVENT.PANEL_LIGHTS_ON);
                    }
                    else if (panel_arg_str == "OFF")
                    {
                        simconnectBridge.fireSimEvent(SIM_EVENT.PANEL_LIGHTS_OFF);
                    }
                    else
                    {
                        // light:p,1
                        uint state = (uint)devCmd.GetArgAsInt(1);
                        simconnectBridge.fireSimEvent(SIM_EVENT.PANEL_LIGHTS_SET, state);
                    }
                }
                else if (devCmd.args[0].StartsWith("L"))
                {
                    // doesn't work in TBM :(
                    if (devCmd.args.Length < 2)
                    {
                        simconnectBridge.fireSimEvent(SIM_EVENT.LANDING_LIGHTS_TOGGLE);
                    }
                    else
                    {
                        uint state = (uint)devCmd.GetArgAsInt(1);
                        simconnectBridge.fireSimEvent(SIM_EVENT.LANDING_LIGHTS_SET, state);
                        //if (state == 0)
                        //{
                        //    simconnectBridge.fireSimEvent(EVENT_ID.LANDING_LIGHTS_OFF, state);
                        //}
                        //else
                        //{
                        //    simconnectBridge.fireSimEvent(EVENT_ID.LANDING_LIGHTS_ON, state);
                        //}
                    }
                }
            }
        }

        private void PrintStates()
        {
            Console.WriteLine($"Nav:         {simconnectBridge.IsBitSet(SimVars.LIGHT_STATES, MASK_NAV)}");
            Console.WriteLine($"Beacon:      {simconnectBridge.IsBitSet(SimVars.LIGHT_STATES, MASK_BEACON)}");
            Console.WriteLine($"Landing:     {simconnectBridge.IsBitSet(SimVars.LIGHT_STATES, MASK_LANDING)}");
            Console.WriteLine($"Taxi:        {simconnectBridge.IsBitSet(SimVars.LIGHT_STATES, MASK_TAXI)}");
            Console.WriteLine($"Strobe:      {simconnectBridge.IsBitSet(SimVars.LIGHT_STATES, MASK_STROBE)}");
            Console.WriteLine($"Panel:       {simconnectBridge.IsBitSet(SimVars.LIGHT_STATES, MASK_PANEL)}");
            Console.WriteLine($"Recognition: {simconnectBridge.IsBitSet(SimVars.LIGHT_STATES, MASK_RECOGNITION)}");
            Console.WriteLine($"Wing:        {simconnectBridge.IsBitSet(SimVars.LIGHT_STATES, MASK_WING)}");
            Console.WriteLine($"Logo:        {simconnectBridge.IsBitSet(SimVars.LIGHT_STATES, MASK_LOGO)}");
            Console.WriteLine($"Cabin:       {simconnectBridge.IsBitSet(SimVars.LIGHT_STATES, MASK_CABIN)}");
        }

        internal void CheckLights()
        {
            if (!TimeUtil.IsTimedOut(latestCheckTimestamp, CHECK_PERIOD))
            {
                return;
            }
            latestCheckTimestamp = DateTime.Now;


            double altAgl = simconnectBridge.GetAltitudeAgl();

            // set taxi lights
            bool currentTaxiLightState = simconnectBridge.IsBitSet(SimVars.LIGHT_STATES, MASK_TAXI);
            bool desiredTaxiLightState = (altAgl < 500);
            if (currentTaxiLightState != desiredTaxiLightState)
            {
                simconnectBridge.fireSimEvent(SIM_EVENT.TOGGLE_TAXI_LIGHTS);
                Console.WriteLine($"toggling taxi lights: {desiredTaxiLightState}");
            }

            // set landing lights
            bool currentLandingLightState = simconnectBridge.IsBitSet(SimVars.LIGHT_STATES, MASK_LANDING);

            bool desiredLandingLightState;
            if (configService.GetMiscCfg().autoCruise.cruiseAlt > 20000)
            {
                // Airliner
                double currentAlt = simconnectBridge.GetCurrentAltitude();
                desiredLandingLightState = (currentAlt < 11000);
            }
            else
            {
                // GA
                desiredLandingLightState = (altAgl < 2000);
            }

            if (currentLandingLightState != desiredLandingLightState)
            {
                simconnectBridge.fireSimEvent(SIM_EVENT.LANDING_LIGHTS_TOGGLE);
                Console.WriteLine($"toggling landing lights: {desiredLandingLightState}");
            }
        }

    }
}
