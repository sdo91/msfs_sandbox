﻿using FsAutopilot.Config;
using FsAutopilot.Events;
using FsAutopilot.Hardware;
using FsAutopilot.Simconnect;
using FsAutopilot.Util;
using System;
using System.Collections.Generic;

namespace FsAutopilot.Autopilot
{
    public class FuelManager
    {

        /// <summary>
        /// 
        /// see:
        /// http://www.prepar3d.com/SDKv3/LearningCenter/utilities/variables/simulation_variables.html#fuel%20tank%20selection
        /// 
        /// staggerwing:
        /// 6,19,0,20,5,4
        /// 
        /// </summary>
        public enum FUEL_TANK
        {
            OFF = 0,
            //ALL = 1,
            LEFT = 2,
            RIGHT = 3,
            LEFT_AUX = 4,
            RIGHT_AUX = 5,
            CENTER = 6,
            CENTER2 = 7,
            CENTER3 = 8,
            BOTH = 16,
            LEFT_MAIN = 19,
            RIGHT_MAIN = 20,
        };

        private readonly int CHECK_PERIOD = 60;
        private readonly double MAX_IMBALANCE = 0.5;

        private DateTime latestCheckTimestamp;

        private SimconnectBridge simconnectBridge = null;
        private ConfigService configService = null;
        private EventService eventService = null;

        private MovingAverage movingRangeEstimate = new MovingAverage(0.1);

        private List<FUEL_TANK> availableTanks = null;

        private readonly Timestamp engineLostStamp = Timestamp.Zero();

        // precompute
        private double fuel_capacity_lbs_;
        private double lbs_per_pecent_fuel_;

        /// <summary>
        /// NOTE: this is called when a new aircraft is connected
        /// 
        /// todo
        ///     support staggerwing
        ///     support beaver
        /// </summary>
        public FuelManager(SimconnectBridge simconnectBridge, ConfigService configService, EventService eventService)
        {
            Console.WriteLine("Constructing FuelManager");

            latestCheckTimestamp = TimeUtil.GetOldTimestamp();

            this.simconnectBridge = simconnectBridge;
            this.configService = configService;
            this.eventService = eventService;

            PrecomputeValues();

            // events
            eventService.AddHandler(EventTopic.DEV_CMD, HandleDevCmd);
        }

        private void PrecomputeValues()
        {
            fuel_capacity_lbs_ = simconnectBridge.GetFuelCapacityLbs();
            lbs_per_pecent_fuel_ = fuel_capacity_lbs_ / 100.0;
        }

        /// <summary>
        /// 
        /// examples:
        /// TANK:2  // swith to left
        /// TANK:3  // swith to right
        /// 
        /// </summary>
        private void HandleDevCmd(Event myEvent)
        {
            var devCmd = (DevCommand)myEvent.data;

            if (devCmd.command == "TANK")
            {
                int tankId = 0;
                Int32.TryParse(devCmd.args[0], out tankId);
                FUEL_TANK desiredTank = (FUEL_TANK)tankId;
                SelectTank(desiredTank);
            }
        }

        public void Process(bool is_active)
        {
            CheckTanks();
            CheckFuelBurn();
            CheckOutOfFuel();

            if (is_active)
            {
                Console.WriteLine();
            }
        }

        /// <summary>
        ///
        /// algo:
        /// check if time to run
        /// get tanks
        /// check all for tank w/ most fuel
        /// compare to selected tank
        /// if need to change, fire the event
        ///
        /// </summary>
        private void CheckTanks()
        {
            FuelConfig config = configService.GetFuelCfg();
            if (!config.balanceFuel)
            {
                return;
            }

            if (!TimeUtil.IsTimedOut(latestCheckTimestamp, CHECK_PERIOD) || simconnectBridge == null)
            {
                return;
            }

            // time to check
            Console.WriteLine("Checking fuel tanks");
            latestCheckTimestamp = DateTime.Now;

            try
            {
                // scan for tanks
                if (availableTanks == null)
                {
                    availableTanks = GetAvailableTanks();
                }

                // find tank w/ most fuel
                double maxGallons = 0;
                FUEL_TANK maxTank = FUEL_TANK.OFF;
                foreach (FUEL_TANK tank in availableTanks)
                {
                    double gallons = GetFuelQuantity(tank);
                    if (gallons > maxGallons)
                    {
                        maxGallons = gallons;
                        maxTank = tank;
                    }
                }
                Console.WriteLine($"maxTank: {maxTank}");

                // find selected tank
                FUEL_TANK selectedTank = (FUEL_TANK)simconnectBridge.GetValue(SimVars.FUEL_TANK_SELECTOR_1);
                Console.WriteLine($"currently selected tank: {selectedTank}");
                if (maxTank == selectedTank)
                {
                    Console.WriteLine("best tank already selected");
                    return;
                }

                // compare
                double selectedGallons = GetFuelQuantity(selectedTank);
                double imbalance = maxGallons - selectedGallons;
                if (imbalance > MAX_IMBALANCE)
                {
                    Console.WriteLine($"switching tanks: {selectedTank} -> {maxTank}");
                    SelectTank(maxTank);
                }
                else
                {
                    Console.WriteLine($"imbalance small ({imbalance} < {MAX_IMBALANCE})");
                }
            }
            catch
            {
                Console.WriteLine("checkFuel exception");
            }
        }

        private void SelectTank(FUEL_TANK desiredTank)
        {
            if (configService.GetFuelCfg().useVjoyFuelSwitch)
            {
                // eg: turbo arrow
                if (desiredTank == FUEL_TANK.LEFT)
                {
                    eventService.FireEvent(EventTopic.VJOY_BUTTON_REQUEST, VJoyFeeder.BUTTON_FUEL_SELECTOR_1_LEFT);
                }
                else if (desiredTank == FUEL_TANK.RIGHT)
                {
                    eventService.FireEvent(EventTopic.VJOY_BUTTON_REQUEST, VJoyFeeder.BUTTON_FUEL_SELECTOR_1_RIGHT);
                }
            }
            else
            {
                simconnectBridge.fireSimEvent(SIM_EVENT.FUEL_SELECTOR_SET, (uint)desiredTank);
            }
        }

        private List<FUEL_TANK> GetAvailableTanks()
        {
            // scan tanks (beaver)
            List<FUEL_TANK> result = new List<FUEL_TANK>();

            // handle special cases
            string aircraftName = configService.GetAircraftCfg().name;
            if (aircraftName == "EXTRA_330")
            {
                result.Add(FUEL_TANK.BOTH);
                result.Add(FUEL_TANK.CENTER);
                return result;
            }

            // scan for tanks
            foreach (FUEL_TANK tank in Enum.GetValues(typeof(FUEL_TANK)))
            {
                if (tank == FUEL_TANK.BOTH)
                {
                    continue;
                }

                if (GetFuelQuantity(tank) > 0)
                {
                    result.Add(tank);
                    Console.WriteLine($"Found tank: {tank}");
                }
            }
            return result;
        }

        private double GetFuelQuantity(FUEL_TANK tankId)
        {
            string fuelQuantitySimVar = ToSimVar(tankId);
            if (fuelQuantitySimVar == "")
            {
                return 0;
            }
            return simconnectBridge.GetValue(fuelQuantitySimVar);
        }

        private double GetFuelQuantityLbs(FUEL_TANK tankId)
        {
            double qtyGallons = GetFuelQuantity(tankId);
            double lbsPerGallon = simconnectBridge.GetFuelLbsPerGallon();
            return qtyGallons * lbsPerGallon;
        }

        private string ToSimVar(FUEL_TANK tankId)
        {
            switch (tankId)
            {
                case FUEL_TANK.LEFT:
                case FUEL_TANK.BOTH:
                    return SimVars.FUEL_LEFT_QUANTITY;
                case FUEL_TANK.RIGHT:
                    return SimVars.FUEL_RIGHT_QUANTITY;
                case FUEL_TANK.CENTER:
                    return SimVars.FUEL_TANK_CENTER_QUANTITY;
                case FUEL_TANK.CENTER2:
                    return SimVars.FUEL_TANK_CENTER_2_QUANTITY;
                case FUEL_TANK.CENTER3:
                    return SimVars.FUEL_TANK_CENTER_3_QUANTITY;
                default:
                    return "";
            }
        }

        /// <summary>
        /// 
        /// rate lbs/nm = PPH * NUM ENG / GS
        /// 
        /// next:
        /// est fuel at dest
        /// 
        /// time to next/dist to next
        /// 
        /// GPS ETE
        /// 
        /// 
        /// </summary>
        private void CheckFuelBurn()
        {
            double gs_knots = simconnectBridge.GetValue(SimVars.GROUND_VELOCITY);
            if (gs_knots < 5)
            {
                return;
            }

            double fudge_factor = (100 + configService.GetFuelCfg().fuelFudgeFactor) / 100.0;

            // calc burn rate (lbs/hr)
            double num_engines = simconnectBridge.GetValue(SimVars.NUMBER_OF_ENGINES);
            double burn_rate_pph = simconnectBridge.GetValue(SimVars.ENG_FUEL_FLOW_PPH_1) * num_engines;
            burn_rate_pph *= fudge_factor;

            // calc fuel levels
            double current_fuel_lbs = simconnectBridge.GetValue(SimVars.FUEL_TOTAL_QUANTITY_WEIGHT);
            double unusable_fuel_lbs = CalcLbsUnusable();
            EstimateRangeFullTank(burn_rate_pph, unusable_fuel_lbs);

            // calc burn rate (lbs/nm = lbs/hr * hr/nm)
            double burn_rate_ppnm = burn_rate_pph / gs_knots;
            Console.WriteLine("fuel burn: {0:f2} lbs/hr, {1:f2} lbs/nm", burn_rate_pph, burn_rate_ppnm);

            // NM left = lbs * (nm/lb)
            double usable_fuel_lbs = current_fuel_lbs - unusable_fuel_lbs;
            int nm_left_in_tank = (int)(usable_fuel_lbs / burn_rate_ppnm);
            Console.WriteLine($"nm_left_in_tank: {nm_left_in_tank} NM");

            // estimate remaining fuel
            EstimateRemaining(burn_rate_pph, current_fuel_lbs, unusable_fuel_lbs);
        }

        private void EstimateRangeFullTank(double burn_rate_pph, double unusable_fuel_lbs)
        {
            double ktas = simconnectBridge.GetTrueAirspeed();
            double burn_rate_ppnm = burn_rate_pph / ktas;
            double usable_capacity_lbs = fuel_capacity_lbs_ - unusable_fuel_lbs;

            // calc range of full tank (range NM = lbs * nm/lb)
            double range_with_full_tank = usable_capacity_lbs / burn_rate_ppnm;
            range_with_full_tank = movingRangeEstimate.Update(range_with_full_tank);
            Console.WriteLine("stats w/o wind: full range={0:f0} NM (@{1:f1} KTAS, {2:f2} lbs/nm)",
                range_with_full_tank, ktas, burn_rate_ppnm);
        }

        private void EstimateRemaining(double burn_rate_pph, double current_fuel_lbs, double unusable_fuel_lbs)
        {
            // calc hours to target
            double hours_to_landing = simconnectBridge.GetGpsMinutesToLanding() / 60;
            if (hours_to_landing == 0)
            {
                return;
            }

            // target_str
            string target_str = "final";
            if (configService.GetFlightPlanConfig().isLandingAtNextWaypoint)
            {
                target_str = "next";
            }

            // calc estimated fuel at target waypoint
            double estimated_burn_lbs = hours_to_landing * burn_rate_pph;

            double estimated_usable_lbs_at_target = current_fuel_lbs - estimated_burn_lbs - unusable_fuel_lbs;
            Console.WriteLine("estimated_lbs (at {0:s}): {1:f1} lbs (= {2:f1} - {3:f1} - {4:f1})",
                target_str, estimated_usable_lbs_at_target, current_fuel_lbs, estimated_burn_lbs, unusable_fuel_lbs);

            // estimate %
            double estimated_total_lbs_at_target = current_fuel_lbs - estimated_burn_lbs;
            Console.WriteLine("estimated fuel (at {0:s}): {1:f1}%", target_str, estimated_total_lbs_at_target / lbs_per_pecent_fuel_);

            // estimate time before active tank is empty
            FUEL_TANK selectedTank = (FUEL_TANK)simconnectBridge.GetValue(SimVars.FUEL_TANK_SELECTOR_1);
            double selectedTankLbs = GetFuelQuantityLbs(selectedTank);
            double minutesLeftInSelectedTank = 60 * selectedTankLbs / burn_rate_pph;
            Console.WriteLine("minutesLeftInSelectedTank: {0:f1}", minutesLeftInSelectedTank);
        }

        private double CalcLbsUnusable()
        {
            double percentUnusable = configService.GetFuelCfg().fuelCutoffPercentage + 1;
            return percentUnusable * lbs_per_pecent_fuel_;
        }

        /// <summary>
        ///
        /// check if we have run out of fuel
        /// signs (tested w/ turbo arrow):
        /// prop thrust is negative
        /// rpm is low
        /// fuel flow:
        ///     drops, but not to 0
        ///     freezes
        ///
        /// </summary>
        private void CheckOutOfFuel()
        {
            if (!simconnectBridge.IsEngineLost())
            {
                return;
            }

            // todo: fix bug where this triggers after being paused

            Console.WriteLine("[FuelManager] engine lost!");
            if (configService.GetFlightPlanConfig().pauseOnEngineLoss)
            {
                if (engineLostStamp.IsTimedOut(60))
                {
                    engineLostStamp.SetToNow();
                    eventService.FireEvent(EventTopic.IR_REMOTE_COMMAND, "PAUSE_SIM");
                }
            }
        }

    }
}
