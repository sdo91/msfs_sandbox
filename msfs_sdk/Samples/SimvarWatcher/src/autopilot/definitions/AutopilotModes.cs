﻿using System;

namespace FsAutopilot.Autopilot
{
    public enum PitchMode
    {
        AltHold,
        AutoCruise,
        FlcMode,
        AglMode,
        VertSpeed,
        PitchHold,
        Off,
    }

    public enum RollMode
    {
        Off,
        RollHold,
        HeadingHold,
        Gps,
        Vor,
    }

    public enum ThrottleMode
    {
        IndicatedAirspeedHold,
        TrueAirspeedHold,
    }

    /// <summary>
    ///
    /// NOTE: the order of these modes will be used in the UI dropdowns
    ///
    /// </summary>
    public class AutopilotModes
    {
        public const string OFF = "Off";

        public const string ALTITUDE = "Altitude";
        public const string AUTO_CRUISE = "Auto Cruise";
        public const string FLC = "FLC";
        public const string AGL = "AGL";
        public const string VERT_SPEED = "Vert Speed";
        public const string PITCH = "Pitch";

        public const string ROLL = "Roll";
        public const string HEADING = "Heading";
        public const string GPS = "GPS";
        public const string VOR = "VOR";

        public const string INDICATED_AIRSPEED = "Indicated Airspeed";
        public const string TRUE_AIRSPEED = "True Airspeed";

        // NOTE: the order of these modes will be used in the UI dropdowns
        public static readonly string[] PITCH_MODES = {
            ALTITUDE,
            AUTO_CRUISE,
            FLC,
            AGL,
            VERT_SPEED,
            PITCH,
            OFF,
        };

        public static readonly string[] ROLL_MODES = {
            OFF,
            ROLL,
            HEADING,
            GPS,
            VOR,
        };

        public static readonly string[] THROTTLE_MODES = {
            INDICATED_AIRSPEED,
            TRUE_AIRSPEED,
        };

        public static string ToString(PitchMode pitchMode)
        {
            switch (pitchMode)
            {
                case PitchMode.AltHold:
                    return ALTITUDE;
                case PitchMode.AutoCruise:
                    return AUTO_CRUISE;
                case PitchMode.FlcMode:
                    return FLC;
                case PitchMode.AglMode:
                    return AGL;
                case PitchMode.VertSpeed:
                    return VERT_SPEED;
                case PitchMode.PitchHold:
                    return PITCH;
                case PitchMode.Off:
                    return OFF;
                default:
                    throw new ApplicationException($"[AutopilotModes] unrecognized PitchMode: {pitchMode}");
            }
        }

        public static string ToString(RollMode rollMode)
        {
            switch (rollMode)
            {
                case RollMode.Off:
                    return OFF;
                case RollMode.RollHold:
                    return ROLL;
                case RollMode.HeadingHold:
                    return HEADING;
                case RollMode.Gps:
                    return GPS;
                case RollMode.Vor:
                    return VOR;
                default:
                    throw new ApplicationException($"[AutopilotModes] unrecognized RollMode: {rollMode}");
            }
        }

        public static PitchMode ToPitchMode(string pitchModeStr)
        {
            switch (pitchModeStr)
            {
                case ALTITUDE:
                    return PitchMode.AltHold;
                case AUTO_CRUISE:
                    return PitchMode.AutoCruise;
                case FLC:
                    return PitchMode.FlcMode;
                case AGL:
                    return PitchMode.AglMode;
                case VERT_SPEED:
                    return PitchMode.VertSpeed;
                case PITCH:
                    return PitchMode.PitchHold;
                case OFF:
                    return PitchMode.Off;
                default:
                    throw new ApplicationException($"[AutopilotModes] unrecognized pitchModeStr: {pitchModeStr}");
            }
        }

        public static RollMode ToRollMode(string rollModeStr)
        {
            switch (rollModeStr)
            {
                case OFF:
                    return RollMode.Off;
                case ROLL:
                    return RollMode.RollHold;
                case HEADING:
                    return RollMode.HeadingHold;
                case GPS:
                    return RollMode.Gps;
                case VOR:
                    return RollMode.Vor;
                default:
                    throw new ApplicationException($"[AutopilotModes] unrecognized rollModeStr: {rollModeStr}");
            }
        }

        public static ThrottleMode ToThrottleMode(string throttleModeStr)
        {
            switch (throttleModeStr)
            {
                case INDICATED_AIRSPEED:
                    return ThrottleMode.IndicatedAirspeedHold;
                case TRUE_AIRSPEED:
                    return ThrottleMode.TrueAirspeedHold;
                default:
                    throw new ApplicationException($"[AutopilotModes] unrecognized throttleModeStr: {throttleModeStr}");
            }
        }

    } // end class AutopilotModes
}
