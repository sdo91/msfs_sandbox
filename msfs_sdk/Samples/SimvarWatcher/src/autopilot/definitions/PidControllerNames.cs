﻿namespace FsAutopilot.Autopilot
{
    /// <summary>
    ///
    /// NOTE: while these don't map 1:1 with AP modes, there are often similarities
    ///
    /// places to add a new PID controller/flight mode:
    ///     PidControllerNames
    ///     AutopilotModes
    ///     PidControlConfig
    ///     MapControllerName()
    ///     pick a similar existing controller and search
    ///     maybe:
    ///         PidResponseData
    ///         PidResponseChart.GetDefaultYAxisMax()
    ///         AlphaYoke (if a new roll mode)
    /// 
    /// </summary>
    public class PidControllerNames
    {
        public const string ALTITUDE = "Altitude";
        public const string GLIDE_SLOPE = "Glide Slope";
        public const string FLC = "FLC";
        public const string VERT_SPEED = "Vert Speed";
        public const string PITCH = "Pitch";
        public const string PITCH_2 = "Pitch 2";
        public const string PITCH_RATE = "Pitch Rate";

        public const string NAVIGATION = "Navigation";
        public const string HEADING = "Heading";
        public const string ROLL = "Roll";

        public const string AIRSPEED = "Airspeed";

        // used to populate the UI dropdown menu (to select which controller to tune)
        public static readonly string[] CONTROLLER_NAMES = {
            ALTITUDE,
            GLIDE_SLOPE,
            FLC,
            VERT_SPEED,
            PITCH,
            PITCH_2,
            PITCH_RATE,

            NAVIGATION,
            HEADING,
            ROLL,

            AIRSPEED,
        };
    }
}
