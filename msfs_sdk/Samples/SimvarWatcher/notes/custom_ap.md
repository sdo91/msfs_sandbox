
# using the custom AutoPilot

### start/connect
- start visual studio
    - use the windows start menu
    - it's the purple infinity icon
- open the SimVars project
- click the start button at the top (w/ the green play icon)
    - you should see a white window open
- once the aircraft is loaded in the sim, click the large 'Connect' button

### Roll mode
- Select the roll mode with the dropdown menu in the bottom left
- Roll Modes:
    - Nav: 
        - AP will follow the GPS flight plan (aka 'the magenta line')
        - Note: this does not work with VOR/LOC yet
    - Heading: 
        - AP will maintain the target heading indicated by the heading bug 
        - you set the heading bug in the sim or using the logitech panel
    - Roll:
        - AP will maintain the target roll angle 
        - default roll angle is 0 (wings level)
    - Off:
        - AP will not control the aircraft roll
        - you must control the roll yourself with the yoke

### Pitch mode
- Select the pitch mode with the 2nd dropdown menu
- Pitch Modes:
    - Altitude:
        - AP will climb/descend to and maintain target altitude
        - AP will maintain the target Vertical Speed
            - default VS is saved per aicraft
            - VS can be adjusted in the 'Vert Speed' text box
    - FLC:
        - AP will climb/descend to and maintain target altitude
        - AP will maintain the target Airspeed
            - can be seen in the 'Airspeed' text box
            - can be adjusted using the up/down buttons below the throttle lever
    - AGL:
        - AP will try to maintain the target altitude Above Ground Level
    - Vert Speed:
        - AP will maintain target VS (make negative to descend)
        - mainly just used to help me tune the PID controllers
    - Pitch
        - AP will maintain target pitch
        - mainly just used to help me tune the PID controllers
    - Off
        - AP will not control the aircraft pitch
        
### Activating the AutoPilot
- use the left of the 2 white switches on the yoke to enable/disable the AutoPilot


