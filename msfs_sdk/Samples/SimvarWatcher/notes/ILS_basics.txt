
this guide assumes you are using the C172 G1000
    other planes should be similar, but could be slightly different

from the world map, make or load a flight plan
    to load: 
        more -> load/save -> load 
        then double click desired .pln file
        example: KBVU-KLAS.pln

    [optional] select desired takeoff/landing runways
        you can do this from the dropdowns near the top of the screen
        or by zooming into the airport and clicking a white dot
        example: select KLAS 26L
            
    click fly button when ready

before takeoff:
    select the first leg of your flight plan
        (hopefully MSFS fixes this bug eventually and we won't have to do this)
        zoom to the MFD (Multi-Function Display)
            (all the way down and then 1 to the right using the xbox or yoke D-pad)
        press the FPL (Flight PLan) button to the bottom right of the MFD
        press the FMS knob to activate the selection cursor
            (located to the bottom right of the MFD)
            hold the left and then click the right mouse button to press the knob
            you should see a blinking cursor on your first waypoint
        scroll the FMS knob to select your second waypoint
            (hover over the large knob and use the mouse scroll wheel)
        click the ACT LEG (ACTivate LEG) softkey
            (at the bottom of the screen)
            you should see the magenta line now point from your 1st to your 2nd waypoint
        click FPL again to close that menu

    tune NAV1 to the desired ILS frequency
        Lookup the desired ILS frequency:
            you can use Little Navmap
            or a website like skyvector
            or an app like flyQ
            example: KLAS 26L is 111.50
        use the black knob to the top left of the PFD (Primary Flight Display)
        don't forget to push the swap button to make it the active frequency

    [optional] configure the BRG1 pointer
        (this is the blue line that points towards the ILS beacon)
        click the PFD softkey
        click the BRG1 softkey 
            NAV1 should show up to the bottom left of the compass
        click the BACK softkey
        
    set up autopilot:
        set desired target altitude and vertical speed
        press the orange button to select NAV mode
        press the orange button to select VS mode
        (you should now see GPS and VS in green at the top of the PFD)
        NOTE: don't press the white AP button yet

do the flight!
    takeoff/start climb
    activate the autopilot
    fly towards a point a ~10 miles out from the ILS runway
        example: LARRE

    once the AP starts turning for final:
        click CDI softkey on PFD
            (this is to switch the NAV sourse from GPS to the ILS localizer)
            magenta arrow should switch to green
            should see LOC at the top of the PFD
            glideslope diamond should also turn green
            the aircraft should start to intercept the localizer

    before the green diamond drops to the middle
        click orange APR button (APpRoach)
            should see GS appear in white at the top of the PFD
            (this means glideslope mode is armed, but not yet active)

    intercept the glideslope:
        once the green diamond drops to the middle, the AP should intercept the glideslope
        you'll see GS in green at the top of the PFD
        the plane will start to descend

    landing
        manage throttle/flaps down to the runway
        don't forget to turn off the AP when you get close
        land!

