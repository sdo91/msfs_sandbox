﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using FsAutopilot.Events;

namespace FsAutopilot.Hardware.Tests
{
    [TestClass()]
    public class ArduinoBridgeTests
    {
        /// <summary>
        /// todo
        /// 
        /// init the service
        /// handle a power button press
        /// 
        /// </summary>
        [TestMethod()]
        public void ArduinoBridgeTest()
        {
            EventService eventService = EventService.Create(true);
            ArduinoBridge arduinoBridge = new ArduinoBridge(eventService, null, null);

            arduinoBridge.Start();
        }
    }
}