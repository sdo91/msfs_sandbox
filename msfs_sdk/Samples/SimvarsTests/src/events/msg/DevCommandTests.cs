﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using FsAutopilot.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FsAutopilot.Events.Tests
{
    [TestClass()]
    public class DevCommandTests
    {
        [TestMethod()]
        public void DevCommandTest()
        {
            DevCommand devCommand;

            devCommand = new DevCommand("cmd");
            Assert.AreEqual("CMD", devCommand.command);
            Assert.AreEqual(0, devCommand.args.Length);

            devCommand = new DevCommand("cmd:1,2,3");
            Assert.AreEqual("CMD", devCommand.command);
            Assert.AreEqual(3, devCommand.args.Length);
            Assert.AreEqual("1", devCommand.args[0]);
            Assert.AreEqual(2, devCommand.GetArgAsInt(1));
            Assert.AreEqual("{DevCommand: cmd=CMD, args=[1, 2, 3]}", devCommand.ToString());
        }
    }
}