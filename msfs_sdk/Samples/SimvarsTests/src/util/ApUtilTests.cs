﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;
using System.Collections;

namespace FsAutopilot.Util.Tests
{
    [TestClass()]
    public class ApUtilTests
    {

        [TestMethod]
        public void GetProjectPathTest()
        {
            string path = ApUtil.GetProjectPath();
            Assert.AreEqual(true, path.EndsWith("msfs_sandbox/msfs_sdk/Samples/SimvarWatcher"));
        }

        [TestMethod()]
        public void WrapToMatchTest()
        {
            var wrapped = ApUtil.WrapToMatch(280, 80);
            Assert.AreEqual(-80, wrapped);

            Assert.AreEqual(180, ApUtil.WrapToMatch(180, 0));
        }

        [TestMethod()]
        public void AbsAngleDiffTest()
        {
            Assert.AreEqual(10, ApUtil.AbsAngleDiff(20, 30));
            Assert.AreEqual(10, ApUtil.AbsAngleDiff(30, 20));

            Assert.AreEqual(2, ApUtil.AbsAngleDiff(1, 359));
            Assert.AreEqual(2, ApUtil.AbsAngleDiff(359, 1));
        }

        [TestMethod()]
        public void RoundToTest()
        {
            Assert.AreEqual(100, ApUtil.RoundTo(123, 100));
            Assert.AreEqual(85, ApUtil.RoundTo(86, 5));
        }

        [TestMethod()]
        public void RoundUpTest()
        {
            Assert.AreEqual(0, ApUtil.RoundUp(0, 1));
            Assert.AreEqual(1, ApUtil.RoundUp(0.01, 1));
            Assert.AreEqual(1, ApUtil.RoundUp(0.99, 1));
            Assert.AreEqual(1, ApUtil.RoundUp(1, 1));
            Assert.AreEqual(2, ApUtil.RoundUp(1.01, 1));

            Assert.AreEqual(200, ApUtil.RoundUp(123, 100));
            Assert.AreEqual(90, ApUtil.RoundUp(86, 5));
        }

        [TestMethod()]
        public void IsNeoflyRunningTest()
        {
            bool x = ApUtil.IsNeoflyRunning();
            Console.WriteLine($"IsNeoflyRunning: {x}");
            //Assert.IsFalse(x);
        }

        [TestMethod()]
        public void GetMatchingProcessNamesTest()
        {
            var names = ApUtil.GetMatchingProcessNames("flight");
            Console.WriteLine($"matches: {ApUtil.FormatList(names)}");
            Assert.IsTrue(names.Count == 0 || names.Count == 1);
        }

        [TestMethod()]
        public void CalcXtkLeftTest()
        {
            double dme = 31;
            double delta = 0.01;

            Assert.AreEqual(0, ApUtil.CalcXtkLeft(0, dme), delta);
            Assert.AreEqual(0, ApUtil.CalcXtkLeft(180, dme), delta);

            Assert.AreEqual(0.54, ApUtil.CalcXtkLeft(1, dme), delta);
            Assert.AreEqual(-0.54, ApUtil.CalcXtkLeft(-1, dme), delta);

            Assert.AreEqual(0.54, ApUtil.CalcXtkLeft(179, dme), delta);
            Assert.AreEqual(-0.54, ApUtil.CalcXtkLeft(-179, dme), delta);

            Assert.AreEqual(-30.43, ApUtil.CalcXtkLeft(-79, dme), delta);
            Assert.AreEqual(-5.91, ApUtil.CalcXtkLeft(-169, dme), delta);
            Assert.AreEqual(30.53, ApUtil.CalcXtkLeft(100, dme), delta);
            Assert.AreEqual(5.68, ApUtil.CalcXtkLeft(10.55, dme), delta);
        }

        [TestMethod()]
        public void CalcNavInterceptAngleTest()
        {
            double radialErrorDegLeft = 30;
            double approachDistanceNm = 6;

            double interceptAngleDeg;

            // normal (below intercept point)
            interceptAngleDeg = ApUtil.CalcNavInterceptAngle(radialErrorDegLeft, 8, approachDistanceNm);
            Assert.AreEqual(76.9, interceptAngleDeg);

            // normal, but on the right side
            interceptAngleDeg = ApUtil.CalcNavInterceptAngle(-radialErrorDegLeft, 8, approachDistanceNm);
            Assert.AreEqual(76.9, interceptAngleDeg);

            // 90 deg from intercept point
            interceptAngleDeg = ApUtil.CalcNavInterceptAngle(radialErrorDegLeft, 6.93, approachDistanceNm);
            Assert.AreEqual(90.0, interceptAngleDeg);

            // between intercept point and airport
            interceptAngleDeg = ApUtil.CalcNavInterceptAngle(radialErrorDegLeft, 5, approachDistanceNm);
            Assert.AreEqual(123.7, interceptAngleDeg);

            // 90 deg from airport
            interceptAngleDeg = ApUtil.CalcNavInterceptAngle(90, 6, approachDistanceNm);
            Assert.AreEqual(135.0, interceptAngleDeg);

            // above airport
            interceptAngleDeg = ApUtil.CalcNavInterceptAngle(135, 10, approachDistanceNm);
            Assert.AreEqual(151.6, interceptAngleDeg);
        }

        [TestMethod()]
        public void CalcAirfieldElevationTest()
        {
            double indicatedAlt, glideSlopeError, dmeDist, airfieldElevation;
            double rawGlideSlope = 3;
            double delta = 1;

            indicatedAlt = 4500;
            glideSlopeError = 0.3;
            dmeDist = 10;
            airfieldElevation = ApUtil.CalcAirfieldElevation(indicatedAlt, rawGlideSlope, glideSlopeError, dmeDist);
            Assert.AreEqual(1002, airfieldElevation, delta);

            // numbers from KLAS
            indicatedAlt = 2052.7;
            glideSlopeError = -2.88;
            dmeDist = 1.912;
            airfieldElevation = ApUtil.CalcAirfieldElevation(indicatedAlt, rawGlideSlope, glideSlopeError, dmeDist);
            Assert.AreEqual(2028, airfieldElevation, delta);

            indicatedAlt = 3981.4;
            glideSlopeError = -0.68;
            dmeDist = 9.55;
            airfieldElevation = ApUtil.CalcAirfieldElevation(indicatedAlt, rawGlideSlope, glideSlopeError, dmeDist);
            Assert.AreEqual(1632, airfieldElevation, delta);

            indicatedAlt = 2493.6;
            glideSlopeError = -0.0084;
            dmeDist = 3.105;
            airfieldElevation = ApUtil.CalcAirfieldElevation(indicatedAlt, rawGlideSlope, glideSlopeError, dmeDist);
            Assert.AreEqual(1509, airfieldElevation, delta);
        }

        [TestMethod()]
        public void CalcFeetAboveGlideslopeTest()
        {
            double degAboveGlideSlope, dmeNm;
            double delta = 1;

            degAboveGlideSlope = 0.5;
            dmeNm = 10;
            Assert.AreEqual(530, ApUtil.CalcFeetAboveGlideslope(degAboveGlideSlope, dmeNm), delta);

            degAboveGlideSlope = 0.5;
            dmeNm = 5;
            Assert.AreEqual(265, ApUtil.CalcFeetAboveGlideslope(degAboveGlideSlope, dmeNm), delta);

            degAboveGlideSlope = 0.1;
            dmeNm = 5;
            Assert.AreEqual(53, ApUtil.CalcFeetAboveGlideslope(degAboveGlideSlope, dmeNm), delta);
        }

        [TestMethod()]
        public void EncodeFreqToHexTest()
        {
            // reminder that c# requires types to be the same...
            Assert.AreNotEqual(1, (uint)1);

            Assert.AreEqual((uint)0x0950, ApUtil.EncodeFreqToHex(109.5));
            Assert.AreEqual((uint)0x1235, ApUtil.EncodeFreqToHex(112.35));
        }

        [TestMethod()]
        public void ScaleTest()
        {
            // test normal cases
            Assert.AreEqual(0, ApUtil.Scale(-1, 0, 10, 0, 100));
            Assert.AreEqual(31.4, ApUtil.Scale(3.14, 0, 10, 0, 100));
            Assert.AreEqual(100, ApUtil.Scale(11, 0, 10, 0, 100));

            // test inverted input range
            Assert.AreEqual(100, ApUtil.Scale(-1, 10, 0, 0, 100));
            Assert.AreEqual(68.6, ApUtil.Scale(3.14, 10, 0, 0, 100));
            Assert.AreEqual(0, ApUtil.Scale(11, 10, 0, 0, 100));

            // test inverted output range
            Assert.AreEqual(100, ApUtil.Scale(-1, 0, 10, 100, 0));
            Assert.AreEqual(68.6, ApUtil.Scale(3.14, 0, 10, 100, 0));
            Assert.AreEqual(0, ApUtil.Scale(11, 0, 10, 100, 0));

            // test both ranges inverted
            Assert.AreEqual(0, ApUtil.Scale(-1, 10, 0, 100, 0));
            Assert.AreEqual(31.4, ApUtil.Scale(3.14, 10, 0, 100, 0));
            Assert.AreEqual(100, ApUtil.Scale(11, 10, 0, 100, 0));
        }

        [TestMethod()]
        public void ScaleTest_Altitude()
        {
            // altitude tests
            var altMin = 100;
            var altMax = 1000;
            var vRotate = 70;
            var vClimb = 100;

            Assert.AreEqual(vRotate, ApUtil.Scale(altMin - 10, altMin, altMax, vRotate, vClimb));
            Assert.AreEqual(vRotate, ApUtil.Scale(altMin, altMin, altMax, vRotate, vClimb));
            Assert.AreEqual(73, (int)ApUtil.Scale(200, altMin, altMax, vRotate, vClimb));
            Assert.AreEqual(83, (int)ApUtil.Scale(500, altMin, altMax, vRotate, vClimb));
            Assert.AreEqual(96, (int)ApUtil.Scale(900, altMin, altMax, vRotate, vClimb));
            Assert.AreEqual(vClimb, ApUtil.Scale(altMax, altMin, altMax, vRotate, vClimb));
            Assert.AreEqual(vClimb, ApUtil.Scale(altMax + 10, altMin, altMax, vRotate, vClimb));
        }

        [TestMethod()]
        public void ScaleTest_GlideSlope()
        {
            // glide slope tests
            double feedForwardTerm;
            double ftAboveGs;
            double estimatedGlideSlopeVertSpeed = -500;

            // when 300+ below GS, FF should be 0
            ftAboveGs = -301;
            feedForwardTerm = ApUtil.Scale(ftAboveGs, -300, -200, 0, estimatedGlideSlopeVertSpeed);
            Assert.AreEqual(0, feedForwardTerm);

            // test within the range
            ftAboveGs = -222;
            feedForwardTerm = ApUtil.Scale(ftAboveGs, -300, -200, 0, estimatedGlideSlopeVertSpeed);
            Assert.AreEqual(-390, feedForwardTerm);

            // when 200 below (or above), FF should be 500
            ftAboveGs = -199;
            feedForwardTerm = ApUtil.Scale(ftAboveGs, -300, -200, 0, estimatedGlideSlopeVertSpeed);
            Assert.AreEqual(-500, feedForwardTerm);
        }

        [TestMethod()]
        public void CalcIlsDistanceTest()
        {
            Assert.AreEqual(10, ApUtil.CalcIlsDistance(120, 5));
            Assert.AreEqual(15, ApUtil.CalcIlsDistance(180, 5));
            Assert.AreEqual(5, ApUtil.CalcIlsDistance(100, 3));
        }

        [TestMethod()]
        public void Vector2DAngleDeg()
        {
            double delta = 1e-6;

            Vector2D vec = Vector2D.CreateFromXy(1, 1);
            double angle_deg = vec.AngleDeg();
            Assert.AreEqual(45, angle_deg, delta);

            vec = Vector2D.CreateFromXy(1, -1);
            angle_deg = vec.AngleDeg();
            Assert.AreEqual(135, angle_deg, delta);

            vec = Vector2D.CreateFromXy(-1, -1);
            angle_deg = vec.AngleDeg();
            Assert.AreEqual(225, angle_deg, delta);

            vec = Vector2D.CreateFromXy(-1, 1);
            angle_deg = vec.AngleDeg();
            Assert.AreEqual(315, angle_deg, delta);
        }

        [TestMethod()]
        public void CalcVectorToVorOffsetPoint()
        {
            Vector2D vor_to_ownship = Vector2D.CreateFromRadial(252, 11);
            Vector2D vor_to_destination = Vector2D.CreateFromRadial(43, 126);

            Vector2D ownship_to_destination = ApUtil.CalcVectorToVorOffsetPoint(vor_to_ownship, vor_to_destination);

            double delta = 0.005;
            Assert.AreEqual(-13.30, ownship_to_destination.x, delta);
            Assert.AreEqual(-272.64, ownship_to_destination.y, delta);
            Assert.AreEqual(272.97, ownship_to_destination.Magnitude(), delta);
            Assert.AreEqual(182.79, ownship_to_destination.AngleDeg(), delta);
        }

        [TestMethod()]
        public void CreateFromRadialStr()
        {
            Vector2D vor_to_ownship = Vector2D.CreateFromRadialStr("16.5/104");
            Vector2D vor_to_destination = Vector2D.CreateFromRadialStr("6.5/154");

            Vector2D ownship_to_destination = ApUtil.CalcVectorToVorOffsetPoint(vor_to_ownship, vor_to_destination);

            double delta = 0.005;
            Assert.AreEqual(13.29, ownship_to_destination.Magnitude(), delta);
            Assert.AreEqual(262.00, ownship_to_destination.AngleDeg(), delta);
        }

        [TestMethod()]
        public void NegateString()
        {
            Assert.AreEqual("500", ApUtil.NegateString("-500"));
            Assert.AreEqual("-500", ApUtil.NegateString("500"));
            Assert.AreEqual("x", ApUtil.NegateString("-x"));
            Assert.AreEqual("-x", ApUtil.NegateString("x"));

            // NOTE: this case is not handled:
            //Assert.AreEqual("0", ApUtil.NegateString("0"));
        }

        [TestMethod()]
        public void AdjustRangeToFitX()
        {
            // handle case: put x close to the edge
            double max = 30;
            double min = 20;
            ApUtil.AdjustRangeToFitX(ref min, ref max, 1, 0.1);
            Assert.AreEqual(0, min);
            Assert.AreEqual(10, max);

            // handle case: center x
            max = 5;
            min = -5;
            ApUtil.AdjustRangeToFitX(ref min, ref max, -5, 0.5);
            Assert.AreEqual(-10, min);
            Assert.AreEqual(0, max);

            // handle case: inverted range
            max = 20;
            min = 30;
            ApUtil.AdjustRangeToFitX(ref min, ref max, 1, 0.1);
            Assert.AreEqual(10, min);
            Assert.AreEqual(0, max);
        }

        [TestMethod()]
        public void HasCrossedUnder()
        {
            // handle valid case
            Assert.IsTrue(ApUtil.HasCrossedUnder(10.4, 9.6, 10, 0.5));

            // handle invalid cases
            Assert.IsFalse(ApUtil.HasCrossedUnder(9.9, 9.6, 10, 0.5));
            Assert.IsFalse(ApUtil.HasCrossedUnder(10.6, 9.6, 10, 0.5));
            Assert.IsFalse(ApUtil.HasCrossedUnder(10.4, 10.1, 10, 0.5));
            Assert.IsFalse(ApUtil.HasCrossedUnder(10.4, 9.4, 10, 0.5));
        }

    }
}
