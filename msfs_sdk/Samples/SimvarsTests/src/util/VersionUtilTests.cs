﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace FsAutopilot.Util.Tests
{
    [TestClass()]
    public class VersionUtilTests
    {
        [TestMethod()]
        public void IsFlightSim2024RunningTest()
        {
            bool x = VersionUtil.IsFlightSim2024Running();
            Console.WriteLine($"IsFlightSim2024Running: {x}");
        }
    }
}
