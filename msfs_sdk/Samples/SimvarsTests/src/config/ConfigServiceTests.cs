﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using FsAutopilot.Events;
using FsAutopilot.Util;

namespace FsAutopilot.Config.Tests
{
    [TestClass()]
    public class ConfigServiceTests
    {
        [TestMethod]
        public void LoadConfigServiceTest()
        {
            // setup
            VersionUtil.Set2020();
            ConfigService configService = new ConfigService(EventService.Create(true), null);

            // execute
            var name = configService.GetAircraftCfg().name;

            // assert
            Assert.AreEqual("DEFAULT", name); // should start w/ default aircraft
        }

        [TestMethod]
        public void TestAero()
        {
            // setup
            VersionUtil.Set2020();
            ConfigService configService = new ConfigService(EventService.Create(true), null);
            string raw_title = "Britten Norman Trislander BN2A MKIII-1 RockHopper Aero";

            // execute
            configService.HandleAircraftTitle(raw_title);

            // assert
            Assert.AreEqual("BRITTEN_NORMAN_TRISLANDER", configService.GetAircraftCfg().name);
        }
    }
}
