﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.IO;

namespace FsAutopilot.Config.Tests
{
    [TestClass()]
    public class YamlUtilTests
    {

        [TestMethod]
        public void TrimDoubleTest()
        {
            Assert.AreEqual("0.1", YamlUtil.TrimDouble("0.10000000000000001"));
            Assert.AreEqual("0.3", YamlUtil.TrimDouble("0.29999999999999999"));
            Assert.AreEqual("1", YamlUtil.TrimDouble("1.0"));

            Assert.AreEqual("", YamlUtil.TrimDouble("0"));
            Assert.AreEqual("", YamlUtil.TrimDouble("0.1"));
            Assert.AreEqual("", YamlUtil.TrimDouble("1.1"));
            Assert.AreEqual("", YamlUtil.TrimDouble("hello"));
        }

        [TestMethod]
        public void TrimYamlTest()
        {
            PidGains gains = new PidGains(0.1, 0.2, 0.3, -1, -1, -1);

            string gainsString = YamlUtil.ObjToStr(gains);

            Assert.IsTrue(gainsString.StartsWith("p: 0.1\r\ni: 0.2\r\nd: 0.3\r\n"));
        }

        [TestMethod]
        public void TextToObjToTextTest()
        {
            // read text from file
            string textBefore = File.ReadAllText(YamlUtil.CONFIG_FILE_PATH);

            // convert to object
            var obj = YamlUtil.StrToObj<BaseConfig>(textBefore);

            // back to text
            string textAfter = YamlUtil.ObjToStr(obj);

            // compare
            Assert.AreEqual(textBefore, textAfter);
        }

        [TestMethod]
        public void ListTest()
        {
            // setup
            FlightPlanConfig configWithList = new FlightPlanConfig();
            configWithList.vorWaypoints = new List<YamlVorWaypoint>();
            string str = YamlUtil.ObjToStr(configWithList);
            str = str.Replace("[]", "");  // empty -> null

            // execute
            configWithList = YamlUtil.StrToObj<FlightPlanConfig>(str);
            configWithList.AfterDeserialize();

            // assert
            Assert.IsNotNull(configWithList.vorWaypoints);
            Assert.AreEqual(0, configWithList.vorWaypoints.Count);
        }

        [TestMethod]
        public void AircraftReadWriteTest()
        {
            // get path
            string fullPath = YamlUtil.GetFullAircraftPath(ConfigService.DEFAULT_AIRCRAFT_BASENAME);

            // read text before
            string textBefore = File.ReadAllText(fullPath);

            // read
            AircraftConfig config = YamlUtil.FileToObj<AircraftConfig>(fullPath);

            // write
            YamlUtil.ObjToFile(config, fullPath);

            // read text after
            string textAfter = File.ReadAllText(fullPath);

            // compare
            Assert.AreEqual(textBefore, textAfter);
        }

    }
}