﻿using FsAutopilot.Config;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FsAutopilot.Autopilot.Tests
{
    [TestClass()]
    public class FlightPlanManagerTests
    {
        /// <summary>
        /// 
        /// valid waypoints:
        ///     '115.9@90'
        ///     '117.9@90'
        ///     '117.9@44'
        ///     
        /// </summary>
        [TestMethod()]
        public void TestFlightWaypoint()
        {
            FlightWaypoint waypoint = null;

            waypoint = new FlightWaypoint("112.35");
            Assert.AreEqual(112.35, waypoint.frequency);
            Assert.AreEqual(-1, waypoint.radial);

            waypoint = new FlightWaypoint("115.9@90");
            Assert.AreEqual(115.9, waypoint.frequency);
            Assert.AreEqual(90, waypoint.radial);
        }

        [TestMethod()]
        public void TestSandbox()
        {
            string freq = "114.3";
            string radialIn = "328";
            string radialOut = "335";
            YamlVorWaypoint vorWaypoint = YamlVorWaypoint.CreateWaypoint(freq, radialIn, radialOut);

            Assert.AreEqual(114.3, vorWaypoint.frequency);
            Assert.AreEqual("328", vorWaypoint.radialInbound);
            Assert.AreEqual("335", vorWaypoint.radialOutbound);

            // test ILS
            freq = "111.5";
            radialIn = "";
            radialOut = "";
            vorWaypoint = YamlVorWaypoint.CreateWaypoint(freq, radialIn, radialOut);

            Assert.AreEqual(111.5, vorWaypoint.frequency);
            Assert.AreEqual("x", vorWaypoint.radialInbound);
            Assert.AreEqual("x", vorWaypoint.radialOutbound);
        }
    }
}
