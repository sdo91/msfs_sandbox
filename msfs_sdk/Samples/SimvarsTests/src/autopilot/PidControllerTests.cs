﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using FsAutopilot.Autopilot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FsAutopilot.Util;

namespace FsAutopilot.Autopilot.Tests
{
    [TestClass()]
    public class PidControllerTests
    {

        /// <summary>
        /// 
        /// P * error + Integrator = out
        /// Integrator = (out - (P * error))
        /// 
        /// let P=5, I=0.5
        /// plane is at 180
        /// throttle at 90
        /// target speed is 180
        /// 
        /// Integrator = (90 - (5 * 0)) = 90
        /// 
        /// or:
        /// current speed = 130
        /// throttle = 100
        /// target = 150
        /// 
        /// I = (100 - (5 * 20)) = 20
        /// 
        /// </summary>
        [TestMethod()]
        public void ThrottleTest()
        {
            PidController airspeedController = new PidController(5.0, 0.5, 0.0, 1, 105);
            airspeedController.logSettings("Airspeed Controller", true, "throttle %", "knots");
            airspeedController.MaxControlVarRate = 20;

            double desiredAirspeed = 180;
            airspeedController.SetPoint = desiredAirspeed;

            double currentAirspeed = 180;
            airspeedController.ProcessVariable = currentAirspeed;

            double currentThrottle = 90;
            airspeedController.CurrentControlVariable = currentThrottle;

            var throttlePercent = airspeedController.ControlVariable(TimeUtil.DeltaTime);

            Assert.AreEqual(90, throttlePercent);
        }
    }
}