# FALCON Autopilot
- Flight Autonomy Logic and Controller Of Navigation
  - for MSFS 2020
- In order to build & run the sample, you will need to have Visual Studio 2017 or later installed on your machine.
  - Note that any edition of the product, including the Community Edition, can be used for this.
  - I am using VS 2022
- Open the project by clicking on Simvars.csproj, and build it within Visual Studio. You can then run it directly from the IDE
  - msfs_sdk/Samples/SimvarWatcher/Simvars.csproj
  - todo: rename this project at some point

# Dev commands:
- `TANK:<tank id>` - switch fuel tank
- `LIGHT:<args>` - switch lights on/off
- `PAUSE:<arg>` - queue a simulator pause
- `VJT:<percent>` - set vjoy throttle percent
- `VJE:<percent>` - set vjoy elevator percent
- `VJB:<button>` - press vjoy button
- `NAV2:<navFreqMhz>` - set NAV2 frequency
- `HDG:<degrees>` - set heading bug
- `HI` - increment heading bug
- `HD` - decrement heading bug
- `SPEED:<knots>` - set speed bug
- `BRAKE:<bool>` - set parking brake
- `CDI` - toggle CDI
- `VS` - set target Vert Speed
- `THROTTLE:<percent>` - set throttle
- `MIX` - set best mixture
- `FAST` - toggle fast tune

# Visual Studio tips

- first time build/run:
  - right click simvars (in solution explorer), build
  - right click simvars, set as startup project
- plugins:
  - https://marketplace.visualstudio.com/items?itemName=LaurentKempe.GitDiffMargin
  - https://marketplace.visualstudio.com/items?itemName=JustinClareburtMSFT.HotKeys
    - only needed first time?
    - tools -> KB shortcuts -> load shortcuts
    - this should make ctrl + alt + <left/right> work
  - https://marketplace.visualstudio.com/items?itemName=SteveCadwallader.CodeMaidVS2022
- settings:
  - Tools > Options > Expand Environment > Keyboard
    - swap Edit.MoveSelectedLines<Up/Down> and Edit.Line<Up/Down>ExtendColumn
